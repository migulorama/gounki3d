#include <string>
#include <cstdlib>
#include <limits>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <sstream>

#include "utils.hpp"


bool parse_bool_from_string(const std::string& str)
{
    if(str == "true")
        return true;
    else if(str == "false")
        return false;

    throw std::runtime_error("Invalid string to be parsed.");
}