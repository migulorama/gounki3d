#include "gounki/logic.hpp"
#include "gounki/game.hpp"
#include "gounki/menu_state.hpp"
#include <iostream>

int main()
{
    try
    {
        gounki::game game("Gounki 3D");
        
        game.set_yaf_filename("themes\\default.yaf");

        game.change_to_state<gounki::menu_state>();

        game.init();

        game.run();
    }
    catch (std::exception& ex)
    {
        std::cerr << ex.what() << std::endl;
        std::cin.get();
    }
}