#include "gl/camera.hpp"

void gl::Camera::ApplyView() const
{
    glMultMatrixf(glm::value_ptr(GetModelViewMatrix()));
}

void gl::Camera::UpdateProjectionMatrix(int width, int height) const
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMultMatrixf(glm::value_ptr(GetProjectionMatrix(width, height)));
}
