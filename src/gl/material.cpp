#include <gl_4_4.h>

#include "gl/material.hpp"
#include "gl/types.hpp"


gl::Material::Material()
{
    _ambient[0] =  0.2f;
    _ambient[1] = 0.2f; 
    _ambient[2] = 0.2f; 
    _ambient[3] = 1.0f;

    _diffuse[0] = 0.8f;
    _diffuse[1] = 0.8f; 
    _diffuse[2] = 0.8f; 
    _diffuse[3] = 1.0f;

    _specular[0] = 0.0f; 
    _specular[1] = 0.0f; 
    _specular[2] = 0.0f; 
    _specular[3] = 1.0f;

    _emissive[0] = 0.0f; 
    _emissive[1] = 0.0f; 
    _emissive[2] = 0.0f; 
    _emissive[3] = 1.0f;

    _shininess = 0.0f;
    _texture = nullptr;
    _tWrap = GL_REPEAT;
    _sWrap = GL_REPEAT;
}

gl::Material::Material(ColorRGBA ambient, ColorRGBA diffuse, ColorRGBA specular, ColorRGBA emissive, float shininess)
{
    _ambient = ambient;
    _diffuse = diffuse;
    _specular = specular;
    _emissive = emissive;
    _shininess = shininess;
    _texture = nullptr;
    _tWrap = GL_REPEAT;
    _sWrap = GL_REPEAT;
}

gl::Material::Material(TexturePtr tex, GLenum sWrap, GLenum tWrap)
{
    _ambient[0] =  0.2f;
    _ambient[1] = 0.2f; 
    _ambient[2] = 0.2f; 
    _ambient[3] = 1.0f;

    _diffuse[0] = 0.8f;
    _diffuse[1] = 0.8f; 
    _diffuse[2] = 0.8f; 
    _diffuse[3] = 1.0f;

    _specular[0] = 0.0f; 
    _specular[1] = 0.0f; 
    _specular[2] = 0.0f; 
    _specular[3] = 1.0f;

    _emissive[0] = 0.0f; 
    _emissive[1] = 0.0f; 
    _emissive[2] = 0.0f; 
    _emissive[3] = 1.0f;

    _shininess = 0.0f;
    _texture = tex;
    _sWrap = sWrap;
    _tWrap = tWrap;
}

void gl::Material::Apply()
{
    glDisable(GL_COLOR_MATERIAL);
    glMaterialf(GL_FRONT, GL_SHININESS, _shininess);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  _specular.data());
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   _diffuse.data());
    glMaterialfv(GL_FRONT, GL_AMBIENT,   _ambient.data());
    glMaterialfv(GL_FRONT, GL_EMISSION,   _emissive.data());

    if (!_texture) { glDisable(GL_TEXTURE_2D); return; }

    glEnable(GL_TEXTURE_2D);
    _texture->Apply();
    _texture->SetWrap(_sWrap, _tWrap);
}

void gl::Material::SetAmbient(ColorRGBA ambient)
{
    _ambient = ambient;
}

void gl::Material::SetDiffuse(ColorRGBA diffuse)
{
    _diffuse = diffuse;
}

void gl::Material::SetSpecular(ColorRGBA specular)
{
    _specular = specular;
}

void gl::Material::SetEmissive(ColorRGBA emissive)
{
    _emissive = emissive;
}

void gl::Material::SetShininess(float shininess)
{
    _shininess = shininess;
}

void gl::Material::SetTexture(TexturePtr tex)
{
    _texture = tex;
}

gl::TexturePtr gl::Material::GetTexture() const
{
    return _texture;
}

void gl::Material::SetTextureWrap(GLenum sWrap, GLenum tWrap)
{
    SetTextureSWrap(sWrap);
    SetTextureTWrap(tWrap);
}

void gl::Material::SetTextureSWrap(GLenum sWrap)
{
    _sWrap = sWrap;
}

void gl::Material::SetTextureTWrap(GLenum tWrap)
{
    _tWrap = tWrap;
}

void gl::Material::SetTextureScale(float s, float t)
{
    _textureScale = glm::vec2(s, t);
}

const glm::vec2& gl::Material::GetTextureScale() const
{
    return _textureScale;
}