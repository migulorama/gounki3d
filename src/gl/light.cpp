#include "gl/light.hpp"

#include <gl_4_4.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

gl::Light::Light(GLuint lightid) : 
    _id(lightid), 
    _position(0.0f, 0.0f, 0.0f, 1.0f),
    _direction(0.0f, 0.0f, -1.0f),
    _angle(180.0f),
    _exponent(0.0f)
{
    SetKc(1.0f);
    SetKl(0.0f);
    SetKq(0.0f);
    glLightf(_id, GL_SPOT_CUTOFF, _angle);
    glLightf(_id, GL_SPOT_EXPONENT, _exponent);
    Enable(false);
}

gl::Light::Light( GLuint lightId, const glm::vec3& position ) :
    _id(lightId),
    _position(position, 1.0f),
    _direction(0.0f, 0.0f, -1.0f),
    _angle(180.0f),
    _exponent(0.0f)
{

    SetKc(1.0f);
    SetKl(0.0f);
    SetKq(0.0f);
    glLightf(_id, GL_SPOT_CUTOFF, _angle);
    glLightf(_id, GL_SPOT_EXPONENT, _exponent);
    Enable(true);
}

gl::Light::~Light()
{
    glDisable(_id);
}

void gl::Light::Update()
{
    if (_enabled)
    {
        glEnable(_id);
        glLightfv(_id, GL_POSITION, glm::value_ptr(_position));
        glLightfv(_id, GL_SPOT_DIRECTION, glm::value_ptr(_direction));
    }
    else
        glDisable(_id);
}

void gl::Light::Enable( bool val )
{
    _enabled = val;
}

bool gl::Light::IsEnabled() const
{
    return _enabled;
}

void gl::Light::SetPosition( const glm::vec3& position )
{
    _position.x = position.x;
    _position.y = position.y;
    _position.z = position.z;
}

void gl::Light::SetAmbient( const glm::vec4& ambient )
{
    _ambient = ambient;
    glLightfv(_id, GL_AMBIENT, glm::value_ptr(_ambient));
}

void gl::Light::SetDiffuse( const glm::vec4& diffuse )
{
    _diffuse = diffuse;
    glLightfv(_id, GL_DIFFUSE, glm::value_ptr(_diffuse));
}

void gl::Light::SetSpecular( const glm::vec4& specular )
{
    _specular = specular;
    glLightfv(_id, GL_SPECULAR, glm::value_ptr(_specular));
}

void gl::Light::SetKc( float kc )
{
    _kc = kc;
    glLightf(_id, GL_CONSTANT_ATTENUATION, _kc);
}

void gl::Light::SetKl( float kl )
{
    _kl = kl;
    glLightf(_id, GL_LINEAR_ATTENUATION, _kl);
}

void gl::Light::SetKq( float kq )
{
    _kq = kq;
    glLightf(_id, GL_QUADRATIC_ATTENUATION, _kq);
}
