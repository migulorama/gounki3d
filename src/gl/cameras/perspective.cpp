#include "gl/cameras/perspective.hpp"

#include <gl_4_4.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

gl::Cameras::Perspective::Perspective(float fov, float n, float f, const glm::vec3& pos, const glm::vec3& tar) : 
    _fov(fov), 
    _near(n), 
    _far(f),
    _position(pos, 0.0f),
    _target(tar, 0.0f),
    _direction(glm::normalize(_target - _position))
{
    _initPosition = _position;
    _initTarget = _target;
    UpdateUp();
}

void gl::Cameras::Perspective::SetFOV(float val)
{
    _fov = val;
}

void gl::Cameras::Perspective::SetNear(float val)
{
    _near = val;
}

void gl::Cameras::Perspective::SetFar(float val)
{
    _far = val;
}

void gl::Cameras::Perspective::SetPosition(const glm::vec3& val)
{
    _position = glm::vec4(val, 0.0f);
    _initPosition = _position;
    _direction = glm::normalize(_target - _position);
    UpdateUp();
}

void gl::Cameras::Perspective::SetTarget(const glm::vec3& val)
{
    _target = glm::vec4(val, 0.0f);
    _initTarget = _target;
    _direction = glm::normalize(_target - _position);
    UpdateUp();
}

glm::mat4 gl::Cameras::Perspective::GetModelViewMatrix() const
{
    return glm::lookAt(glm::vec3(_position), glm::vec3(_target), _up);
}

glm::mat4 gl::Cameras::Perspective::GetProjectionMatrix(int width, int height) const
{
    float aspect = static_cast<float>(width) / static_cast<float>(height);
    return glm::perspective(_fov, aspect, _near, _far);
}

void gl::Cameras::Perspective::Translate(const glm::vec3& val)
{
    glm::vec4 tZ = - val.z * _direction;
    glm::vec4 tUp = val.y * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
    glm::vec4 tLeft = glm::vec4(val.x * glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(_direction)), 0.0f);

    _position = _position + tZ + tUp + tLeft;
    // _target = _position + _direction * glm::length(_target - _position);
}

void gl::Cameras::Perspective::RotatePosition(float angle, const glm::vec3& axis)
{
    _direction = glm::rotate(glm::mat4(), angle, axis) * _direction;
    _position = _target - _direction * glm::length(_target - _position);
    UpdateUp();
}

void gl::Cameras::Perspective::Rotate(float angle, const glm::vec3& axis)
{
    _direction = glm::rotate(glm::mat4(), angle, axis) * _direction;
    _target = _position + _direction * glm::length(_target - _position);
    UpdateUp();
}

void gl::Cameras::Perspective::Reset()
{ 
    _position = _initPosition; 
    _target = _initTarget; 
    _direction = glm::normalize(_target - _position);
    UpdateUp();
}

void gl::Cameras::Perspective::UpdateUp()
{
    float cosTheta = cos(asin(_direction.y));
    _right = glm::vec3(-_direction.z / cosTheta, 0.0f, _direction.x / cosTheta);
    _up = glm::cross(_right, glm::vec3(_direction));
}

glm::vec3 gl::Cameras::Perspective::GetPosition() const
{
    return glm::vec3(_position);
}

glm::vec3 gl::Cameras::Perspective::GetTarget() const
{
    return glm::vec3(_target);
}

glm::vec3 gl::Cameras::Perspective::GetDirection() const
{
    return glm::vec3(_direction);
}
