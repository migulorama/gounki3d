#include "gl/cameras/ortho.hpp"

#include <gl_4_4.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

gl::Cameras::Ortho::Ortho(float n, float f, float left, float right, float top, float bottom) : 
    _near(n), 
    _far(f), 
    _left(left), 
    _right(right),
    _top(top), 
    _bottom(bottom),
    _position(-10, 0, 0, 1),
    _target(0, 0, 0, 1)
{
    _direction = glm::normalize(_target - _position);
    UpdateUp();
}

glm::mat4 gl::Cameras::Ortho::GetProjectionMatrix(int width, int height) const
{
    return glm::ortho(_left, _right, _bottom, _top, _near, _far);
}

glm::mat4 gl::Cameras::Ortho::GetModelViewMatrix() const
{
    return glm::lookAt(glm::vec3(-10, 0, 0), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
}

void gl::Cameras::Ortho::SetNear(float val)
{
    _near = val;
}

void gl::Cameras::Ortho::SetFar(float val)
{
    _far = val;
}

void gl::Cameras::Ortho::SetLeft(float left)
{
    _left = left;
}

void gl::Cameras::Ortho::SetRight(float right)
{
    _right = right;
}

void gl::Cameras::Ortho::SetTop(float top)
{
    _top = top;
}

void gl::Cameras::Ortho::SetBottom(float bottom)
{
    _bottom = bottom;
}

void gl::Cameras::Ortho::Translate(const glm::vec3& val)
{
    glm::vec4 tZ = -val.z * _direction;
    glm::vec4 tUp = val.y * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
    glm::vec4 tLeft = glm::vec4(val.x * glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(_direction)), 0.0f);

    _position = _position + tZ + tUp + tLeft;
    _target = _position + _direction * glm::length(_target - _position);
}

void gl::Cameras::Ortho::Rotate(float angle, const glm::vec3& axis)
{
    _direction = glm::rotate(glm::mat4(), angle, axis) * _direction;
    _position = _target - _direction * glm::length(_target - _position);
    UpdateUp();
}

void gl::Cameras::Ortho::UpdateUp()
{
    float cosTheta = cos(asin(_direction.y));
    _rightVec = glm::vec3(-_direction.z / cosTheta, 0.0f, _direction.x / cosTheta);
    _up = glm::cross(_rightVec, glm::vec3(_direction));
}