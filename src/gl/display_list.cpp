#include "gl/display_list.hpp"

#include <gl_4_4.h>

gl::details::DisplayList::DisplayList() : _initialized(false)
{
    _id = glGenLists(1);
}

gl::details::DisplayList::~DisplayList()
{
    glDeleteLists(_id, 1);
}

void gl::details::DisplayList::Init(std::function<void()> func)
{
    glNewList(_id, GL_COMPILE);
    func();
    glEndList();
    _initialized = true;
}

bool gl::details::DisplayList::IsInitialized() const
{
    return _initialized;
}

void gl::details::DisplayList::Call() const
{
    glCallList(_id);
}

gl::DisplayListPtr gl::make_display_list()
{
    return std::make_shared<details::DisplayList>();
}
