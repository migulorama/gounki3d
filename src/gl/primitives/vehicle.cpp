#include "gl/primitives/vehicle.hpp"
#include "glm/glm.hpp"

#include <vector>
#include <algorithm>

const std::vector<glm::vec3> patchCtrlPoints = { 
    glm::vec3(2, -1.5, -3), glm::vec3(1, -1, -5.5), glm::vec3(-1, -1, -5.5),    glm::vec3(-2, -1.5, -3),
    glm::vec3(2, 0, -1),    glm::vec3(1, 0.5, -1),    glm::vec3(-1, 0.5, -1),        glm::vec3(-2, 0, -1),
    glm::vec3(2, 0, 1),        glm::vec3(1, 0.5, 1),    glm::vec3(-1, 0.5, 1),        glm::vec3(-2, 0, 1),
    glm::vec3(2, -1.5, 3),    glm::vec3(1, -1, 5.5),    glm::vec3(-1, -1, 5.5),        glm::vec3(-2, -1.5, 3) 
};

const std::vector<glm::vec3> legPatchCtrlPoints = {
    glm::vec3(1, -1, -1),    glm::vec3(0.5, -1, -3.5),    glm::vec3(-0.5, -1, -3.5),    glm::vec3(-1, -1, -1),
    glm::vec3(1, 0, -1),    glm::vec3(0.5, 0.5, -1),    glm::vec3(-0.5, 0.5, -1),    glm::vec3(-1, 0, -1),
    glm::vec3(1, 0, 1),        glm::vec3(0.5, 0.5, 1),        glm::vec3(-0.5, 0.5, 1),    glm::vec3(-1, 0, 1),
    glm::vec3(0, -1, 2),    glm::vec3(0, -1, 2),        glm::vec3(0, -1, 2),        glm::vec3(0, -1, 2)
};

// static members initialization
gl::primitives::Patch gl::primitives::Vehicle::ovniPatch(3, 50, 50, patchCtrlPoints);
gl::primitives::Patch gl::primitives::Vehicle::legSupportPatch(3, 50, 50, legPatchCtrlPoints);
gl::primitives::Cylinder gl::primitives::Vehicle::ovniPatchSupport(0.2f, 0.2f, 0.4f, 30, 30);
gl::primitives::Sphere gl::primitives::Vehicle::s(1.0f, 30, 30);

gl::primitives::Vehicle::Vehicle()
{
}

gl::primitives::Vehicle::~Vehicle()
{
}

void gl::primitives::Vehicle::draw(gl::MaterialPtr mat) const
{
    // ship's main body
    glPushMatrix();
    {
        glScaled(0.5, 2, 1);
        s.draw(mat);
    }
    glPopMatrix();

    // ship's eye
    glPushMatrix();
    {

        glPushMatrix();
        glScaled(0.2, 0.2, 0.2);
        glTranslated(0, 0, 4.5);
        s.draw(mat);
        glPopMatrix();
    }
    glPopMatrix();

    glPushMatrix();
    {
        glScaled(0.2, 0.2, 0.2);

        // Right patch
        glPushMatrix();
        {
            glRotated(90, 0, 1, 0);
            glRotated(90, 1, 0, 0);
            glPushMatrix();
            glTranslated(0, 5, 0);
            ovniPatch.draw(mat);
            glPopMatrix();
        }
        glPopMatrix();

        // left patch
        glPushMatrix();
        {
            glRotated(90, 0, 1, 0);
            glRotated(-90, 1, 0, 0);
            glPushMatrix();
            glTranslated(0, 5, 0);
            ovniPatch.draw(mat);
            glPopMatrix();
        }
        glPopMatrix();
    }
    glPopMatrix();

    // right patch support
    glPushMatrix();
    {
        glRotated(90, 0, 1, 0);
        glTranslated(0, 0, 0.5);
        ovniPatchSupport.draw(mat);
    }
    glPopMatrix();

    // left patch support
    glPushMatrix();
    {
        glRotated(90, 0, 1, 0);
        glTranslated(0, 0, -0.9);
        ovniPatchSupport.draw(mat);
    }
    glPopMatrix();

    // left leg
    glPushMatrix();
    {
        glPushMatrix();
        {
            glRotated(80, 0, 1, 0);
            glRotated(-45, 0, 0, 1);
            glPushMatrix();
            {
                glTranslated(1.2, -1, 0.2);
                ovniPatchSupport.draw(mat);
            }
            glPopMatrix();
        }
        glPopMatrix();

        glPushMatrix();
        {
            glPushMatrix();
            glRotated(90, 1, 0, 0);
            glPushMatrix();
            glTranslated(0.7, 0, 1.6);
            ovniPatchSupport.draw(mat);
            glPopMatrix();
            glPopMatrix();
        }
        glPopMatrix();

        glPushMatrix();
        {
           
            glRotated(90, 0, 1, 0);
            glRotated(90, 1, 0, 0);
            glPushMatrix();
            glTranslated(0, 1, 2);
            glScaled(0.3, 0.3, 0.3);
            legSupportPatch.draw(mat);
            glPopMatrix(); 
        }
        glPopMatrix();
    }
    glPopMatrix();

    // center leg
    glPushMatrix();
    {
        glRotated(-90, 0, 1, 0);
        glTranslated(0.3, 0, 0.1);

        glPushMatrix();
        {
            glRotated(80, 0, 1, 0);
            glRotated(-45, 0, 0, 1);
            glPushMatrix();
            {
                glTranslated(1.2, -1, 0.2);
                ovniPatchSupport.draw(mat);
            }
            glPopMatrix();
        }
        glPopMatrix();

        glPushMatrix();
        {
            glPushMatrix();
            glRotated(90, 1, 0, 0);
            glPushMatrix();
            glTranslated(0.7, 0, 1.6);
            ovniPatchSupport.draw(mat);
            glPopMatrix();
            glPopMatrix();
        }
        glPopMatrix();

        glPushMatrix();
        {

            glRotated(90, 0, 1, 0);
            glRotated(90, 1, 0, 0);
            glPushMatrix();
            glTranslated(0, 1, 2);
            glScaled(0.3, 0.3, 0.3);
            legSupportPatch.draw(mat);
            glPopMatrix();
        }
        glPopMatrix();
    }
    glPopMatrix();

    // left leg
    glPushMatrix();
    {
        glScaled(-1, 1, 1);

        glPushMatrix();
        {
            glRotated(80, 0, 1, 0);
            glRotated(-45, 0, 0, 1);
            glPushMatrix();
            {
                glTranslated(1.2, -1, 0.2);
                ovniPatchSupport.draw(mat);
            }
            glPopMatrix();
        }
        glPopMatrix();

        glPushMatrix();
        {
            glPushMatrix();
            glRotated(90, 1, 0, 0);
            glPushMatrix();
            glTranslated(0.7, 0, 1.6);
            ovniPatchSupport.draw(mat);
            glPopMatrix();
            glPopMatrix();
        }
        glPopMatrix();

        glPushMatrix();
        {

            glRotated(90, 0, 1, 0);
            glRotated(90, 1, 0, 0);
            glPushMatrix();
            glTranslated(0, 1, 2);
            glScaled(0.3, 0.3, 0.3);
            legSupportPatch.draw(mat);
            glPopMatrix();
        }
        glPopMatrix();
    }
    glPopMatrix();
}