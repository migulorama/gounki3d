#include "gl/primitives/sphere.hpp"
#include <gl_4_4.h>

gl::primitives::Sphere::Sphere(float radius, unsigned int slices, unsigned int stacks) : _radius(radius), _slices(slices), _stacks(stacks)
{
  //  _quadric = gluNewQuadric();
  //  gluQuadricTexture(_quadric, GL_TRUE);
}

gl::primitives::Sphere::~Sphere()
{
   // gluDeleteQuadric(_quadric);
}

void gl::primitives::Sphere::draw(gl::MaterialPtr mat) const
{
    // if (mat) mat->Apply();

    glPushAttrib(GL_POLYGON_BIT);
    {
        glFrontFace(GL_CCW);
        glPushMatrix();

        size_t i, j;
        for (i = 0; i <= _slices; i++) {
            float lat0 = acos(-1.0f) * (-0.5f + (float)(i - 1) / _slices);
            float s0 = (float)(i - 1) / _slices;
            float z0 = sin(lat0);
            float zr0 = cos(lat0);

            float lat1 = acos(-1.0f) * (-0.5f + (float)i / _slices);
            float s1 = (float)(i) / _slices;
            float z1 = sin(lat1);
            float zr1 = cos(lat1);

            glBegin(GL_QUAD_STRIP);
            for (j = 0; j <= _stacks; j++) {
                float lng = 2 * acos(-1.0f) * (float)(j - 1) / _stacks;
                float t = (float)(j - 1) / _stacks;
                float x = cos(lng);
                float y = sin(lng);

                glTexCoord2f(s0, t);
                glNormal3f(x * zr0, y * zr0, z0);
                glVertex3f(x * zr0, y * zr0, z0);
                glTexCoord2f(s1, t);
                glNormal3f(x * zr1, y * zr1, z1);
                glVertex3f(x * zr1, y * zr1, z1);

            }
            glEnd();

        }

        glPopMatrix();
    }
    glPopAttrib();
}