#include "gl/primitives/triangle.hpp"
#include <gl_4_4.h>

#include "utils.hpp"
#include "gl/material.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/vector_angle.hpp>

gl::primitives::Triangle::Triangle(const glm::vec3& xyz1, const glm::vec3& xyz2, const glm::vec3& xyz3)
{
    SetPosition(xyz1, xyz2, xyz3);
}

void gl::primitives::Triangle::draw(gl::MaterialPtr mat) const
{
    gl::TexturePtr tex;
    glm::vec2 textureScale;

    if (mat)
    {
        tex = mat->GetTexture();
        textureScale = mat->GetTextureScale();
        // mat->Apply();
    }

    glm::vec3 vc = _vertices[0] - _vertices[1];
    glm::vec3 va = _vertices[2] - _vertices[1];

    float beta = acos(glm::dot(glm::normalize(va),glm::normalize(vc)));

    float c = glm::length(vc);
    float a = glm::length(va);

    glBegin(GL_TRIANGLES);

    glNormal3fv(glm::value_ptr(_normal));

    if (mat && tex)
        glTexCoord2d(0.0f, 0.0f);
    glVertex3fv(glm::value_ptr(_vertices[0]));

    if (mat && tex)
        glTexCoord2d(c / textureScale.s, 0.0f);
    glVertex3fv(glm::value_ptr(_vertices[1]));

    float dummie = c - (a * cos(beta));

    if (mat && tex)
        glTexCoord2d((c - (a * cos(beta))) / textureScale.s, (a * sin(beta)) / textureScale.t);
    glVertex3fv(glm::value_ptr(_vertices[2]));

    glEnd();
}

void gl::primitives::Triangle::SetPosition(const glm::vec3& xyz1, const glm::vec3& xyz2, const glm::vec3& xyz3)
{
    _vertices[0] = xyz1;
    _vertices[1] = xyz2;
    _vertices[2] = xyz3;

    _normal = calc_newell(_vertices);
}
