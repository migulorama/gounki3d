#include "gl/primitives/rectangle.hpp"
#include <gl_4_4.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

gl::primitives::Rectangle::Rectangle(glm::vec2 xy1, glm::vec2 xy2)
{
    SetDimensions(xy1, xy2);
}

void gl::primitives::Rectangle::draw(gl::MaterialPtr mat) const
{
    glm::vec2 textureScale;
    gl::TexturePtr tex;

    if (mat != nullptr)
    {
        textureScale = mat->GetTextureScale();
        tex = mat->GetTexture();
        // mat->Apply();
    }

    glBegin(GL_QUADS);

    glNormal3fv(glm::value_ptr(_normal));

        if (mat && tex)
            glTexCoord2f(0.0f, 0.0f);
        glVertex2f(_xy1.x, _xy1.y);

        if (mat && tex)
            glTexCoord2f(glm::abs(_xy2.x - _xy1.x) / textureScale.s, 0.0f);
        glVertex2f(_xy2.x, _xy1.y);

        if (mat && tex)
            glTexCoord2f(glm::abs(_xy2.x - _xy1.x) / textureScale.s, glm::abs(_xy2.y - _xy1.y) / textureScale.t);
        glVertex2f(_xy2.x, _xy2.y);

        if (mat && tex)
            glTexCoord2f(0.0f, glm::abs(_xy2.y - _xy1.y) /  (textureScale.t));
        glVertex2f(_xy1.x, _xy2.y);
    
    glEnd();
}

void gl::primitives::Rectangle::SetDimensions(glm::vec2 xy1, glm::vec2 xy2)
{
    _xy1 = xy1;
    _xy2 = xy2;

    boost::array<glm::vec3, 4> vertices;
    vertices[0] = glm::vec3(_xy1, 0.0f);
    vertices[1] = glm::vec3(_xy2.x, _xy1.y, 0.0f);
    vertices[2] = glm::vec3(_xy2, 0.0f);
    vertices[3] = glm::vec3(_xy1.x, _xy2.y, 0.0f);

    _normal = glm::normalize(calc_newell(vertices));
}