#include "gl/primitives/model.hpp"

#include <fstream>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

gl::mesh::mesh(const std::string& fileName) : _fileName(fileName), _initialized(false)
{
}

void gl::mesh::init(gl::MaterialPtr)
{
    if (!_initialized) 
    {
        std::cout << "Loading " << _fileName << std::endl;
        load_obj_file();
        _initialized = true;
    }
}

void gl::mesh::draw(gl::MaterialPtr mat) const
{
    glPushMatrix();

    glTranslatef(0.0f, _size.y / 2.0f, 0.0f);

    for (const auto& grp : _groups)
    {
        if (auto material = grp.material_ptr.lock()) 
        {
            if (mat)
                (*material * mat->GetDiffuse()).Apply();
            else
                material->Apply();
        }

        for (const auto& f : grp.faces)
        {
            glBegin(GL_TRIANGLES);
            for (int i = 0; i < 3; ++i)
            {
                if (f.normals[i] != -1)
                    glNormal3fv(glm::value_ptr(_normals[f.normals[i]]));

                if (f.texCoords[i] != -1)
                    glTexCoord2fv(glm::value_ptr(_texCoords[f.texCoords[i]]));

                if (f.vertices[i] != -1)
                    glVertex3fv(glm::value_ptr(_vertices[f.vertices[i]]));
            }
            glEnd();
        }
    }

    glPopMatrix();
}

void gl::mesh::load_obj_file()
{
    std::replace(_fileName.begin(), _fileName.end(), '\\', '/');

    std::ifstream file{ _fileName };

    if (!file.is_open()) throw std::runtime_error("File " + _fileName + " was not found!");

    std::string filePath = "";
    auto last_slash_pos = _fileName.find_last_of('/');
    if (last_slash_pos != std::string::npos) filePath = _fileName.substr(0, last_slash_pos + 1);
    
    glm::vec3 min(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
    glm::vec3 max(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min());

    std::string buf;
    while (std::getline(file, buf))
    {
        std::istringstream iss{ buf };

        std::string type;
        iss >> type;

        if (type[0] == '#') continue;

        if (type == "mtllib")
        {
            std::string mtl_file_name;
            iss >> mtl_file_name;
            load_mat_file(filePath, mtl_file_name);
        }
        else if (type == "v")
        {
            glm::vec3 vertex;
            iss >> vertex.x >> vertex.y >> vertex.z;

            if (vertex.x < min.x) min.x = vertex.x;
            else if (vertex.x > max.x) max.x = vertex.x;

            if (vertex.y < min.y) min.y = vertex.y;
            else if (vertex.y > max.y) max.y = vertex.y;

            if (vertex.z < min.z) min.z = vertex.z;
            else if (vertex.z > max.z) max.z = vertex.z;


            _vertices.push_back(vertex);
        }
        else if (type == "vt")
        {
            glm::vec2 texCoord;
            iss >> texCoord.s >> texCoord.t;
            _texCoords.push_back(texCoord);
        }
        else if (type == "vn")
        {
            glm::vec3 normal;
            iss >> normal.x >> normal.y >> normal.z;
            _normals.push_back(glm::normalize(normal));
        }
        else if (type == "g")
        {
            group grp;
            iss >> grp.name;

            _groups.push_back(grp);
        }
        else if (type == "usemtl")
        {
            if (_groups.empty())
                throw std::runtime_error("Material without group!!! USE ANOTHER LOADER!!!");

            std::string mtl_name;

            iss >> mtl_name;
            auto mat = _materials.find(mtl_name);
            if (mat == _materials.end()) throw std::runtime_error("Undefined material used!");
            _groups.back().material_ptr = mat->second;
        }
        else if (type == "f")
        {
            if (_groups.empty())
                throw std::runtime_error("Face without group!!! USE ANOTHER LOADER!!!");

            std::getline(iss, buf, '#');
            iss = std::istringstream{ buf };
            auto number_slashes = std::count(buf.begin(), buf.end(), '/');

            switch (number_slashes)
            {
            case 0: // Only vertices
            {
                        auto f = face{};
                        iss >> f.vertices[0] >> f.vertices[1] >> f.vertices[2];
                        _groups.back().faces.push_back(f);
            }
                break;
            case 3: // Vertices and Tex Coords
            {
                        auto f = face{};
                        iss >> f.vertices[0]; iss.ignore(1, '/'); iss >> f.texCoords[0];
                        iss >> f.vertices[1]; iss.ignore(1, '/'); iss >> f.texCoords[1];
                        iss >> f.vertices[2]; iss.ignore(1, '/'); iss >> f.texCoords[2];

                        _groups.back().faces.push_back(f);
            }
                break;
            case 6:
            {
                      if (buf.find("//") != std::string::npos)
                      {
                          // Vertices and Normals
                          auto f = face{};
                          iss >> f.vertices[0]; iss.ignore(1, '/'); iss.ignore(1, '/'); iss >> f.normals[0];
                          iss >> f.vertices[1]; iss.ignore(1, '/'); iss.ignore(1, '/'); iss >> f.normals[1];
                          iss >> f.vertices[2]; iss.ignore(1, '/'); iss.ignore(1, '/'); iss >> f.normals[2];

                          _groups.back().faces.push_back(f);
                      }
                      else
                      {
                          // Vertices, Tex Coords and Normals
                          auto f = face{};
                          iss >> f.vertices[0]; iss.ignore(1, '/'); iss >> f.texCoords[0]; iss.ignore(1, '/'); iss >> f.normals[0];
                          iss >> f.vertices[1]; iss.ignore(1, '/'); iss >> f.texCoords[1]; iss.ignore(1, '/'); iss >> f.normals[1];
                          iss >> f.vertices[2]; iss.ignore(1, '/'); iss >> f.texCoords[2]; iss.ignore(1, '/'); iss >> f.normals[2];

                          _groups.back().faces.push_back(f);
                      }
            }
                break;
            default:
                throw std::runtime_error("Invalid face detected.");
            }
        }
    }

    for (auto& grp : _groups)
    {
        for (auto& face : grp.faces)
        {
            face.vertices[0] -= 1; face.vertices[1] -= 1; face.vertices[2] -= 1;
            face.normals[0] -= 1; face.normals[1] -= 1; face.normals[2] -= 1;
            face.texCoords[0] -= 1; face.texCoords[1] -= 1; face.texCoords[2] -= 1;
        }
    }

    _size = max - min;
}

void gl::mesh::load_mat_file(const std::string& file_path, const std::string& fileName)
{
    std::cout << "Loading " << fileName << std::endl;

    std::ifstream file{ file_path + fileName };

    gl::MaterialPtr last_mat = nullptr;
    std::string buf;
    while (std::getline(file, buf))
    {
        std::istringstream iss{ buf };

        std::string type;
        iss >> type;

        if (type[0] == '#') continue;

        if (type == "newmtl")
        {
            std::string name;
            iss >> name;
            last_mat = std::make_shared<gl::Material>();
            _materials.insert(std::make_pair(name, last_mat));
        }
        else if (type == "Ka")
        {
            if (!last_mat) throw std::runtime_error("No material defined.");

            gl::ColorRGBA ambient{ { 0.0f, 0.0f, 0.0f, 1.0f } };
            iss >> ambient[0] >> ambient[1] >> ambient[2];
            last_mat->SetAmbient(ambient);
        }
        else if (type == "Kd")
        {
            if (!last_mat) throw std::runtime_error("No material defined.");

            gl::ColorRGBA diffuse{ { 0.0f, 0.0f, 0.0f, 1.0f } };
            iss >> diffuse[0] >> diffuse[1] >> diffuse[2];
            last_mat->SetDiffuse(diffuse);
        }
        else if (type == "Ks")
        {
            gl::ColorRGBA specular{ { 0.0f, 0.0f, 0.0f, 1.0f } };

            if (!last_mat) throw std::runtime_error("No material defined.");
            iss >> specular[0] >> specular[1] >> specular[2];
            last_mat->SetSpecular(specular);
        }
        else if (type == "Ns")
        {
            if (!last_mat) throw std::runtime_error("No material defined.");

            float shininess;
            iss >> shininess;
            last_mat->SetShininess((shininess * 128.f) / 1000.f);
        }
        else if (type == "Tr" || type == "d")
        {
            if (!last_mat) throw std::runtime_error("No material defined.");
            
            float transparency;
            iss >> transparency;
        }
        else if (type == "map_Kd")
        {
            if (!last_mat) throw std::runtime_error("No material defined.");
            
            std::string diffuse_texture;
            iss >> diffuse_texture;
            std::cout << "Loading Texture " << diffuse_texture << std::endl;
            last_mat->SetTexture(gl::Texture::Load(file_path + diffuse_texture));
        }
        else if (type == "map_bump" || type == "bump")
        {
            if (!last_mat) throw std::runtime_error("No material defined.");

            std::string bump_texture;
            iss >> bump_texture;
        }
    }
}


