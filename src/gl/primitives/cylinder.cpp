#include "gl/primitives/cylinder.hpp"
#include <gl_4_4.h>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

gl::primitives::Cylinder::Cylinder(float base, float top, float height,unsigned int slices, unsigned int stacks) : _base(base), _top(top), _height(height), _slices(slices), _stacks(stacks)
{
    // _gluQuadric = gluNewQuadric();
    // gluQuadricTexture(_gluQuadric, GL_TRUE);
}

void gl::primitives::Cylinder::draw(gl::MaterialPtr mat) const
{
    glPushAttrib(GL_POLYGON_BIT);
    {
        glFrontFace(GL_CCW);
        glPushMatrix();
        glRotatef(180.0f, 1.0f, 0.0f, 0.0f);

        // gluDisk(_gluQuadric, 0.0, _base, _slices, _stacks);

        glPopMatrix();

        // gluCylinder(_gluQuadric, _base, _top, _height, _slices, _stacks);

        glPushMatrix();

        glTranslatef(0.0f, 0.0f, _height);

        // gluDisk(_gluQuadric, 0.0, _top, _slices, _stacks);

        glPopMatrix();
    }
    glPopAttrib();
}