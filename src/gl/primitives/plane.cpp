#include "gl/primitives/plane.hpp"

void gl::primitives::Plane::draw( gl::MaterialPtr mat ) const
{
    glPushAttrib(GL_POLYGON_BIT);
    {
        if (mat)
        {
            mat->Apply();

            auto textureScale = mat->GetTextureScale();
            auto& ctrlPoints = _texCoordsEval->GetControlPoints();
            ctrlPoints[0] = ctrlPoints[4] = 1.0f / textureScale.s;
            ctrlPoints[5] = ctrlPoints[7] = 1.0f / textureScale.t;
        }

        if (mat && mat->GetTexture())
        {
            auto tex = mat->GetTexture();
            tex->SetWrap(GL_REPEAT, GL_REPEAT);
        }

        glFrontFace(GL_CCW);
        _verticesEval->Map();
        _normalsEval->Map();
        _texCoordsEval->Map();

        _verticesEval->Draw();

        _verticesEval->UnMap();
        _normalsEval->UnMap();
        _texCoordsEval->UnMap();
    }
    glPopAttrib();
}

gl::primitives::Plane::Plane( GLint num_div_x, GLint num_div_z )
{
    std::vector<float> vertices(12);
    vertices[0] =  0.5f; vertices[1]  = 0.0f; vertices[2]  =  0.5f;
    vertices[3] = -0.5f; vertices[4]  = 0.0f; vertices[5]  =  0.5f;
    vertices[6] =  0.5f; vertices[7] = 0.0f; vertices[8] = -0.5f;
    vertices[9] = -0.5f; vertices[10]  = 0.0f; vertices[11]  = -0.5f;
    std::vector<float> normals(12);
    normals[0] = 0.0f; normals[1]  = 1.0f; normals[2]  = 0.0f;
    normals[3] = 0.0f; normals[4]  = 1.0f; normals[5]  = 0.0f;
    normals[6] = 0.0f; normals[7]  = 1.0f; normals[8]  = 0.0f;
    normals[9] = 0.0f; normals[10] = 1.0f; normals[11] = 0.0f;
    std::vector<float> texCoords(8);
    texCoords[0] = 1.0f; texCoords[1] = 0.0f;
    texCoords[2] = 0.0f; texCoords[3] = 0.0f;
    texCoords[4] = 1.0f; texCoords[5] = 1.0f;
    texCoords[6] = 0.0f; texCoords[7] = 1.0f;


    _verticesEval.reset(new gl::Evaluator2D(Evaluator::VERTEX_3, 1, 1, num_div_x, num_div_z, vertices));
    _normalsEval.reset(new gl::Evaluator2D(Evaluator::NORMAL, 1, 1, num_div_x, num_div_z, normals));
    _texCoordsEval.reset(new gl::Evaluator2D(Evaluator::TEXTURE_COORD_2, 1, 1, num_div_x, num_div_z, texCoords));
}
