#include "gl/primitives/patch.hpp"

gl::primitives::Patch::Patch(GLint order, GLint uParts, GLint vParts, std::vector<glm::vec3> controlPoints)
{
    _order = order;
    _uParts = uParts;
    _vParts = vParts;

    std::vector<float> controls(controlPoints.size() * 3);
    std::vector<float> texCoords(controlPoints.size() * 2);

    auto fItr = controls.begin();
    auto cItr = controlPoints.begin();
    auto tItr = texCoords.begin();

    float texUInc = 1.0f / _uParts, texU = 0.0f;
    float texVInc = 1.0f / _vParts, texV = 0.0f;

    while (fItr != controls.end() && cItr != controlPoints.end() && tItr != texCoords.end())
    {
        *fItr = cItr->x; ++fItr;
        *fItr = cItr->y; ++fItr;
        *fItr = cItr->z; ++fItr;

        *tItr = texU; *tItr++; texU += texUInc;
        *tItr = texV; *tItr++; texV += texVInc;

        ++cItr;
    }

    _verticesEval.reset(new gl::Evaluator2D(Evaluator::VERTEX_3, _order, _order, _uParts, _vParts, controls));
    _texCoordEval.reset(new gl::Evaluator2D(Evaluator::TEXTURE_COORD_2, _order, _order, _uParts, _vParts, texCoords));
}

gl::primitives::Patch::~Patch()
{

}

void gl::primitives::Patch::draw( gl::MaterialPtr mat ) const
{
    glPushAttrib(GL_POLYGON_BIT);
    {
        glEnable(GL_AUTO_NORMAL);

        glFrontFace(GL_CW);

        _verticesEval->Map();
        _texCoordEval->Map();

        _verticesEval->Draw();

        _verticesEval->UnMap();
        _texCoordEval->UnMap();

        glDisable(GL_AUTO_NORMAL);
    }
    glPopAttrib();
}

