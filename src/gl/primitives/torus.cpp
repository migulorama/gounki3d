#include "gl/primitives/torus.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

gl::primitives::Torus::Torus(float inner, float outer, unsigned int slices, unsigned int loops) : _inner(inner), _outer(outer), _slices(slices), _loops(loops)
{
    ++_loops; ++_slices;

    _vertices.resize(_loops);
    for (auto itr = _vertices.begin(); itr != _vertices.end(); ++itr)
        itr->resize(_slices);

    _normals.resize(_loops);
    for (auto itr = _normals.begin(); itr != _normals.end(); ++itr)
        itr->resize(_slices);

    _texCoords.resize(_loops);
    for (auto itr = _texCoords.begin(); itr != _texCoords.end(); ++itr)
        itr->resize(_slices);

    float dPsi = 2.0f * acos(-1.0f) / (_loops - 1.0f);
    float dPhi = - 2.0f * acos(-1.0f) / (_slices - 1.0f);

    float phi = 0.0f;
    float psi = 0.0f;

    for (size_t j = 0; j < _loops; ++j)
    {
        float cpsi = cos(psi);
        float spsi = sin(psi);

        float s = (j) / static_cast<float>(_loops);

        phi = 0.0f;

        for (size_t i = 0; i < _slices; ++i)
        {
            float cphi = cos(phi);
            float sphi = sin(phi);

            _vertices[j][i].x = cpsi * (_outer + cphi * _inner);
            _vertices[j][i].y = spsi * (_outer + cphi * _inner);
            _vertices[j][i].z = sphi * _inner;

            _normals[j][i].x = cpsi * cphi;
            _normals[j][i].y = - spsi * cphi;
            _normals[j][i].z = sphi;
            _normals[j][i] = glm::normalize(_normals[j][i]);

            _texCoords[j][i].s = s;
            _texCoords[j][i].t = i / static_cast<float>(_slices);

            phi += dPhi;
        }

        psi += dPsi;
    }

    --_loops; --_slices;

}

void gl::primitives::Torus::draw(gl::MaterialPtr mat) const
{
    glPushMatrix();
    glBegin( GL_QUADS );

    for (size_t j = 0; j < _loops; ++j)
    {
        for (size_t i = 0; i < _slices; ++i)
        {
            glTexCoord2fv(glm::value_ptr(_texCoords[j][i]));
            glNormal3fv(glm::value_ptr(_normals[j][i]));
            glVertex3fv(glm::value_ptr(_vertices[j][i]));

            glTexCoord2fv(glm::value_ptr(_texCoords[j][i+1]));
            glNormal3fv(glm::value_ptr(_normals[j][i+1]));
            glVertex3fv(glm::value_ptr(_vertices[j][i+1]));

            glTexCoord2fv(glm::value_ptr(_texCoords[j+1][i+1]));
            glNormal3fv(glm::value_ptr(_normals[j+1][i+1]));
            glVertex3fv(glm::value_ptr(_vertices[j+1][i+1]));

            glTexCoord2fv(glm::value_ptr(_texCoords[j+1][i]));
            glNormal3fv(glm::value_ptr(_normals[j+1][i]));
            glVertex3fv(glm::value_ptr(_vertices[j+1][i]));
        }
    }

    glEnd();
    glPopMatrix();
}