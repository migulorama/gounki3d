#include "gl/shader.hpp"

static std::vector<char> GetShaderCompilationStatus(GLuint shaderID)
{
    int infoLogLength;

    glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);

    if (infoLogLength > 0)
    {
        std::vector<char> vertexShaderErrorMessage(infoLogLength);
        glGetShaderInfoLog(shaderID, infoLogLength, nullptr, vertexShaderErrorMessage.data());
        return vertexShaderErrorMessage;
    }

    return { '\0' };
}

static std::vector<char> GetProgramLinkStatus(GLuint programID)
{
    int infoLogLength;

    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);

    if (infoLogLength > 0)
    {
        std::vector<char> programErrorMessage(infoLogLength);
        glGetProgramInfoLog(programID, infoLogLength, nullptr, programErrorMessage.data());
        return programErrorMessage;
    }
    
    return { '\0' };
}

void gl::details::Shader::Compile()
{
    std::string vertexShaderCode;
    std::ifstream vertexShaderStream(_fileName, std::ios::in);

    if (vertexShaderStream.is_open())
    {
        vertexShaderStream.seekg(0, std::ios::end);
        auto size = static_cast<size_t>(vertexShaderStream.tellg());
        vertexShaderStream.seekg(0, std::ios::beg);
        vertexShaderCode.resize(size);
        vertexShaderStream.read(&vertexShaderCode[0], size);
    }
    else
    {
        throw std::exception(("Couldn't find file: " + _fileName).c_str());
    }

    GLint result = false;

    auto vertexSourcePointer = vertexShaderCode.c_str();
    glShaderSource(_shaderID, 1, &vertexSourcePointer, nullptr);
    glCompileShader(_shaderID);

    glGetShaderiv(_shaderID, GL_COMPILE_STATUS, &result);

    std::cout << _shaderID << std::endl;

    if (!result) throw ShaderCompileError(_shaderID);

    auto comp = GetShaderCompilationStatus(_shaderID);
    std::cout << comp.data() << std::endl;
}

gl::details::Shader::~Shader()
{
    glDeleteShader(_shaderID);
}

gl::details::Shader::Shader( GLenum type, const std::string& fileName ) : _type(type), _fileName(fileName)
{
    _shaderID = glCreateShader(type);
    if (_shaderID == 0)
    {
        auto err = glGetError();
        throw std::runtime_error("Error generating shader: " + std::to_string(err) + " type: " + std::to_string(type));
    }

}

void gl::details::Program::Unbind()
{
    glUseProgram(0);
}

void gl::details::Program::Bind()
{
    glUseProgram(_programID);
}

void gl::details::Program::Compile()
{
    for (auto itr = _shaders.begin(); itr != _shaders.end(); ++itr)
    {
        (*itr)->Compile();
        glAttachShader(_programID, (*itr)->_shaderID);
    }

    glLinkProgram(_programID);

    GLint result = false;

    glGetProgramiv(_programID, GL_LINK_STATUS, &result);

    if (!result) throw ProgramLinkError(_programID);

    auto comp = GetProgramLinkStatus(_programID);
    std::cout << comp.data() << std::endl;

    _shaders.clear();
    _shaders.resize(0);
}

gl::details::Program::Program( std::vector<ShaderPtr> shaders )
{
    std::swap(_shaders, shaders);
    _programID = glCreateProgram();
}

gl::details::Program::~Program()
{
    glDeleteProgram(_programID);
}

void gl::details::Program::AddAttribute( const std::string& attribute )
{
    GLint attribLoc = glGetAttribLocation(_programID, attribute.c_str());

    if (attribLoc != -1)
        _attributeList[attribute] = attribLoc;
}

void gl::details::Program::AddUniform( const std::string& uniform )
{
    GLint uniformLoc = glGetUniformLocation(_programID, uniform.c_str());

    // if (uniformLoc != -1)
        _uniformLocationList[uniform] = uniformLoc;
        GLint actualuniformLoc =  _uniformLocationList[uniform];
}

GLuint gl::details::Program::operator[]( const std::string& attribute ) const
{
    return _attributeList.at(attribute);
}

GLuint gl::details::Program::operator()( const std::string& uniform ) const
{
    return _uniformLocationList.at(uniform);
}

gl::ShaderCompileError::ShaderCompileError( GLuint shaderID ) : std::runtime_error(std::string("Shader Compile Error: ") + GetShaderCompilationStatus(shaderID).data())
{

}

gl::ProgramLinkError::ProgramLinkError( GLuint programID ) : std::runtime_error(std::string("Program Link Error: ") + GetProgramLinkStatus(programID).data())
{

}
