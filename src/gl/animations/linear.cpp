#include "gl/animations/linear.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

size_t gl::Animations::Linear::GetPath( float animTime ) const
{
    for (size_t i = 0; i < _paths.size(); ++i)
        if (animTime < _paths[i].timeStart) 
            return i - 1;

    return _paths.size() - 1;
}

void gl::Animations::Linear::Draw() const 
{ 
    glTranslatef(_position.x, _position.y, _position.z);
    glRotatef(_rotY, 0.0f, 1.0f, 0.0f);
}
void gl::Animations::Linear::Update(double time )
{
    if (_doReset)
    {
        _animTimeStart = static_cast<float>(time);
        _doReset = false;
    }

    float animTime = static_cast<float>(time) - _animTimeStart;
    if (animTime > _timeInMs)
    {
        _animTimeStart = static_cast<float>(time);
        animTime -= _timeInMs;
    }

    size_t currentPathID = GetPath(animTime);
    Path& currentPath = _paths[currentPathID % _paths.size()];
    _position = currentPath.positionStart  + currentPath.direction * (_speedPerMs * (animTime - currentPath.timeStart));
    _rotY = currentPath.rotY;
}

gl::Animations::Linear::~Linear()
{

}

gl::Animations::Linear::Linear( float timeInSeconds, std::vector<glm::vec3> controlPoints ) : _position()
{
    std::swap(controlPoints, _controlPoints);
    _timeInMs = timeInSeconds * 1000.0f;
    _paths.resize(_controlPoints.size() - 1);

    _speedPerMs = 0.0f;
    for (size_t i = 0; i < _paths.size(); ++i)
    {
        glm::vec3 pathVector = _controlPoints[i + 1] - _controlPoints[i];
        _paths[i].positionStart = _controlPoints[i];
        _paths[i].direction = glm::normalize(pathVector);

        if (_paths[i].direction.x == 0.0f && _paths[i].direction.z == 0.0f)
            _paths[i].rotY = 0.0f;
        else
            _paths[i].rotY = glm::angle(glm::vec3(0.0f, 0.0f, 1.0f), glm::normalize(glm::vec3(_paths[i].direction.x, 0.0f, _paths[i].direction.z)));

        _paths[i].distance = glm::length(pathVector);
        _speedPerMs += _paths[i].distance;
    }
    _speedPerMs /= _timeInMs;

    _paths.front().timeStart = 0.0f;
    for (auto itr = _paths.begin() + 1; itr != _paths.end(); ++itr)
    {
        itr->timeStart = (itr - 1)->timeStart + (itr - 1)->distance / _speedPerMs;
    }

    _doReset = true;
}
