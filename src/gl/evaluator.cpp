#include "gl/evaluator.hpp"

#include <stdexcept>

GLint gl::Evaluator::CalculateUStride()
{
    switch (_target)
    {
    case INDEX:
    case TEXTURE_COORD_1:
        return 1;
    case TEXTURE_COORD_2:
        return 2;
    case VERTEX_3:
    case NORMAL:
    case TEXTURE_COORD_3:
        return 3;
    case VERTEX_4:
    case COLOR_4:
    case TEXTURE_COORD_4:
        return 4;
    default:
        throw std::runtime_error("Invalid target!");
    }
}

gl::Evaluator::~Evaluator()
{

}

gl::Evaluator::Evaluator( Target tar )
{
    _target = tar;
}

GLenum gl::Evaluator1D::GetTargetEnum()
{
    switch (_target)
    {
    case VERTEX_3: return GL_MAP1_VERTEX_3;
    case VERTEX_4: return GL_MAP1_VERTEX_4;
    case INDEX: return GL_MAP1_INDEX;
    case COLOR_4: return GL_MAP1_COLOR_4;
    case NORMAL: return GL_MAP1_NORMAL;
    case TEXTURE_COORD_1: return GL_MAP1_TEXTURE_COORD_1;
    case TEXTURE_COORD_2: return GL_MAP1_TEXTURE_COORD_2;
    case TEXTURE_COORD_3: return GL_MAP1_TEXTURE_COORD_3;
    case TEXTURE_COORD_4: return GL_MAP1_TEXTURE_COORD_4;
    default: throw std::runtime_error("Invalid Target!");
    }
}

void gl::Evaluator1D::Draw() const 
{
    GLint polygonMode[2];
    glGetIntegerv(GL_POLYGON_MODE,  polygonMode);
    glMapGrid1f(_un, _u1, _u2);

    glEvalMesh1(polygonMode[0], _i1, _i2);
}

void gl::Evaluator1D::UnMap() const 
{
    glDisable(_targetEnum);
}

void gl::Evaluator1D::Map() const 
{
    glMap1f(_targetEnum, _u1, _u2, _stride, _order, _controlPoints.data());
    glEnable(_targetEnum);
}

gl::Evaluator1D::~Evaluator1D()
{

}

gl::Evaluator1D::Evaluator1D( Target tar, GLint order, GLint num_divisions, std::vector<float> controlPoints ) : Evaluator(tar)
{
    _u1 = 0.0f;
    _u2 = 1.0f;
    _i1 = 0;
    _i2 = _un = num_divisions;
    _order = order;
    _stride = CalculateUStride();
    size_t noFloats = _stride * (_order + 1);

    if (controlPoints.size() != noFloats) throw std::invalid_argument("Number of Control Points doesn't match with target and order.");

    std::swap(_controlPoints, controlPoints);

    _targetEnum = GetTargetEnum();
}

GLenum gl::Evaluator2D::GetTargetEnum()
{
    switch (_target)
    {
    case VERTEX_3: return GL_MAP2_VERTEX_3;
    case VERTEX_4: return GL_MAP2_VERTEX_4;
    case INDEX: return GL_MAP2_INDEX;
    case COLOR_4: return GL_MAP2_COLOR_4;
    case NORMAL: return GL_MAP2_NORMAL;
    case TEXTURE_COORD_1: return GL_MAP2_TEXTURE_COORD_1;
    case TEXTURE_COORD_2: return GL_MAP2_TEXTURE_COORD_2;
    case TEXTURE_COORD_3: return GL_MAP2_TEXTURE_COORD_3;
    case TEXTURE_COORD_4: return GL_MAP2_TEXTURE_COORD_4;
    default: throw std::runtime_error("Invalid Target!");
    }
}

void gl::Evaluator2D::Draw() const 
{
    GLint polygonMode[2];
    glGetIntegerv(GL_POLYGON_MODE,  polygonMode);
    glMapGrid2f(_un, _u1, _u2, _vn, _v1, _v2);

    glEvalMesh2(polygonMode[0], _i1, _i2, _j1, _j2);    
}

void gl::Evaluator2D::UnMap() const 
{
    glDisable(_targetEnum);
}

void gl::Evaluator2D::Map() const 
{
    glMap2f(_targetEnum, _u1, _u2, _ustride, _uorder + 1, _v1, _v2, _vstride, _vorder + 1, _controlPoints.data());
    glEnable(_targetEnum);
}

gl::Evaluator2D::~Evaluator2D()
{

}

gl::Evaluator2D::Evaluator2D( Target tar, GLint uorder, GLint vorder, GLint num_u_divisions, GLint num_v_division, std::vector<float> controlPoints ) : Evaluator(tar)
{
    _u1 = _v1 = 0.0f;
    _u2 = _v2 = 1.0f;
    _i1 = _j1 = 0;
    _i2 = _un = num_u_divisions;
    _j2 = _vn = num_v_division;
    _uorder = uorder;
    _vorder = vorder;
    _ustride = CalculateUStride();
    _vstride = _ustride * (_uorder + 1);

    size_t noFloats = _ustride * (_uorder + 1) * (_vorder + 1);

    if (controlPoints.size() != noFloats) throw std::invalid_argument("Number of Control Points doesn't match with target and order.");

    std::swap(_controlPoints, controlPoints);

    _targetEnum = GetTargetEnum();
}
