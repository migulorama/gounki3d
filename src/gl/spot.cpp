#include "gl/lights/spot.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

gl::Lights::Spot::Spot(GLuint lightid) : Light(lightid)
{
    _position.w = 1.0f;
}

gl::Lights::Spot::Spot(GLuint lightid, const glm::vec3& pos, float angle, float exponent, const glm::vec3& dir) : Light(lightid, pos)
{
    SetExponent(exponent);
    SetDirection(dir);
    SetAngle(angle);
    _position.w = 1.0f;
}

void gl::Lights::Spot::SetExponent(float exponent)
{
    _exponent = exponent;
    glLightf(_id, GL_SPOT_EXPONENT, _exponent);
}

void gl::Lights::Spot::SetDirection(const glm::vec3& dir)
{
    _direction =  dir;
}

void gl::Lights::Spot::SetAngle( float angle )
{
    _angle = angle;
    glLightf(_id, GL_SPOT_CUTOFF, angle);
}
