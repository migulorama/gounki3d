#include <gl_4_4.h>

#include "gl/texture.hpp"

#include <stdexcept>
#include <cstdint>
#include <iostream>

#include <SOIL.h>

bool gl::details::Texture::is_constructing = false;

gl::TexturePtr gl::details::Texture::Load(const std::string& fileName)
{
    is_constructing = true;
    auto val = std::make_shared<gl::details::Texture>(fileName);
    is_constructing = false;
    return val;
}

gl::details::Texture::Texture(const std::string& fileName) : _id(SOIL_CREATE_NEW_ID)
{
    if (!is_constructing) throw std::runtime_error("Can't build non pointer Textures.");
    LoadFromFile(fileName);
}

gl::details::Texture::~Texture()
{
    if (_id != SOIL_CREATE_NEW_ID) glDeleteTextures(1, &_id);
}

void gl::details::Texture::Apply(GLenum textureUnit/* = GL_TEXTURE0 */)
{
    if (_id != SOIL_CREATE_NEW_ID)
    {
        glActiveTexture(textureUnit);
        glBindTexture(GL_TEXTURE_2D, _id);
    }
}

void gl::details::Texture::SetWrap(GLenum sWrap, GLenum tWrap)
{
    if (_id != SOIL_CREATE_NEW_ID)
    {
        glPushAttrib(GL_TEXTURE_BIT);
        glBindTexture(GL_TEXTURE_2D, _id);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sWrap);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, tWrap);
        glPopAttrib();
    }
}

void gl::details::Texture::LoadFromFile(const std::string& fileName)
{
    _id = SOIL_load_OGL_texture(fileName.c_str(), SOIL_LOAD_AUTO, _id, SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
    if (_id == 0) throw std::runtime_error(std::string("Error loading image: ") + fileName + ": " + SOIL_last_result());

    glBindTexture(GL_TEXTURE_2D, _id);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    SetWrap(GL_REPEAT, GL_REPEAT);
}
