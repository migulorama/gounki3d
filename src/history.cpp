#include "history.hpp"

game_history::entry game_history::get_last()
{
    return _entries.back();
}

bool game_history::redo()
{
    if (!_future_entries.empty())
    {
        _entries.push_back(_future_entries.back());
        _future_entries.pop_back();
        return true;
    }
    return false;
}

void game_history::undo_all()
{
    while (_entries.size() > 1)
    {
        _future_entries.push_back(_entries.back());
        _entries.pop_back();
    }
}

bool game_history::undo()
{
    if (_entries.size() > 1)
    {
        _future_entries.push_back(_entries.back());
        _entries.pop_back();
        return true;
    }
    return false;
}

void game_history::commit(gounki::board board, gounki::player::player player, gounki::movement movement, gounki::captured_pieces captures)
{
    _entries.emplace_back(board, player, movement, captures);
}

void game_history::commit(gounki::board board, gounki::player::player player, gounki::captured_pieces pieces)
{
    _entries.emplace_back(board, player, boost::optional<gounki::movement>(), pieces);
}

const std::vector<game_history::entry>& game_history::get_history() const
{
    return _entries;
}

void game_history::clear()
{
    _entries.clear();
    _future_entries.clear();
}