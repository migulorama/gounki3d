#include "gounki/animation_manager.hpp"
#include <glm/glm.hpp>
#include <gl_4_4.h>
#include <algorithm>    // std::transform
#include <numeric>      // std::iota

#include "gounki/move_animation.hpp"
#include "gounki/deployment_animation.hpp"
#include "gounki/capture_animation.hpp"
#include "gounki/dummy_animation.hpp"

gounki::animation_manager::animation_manager()
{
}

void gounki::animation_manager::init(board_visualizer* boardPositions, const board* logicBoard)
{
    _boardPositions = boardPositions;
    _logicBoard = logicBoard;
}

void gounki::animation_manager::addAnimation(gl::AnimationPtr animation)
{
    _animations.push_back(animation);
}

void gounki::animation_manager::addMovementAnimation(gounki::movement movement, const gounki::board& board, double timeInMilliseconds)
{
    std::vector<glm::uvec2> logicPositions = movement.get_positions();
    std::vector<glm::vec3> realPositions;
    colour sourceCellColour = getCellColor(logicPositions[0].x, logicPositions[0].y);
    glm::vec2 cell_size = _boardPositions->get_cell_size();
    cell_size.x *= 0.3f;

    auto itr = logicPositions.begin();
    unsigned int index;
    realPositions.push_back(toRealPosition(*itr)[0]); // starting position is always the inferior one
    for (++itr; itr < logicPositions.end(); ++itr)
    {
        index = getAvailablePos(itr->x, itr->y, sourceCellColour);
        realPositions.push_back(toRealPosition(*itr)[index]);
        addCaptureAnimation(itr->x, itr->y, timeInMilliseconds, sourceCellColour);
    }

    gl::AnimationPtr animation;
    if (movement.is_simple_move())
    {
        animation = std::make_shared<gounki::move_animation>(toRealPosition(logicPositions[0]), realPositions, timeInMilliseconds);
    }
    else if (movement.is_deployment())
    {
        const auto& move_elems = movement.get_pieces();
        const auto& cell_elems = board.cell_at(position(logicPositions[0].x, logicPositions[0].y - 1)).get_elements();
        std::vector<int> elem_order(move_elems.size());
        std::iota(elem_order.begin(), elem_order.end(), 0);

        for (int i = 0; i < move_elems.size(); ++i)
            if (move_elems[i] != cell_elems[elem_order[i]])
            {
                auto itr = std::find_if(elem_order.begin() + i + 1, elem_order.end(), [&cell_elems, &move_elems, &i](const int& index) {return cell_elems[index] == move_elems[i]; });
                std::iter_swap(itr, elem_order.begin() + i);
            }

        std::vector<glm::vec3> new_real_positions(realPositions.size());
        new_real_positions[0] = realPositions[0];

        for (int i = 0; i < elem_order.size(); ++i)
            new_real_positions[i + 1] = realPositions[elem_order[i] + 1];


        animation = std::make_shared<gounki::deployment_animation>(toRealPosition(logicPositions[0]), new_real_positions, cell_size, timeInMilliseconds);
    }

    addAnimation(animation);
}

void gounki::animation_manager::addCameraAnimation(gl::PerspectiveCameraPtr cam, const glm::vec3& targetPosition, double timeInMs)
{
    gl::AnimationPtr animation = std::make_shared<gounki::camera_animation>(cam, targetPosition, timeInMs);
    _animations.push_back(animation);
}

void gounki::animation_manager::addCaptureAnimation(unsigned int row, unsigned int column, double timeInMilliseconds, colour sourceColour)
{
    column = column > 0 ? column - 1 : column;
    std::array<glm::vec3, 3>& sourcePos = _boardPositions->get_cell_position(row, column);
    const cell& chosenCell = _logicBoard->cell_at(row, column);

    colour targetColour = chosenCell.get_colour();
    if (targetColour == colour::Blank || targetColour == sourceColour) // no pieces are subject to capture
        return;

    player::player p = sourceColour == colour::Black ? player::Player2 : player::Player1;

    gounki::cell::elements elems = chosenCell.get_elements();
    std::vector<glm::vec3> controlPoints;
    controlPoints.push_back(sourcePos[0]);
    for (unsigned int i = 0; i < elems.size(); ++i)
    {
        controlPoints.push_back(glm::vec3(_boardPositions->get_inv_transforms() * glm::vec4(_boardPositions->get_next_captured_cells_position(p), 1.0f)));
    }

    glm::vec2 cell_size = _boardPositions->get_cell_size();
    cell_size.x *= 3;
    gl::AnimationPtr animation = std::make_shared<gounki::capture_animation>(_boardPositions->get_cell_position(row, column), controlPoints, cell_size, timeInMilliseconds);
    _animations.push_back(animation);
}

void gounki::animation_manager::update(double timeElapsed)
{
    for (auto& anim : _animations)
        anim->Update(timeElapsed);

    _animations.erase(std::remove_if(_animations.begin(), _animations.end(), [] (const gl::AnimationPtr& anim) { return anim->isFinished(); }), 
                        _animations.end());
}

bool gounki::animation_manager::hasAnimations() const
{
    return !_animations.empty();
}

std::array<glm::vec3, 3>& gounki::animation_manager::toRealPosition(unsigned int row, unsigned int column)
{
    return toRealPosition(glm::uvec2(row, column));
}

std::array<glm::vec3, 3>& gounki::animation_manager::toRealPosition(const glm::uvec2& logicPosition)
{
    if (logicPosition.y > 0)
        return _boardPositions->get_cell_position(logicPosition.x, logicPosition.y - 1);
    else
        return _boardPositions->get_cell_position(logicPosition.x, logicPosition.y);
}

unsigned int gounki::animation_manager::getAvailablePos(unsigned int row, unsigned int column, colour sourceColour) const
{
    column = column > 0 ? column - 1 : column;
    const cell& chosenCell = _logicBoard->cell_at(row, column);

    colour targetColour = chosenCell.get_colour();
    if (targetColour == colour::Blank || targetColour != sourceColour)
        return 0;

    return chosenCell.get_elements().size() % 3;
}

gounki::colour gounki::animation_manager::getCellColor(unsigned int row, unsigned int column) const
{
    column = column > 0 ? column - 1 : column;
    return _logicBoard->cell_at(row, column).get_colour();
}

void gounki::animation_manager::addDummyAnimation(double time)
{
    gl::AnimationPtr animation = std::make_shared<gounki::dummy_animation>(time);
    _animations.push_back(animation);
}
