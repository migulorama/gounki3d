#include "gounki/capture_animation.hpp"
#include "gounki/linear_animation.hpp"

#include <iostream>

gounki::capture_animation::capture_animation(std::array<glm::vec3, 3>& source, std::vector<glm::vec3> controlPoints, const glm::vec2& cell_size,double timeInMilliseconds)
{
    for (unsigned int i = 1; i < controlPoints.size(); ++i)
    {
        std::vector<glm::vec3> intermediatePoints;
        intermediatePoints.push_back(source[i - 1]);
        glm::vec3 upperPoint = controlPoints[i];
        upperPoint.y = source[i - 1].y + cell_size.y;
        intermediatePoints.push_back(upperPoint);
        intermediatePoints.push_back(controlPoints[i]);
        _linearAnimations.emplace_back(source[i - 1], intermediatePoints, timeInMilliseconds);
    }
}

void gounki::capture_animation::Update(double time)
{
    static bool _finishedAllAnimations = true;

    _finishedAllAnimations = true;
    for (auto& linearAnimation : _linearAnimations)
    {
        linearAnimation.Update(time);
        _finishedAllAnimations = _finishedAllAnimations && linearAnimation.isFinished();
    }

    _isFinished = _isFinished || _finishedAllAnimations;
}
