#include "gounki/theater_scene.hpp"
#include "gounki/theater_state.hpp"
#include "gounki/animation_manager.hpp"
#include "gounki/board_visualizer.hpp"

#include "scene/loader.hpp"

#include <gl_4_4.h>

void theater_undo_button_callback(puObject* obj);
void theater_redo_button_callback(puObject* obj);
void theater_auto_play_button_callback(puObject* obj);

gl::TexturePtr  _theater_overlay_background_tex;
gl::MaterialPtr _theater_overlay_background_mat;

gounki::theater_scene::theater_scene(theater_state& ts) :
_state(ts),
_controls_group(nullptr, puDeleteObject),
_undo_button(nullptr, puDeleteObject),
_redo_button(nullptr, puDeleteObject),
_auto_play_button(nullptr, puDeleteObject)
{

}

void gounki::theater_scene::init()
{
    YAF::Loader loader(_state.get_context()->get_game()->get_yaf_filename());

    _scene = loader.Load();
    auto settings = loader.GetBoardSettings();

    _scene.Init();

    _board_visualizer.set_position(settings.position);
    _board_visualizer.set_size(settings.size);
    _board_rotation = settings.rotation;
    _board_visualizer.init(_state.get_current_board(), settings.player_1_capPieces, settings.player_2_capPieces, _board_rotation);

    glm::quat rotating_quaternion = glm::toQuat(_board_rotation);

    glm::vec3 target = _board_visualizer.get_position() + glm::rotate(rotating_quaternion, glm::vec3(_board_visualizer.get_size().x * ((1.0f / 2.0f) - (_board_visualizer.get_cell_size().x / 2.0f)), 0.0f, _board_visualizer.get_size().y * ((1.0f / 2.0f) - (_board_visualizer.get_cell_size().y / 2.0f))));
    _player_position[player::Player1] = target + glm::rotate(rotating_quaternion, glm::vec3(0.0f, 3.f, _board_visualizer.get_size().x));
    _player_position[player::Player2] = target + glm::rotate(rotating_quaternion, glm::vec3(0.0f, 3.f, -_board_visualizer.get_size().x));
    _board_normal = glm::normalize(glm::rotate(rotating_quaternion, glm::vec3(0.0f, 1.0f, 0.0f)));
    _board_right_vector = glm::normalize(glm::rotate(rotating_quaternion, glm::vec3(1.0f, 0.0f, 0.0f)));

    _camera = std::make_shared<gl::Cameras::Perspective>(60.0f, 0.1f, 1000.f, _player_position[player::Player1], target);

    _quad_white_mesh = settings.quad_white_object;
    _quad_black_mesh = settings.quad_black_object;
    _round_white_mesh = settings.round_white_object;
    _round_black_mesh = settings.round_black_object;

    _quad_white_mesh->init(nullptr);
    _quad_black_mesh->init(nullptr);
    _round_white_mesh->init(nullptr);
    _round_black_mesh->init(nullptr);

    _white_mat = settings.white_app;
    _black_mat = settings.black_app;
    _white_transparent_mat = gl::make_material({ { 0.0f, 0.0f, 0.0f, 0.0f } }, { { 0.0f, 0.0f, 0.0f, 0.0f } }, { { 0.0f, 0.0f, 0.0f, 0.0f } }, { { 0.0f, 0.0f, 0.0f, 0.0f } }, 0.0f);

    _light = std::make_unique<gl::Light>(GL_LIGHT1);
    _light->Enable(true);
    _light->SetPosition(glm::vec3(0.0f, 10.0, 0.0f));
    _light->SetAmbient(glm::vec4(0.5f, 0.5f, 0.5f, 1.0f));
    _light->SetDiffuse(glm::vec4(1.f, 1.f, 1.f, 1.f));
    _light->SetSpecular(glm::vec4(0.2, 0.2, 0.3, 1));

    _animation_manager.init(&_board_visualizer, &_state.get_current_board());

    int width = _state.get_context()->get_window_width();
    int height = _state.get_context()->get_window_height();

    // elements border limits configuration
    unsigned int margin = ((width - 500) / 2) > 0 ? (width - 500) / 2 : 0;
    _overlay_borders.margin_left = margin;
    _overlay_borders.margin_right = width - margin;
    _overlay_borders.margin_bottom = 0;
    _overlay_borders.margin_top = 150;

    // interface initialization
    _controls_group.reset(new puGroup(0, 0));

    _undo_button.reset(new puArrowButton(_overlay_borders.margin_left + 155, _overlay_borders.margin_bottom + 20, _overlay_borders.margin_left + 215, _overlay_borders.margin_bottom + 65, PUARROW_FASTLEFT));
    _redo_button.reset(new puArrowButton(_overlay_borders.margin_left + 285, _overlay_borders.margin_bottom + 20, _overlay_borders.margin_left + 345, _overlay_borders.margin_bottom + 65, PUARROW_FASTRIGHT));
    _auto_play_button.reset(new puArrowButton(_overlay_borders.margin_left + 220, _overlay_borders.margin_bottom + 20, _overlay_borders.margin_left + 280, _overlay_borders.margin_bottom + 65, PUARROW_RIGHT));

    _undo_button->setCallback(::theater_undo_button_callback);
    _undo_button->setStyle(PUSTYLE_SHADED);
    _undo_button->setUserData(&_state);
    _undo_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);

    _redo_button->setCallback(::theater_redo_button_callback);
    _redo_button->setStyle(PUSTYLE_SHADED);
    _redo_button->setUserData(&_state);
    _redo_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);

    _auto_play_button->setCallback(::theater_auto_play_button_callback);
    _auto_play_button->setStyle(PUSTYLE_SHADED);
    _auto_play_button->setUserData(&_state);
    _auto_play_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);

    _controls_group->close();
	_controls_group->hide();

    _theater_overlay_background_tex = gl::Texture::Load("resources/ui/overlay_background_edgy.png");
    _theater_overlay_background_mat = gl::make_material(_theater_overlay_background_tex, GL_REPEAT, GL_REPEAT);
}

void gounki::theater_scene::draw()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Update Lights and Transformations
    _light->Update();
    if (_camera) _camera->ApplyView();

    _scene.draw();

    const auto& cells = _state.get_current_board().get_cells();
    const auto& board = _state.get_current_board();
    const auto& cell_size = _board_visualizer.get_cell_size();

    size_t nr = board.get_num_rows();
    size_t nc = board.get_num_cols();
    size_t index = 0;

    // Player 1
    auto captured_pieces = _state.get_captured_pieces(player::Player1);
    auto colour = _state.get_player_colour(player::Player2);

    glm::vec3 location;
    for (unsigned int i = 0; i < captured_pieces.size(); ++i)
    {
        const auto& mesh = get_object(colour, captured_pieces[i]);
        auto size = mesh->get_size();
        location = _board_visualizer.get_captured_cells_position(player::Player1, i);
        glPushMatrix();
        glTranslatef(location.x, location.y, location.z);
        glScalef(_board_visualizer.get_size().x, _board_visualizer.get_size().x, _board_visualizer.get_size().y);
        glScalef(0.6f * _board_visualizer.get_cell_size().x / size.x, 0.3f * _board_visualizer.get_cell_size().x / size.y, 0.6f * _board_visualizer.get_cell_size().y / size.z);
        mesh->draw(nullptr);
        glPopMatrix();
    }
    // player 2
    captured_pieces = _state.get_captured_pieces(player::Player2);
    colour = _state.get_player_colour(player::Player1);

    for (unsigned int i = 0; i < captured_pieces.size(); ++i)
    {
        const auto& mesh = get_object(colour, captured_pieces[i]);
        auto size = mesh->get_size();
        location = _board_visualizer.get_captured_cells_position(player::Player2, i);

        glPushMatrix();

        glTranslatef(location.x, location.y, location.z);
        glScalef(_board_visualizer.get_size().x, _board_visualizer.get_size().x, _board_visualizer.get_size().y);
        glScalef(0.6f * _board_visualizer.get_cell_size().x / size.x, 0.3f * _board_visualizer.get_cell_size().x / size.y, 0.6f * _board_visualizer.get_cell_size().y / size.z);
        mesh->draw(nullptr);

        glPopMatrix();
    }

    glPushMatrix();

    glTranslatef(_board_visualizer.get_position().x, _board_visualizer.get_position().y, _board_visualizer.get_position().z);
    glMultMatrixf(glm::value_ptr(_board_rotation));
    glScalef(_board_visualizer.get_size().x, _board_visualizer.get_size().x, _board_visualizer.get_size().y);

    for (GLuint r = 0; r < nr; ++r)
    for (GLuint c = 0; c < nc; ++c, ++index)
    {
        glPushMatrix();

        glm::vec3 pos = _board_visualizer.get_default_cell_position(r, c)[0];
        glTranslatef(pos.x, pos.y, pos.z);

        if (r == 0 || r == (nr - 1))
            _white_transparent_mat->Apply();
        else if (r % 2 == index % 2)
            _white_mat->Apply();
        else
            _black_mat->Apply();

        glScalef(cell_size.x, 1.0f, cell_size.y);

        glBegin(GL_QUADS);
        glNormal3f(0.0f, 1.0f, 0.0f);
        glTexCoord2i(1, 0);
        glVertex3f(-.5f, 0.f, -.5f);
        glTexCoord2i(0, 0);
        glVertex3f(-.5f, 0.f, .5f);
        glTexCoord2i(0, 1);
        glVertex3f(.5f, 0.f, .5f);
        glTexCoord2i(1, 1);
        glVertex3f(.5f, 0.f, -.5f);
        glEnd();


        glPopMatrix();

        draw_cell(cells[r * 8 + c], _board_visualizer.get_cell_position(r, c));
    }

    glPopMatrix();

    glPushAttrib(GL_ENABLE_BIT | GL_VIEWPORT_BIT | GL_TRANSFORM_BIT | GL_LIGHTING_BIT);

    glDisable(GL_LIGHTING);
    glDisable(GL_FOG);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    unsigned int w = _state.get_context()->get_window_width();
    unsigned int h = _state.get_context()->get_window_height();

    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0, w, 0, h, -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glTranslatef(0.375, 0.375, 0);
    glPushMatrix();
    glLoadIdentity();

    // interface background
    glColor3f(1.0f, 1.0f, 1.0f);
    _theater_overlay_background_mat->Apply();
    glBegin(GL_QUADS);
    glTexCoord2i(0, 0);
    glVertex2i(_overlay_borders.margin_left, _overlay_borders.margin_bottom);
    glTexCoord2i(1, 0);
    glVertex2i(_overlay_borders.margin_right, _overlay_borders.margin_bottom);
    glTexCoord2i(1, 1);
    glVertex2i(_overlay_borders.margin_right, _overlay_borders.margin_top);
    glTexCoord2i(0, 1);
    glVertex2i(_overlay_borders.margin_left, _overlay_borders.margin_top);
    glEnd();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glPopAttrib();

    puDisplay();
}

void gounki::theater_scene::update(double time)
{
    _animation_manager.update(time);
}

void gounki::theater_scene::reshape(int width, int height)
{
    glViewport(0, 0, width, height);
    if (_camera) _camera->UpdateProjectionMatrix(width, height);

    unsigned int margin = ((width - 500) / 2) > 0 ? (width - 500) / 2 : 0;
    _overlay_borders.margin_left = margin;
    _overlay_borders.margin_right = width - margin;
    _overlay_borders.margin_bottom = 0;
    _overlay_borders.margin_top = 150;

    _undo_button->setPosition(_overlay_borders.margin_left + 40, _overlay_borders.margin_bottom + 20);
    _redo_button->setPosition(_overlay_borders.margin_left + 160, _overlay_borders.margin_bottom + 20);

    _undo_button->setPosition(_overlay_borders.margin_left + 155, _overlay_borders.margin_bottom + 20);
    _redo_button->setPosition(_overlay_borders.margin_left + 285, _overlay_borders.margin_bottom + 20);
    _auto_play_button->setPosition(_overlay_borders.margin_left + 220, _overlay_borders.margin_bottom + 20);
}

gounki::animation_manager& gounki::theater_scene::get_animation_manager()
{
    return _animation_manager;
}

gounki::board_visualizer& gounki::theater_scene::get_board_visualizer()
{
    return _board_visualizer;
}

YAF::ComposedObjectPtr gounki::theater_scene::get_object(const gounki::colour& colour, const gounki::piece& piece)
{
    switch (colour)
    {
    case gounki::colour::Black: return piece == gounki::piece::Circle ? _round_black_mesh : _quad_black_mesh;
    case gounki::colour::White: return piece == gounki::piece::Circle ? _round_white_mesh : _quad_white_mesh;
    default: return nullptr;
    }
}

void gounki::theater_scene::draw_cell(const gounki::cell& cell, const std::array<glm::vec3, 3>& cellPositions)
{
    auto col = cell.get_colour();

    auto elems = cell.get_elements();
    for (int i = 0; i < elems.size(); ++i)
    {
        const auto& mesh = get_object(col, elems[i]);

        auto size = mesh->get_size();
        glPushMatrix();
        glm::vec3 pos = cellPositions[i];
        glTranslated(pos.x, pos.y, pos.z);
        glScalef(0.6f * _board_visualizer.get_cell_size().x / size.x, 0.3f * _board_visualizer.get_cell_size().x / size.y, 0.6f * _board_visualizer.get_cell_size().y / size.z);
        mesh->draw(nullptr);
        glPopMatrix();
    }

}

void gounki::theater_scene::on_pause()
{
    _controls_group->hide();
}

void gounki::theater_scene::on_resume()
{
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_DEPTH_TEST);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glDisable(GL_CULL_FACE);
    glClearColor(0.0, 1.0, 0.0, 1.0);
    glShadeModel(GL_SMOOTH);
    glPolygonMode(GL_FRONT, GL_FILL);
    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
    _controls_group->reveal();
}

const glm::vec3& gounki::theater_scene::get_board_normal_vector()
{
    return _board_normal;
}

const glm::vec3& gounki::theater_scene::get_board_right_vector()
{
    return _board_right_vector;
}

void gounki::theater_scene::disableUndoRedo()
{
    _undo_button->greyOut();
    _undo_button->setColourScheme(101 / 255.0f, 101 / 255.0f, 101 / 255.0f);

    _redo_button->greyOut();
    _redo_button->setColourScheme(101 / 255.0f, 101 / 255.0f, 101 / 255.0f);
}

void gounki::theater_scene::enableUndoRedo()
{
    _undo_button->activate();
    _undo_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);

    _redo_button->activate();
    _redo_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);
}

void theater_undo_button_callback(puObject* obj)
{
    gounki::theater_state* st = reinterpret_cast<gounki::theater_state*>(obj->getUserData());
    st->undo();
    obj->setValue(false);
}

void theater_redo_button_callback(puObject* obj)
{
    gounki::theater_state* st = reinterpret_cast<gounki::theater_state*>(obj->getUserData());
    st->redo();
    obj->setValue(false);
}

void theater_auto_play_button_callback(puObject* obj)
{
    static bool auto_play = true;
    gounki::theater_state* st = reinterpret_cast<gounki::theater_state*>(obj->getUserData());
    st->set_auto_play(auto_play);

    if (auto_play)
    {
        auto_play = false;
        obj->setColourScheme(1, 0, 0);
    }
    else
    {
        auto_play = true;
        obj->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);
    }

    obj->setValue(false);
}