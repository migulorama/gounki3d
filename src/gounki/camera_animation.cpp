#include "gounki/camera_animation.hpp"

#define _USE_MATH_DEFINES
#include <math.h>
#include <glm/gtx/vector_angle.hpp>

#include <iostream>

inline double to_degrees(double radians) {
    return radians*(180.0 / M_PI);
}

inline double to_radians(double degrees) {
    return degrees * (M_PI / 180.0);
}

gounki::camera_animation::camera_animation(gl::PerspectiveCameraPtr cam, const glm::vec3& targetPosition, double timeInMs) : _cam(cam), _timeInMs(timeInMs), _doReset(true), _target_position(targetPosition)
{
    if (cam->GetPosition() == targetPosition)
    {
        _isFinished = true;
    }
    else
    {
        glm::vec3 current_direction = glm::normalize(cam->GetTarget() - cam->GetPosition());
        glm::vec3 new_direction = glm::normalize(cam->GetTarget() - targetPosition);

        glm::vec3 proj_current_direction = glm::normalize(glm::vec3(current_direction.x, 0.0f, current_direction.z));
        glm::vec3 proj_new_direction = glm::normalize(glm::vec3(new_direction.x, 0.0f, new_direction.z));
        float angle_y = glm::orientedAngle(proj_current_direction, proj_new_direction, glm::vec3(0.0f, 1.0f, 0.0f));
        if (angle_y < 0) angle_y = 360.0f + angle_y;
        _angles_per_ms.y = static_cast<float>(angle_y / _timeInMs);

        proj_current_direction = glm::normalize(glm::vec3(current_direction.x, current_direction.y, 0.0f));
        proj_new_direction = glm::normalize(glm::vec3(new_direction.x, new_direction.y, 0.0f));

        float angle_x_1 = 90.0f - glm::angle(glm::vec3(0.0f, 1.0f, 0.0f), current_direction);
        float angle_x_2 = 90.0f - glm::angle(glm::vec3(0.0f, 1.0f, 0.0f), new_direction);

        _angles_per_ms.x = static_cast<float>((angle_x_2 - angle_x_1) / _timeInMs);

        proj_current_direction = glm::normalize(glm::vec3(0.0f, current_direction.y, current_direction.z));
        proj_new_direction = glm::normalize(glm::vec3(0.0f, new_direction.y, new_direction.z));

        float current_dist = glm::length(cam->GetTarget() - cam->GetPosition());
        float target_dist = glm::length(cam->GetTarget() - targetPosition);
        _speedPerMs = (target_dist - current_dist) / _timeInMs;

    }
}

void gounki::camera_animation::Update(double time)
{
    if (_isFinished)
        return;

    if (_doReset)
    {
        _last_update = time;
        _animTimeStart = static_cast<float>(time);
        _doReset = false;
    }

    double animTime = time - _animTimeStart;
    double partialTime = time - _last_update;
    
    _cam->RotatePosition(static_cast<float>(_angles_per_ms.x * partialTime), _cam->GetRightVector());
    _cam->RotatePosition(static_cast<float>(_angles_per_ms.y * partialTime), glm::vec3(0.0f, 1.0f, 0.0f));
    _cam->Translate(glm::vec3(0.0f, 0.0f, _speedPerMs * partialTime));

    if (animTime > _timeInMs)
    {
        _cam->SetPosition(_target_position);
        _animTimeStart = time;
        animTime -= _timeInMs;
        _isFinished = true;
        return;
    }

    _last_update = time;
}
