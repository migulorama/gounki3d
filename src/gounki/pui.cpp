#include "gounki/pui.hpp"
#include <window.hpp>
int pu_glfw_get_window()
{
    auto win = glfwGetCurrentContext();
    framework::window* window = reinterpret_cast<framework::window*>(glfwGetWindowUserPointer(win));
    return window->get_unique_identifier();
}

void pu_glfw_get_window_size(int *width, int *height)
{
    auto win = glfwGetCurrentContext();
    framework::window* window = reinterpret_cast<framework::window*>(glfwGetWindowUserPointer(win));
    auto size = window->get_framebuffer_size();
    *width = size.x;
    *height = size.y;
}