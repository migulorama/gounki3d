#include "gounki/menu_state.hpp"
#include "gounki/game_state.hpp"
#include "gounki/main_state.hpp"
#include "gounki/theater_state.hpp"
#include "gounki/settings_state.hpp"
#include "gl/texture.hpp"
#include "gl/material.hpp"
#include "gl/primitives/rectangle.hpp"

#include <algorithm>
#include <iostream>
#include <array>

#include <gl_4_4.h>
#include "gounki/pui.hpp"

const std::array<float, 3> dark_brown = { 101 / 255.0f, 67 / 255.0f, 33 / 255.0f };
gl::TexturePtr _menu_background_tex;
gl::MaterialPtr _menu_background_mat;

gounki::menu_state::menu_state(state_manager& manager) : 
    game_state(manager), 
    _interface(this),
    b(nullptr, puDeleteObject),
    theater_mode_button(nullptr, puDeleteObject),
    settings_mode_button(nullptr, puDeleteObject),
    start_button(nullptr, puDeleteObject),
    back_button(nullptr, puDeleteObject),
    player_1_mode_cb(nullptr, puDeleteObject),
    player_2_mode_cb(nullptr, puDeleteObject),
    player_1_diff_cb(nullptr, puDeleteObject),
    player_2_diff_cb(nullptr, puDeleteObject),
    _pu_group(nullptr, puDeleteObject),
    _player_1_text(nullptr, puDeleteObject),
    _player_2_text(nullptr, puDeleteObject),
    _vs_text(nullptr, puDeleteObject),
    theater_mode_file_selector(nullptr, puDeleteObject)
{
}

gounki::menu_state::~menu_state()
{
}

void gounki::menu_state::button_cb(puObject* obj)
{
    //reinterpret_cast<gounki::menu_state*>(obj->getUserData())->change_to_state<gounki::MainGameState>();
    player_1_mode_cb->reveal();
    player_2_mode_cb->reveal();
    player_1_mode_cb->invokeCallback();
    player_2_mode_cb->invokeCallback();

    start_button->reveal();
    back_button->reveal();

    _player_1_text->reveal();
    _player_2_text->reveal();

    settings_mode_button->hide();
    theater_mode_button->hide();
    b->hide();
}

void theater_mode_button_cb(puObject* obj)
{
    reinterpret_cast<gounki::menu_state*>(obj->getUserData())->theater_mode_cb(obj);
}

void theater_mode_file_selector_cb(puObject* obj)
{
    reinterpret_cast<gounki::menu_state*>(obj->getUserData())->file_selector_cb(obj);
}

void settings_mode_button_cb(puObject* obj)
{
    reinterpret_cast<gounki::menu_state*>(obj->getUserData())->change_to_state<gounki::settings_state>();
}

void start_button_cb(puObject* obj)
{
    auto ms = reinterpret_cast<gounki::menu_state*>(obj->getUserData());
    ms->get_context()->add_state<gounki::main_state>();
    ms->change_to_state<gounki::main_state>();
}

void gounki::menu_state::back_button_cb(puObject* obj)
{
    start_button->hide();
    back_button->hide();

    player_1_mode_cb->hide();
    player_2_mode_cb->hide();
    player_1_diff_cb->hide();
    player_2_diff_cb->hide();

    _player_1_text->hide();
    _player_2_text->hide();

    settings_mode_button->reveal();
    theater_mode_button->reveal();
    b->reveal();
}

void cb_callback(puObject* obj)
{
    gounki::menu_state* st = reinterpret_cast<gounki::menu_state*>(obj->getUserData());
    st->cb_callback(obj);
}

void back_button_cb(puObject* obj)
{
    gounki::menu_state* st = reinterpret_cast<gounki::menu_state*>(obj->getUserData());
    st->back_button_cb(obj);
}

void button_cb(puObject* obj)
{
    gounki::menu_state* st = reinterpret_cast<gounki::menu_state*>(obj->getUserData());
    st->button_cb(obj);
}


int styles[] = {
    PUSTYLE_NONE,
    PUSTYLE_PLAIN,
    PUSTYLE_SHADED,
    PUSTYLE_SMALL_SHADED,
    PUSTYLE_BEVELLED,
    PUSTYLE_SMALL_BEVELLED,
    PUSTYLE_BOXED,
    PUSTYLE_SPECIAL_UNDERLINED,
    PUSTYLE_DROPSHADOW,
    PUSTYLE_RADIO
};

int i = 0;

void gounki::menu_state::init()
{
    glEnable(GL_BLEND);

    puSetWindowFuncs(pu_glfw_get_window, nullptr, pu_glfw_get_window_size, nullptr);
    puRealInit();

    fntInit();

    int width = _context.get_window_width();
    int height = _context.get_window_height();

    unsigned int horizontal_margin = (width - 500) > 0 ? width - 500 : 0;
    menu_background.margin_left = static_cast<unsigned int>(0.5 * horizontal_margin);
    menu_background.margin_right = width - menu_background.margin_left;
    unsigned int vertical_margin = (height - 600) > 0 ? height - 600 : 0;
    menu_background.margin_bottom = static_cast<unsigned int>(0.5 * vertical_margin);
    menu_background.margin_top = height - menu_background.margin_bottom;

    _times_roman_fnt.reset(new fntTexFont("resources\\fonts\\Helvetica.txf"));

    _text_out.setFont(_times_roman_fnt.get());
    _text_out.setPointSize(24);

    //puSetDefaultFonts(puFont(_times_roman_fnt.get(), 13), puFont(_times_roman_fnt.get(), 13));

    _pu_group.reset(new puGroup(0, 0));

    b.reset(new puOneShot(menu_background.margin_left + 175, menu_background.margin_top - 300, menu_background.margin_right - 175, menu_background.margin_top - 270));
    theater_mode_button.reset(new puOneShot(menu_background.margin_left + 175, menu_background.margin_top - 380, menu_background.margin_right - 175, menu_background.margin_top - 350));
    settings_mode_button.reset(new puOneShot(menu_background.margin_left + 175, menu_background.margin_top - 460, menu_background.margin_right - 175, menu_background.margin_top - 430));
    start_button.reset(new puOneShot(menu_background.margin_left + 175, menu_background.margin_top - 300, menu_background.margin_right - 175, menu_background.margin_top - 270));
    back_button.reset(new puOneShot(menu_background.margin_left + 175, menu_background.margin_top - 380, menu_background.margin_right - 175, menu_background.margin_top - 350));

    // b button configuration
    b->setLegend("New Game");
    b->setCallback(::button_cb);
    b->setStyle(PUSTYLE_SHADED);
    b->setUserData(this);
    b->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);

    // theater_mode_button configuration
    theater_mode_button->setLegend("Theater Mode");
    theater_mode_button->setCallback(::theater_mode_button_cb);
    theater_mode_button->setStyle(PUSTYLE_SHADED);
    theater_mode_button->setUserData(this);
    theater_mode_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);

    // settings_mode_button configuration
    settings_mode_button->setLegend("Settings");
    settings_mode_button->setCallback(settings_mode_button_cb);
    settings_mode_button->setStyle(PUSTYLE_SHADED);
    settings_mode_button->setUserData(this);
    settings_mode_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);


    // start_button configuration
    start_button->setLegend("Start!");
    start_button->setCallback(start_button_cb);
    start_button->setStyle(PUSTYLE_SHADED);
    start_button->setUserData(this);
    start_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);
    start_button->hide();

    // back_button configuration
    back_button->setLegend("Back");
    back_button->setCallback(::back_button_cb);
    back_button->setStyle(PUSTYLE_SHADED);
    back_button->setUserData(this);
    back_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);
    back_button->hide();

    char* modes[] = { "Player", "Computer", 0 };
    char* difficulties[] = { "Easy", "Medium Easy", "Medium Hard", "Hard", 0 };
    
    player_1_mode_cb.reset(new puaComboBox(menu_background.margin_left + 80, menu_background.margin_top - 180, menu_background.margin_left + 180,
        menu_background.margin_top - 150, modes, false));

    player_2_mode_cb.reset(new puaComboBox(menu_background.margin_right - 80 - 100, menu_background.margin_top - 180, menu_background.margin_right - 80,
        menu_background.margin_top - 150, modes, false));

    player_1_mode_cb->hide();
    player_2_mode_cb->hide();

    player_1_mode_cb->setColourScheme(184 / 255.0f, 134 / 255.0f, 11 / 255.0f);
    player_1_mode_cb->setStyle(PUSTYLE_SHADED);

    player_1_mode_cb->setUserData(this);
    player_2_mode_cb->setUserData(this);

    player_1_mode_cb->setCallback(::cb_callback);
    player_2_mode_cb->setCallback(::cb_callback);

    player_1_diff_cb.reset(new puaComboBox(menu_background.margin_left + 80, menu_background.margin_top - 220, menu_background.margin_left + 180,
        menu_background.margin_top - 190, difficulties, false));

    player_2_diff_cb.reset(new puaComboBox(menu_background.margin_right - 80 - 100, menu_background.margin_top - 220, menu_background.margin_right - 80,
        menu_background.margin_top - 190, difficulties, false));

    player_1_diff_cb->hide();
    player_2_diff_cb->hide();

    _player_1_text.reset(new puText(menu_background.margin_left + 80, menu_background.margin_top - 145));
    _player_1_text->setLabel("Player 1:");
    _player_1_text->setColor(PUCOL_LABEL, 1, 1, 1);
    _player_1_text->hide();

    _player_2_text.reset(new puText(menu_background.margin_right - 80 - 100, menu_background.margin_top - 145));
    _player_2_text->setLabel("Player 2:");
    _player_2_text->setColor(PUCOL_LABEL, 1, 1, 1);
    _player_2_text->hide();

    _pu_group->close();

    _menu_background_tex = gl::Texture::Load("resources/ui/main_menu.png");
    _menu_background_mat = gl::make_material(_menu_background_tex, GL_REPEAT, GL_REPEAT);
}

void gounki::menu_state::update(double time_elapsed)
{
}

void gounki::menu_state::display(bool select_mode)
{
    static gl::primitives::Rectangle rect = gl::primitives::Rectangle(glm::vec2(10, 15), glm::vec2(300, 150));

    glClearColor(0,0,0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glAlphaFunc(GL_GREATER, 0.1f);

    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();
    glOrtho(0, _context.get_window_width(), 0, _context.get_window_height(), -1, 1);

    glDisable(GL_DEPTH_TEST);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();

    glPushMatrix();
    glTranslatef(0.375, 0.375, 0);

    glColor3f(1.0, 1.0, 1.0);
    glPushMatrix();
    _menu_background_mat->Apply();
    glBegin(GL_QUADS);
    glTexCoord2i(0, 0);
    glVertex2i(menu_background.margin_left, menu_background.margin_bottom);
    glTexCoord2i(1, 0);
    glVertex2i(menu_background.margin_right, menu_background.margin_bottom);
    glTexCoord2i(1, 1);
    glVertex2i(menu_background.margin_right, menu_background.margin_top);
    glTexCoord2i(0, 1);
    glVertex2i(menu_background.margin_left, menu_background.margin_top);
    glEnd();
    glPopMatrix();

    puDisplay();
}

void gounki::menu_state::reshape(int width, int height)
{
    glViewport(0, 0, width, height);

    unsigned int horizontal_margin = (width - 500) > 0 ? width - 500 : 0;
    menu_background.margin_left = static_cast<unsigned int>(0.5 * horizontal_margin);
    menu_background.margin_right = width - menu_background.margin_left;
    unsigned int vertical_margin = (height - 600) > 0 ? height - 600 : 0;
    menu_background.margin_bottom = static_cast<unsigned int>(0.5 * vertical_margin);
    menu_background.margin_top = height - menu_background.margin_bottom;

    player_1_mode_cb->setPosition(menu_background.margin_left + 80, menu_background.margin_top - 180);
    player_2_mode_cb->setPosition(menu_background.margin_right - 80 - 100, menu_background.margin_top - 180);

    player_1_diff_cb->setPosition(menu_background.margin_left + 80, menu_background.margin_top - 220);
    player_2_diff_cb->setPosition(menu_background.margin_right - 80 - 100, menu_background.margin_top - 220);

    b->setPosition(menu_background.margin_left + 175, menu_background.margin_top - 300);
    theater_mode_button->setPosition(menu_background.margin_left + 175, menu_background.margin_top - 380);
    settings_mode_button->setPosition(menu_background.margin_left + 175, menu_background.margin_top - 460);
    start_button->setPosition(menu_background.margin_left + 175, menu_background.margin_top - 300);
    back_button->setPosition(menu_background.margin_left + 175, menu_background.margin_top - 380);

    _player_1_text->setPosition(menu_background.margin_left + 80, menu_background.margin_top - 145);
    _player_2_text->setPosition(menu_background.margin_right - 80 - 100, menu_background.margin_top - 145);

    if (theater_mode_file_selector.get() != nullptr)
        theater_mode_file_selector->setPosition(menu_background.margin_left, menu_background.margin_bottom + 200);
}

void gounki::menu_state::mouse_button(int button, int action, int mods)
{
    _interface.mouse_button(button, action, mods);

     int btn;
     switch (button)
     {
     case GLFW_MOUSE_BUTTON_LEFT: btn = PU_LEFT_BUTTON; break;
     case GLFW_MOUSE_BUTTON_MIDDLE: btn = PU_MIDDLE_BUTTON; break;
     case GLFW_MOUSE_BUTTON_RIGHT: btn = PU_RIGHT_BUTTON; break;
     }

     auto mouse_pos = _interface.get_mouse_position();

     puMouse(btn, action == GLFW_PRESS ? PU_DOWN : PU_UP, static_cast<int>(mouse_pos.x), static_cast<int>(mouse_pos.y));
}

void gounki::menu_state::mouse_move(double x, double y)
{
    _interface.mouse_move(x, y);
    puMouse(static_cast<int>(x), static_cast<int>(y));
}

void gounki::menu_state::key_action(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_S && action == GLFW_PRESS) {
        if (_pu_group->isVisible()) _pu_group->hide(); else _pu_group->reveal();

        i = (i + 1) % 10;
    }
}

gounki::mode modes_val[] = { gounki::mode::Player, gounki::mode::Pc };
gounki::difficulty difficulties_val[] = { gounki::difficulty::Easy, gounki::difficulty::Medium_Easy, gounki::difficulty::Medium_Hard, gounki::difficulty::Hard };

void gounki::menu_state::on_pause(game_state* gs)
{
    if (gs->get_type() == typeid(main_state))
    {
        main_state* st = static_cast<main_state*>(gs);

        st->set_player_1_difficulty(difficulties_val[player_1_diff_cb->getCurrentItem()]);
        st->set_player_2_difficulty(difficulties_val[player_2_diff_cb->getCurrentItem()]);
        st->set_player_1_mode(modes_val[player_1_mode_cb->getCurrentItem()]);
        st->set_player_2_mode(modes_val[player_2_mode_cb->getCurrentItem()]);
    }

    _pu_group->hide();
}

void gounki::menu_state::cb_callback(puObject*)
{
    if (modes_val[player_1_mode_cb->getCurrentItem()] == mode::Player)
        player_1_diff_cb->hide();
    else
        player_1_diff_cb->reveal();

    if (modes_val[player_2_mode_cb->getCurrentItem()] == mode::Player)
        player_2_diff_cb->hide();
    else
        player_2_diff_cb->reveal();
}

void gounki::menu_state::on_resume(game_state*)
{
    _pu_group->reveal();
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
}

void gounki::menu_state::resize(float width_factor, float height_factor)
{

}

void gounki::menu_state::theater_mode_cb(puObject*)
{
    theater_mode_file_selector.reset(new puaFileSelector(menu_background.margin_left, menu_background.margin_bottom + 200, 500, 300,"saves", "Pick a save file"));
    theater_mode_file_selector->setCallback(::theater_mode_file_selector_cb);
    theater_mode_file_selector->setUserData(this);
}

void gounki::menu_state::file_selector_cb(puObject* obj)
{
    std::string filename_str(obj->getStringValue());
    if (!filename_str.empty())
    {
        _context.get_game()->set_save_filename(filename_str);
        _context.change_to_state<gounki::theater_state>();
    }

    theater_mode_file_selector.reset(nullptr);
}
