#include "gounki/game.hpp"
#include "gounki/main_state.hpp"
#include "gounki/theater_state.hpp"
#include <time.hpp>

gounki::game::game(const std::string& window_title, const glm::uvec2& window_size) : _window_size(window_size), _window_title(window_title), _state_manager(*this), _reshape_requested(false), _animation_time(1), _round_time(30)
{
}

gounki::game::~game()
{
}

void gounki::game::init()
{
    _window.init(_window_size.x, _window_size.y, _window_title, framework::window_hints().set_aux_buffers(1));
    _window.activate();
    _state_manager.init();
    _window.set_fb_size_changed_callback([this](int w, int h) { _state_manager.reshape(w, h); });
}

void gounki::game::run()
{
    _window.activate();

    request_reshape();

    while (!_window.should_close())
    {
        _window.activate();

        _window.poll_events();

        _state_manager.update();

        if (_reshape_requested) reshape();

        _state_manager.display();

        _window.swap_buffers();
    }
}

void gounki::game::terminate()
{
    _window.set_should_close(true);
}

double gounki::game::get_time() const
{
    return framework::get_time();
}

void gounki::game::set_input_listener(framework::input_listener* in_listener)
{
    _window.set_input_listener(in_listener);
}

void gounki::game::reshape()
{
    glm::uvec2 fbSize = _window.get_framebuffer_size();
    _state_manager.reshape(fbSize.x, fbSize.y);
    _reshape_requested = false;
}

void gounki::game::set_yaf_filename(const std::string& name)
{
    _yaf_filename = name;

    if (_state_manager.has_state<theater_state>())
        _state_manager.add_state<theater_state>();
}
