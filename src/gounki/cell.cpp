#include "gounki/cell.hpp"
#include <boost/algorithm/string.hpp>
#include <regex>

const auto cellRegex = std::regex(R"regex(cell\((black|white),\s*\[([^\]]*)\]\))regex");
const auto cellSplitter = std::regex(R"regex((cell\([^\)]*\)))regex");

gounki::cell::cell(const gounki::colour& colour) : _colour(colour)
{
    if (_colour != gounki::colour::Blank) throw std::invalid_argument("Cell with no elements must be blank.");
}

gounki::cell::cell(const gounki::colour& colour, const elements& elems) : _colour(colour), _elements(elems)
{

}

gounki::colour gounki::colour_from_string(std::string str)
{
    boost::trim(str);

    if (str == "black")
        return colour::Black;
    else if (str == "white")
        return colour::White;
    else if (str == "blank")
        return colour::Blank;

    throw std::invalid_argument("");
}

gounki::piece gounki::piece_from_string(std::string str)
{
    boost::trim(str);

    if (str == "square")
        return piece::Square;
    else if (str == "circle")
        return piece::Circle;

    throw std::invalid_argument("");
}

std::ostream& gounki::operator<<(std::ostream& out, const colour& col)
{
    switch (col)
    {
    case colour::Black: out << "black"; break;
    case colour::White: out << "white"; break;
    case colour::Blank: out << "blank"; break;
    }

    return out;
}

std::ostream& gounki::operator<<(std::ostream& out, const piece& col)
{
    switch (col)
    {
    case piece::Square: out << "square"; break;
    case piece::Circle: out << "circle"; break;
    }

    return out;
}

gounki::cell gounki::parse_cell(const std::string& cell)
{
    if (cell == "cell(blank)") return gounki::cell(colour::Blank);

    auto itr = std::sregex_token_iterator(cell.begin(), cell.end(), cellRegex, { 1, 2 });

    auto colour = colour_from_string(itr->str());
    ++itr;

    std::vector<std::string> elemsStr;
    boost::split(elemsStr, itr->str(), [](char c) { return c == ','; });

    // std::sort(elemsStr.begin(), elemsStr.end(), std::greater<std::string>());

    gounki::cell::elements elems;
    std::transform(elemsStr.begin(), elemsStr.end(), std::back_inserter(elems), piece_from_string);

    return gounki::cell(colour, elems);
}
