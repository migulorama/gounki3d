#include "gounki/linear_animation.hpp"

gounki::linear_animation::linear_animation(glm::vec3& source, std::vector<glm::vec3> controlPoints, double time_in_ms) : _source(source)
{
    std::swap(controlPoints, _controlPoints);
    _time_in_ms = time_in_ms;

    _sourceCache = source;

    _paths.resize(_controlPoints.size() - 1);

    _speed_per_ms = 0.0f;
    for (size_t i = 0; i < _paths.size(); ++i)
    {
        glm::vec3 pathVector = _controlPoints[i + 1] - _controlPoints[i];
        _paths[i].positionStart = _controlPoints[i];
        _paths[i].direction = glm::normalize(pathVector);

        _paths[i].distance = glm::length(pathVector);
        _speed_per_ms += _paths[i].distance;
    }
    _speed_per_ms /= _time_in_ms;

    _paths.front().timeStart = 0.0f;
    for (auto itr = _paths.begin() + 1; itr != _paths.end(); ++itr)
    {
        itr->timeStart = static_cast<float>((itr - 1)->timeStart + (itr - 1)->distance / _speed_per_ms);
    }

    _doReset = true;
}

size_t gounki::linear_animation::get_path(double animTime) const
{
    for (size_t i = 0; i < _paths.size(); ++i)
    if (animTime < _paths[i].timeStart)
        return i - 1;

    return _paths.size() - 1;
}

void gounki::linear_animation::Update(double time)
{
    if (_isFinished)
        return;

    if (_doReset)
    {
        _anim_time_start = time;
        _doReset = false;
    }

    double animTime = time - _anim_time_start;
    if (animTime > _time_in_ms)
    {
        _anim_time_start = time;
        animTime -= _time_in_ms;
        _isFinished = true;
        _source = _sourceCache;
        return;
    }

    size_t currentPathID = get_path(animTime);
    Path& currentPath = _paths[currentPathID % _paths.size()];


    glm::vec3 dir = currentPath.direction * static_cast<float>(_speed_per_ms * (animTime - currentPath.timeStart));
    _source = currentPath.positionStart + dir;
}