#include "gounki/board_visualizer.hpp"
#include "gounki/player.hpp"

gounki::board_visualizer::board_visualizer()
{

}

gounki::board_visualizer::board_visualizer(const gounki::board& logicBoard, const std::vector<YAF::Loader::CapturedPieces>& cpp1, const std::vector<YAF::Loader::CapturedPieces>& cpp2, const glm::mat4& board_rotation)
{
    init(logicBoard, cpp1, cpp2, board_rotation);
}

void gounki::board_visualizer::init(const gounki::board& logicBoard, std::vector<YAF::Loader::CapturedPieces> cpp1, std::vector<YAF::Loader::CapturedPieces> cpp2, const glm::mat4& board_rotation)
{
    _numRows = logicBoard.get_num_rows();
    _numCols = logicBoard.get_num_cols();

    float row_size = 1.0f / (_numRows - 2);
    float column_size = 1.0f / _numCols;

    _cell_size.y = column_size;
    _cell_size.x = row_size;

    for (unsigned int row = 0; row < _numRows; ++row)
    {
        for (unsigned int column = 0; column < _numCols; ++column)
        {
            std::array<glm::vec3, 3> cells =
            { glm::vec3(column_size * column, 0.0f, row_size * (row - 1.f)), glm::vec3(column_size * column, 0.3f * _cell_size.x, row_size * (row - 1.f)), glm::vec3(column_size * column, 2.f * 0.3f * _cell_size.x, row_size * (row - 1.f)) };

            _initialConfiguration.push_back(cells);
            _currentConfiguration.push_back(cells);
        }
    }

    std::sort(cpp1.begin(), cpp1.end());
    cpp1.erase(std::unique(cpp1.begin(), cpp1.end()), cpp1.end());
    std::sort(cpp2.begin(), cpp2.end());
    cpp2.erase(std::unique(cpp2.begin(), cpp2.end()), cpp2.end());

    unsigned int numPieces = _numCols * 2;
    if (cpp1.front().number > 0) cpp1.front().number = 0;
    auto cpItr = cpp1.begin();
    for (unsigned int i = 0; i < numPieces; ++i) // player 1
    {
        while ((cpItr + 1) != cpp1.end() && i >= (cpItr + 1)->number) ++cpItr;
        _capturedPieces[player::Player1].push_back(cpItr->position + static_cast<float>(i - cpItr->number) * cpItr->difference);
    }

    if (cpp2.front().number > 0) cpp2.front().number = 0;
    cpItr = cpp2.begin();
    for (unsigned int i = 0; i < numPieces; ++i) // player 2
    {
        while ((cpItr + 1) != cpp2.end() && i >= (cpItr + 1)->number) ++cpItr;
        _capturedPieces[player::Player2].push_back(cpItr->position + static_cast<float>(i - cpItr->number) * cpItr->difference);
    }

    for (auto& index : _currentIndex) 
        index = 0;

    _board_transforms = glm::translate(_position);
    _board_transforms *= board_rotation;
    _board_transforms = glm::scale(_board_transforms, _size.x, _size.x, _size.y);

    _inv_board_transforms = glm::inverse(_board_transforms);
}

void gounki::board_visualizer::reset()
{
    for (auto itr = _currentIndex.begin(); itr < _currentIndex.end(); ++itr)
        *itr = 0;
}

std::array<glm::vec3, 3>& gounki::board_visualizer::get_cell_position(unsigned int row, unsigned int column)
{
    assert((row * _numCols + column) < _currentConfiguration.size());
    return _currentConfiguration[row * _numCols + column];
}

const std::array<glm::vec3, 3>& gounki::board_visualizer::get_default_cell_position(unsigned int row, unsigned int column) const
{
    assert((row * _numCols + column) < _initialConfiguration.size());
    return _initialConfiguration[row * _numCols + column];
}

glm::vec3 gounki::board_visualizer::get_captured_cells_position(gounki::player::player player) const
{
    if (player == gounki::player::Player1)
    {
        glm::vec3 location(_initialConfiguration[_initialConfiguration.size() - _numCols].front());
        location.z += 3.0f;
        return location;
    }
    else
    {
        glm::vec3 location(_initialConfiguration[_numCols - 1].front());
        location.z -= 2.0f;
        return location;
    }
}

glm::vec3 gounki::board_visualizer::get_captured_cells_position(gounki::player::player player, unsigned int idx)
{
    return _capturedPieces[player][idx];
}

glm::vec3 gounki::board_visualizer::get_next_captured_cells_position(player::player player)
{
    return _capturedPieces[player][_currentIndex[player]++];
}

void gounki::board_visualizer::set_current_captured_index(unsigned int player_1, unsigned int player_2)
{
    _currentIndex[player::Player1] = player_1;
    _currentIndex[player::Player2] = player_2;
}


