#include "gounki/theater_state.hpp"
#include "gounki/main_state.hpp"
#include "gounki/menu_state.hpp"

#include <glm/glm.hpp>

void gounki::theater_state::set_player(gounki::player::player p)
{
    if (_current_player == p) return;

    _current_player = p;
    _scene.get_animation_manager().addCameraAnimation(_scene.get_camera(), _scene.get_player_position(_current_player), 1000);
}

void gounki::theater_state::set_file_name(const std::string& file)
{
    _file_name = file;
}

void gounki::theater_state::redo()
{
    if (_player_transition_pending)
        return;

    if (_history.redo()) 
    {
        gounki::player::player player;
        boost::optional<gounki::movement> move;
        std::tie(std::ignore, player, move, std::ignore) = _history.get_last();
        _scene.get_board_visualizer().set_current_captured_index(static_cast<unsigned int>(_captured_pieces[player::Player1].size()), static_cast<unsigned int>(_captured_pieces[player::Player2].size()));

        if (move)
            _scene.get_animation_manager().addMovementAnimation(*move, _current_board, 1000);

        _player_transition_pending = true;
        _has_cached_info = true;
    }
    else if (_auto_play_enabled)
    {
        set_auto_play(false);
    }
}

void gounki::theater_state::undo()
{
    if (_player_transition_pending)
        return;

    _history.undo();
    gounki::player::player player;
    std::tie(_current_board, player, std::ignore, _captured_pieces) = _history.get_last();
    _scene.get_board_visualizer().set_current_captured_index(static_cast<unsigned int>(_captured_pieces[player::Player1].size()), static_cast<unsigned int>(_captured_pieces[player::Player2].size()));
    _player_transition_pending = true;
}

void gounki::theater_state::key_action(int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
    {
        if (key == GLFW_KEY_LEFT)
            undo();
        else if (key == GLFW_KEY_RIGHT)
            redo();
        else if (key == GLFW_KEY_ESCAPE)
            change_to_state<menu_state>();
    }
}

void gounki::theater_state::reshape(int width, int height)
{
    _scene.reshape(width, height);
}

void gounki::theater_state::display(bool select_mode)
{
    _scene.draw();
}

void gounki::theater_state::update(double time_elapsed)
{
    _scene.update(time_elapsed);


    if (_player_transition_pending)
    {
        if (!_scene.get_animation_manager().hasAnimations())
        {
            if (_has_cached_info)
            {
                _has_cached_info = false;
                std::tie(_current_board, std::ignore, std::ignore, _captured_pieces) = _history.get_last();
                _scene.get_animation_manager().addDummyAnimation(1000);
            }
            else
            {
                gounki::player::player player;
                std::tie(std::ignore, player, std::ignore, std::ignore) = _history.get_last();
                set_player(player);
                _player_transition_pending = false;
            }
        }

        return;
    }

    if (!_auto_play_enabled)
        return;

    if (!_scene.get_animation_manager().hasAnimations())
        redo();
}

void gounki::theater_state::init()
{
    _logic = std::make_unique<gounki::logic>("127.0.0.1", "60012");
    _current_board = _logic->initialize();
    _scene.init();
}

gounki::theater_state::theater_state(state_manager& context) :
game_state(context),
_interface(this),
_logic(nullptr),
_scene(*this),
_has_cached_info(false),
_player_transition_pending(false)
{

}

const gounki::board& gounki::theater_state::get_current_board() const
{
    return _current_board;
}

gounki::colour gounki::theater_state::get_player_colour(gounki::player::player player) const
{
    return gounki::main_state::_player_color[player];
}

const std::vector<gounki::piece>& gounki::theater_state::get_captured_pieces(gounki::player::player player) const
{
    return _captured_pieces[player];
}

void gounki::theater_state::on_resume(game_state*)
{
    std::string str = _context.get_game()->get_save_filename();
    std::ifstream file_in(str);
    if (!file_in.is_open()) {
        change_to_state<gounki::menu_state>();
        return;
    }

    std::string line;
    std::getline(file_in, line);

    gounki::movement first_move(line);
    gounki::colour first_colour = _current_board.cell_at(first_move.get_positions().front() - position(0, 1)).get_colour();
    gounki::player::player first_player = (first_colour == gounki::colour::White ? gounki::player::Player1 : gounki::player::Player2);
    _history.commit(_current_board, first_player);

    file_in.seekg(std::ios::beg);
    while (std::getline(file_in, line))
    {
        gounki::movement move(line);
        gounki::colour colour = _current_board.cell_at(move.get_positions().front() - position(0, 1)).get_colour();
        gounki::player::player player = (colour == gounki::colour::White ? gounki::player::Player1 : gounki::player::Player2);
        std::vector<gounki::piece> captured_pieces;

        std::tie(_current_board, captured_pieces) = _logic->exec_move(_current_board, move);
        if (!captured_pieces.empty()) _captured_pieces[player].insert(_captured_pieces[player].end(), captured_pieces.begin(), captured_pieces.end());
        _history.commit(_current_board, player == player::Player1 ? player::Player2 : player::Player1, move, _captured_pieces);
    }

    _history.undo_all();
    gounki::player::player player;
    std::tie(_current_board, player, std::ignore, _captured_pieces) = _history.get_last();
    set_player(player);

    _scene.on_resume();
    _auto_play_enabled = false;
    _player_transition_pending = false;
}

void gounki::theater_state::mouse_button(int button, int action, int mods)
{
    _interface.mouse_button(button, action, mods);

    int btn;
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT: btn = PU_LEFT_BUTTON; break;
    case GLFW_MOUSE_BUTTON_MIDDLE: btn = PU_MIDDLE_BUTTON; break;
    case GLFW_MOUSE_BUTTON_RIGHT: btn = PU_RIGHT_BUTTON; break;
    }

    auto mouse_pos = _interface.get_mouse_position();

    puMouse(btn, action == GLFW_PRESS ? PU_DOWN : PU_UP, static_cast<int>(mouse_pos.x), static_cast<int>(mouse_pos.y));
}

void gounki::theater_state::mouse_move(double x, double y)
{
    _interface.mouse_move(x, y);

    auto displacement = _interface.get_mouse_displacement();

    int modifiers = _interface.get_modifiers();

    auto prev_position = _scene.get_camera()->GetPosition();

    if (_interface.is_right_button_pressed() && modifiers == 0)
    {
        _scene.get_camera()->RotatePosition(-displacement.y * 0.5f, _scene.get_camera()->GetRightVector());
        _scene.get_camera()->RotatePosition(-displacement.x * 0.5f, glm::vec3(0.0f, 1.0f, 0.0f));
    }
    if (_interface.is_middle_button_pressed() || (_interface.is_right_button_pressed() && modifiers & GLFW_MOD_CONTROL))
    {
        _scene.get_camera()->Translate(glm::vec3(0.0f, 0.0f, displacement.y * 0.5f));
    }

    auto angle = (1 - 2 * (_current_player == player::Player1)) * glm::orientedAngle(glm::normalize(_scene.get_camera()->GetPosition() - _scene.get_camera()->GetTarget()), _scene.get_board_normal_vector(), _scene.get_board_right_vector());
    if (glm::dot(glm::normalize(_scene.get_camera()->GetPosition() - _scene.get_camera()->GetTarget()), _scene.get_board_normal_vector()) < 0)
    {
        _scene.get_camera()->SetPosition(prev_position);
    }
    else if (angle < 0)
    {
        _scene.get_camera()->SetPosition(prev_position);
    }
    else if (glm::distance(_scene.get_camera()->GetTarget(), _scene.get_camera()->GetPosition()) < 1.0f)
    {
        _scene.get_camera()->SetPosition(_scene.get_camera()->GetTarget() - _scene.get_camera()->GetDirection());
    }
}

void gounki::theater_state::set_auto_play(bool value)
{
    _auto_play_enabled = value;
    if (_auto_play_enabled)
        _scene.disableUndoRedo();
    else
        _scene.enableUndoRedo();
}

void gounki::theater_state::on_pause(gounki::game_state*)
{
    _scene.on_pause();
}
