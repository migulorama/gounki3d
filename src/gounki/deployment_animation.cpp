#include "gounki/deployment_animation.hpp"


gounki::deployment_animation::deployment_animation(std::array<glm::vec3, 3>& source, std::vector<glm::vec3> controlPoints, const glm::vec2& cell_size, double timeInMilliseconds)
{
    if (controlPoints.empty())
        throw std::runtime_error("No control points were given for the deployment animation");

    glm::vec3 sourcePos = controlPoints[0];
    float val = (cell_size.x/2.0f) * (controlPoints.size() == 3);
    double halfTime = timeInMilliseconds / 2.0f;
    for (unsigned int i = 1; i < controlPoints.size() && i < 4; ++i)
    {
        std::vector<glm::vec3> intermediateControlPoints;
        intermediateControlPoints.push_back(source[i - 1]);
        glm::vec3 diff((i - cell_size.x) * cell_size.x + val, (cell_size.y * 4) - (i - 1) * cell_size.y, 0.0f);
        intermediateControlPoints.push_back(source[i - 1] + diff);
        _linearAnimations.emplace_back(source[i - 1], intermediateControlPoints, halfTime);
        std::vector<glm::vec3> intermediateControlPoints_2;
        intermediateControlPoints_2.push_back(source[i - 1] + diff);
        intermediateControlPoints_2.push_back(controlPoints[i]);
        _linearAnimations_2.emplace_back(source[i - 1], intermediateControlPoints_2, halfTime);
    }

    _finished_all_animations = true;
}

void gounki::deployment_animation::Update(double time)
{

    _finished_all_animations = true;
    for (auto& linearAnimation : _linearAnimations)
    {
        linearAnimation.Update(time);
        _finished_all_animations = _finished_all_animations && linearAnimation.isFinished();
    }

    if (_finished_all_animations)
    {
        for (auto& linearAnimation : _linearAnimations_2)
        {
            linearAnimation.Update(time);
            _finished_all_animations = _finished_all_animations && linearAnimation.isFinished();
        }
    }

    _isFinished = _isFinished || _finished_all_animations;
}
