#include "gounki/moves.hpp"

std::regex gounki::movement::check_regex(R"regex(^\[(?:\[[^\]]*\],)?\s*[^\]]*\]$)regex");
std::regex gounki::movement::position_splitter(R"regex(position\((\d),(\d)\))regex");
std::regex gounki::movement::piece_splitter(R"regex((circle|square))regex");

gounki::movement::movement(const std::string& str) : _prolog_string(str)
{
    if (std::regex_match(str, check_regex))
    {
        auto end = std::sregex_token_iterator();

        for (auto itr = std::sregex_token_iterator(str.begin(), str.end(), position_splitter, { 1, 2 }); itr != end; ++itr)
        {
            auto x = boost::lexical_cast<unsigned int>(itr->str()); ++itr;
            auto y = boost::lexical_cast<unsigned int>(itr->str());
            _positions.emplace_back(x, y);
        }

        for (auto itr = std::sregex_token_iterator(str.begin(), str.end(), piece_splitter); itr != end; ++itr)
        {
            _pieces.push_back(piece_from_string(itr->str()));
        }
    }
    else
        throw std::runtime_error("Invalid movement string.");
}

std::string gounki::movement::get_prolog_string() const
{
    return _prolog_string;
}