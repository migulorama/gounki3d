#include "gounki/state_manager.hpp"
#include "gounki/game.hpp"
#include "gounki/game_state.hpp"
#include <string>
#include "application.hpp"

gounki::state_manager::state_manager(gounki::game& game) : _currentState(nullptr), _initialized(false), _game(game)
{
}

void gounki::state_manager::init()
{        
    for (auto& state : _states)
        state.second->init();

    if (_currentState) _currentState->on_resume(nullptr);

    _initialized = true;
}

void gounki::state_manager::add_state(game_state* state)
{
    auto state_id = state->get_type();
    _states[state_id] = state;
}

gounki::game_state* gounki::state_manager::get_current_state()
{
    return _currentState;
}

void gounki::state_manager::update()
{
    if (_currentState) _currentState->update(_game.get_time());
}

void gounki::state_manager::display()
{
    if (_currentState) _currentState->display(false);
}

void gounki::state_manager::reshape(int width, int height)
{
	if (_currentState) _currentState->reshape(width, height);
}

void gounki::state_manager::terminate()
{
    _game.terminate();
}

unsigned int gounki::state_manager::get_window_width()
{
    return _game.get_window()->get_framebuffer_width();
}

unsigned int gounki::state_manager::get_window_height()
{
    return _game.get_window()->get_framebuffer_height();
}

gounki::state_manager::~state_manager()
{
    for (auto& st : _states)
        delete st.second;

    _states.clear();
}
