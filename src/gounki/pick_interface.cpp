#include "gounki/pick_interface.hpp"

#include <algorithm>
#include <array>
#include <functional>

#include "application.hpp"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include <glm/gtc/matrix_transform.hpp>

#include "gl_4_4.h"
#include "gounki/game_state.hpp"
#include "gounki/main_state.hpp"
#include "interface.hpp"

std::vector<int> gounki::pick_interface::pick_objects()
{
    std::array<GLuint, 256> buffer;

    glm::mat4 projMat;
    glGetFloatv(GL_PROJECTION_MATRIX, glm::value_ptr(projMat));

    glm::mat4 modelMat;
    glGetFloatv(GL_MODELVIEW_MATRIX, glm::value_ptr(modelMat));

    glm::ivec4 viewport;
    glGetIntegerv(GL_VIEWPORT, glm::value_ptr(viewport));

    glSelectBuffer(static_cast<GLsizei>(buffer.size()), buffer.data());
    glRenderMode(GL_SELECT);
    glInitNames();

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();

    glLoadIdentity();

    glMultMatrixf(glm::value_ptr(glm::pickMatrix(
        glm::vec2(_mouse.position.x, _game_state->get_window()->get_framebuffer_size().y - _mouse.position.y),
        glm::vec2(5.0f, 5.0f),
        viewport)));

    glMultMatrixf(glm::value_ptr(projMat));

    _game_state->display(true);
    _game_state->get_window()->swap_buffers();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    glFlush();

    GLint hits;
    hits = glRenderMode(GL_RENDER);

    GLint *ptr = reinterpret_cast<GLint*>(buffer.data());
    GLuint mindepth = 0xFFFFFFFF;
    GLint *selected = NULL;
    GLuint nselected;

    // iterate over the list of hits, and choosing the one closer to the viewer (lower depth)
    for (int i = 0; i < hits; i++) {
        int num = *ptr; ptr++;
        GLuint z1 = *ptr; ptr++;
        ptr++;
        if (z1 < mindepth && num>0) {
            mindepth = z1;
            selected = ptr;
            nselected = num;
        }
        for (int j = 0; j < num; j++)
            ptr++;
    }
  
    return selected ? std::vector<int>(selected, selected + nselected) : std::vector<int>();
}

void gounki::pick_interface::mouse_button(int button, int action, int mods)
{
    modifiers = mods;

    if (button == GLFW_MOUSE_BUTTON_LEFT)_mouse.pressing_left = (action == GLFW_PRESS);
    if (button == GLFW_MOUSE_BUTTON_RIGHT) _mouse.pressing_right = (action == GLFW_PRESS);
    if (button == GLFW_MOUSE_BUTTON_MIDDLE) _mouse.pressing_middle = (action == GLFW_PRESS);    
}

void gounki::pick_interface::mouse_move(double x, double y)
{
    _mouse.prev_position.x = _mouse.position.x;
    _mouse.prev_position.y = _mouse.position.y;
    _mouse.position.x = static_cast<float>(x);
    _mouse.position.y = static_cast<float>(y);
}

gounki::pick_interface::pick_interface(gounki::game_state* game_state) : _game_state(game_state), modifiers(0)
{
    _mouse.pressing_left = _mouse.pressing_middle = _mouse.pressing_right = false;
}

void gounki::pick_interface::update(double timeElapsed)
{
}

bool gounki::pick_interface::is_left_button_pressed()
{
    return _mouse.pressing_left;
}
