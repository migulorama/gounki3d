#include "gounki/game_state.hpp"
#include "gounki/game.hpp"
#include "gounki/pick_interface.hpp"

gounki::game_state::game_state(state_manager& context) : _context(context)
{
}

std::type_index gounki::game_state::get_type() const
{
    return typeid(*this);
}

framework::window* gounki::game_state::get_window()
{
    return _context.get_game()->get_window();
}