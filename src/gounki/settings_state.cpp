#include "gounki/settings_state.hpp"

#include "gounki/game_state.hpp"
#include "gounki/main_state.hpp"
#include "gounki/menu_state.hpp"
#include "gl/texture.hpp"
#include "gl/material.hpp"
#include "gl/primitives/rectangle.hpp"

#include <algorithm>
#include <iostream>
#include <array>
#include <filesystem>

#include <gl_4_4.h>
#include "gounki/pui.hpp"

#include <boost/lexical_cast.hpp>

//const std::array<float, 3> dark_brown = { 101 / 255.0f, 67 / 255.0f, 33 / 255.0f };
gl::TexturePtr _settings_menu_background_tex;
gl::MaterialPtr _settings_menu_background_mat;

gounki::settings_state::settings_state(state_manager& manager) :
game_state(manager),
_interface(this),
b(nullptr, puDeleteObject),
_animation_time_input(nullptr, puDeleteObject),
settings_mode_button(nullptr, puDeleteObject),
start_button(nullptr, puDeleteObject),
settings_back_button(nullptr, puDeleteObject),
_game_enviroment_cb(nullptr, puDeleteObject),
_settings_group(nullptr, puDeleteObject),
_environment_text(nullptr, puDeleteObject),
_round_time_text(nullptr, puDeleteObject),
_animation_time_text(nullptr, puDeleteObject),
_round_time_input(nullptr, puDeleteObject),
settings_reset_button(nullptr, puDeleteObject),
settings_apply_button(nullptr, puDeleteObject)
{
}

gounki::settings_state::~settings_state()
{
}

void settings_back_button_cb(puObject* obj)
{
    gounki::settings_state* st = reinterpret_cast<gounki::settings_state*>(obj->getUserData());
    st->change_to_state<gounki::menu_state>();
}

void settings_apply_button_cb(puObject*obj)
{
    gounki::settings_state* st = reinterpret_cast<gounki::settings_state*>(obj->getUserData());
    st->apply_button_cb(obj);
}

void settings_reset_button_cb(puObject*obj)
{
    gounki::settings_state* st = reinterpret_cast<gounki::settings_state*>(obj->getUserData());
    st->reset_button_cb(obj);
}

void gounki::settings_state::init()
{
    glEnable(GL_BLEND);

    _settings_group.reset(new puGroup(0, 0));

    int width = _context.get_window_width();
    int height = _context.get_window_height();

    unsigned int horizontal_margin = (width - 500) > 0 ? width - 500 : 0;
    menu_background.margin_left = static_cast<unsigned int>(0.5 * horizontal_margin);
    menu_background.margin_right = width - menu_background.margin_left;
    unsigned int vertical_margin = (height - 600) > 0 ? height - 600 : 0;
    menu_background.margin_bottom = static_cast<unsigned int>(0.5 * vertical_margin);
    menu_background.margin_top = height - menu_background.margin_bottom;

    auto themes = get_all_themes();

    _themes.clear();
    for (const auto& theme : themes)
        _themes.push_back(theme.basename());

    std::vector<const char*> environments;
    for (auto& theme : _themes) environments.push_back(theme.c_str());
    environments.push_back(0);

    _game_enviroment_cb.reset(new puaComboBox(menu_background.margin_right - 210, menu_background.margin_top - 180, menu_background.margin_right - 50,
        menu_background.margin_top - 155, const_cast<char**>(environments.data()), false));    

    // _rount_time_input configuration
    int _upper_x, _upper_y;
    _game_enviroment_cb->getPosition(&_upper_x, &_upper_y);
    _round_time_input.reset(new puInput(menu_background.margin_right - 210, _upper_y - 35, menu_background.margin_right - 50, _upper_y - 10));
    _round_time_input->setStyle(PUSTYLE_PLAIN);
    _round_time_input->setColourScheme(1, 1, 1);
    _round_time_input->setValidData("0123456789");
    _round_time_input->setUserData(this);
    _round_time_input->setDefaultValue(std::to_string(_context.get_game()->get_round_time()).c_str());

    // _animation_time_input configuration
    _round_time_input->getPosition(&_upper_x, &_upper_y);
    _animation_time_input.reset(new puInput(menu_background.margin_right - 210, _upper_y - 35, menu_background.margin_right - 50, _upper_y - 10));
    _animation_time_input->setStyle(PUSTYLE_PLAIN);
    _animation_time_input->setColourScheme(1, 1, 1);
    _animation_time_input->setValidData("0123456789");
    _animation_time_input->setUserData(this);
    _animation_time_input->setDefaultValue(std::to_string(_context.get_game()->get_animation_time()).c_str());

    // back button configuration
    settings_back_button.reset(new puOneShot(menu_background.margin_left + 175, menu_background.margin_top - 460, menu_background.margin_right - 175, menu_background.margin_top - 430));
    settings_back_button->setLegend("Back");
    settings_back_button->setCallback(::settings_back_button_cb);
    settings_back_button->setStyle(PUSTYLE_SHADED);
    settings_back_button->setUserData(this);
    settings_back_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);

    // apply button configuration
    settings_apply_button.reset(new puOneShot(menu_background.margin_left + 125, menu_background.margin_top - 410, menu_background.margin_left + 225, menu_background.margin_top - 380));
    settings_apply_button->setLegend("Apply");
    settings_apply_button->setCallback(::settings_apply_button_cb);
    settings_apply_button->setStyle(PUSTYLE_SHADED);
    settings_apply_button->setUserData(this);
    settings_apply_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);

    // reset button configuration
    settings_reset_button.reset(new puOneShot(menu_background.margin_right - 225, menu_background.margin_top - 410, menu_background.margin_right - 125, menu_background.margin_top - 380));
    settings_reset_button->setLegend("Reset");
    settings_reset_button->setCallback(::settings_reset_button_cb);
    settings_reset_button->setStyle(PUSTYLE_SHADED);
    settings_reset_button->setUserData(this);
    settings_reset_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);

    // environment text configuration
    _environment_text.reset(new puText(menu_background.margin_left + 50, menu_background.margin_top - 180));
    _environment_text->setLabel("Choose Environment:");
    _environment_text->setColor(PUCOL_LABEL, 1, 1, 1);

    // round time text configuration
    _round_time_input->getPosition(&_upper_x, &_upper_y);
    _round_time_text.reset(new puText(menu_background.margin_left + 50, _upper_y));
    _round_time_text->setLabel("Set Round Time Limit:");
    _round_time_text->setColor(PUCOL_LABEL, 1, 1, 1);

    // animation time text configuration
    _animation_time_input->getPosition(&_upper_x, &_upper_y);
    _animation_time_text.reset(new puText(menu_background.margin_left + 50, _upper_y));
    _animation_time_text->setLabel("Set Animation Time:");
    _animation_time_text->setColor(PUCOL_LABEL, 1, 1, 1);

    _settings_group->close();

    _settings_menu_background_tex = gl::Texture::Load("resources/ui/settings_menu.png");
    _settings_menu_background_mat = gl::make_material(_settings_menu_background_tex, GL_REPEAT, GL_REPEAT);

    get_all_themes();
}

void gounki::settings_state::update(double time_elapsed)
{
}

void gounki::settings_state::display(bool select_mode)
{
    glClearColor(0, 0, 0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glAlphaFunc(GL_GREATER, 0.1f);

    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();
    glOrtho(0, _context.get_window_width(), 0, _context.get_window_height(), -1, 1);

    glDisable(GL_DEPTH_TEST);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();

    glPushMatrix();
    glTranslatef(0.375, 0.375, 0);

    glColor3f(1.0, 1.0, 1.0);
    glPushMatrix();
    _settings_menu_background_mat->Apply();
    glBegin(GL_QUADS);
    glTexCoord2i(0, 0);
    glVertex2i(menu_background.margin_left, menu_background.margin_bottom);
    glTexCoord2i(1, 0);
    glVertex2i(menu_background.margin_right, menu_background.margin_bottom);
    glTexCoord2i(1, 1);
    glVertex2i(menu_background.margin_right, menu_background.margin_top);
    glTexCoord2i(0, 1);
    glVertex2i(menu_background.margin_left, menu_background.margin_top);
    glEnd();
    glPopMatrix();

    puDisplay();
}

void gounki::settings_state::reshape(int width, int height)
{
    glViewport(0, 0, width, height);

    unsigned int horizontal_margin = (width - 500) > 0 ? width - 500 : 0;
    menu_background.margin_left = static_cast<unsigned int>(0.5 * horizontal_margin);
    menu_background.margin_right = width - menu_background.margin_left;
    unsigned int vertical_margin = (height - 600) > 0 ? height - 600 : 0;
    menu_background.margin_bottom = static_cast<unsigned int>(0.5 * vertical_margin);
    menu_background.margin_top = height - menu_background.margin_bottom;

    _game_enviroment_cb->setPosition(menu_background.margin_right - 210, menu_background.margin_top - 180);

    int _upper_x, _upper_y;
    _game_enviroment_cb->getPosition(&_upper_x, &_upper_y);
    _round_time_input->setPosition(menu_background.margin_right - 210, _upper_y - 35);

    settings_back_button->setPosition(menu_background.margin_left + 175, menu_background.margin_top - 460);
    _environment_text->setPosition(menu_background.margin_left + 50, menu_background.margin_top - 180);

    _round_time_input->getPosition(&_upper_x, &_upper_y);
    _round_time_text->setPosition(menu_background.margin_left + 50, _upper_y);

    _round_time_input->getPosition(&_upper_x, &_upper_y);
    _animation_time_input->setPosition(menu_background.margin_right - 210, _upper_y - 35);

    _animation_time_input->getPosition(&_upper_x, &_upper_y);
    _animation_time_text->setPosition(menu_background.margin_left + 50, _upper_y);

    settings_apply_button->setPosition(menu_background.margin_left + 125, menu_background.margin_top - 410);
    settings_reset_button->setPosition(menu_background.margin_right - 225, menu_background.margin_top - 410);
}

void gounki::settings_state::mouse_button(int button, int action, int mods)
{
    _interface.mouse_button(button, action, mods);

    int btn;
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT: btn = PU_LEFT_BUTTON; break;
    case GLFW_MOUSE_BUTTON_MIDDLE: btn = PU_MIDDLE_BUTTON; break;
    case GLFW_MOUSE_BUTTON_RIGHT: btn = PU_RIGHT_BUTTON; break;
    }

    auto mouse_pos = _interface.get_mouse_position();

    puMouse(btn, action == GLFW_PRESS ? PU_DOWN : PU_UP, static_cast<int>(mouse_pos.x), static_cast<int>(mouse_pos.y));
}

void gounki::settings_state::mouse_move(double x, double y)
{
    _interface.mouse_move(x, y);
    puMouse(static_cast<int>(x), static_cast<int>(y));
}

void gounki::settings_state::key_action(int key, int scancode, int action, int mods)
{

#define my_case(x, y) case x: key = y; break

    switch (key)
    {
        my_case(GLFW_KEY_F1, PU_KEY_F1);
        my_case(GLFW_KEY_F2, PU_KEY_F2);
        my_case(GLFW_KEY_F3, PU_KEY_F3);
        my_case(GLFW_KEY_F4, PU_KEY_F4);
        my_case(GLFW_KEY_F5, PU_KEY_F5);
        my_case(GLFW_KEY_F6, PU_KEY_F6);
        my_case(GLFW_KEY_F7, PU_KEY_F7);
        my_case(GLFW_KEY_F8, PU_KEY_F8);
        my_case(GLFW_KEY_F9, PU_KEY_F9);
        my_case(GLFW_KEY_F10, PU_KEY_F10);
        my_case(GLFW_KEY_F11, PU_KEY_F11);
        my_case(GLFW_KEY_F12, PU_KEY_F12);
        my_case(GLFW_KEY_LEFT, PU_KEY_LEFT);
        my_case(GLFW_KEY_UP, PU_KEY_UP);
        my_case(GLFW_KEY_RIGHT, PU_KEY_RIGHT);
        my_case(GLFW_KEY_DOWN, PU_KEY_DOWN);
        my_case(GLFW_KEY_PAGE_UP, PU_KEY_PAGE_UP);
        my_case(GLFW_KEY_PAGE_DOWN, PU_KEY_PAGE_DOWN);
        my_case(GLFW_KEY_HOME, PU_KEY_HOME);
        my_case(GLFW_KEY_END, PU_KEY_END);
        my_case(GLFW_KEY_INSERT, PU_KEY_INSERT);
        my_case(GLFW_KEY_BACKSPACE, '\b');
        my_case(GLFW_KEY_DELETE, 127);
        my_case(GLFW_KEY_KP_0, '0');
        my_case(GLFW_KEY_KP_1, '1');
        my_case(GLFW_KEY_KP_2, '2');
        my_case(GLFW_KEY_KP_3, '3');
        my_case(GLFW_KEY_KP_4, '4');
        my_case(GLFW_KEY_KP_5, '5');
        my_case(GLFW_KEY_KP_6, '6');
        my_case(GLFW_KEY_KP_7, '7');
        my_case(GLFW_KEY_KP_8, '8');
        my_case(GLFW_KEY_KP_9, '9');
        my_case(GLFW_KEY_KP_DECIMAL, '.');
        my_case(GLFW_KEY_KP_DIVIDE, '/');
        my_case(GLFW_KEY_KP_MULTIPLY, '*');
        my_case(GLFW_KEY_KP_SUBTRACT, '-');
        my_case(GLFW_KEY_KP_ADD, '+');
        my_case(GLFW_KEY_KP_ENTER, '\n');
        my_case(GLFW_KEY_KP_EQUAL, '=');

    }

#undef my_case

    puKeyboard(key, action == GLFW_PRESS ? PU_DOWN : PU_UP);
}

void gounki::settings_state::on_pause(game_state* gs)
{
    _settings_group->hide();
}

void gounki::settings_state::cb_callback(puObject*)
{

}

void gounki::settings_state::on_resume(game_state*)
{
    settings_reset_button->invokeCallback();
    _settings_group->reveal();
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
}

void gounki::settings_state::resize(float width_factor, float height_factor)
{

}

std::vector<std::tr2::sys::path> gounki::settings_state::get_all_themes()
{
    using namespace std::tr2::sys;

    path themes_dir("themes");

    if (exists(themes_dir) && is_directory(themes_dir))
    {
        std::vector<directory_entry> themes;

        std::copy_if(directory_iterator(themes_dir), directory_iterator(), std::back_inserter(themes), [](const directory_entry& entry) { return is_regular_file(entry.path()) && entry.path().extension() == ".yaf"; });
        std::sort(themes.begin(), themes.end(), [](const directory_entry& lhs, const directory_entry& rhs) { return lhs.path().basename() < rhs.path().basename(); });

        std::vector<path> themes_path;

        std::transform(themes.begin(), themes.end(), std::back_inserter(themes_path), [](const directory_entry& entry) {return entry.path(); });

        return themes_path;
    }
     
    throw std::runtime_error("themes directory doesn't exist.");
}

void gounki::settings_state::apply_button_cb(puObject*)
{
    std::string selected_theme = std::string("themes\\") + _game_enviroment_cb->getStringValue() + ".yaf";
    _context.get_game()->set_yaf_filename(selected_theme);

    try
    {
        auto round_time = boost::lexical_cast<unsigned int>(_round_time_input->getStringValue());
        _context.get_game()->set_round_time(round_time);

        auto animation_time = boost::lexical_cast<unsigned int>(_animation_time_input->getStringValue());
        _context.get_game()->set_animation_time(animation_time);
    }
    catch (const boost::bad_lexical_cast&)
    {
        settings_reset_button->invokeCallback();
    }
}

void gounki::settings_state::reset_button_cb(puObject*)
{
    std::string value_str = std::to_string(_context.get_game()->get_round_time());
    _round_time_input->setValue(value_str.c_str());

    value_str = std::to_string(_context.get_game()->get_animation_time());
    _animation_time_input->setValue(value_str.c_str());

    _game_enviroment_cb->setValue("default");
}
