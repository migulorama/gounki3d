#include "gounki/dummy_animation.hpp"

gounki::dummy_animation::dummy_animation(double time) : _timeInMs(time)
{
    _doReset = true;
}

void gounki::dummy_animation::Update(double time)
{
    if (_isFinished)
        return;

    if (_doReset)
    {
        _animTimeStart = time;
        _doReset = false;
    }

    double animTime = time - _animTimeStart;
    if (animTime > _timeInMs)
    {
        _animTimeStart = time;
        animTime -= _timeInMs;
        _isFinished = true;
        return;
    }
}
