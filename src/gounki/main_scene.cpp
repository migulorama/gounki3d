#include "gounki/main_scene.hpp"
#include "gounki/main_state.hpp"
#include "scene/loader.hpp"
#include "gounki/menu_state.hpp"

#include <glm/glm.hpp>
#include <sstream>
#include "gounki/pui.hpp"


void undo_button_callback(puObject* obj);
void redo_button_callback(puObject* obj);
void move_camera_button_callback(puObject* obj);
void dialog_resume_button_callback(puObject* obj);
void dialog_interrupt_buton_callback(puObject* obj);

gl::TexturePtr _overlay_background_tex;
gl::MaterialPtr _overlay_background_mat;

gl::TexturePtr _scoreboard_background_tex;
gl::MaterialPtr _scoreboard_background_mat;

gl::TexturePtr _player_1_background_tex;
gl::MaterialPtr _player_1_background_mat;

gl::TexturePtr _player_2_background_tex;
gl::MaterialPtr _player_2_background_mat;

gl::TexturePtr _highlighted_background_tex;
gl::MaterialPtr _highlighted_background_mat;

gl::TexturePtr _two_points_background_tex;
gl::MaterialPtr _two_points_background_mat;

gl::TexturePtr _scoreboard_big_background_tex;
gl::MaterialPtr _scoreboard_big_background_mat;

overlay_borders _overlay_borders;
overlay_borders _round_time_borders;
overlay_borders _time_elapsed_borders;
overlay_borders _scoreboard_borders;
overlay_borders _scoreboard_1_borders;
overlay_borders _scoreboard_2_borders;
overlay_borders _golden_highlight_borders;

gounki::main_scene::main_scene(main_state& gs) :
_main_game_state(gs),
_overlay_group(nullptr, puDeleteObject),
_time_text(nullptr, puDeleteObject),
_undo_button(nullptr, puDeleteObject),
_redo_button(nullptr, puDeleteObject),
_move_camera_button(nullptr, puDeleteObject),
_player_1_points_text(nullptr, puDeleteObject),
_player_2_points_text(nullptr, puDeleteObject),
_round_time_static_text(nullptr, puDeleteObject),
_player_1_static_text(nullptr, puDeleteObject),
_player_2_static_text(nullptr, puDeleteObject),
_move_cameras_static_text(nullptr, puDeleteObject), 
_elapsed_time_text(nullptr, puDeleteObject),
_elapsed_time_static_text(nullptr, puDeleteObject),
_pause_dialog(nullptr, puDeleteObject),
_dialog_text(nullptr, puDeleteObject),
_dialog_frame(nullptr, puDeleteObject),
_dialog_ok_button(nullptr, puDeleteObject),
_dialog_interrupt_button(nullptr, puDeleteObject)
{}

void gounki::main_scene::init()
{
    YAF::Loader loader(_main_game_state.get_context()->get_game()->get_yaf_filename());

    _scene = loader.Load();
    auto settings = loader.GetBoardSettings();

    _scene.Init();

    _board_visualizer.set_position(settings.position);
    _board_visualizer.set_size(settings.size);
    _board_rotation = settings.rotation;
    _board_visualizer.init(_main_game_state.get_current_board(), settings.player_1_capPieces, settings.player_2_capPieces, _board_rotation);

    glm::quat rotating_quaternion = glm::toQuat(_board_rotation);

    glm::vec3 target = _board_visualizer.get_position() + glm::rotate(rotating_quaternion, glm::vec3(_board_visualizer.get_size().x * ((1.0f / 2.0f) - (_board_visualizer.get_cell_size().x / 2.0f)), 0.0f, _board_visualizer.get_size().y * ((1.0f / 2.0f) - (_board_visualizer.get_cell_size().y / 2.0f))));
    _player_position[player::Player1] = target + glm::rotate(rotating_quaternion, glm::vec3(0.0f, 3.f, _board_visualizer.get_size().x));
    _player_position[player::Player2]  = target + glm::rotate(rotating_quaternion, glm::vec3(0.0f, 3.f, - _board_visualizer.get_size().x));
    _board_normal = glm::normalize(glm::rotate(rotating_quaternion, glm::vec3(0.0f, 1.0f, 0.0f)));
    _board_right_vector = glm::normalize(glm::rotate(rotating_quaternion, glm::vec3(1.0f, 0.0f, 0.0f)));

    _camera = std::make_shared<gl::Cameras::Perspective>(60.0f, 0.1f, 1000.f, _player_position[player::Player1], target);

    _quad_white_mesh = settings.quad_white_object;
    _quad_black_mesh = settings.quad_black_object;
    _round_white_mesh = settings.round_white_object;
    _round_black_mesh = settings.round_black_object;

    _quad_white_mesh->init(nullptr);
    _quad_black_mesh->init(nullptr);
    _round_white_mesh->init(nullptr);
    _round_black_mesh->init(nullptr);

    _white_mat = settings.white_app;
    _black_mat = settings.black_app;
    _white_transparent_mat = gl::make_material({ { 0.0f, 0.0f, 0.0f, 0.0f } }, { { 0.0f, 0.0f, 0.0f, 0.0f } }, { { 0.0f, 0.0f, 0.0f, 0.0f } }, { { 0.0f, 0.0f, 0.0f, 0.0f } }, 0.0f);
    _highlight_move_mat = gl::make_material({ { 0.0f, 0.0f, 0.0f, 0.4f } }, { { 0.0f, 0.0f, 0.0f, 0.4f } }, { { 0.0f, 0.0f, 0.0f, 0.4f } }, { { 0.0f, 0.0f, 1.0f, 0.4f } }, 0.0f);
    _highlight_deploy_mat = gl::make_material({ { 0.0f, 0.0f, 0.0f, 0.4f } }, { { 0.0f, 0.0f, 0.0f, 0.4f } }, { { 0.0f, 0.0f, 0.0f, 0.4f } }, { { 1.0f, 1.0f, 0.0f, 0.4f } }, 0.0f);

    _animation_manager.init(&_board_visualizer, &_main_game_state.get_current_board());

    puSetWindowFuncs(pu_glfw_get_window, nullptr, pu_glfw_get_window_size, nullptr);
    fntInit();
    puRealInit();

    int width = _main_game_state.get_context()->get_window_width();
    int height = _main_game_state.get_context()->get_window_height();

    // elements border limits configuration
    unsigned int margin = ((width - 700) / 2) > 0 ? (width - 700) / 2 : 0;
    _overlay_borders.margin_left = margin;
    _overlay_borders.margin_right = width - margin;
    _overlay_borders.margin_bottom = 0;
    _overlay_borders.margin_top = 150;

    _round_time_borders.margin_left = _overlay_borders.margin_right - 100;
    _round_time_borders.margin_right = _overlay_borders.margin_right - 5;
    _round_time_borders.margin_bottom = _overlay_borders.margin_bottom + 75;
    _round_time_borders.margin_top = _overlay_borders.margin_bottom + 140;

    _time_elapsed_borders.margin_left = _round_time_borders.margin_left;
    _time_elapsed_borders.margin_right = _round_time_borders.margin_right;
    _time_elapsed_borders.margin_bottom = _overlay_borders.margin_bottom + 10;
    _time_elapsed_borders.margin_top = _overlay_borders.margin_bottom + 75;

    _overlay_group.reset(new puGroup(0, 0));

    _global_font.reset(new puFont(new fntTexFont("resources/fonts/Helvetica-Bold.txf") , 24));
    _score_font.reset(new puFont(new fntTexFont("resources/fonts/Helvetica-Bold.txf"), 26));
    _move_cameras_font.reset(new puFont(new fntTexFont("resources/fonts/Helvetica-Bold.txf"), 16));

    _time_text.reset(new puText(200, _round_time_borders.margin_bottom + 2));
    _time_text->setLabelFont(*_global_font.get());
    _time_text->setLabel("0");
    _time_text->setColor(PUCOL_LABEL, 1, 1, 1);

    _elapsed_time_text.reset(new puText(300, 300));
    _elapsed_time_text->setLabelFont(*_global_font.get());
    _elapsed_time_text->setLabel("0");
    _elapsed_time_text->setColor(PUCOL_LABEL, 1, 1, 1);

    unsigned int text_width = _time_text->getLabelFont().getStringWidth(_time_text->getLabel());
    unsigned int _time_elapsed_width = ((_round_time_borders.margin_right - _round_time_borders.margin_left) - text_width) / 2;

    _time_text->setPosition(_time_elapsed_borders.margin_left + _time_elapsed_width, _round_time_borders.margin_bottom + 2);
    

    text_width = _elapsed_time_text->getLabelFont().getStringWidth(_elapsed_time_text->getLabel());
    _time_elapsed_width = ((_round_time_borders.margin_right - _round_time_borders.margin_left) - text_width) / 2;

    _elapsed_time_text->setPosition(_time_elapsed_borders.margin_left + _time_elapsed_width, _time_elapsed_borders.margin_bottom + 2);

    _round_time_static_text.reset(new puText(200, 0));
    _round_time_static_text->setLabelFont(*_global_font.get());
    _round_time_static_text->setLabel("Round Time");
    _round_time_static_text->setColor(PUCOL_LABEL, 1, 1, 1);
     
    _elapsed_time_static_text.reset(new puText(300, 300));
    _elapsed_time_static_text->setLabelFont(*_global_font.get());
    _elapsed_time_static_text->setLabel("Total Time");
    _elapsed_time_static_text->setColor(PUCOL_LABEL, 1, 1, 1);


    text_width = _round_time_static_text->getLabelFont().getStringWidth(_round_time_static_text->getLabel());
    _time_elapsed_width = ((_round_time_borders.margin_right - _round_time_borders.margin_left) - text_width) / 2;

    _round_time_static_text->setPosition(_time_elapsed_borders.margin_left + _time_elapsed_width, _round_time_borders.margin_bottom +
        _time_text->getLabelFont().getStringHeight() + 2);

    text_width = _elapsed_time_static_text->getLabelFont().getStringWidth(_elapsed_time_static_text->getLabel());
    _time_elapsed_width = ((_round_time_borders.margin_right - _round_time_borders.margin_left) - text_width) / 2;

    _elapsed_time_static_text->setPosition(_time_elapsed_borders.margin_left + _time_elapsed_width, _time_elapsed_borders.margin_bottom +
        _elapsed_time_text->getLabelFont().getStringHeight());

    _player_1_points_text.reset(new puText(250, 250));
    _player_1_points_text->setLabel("0");
    _player_1_points_text->setColor(PUCOL_LABEL, 0, 0, 0);
    _player_1_points_text->setLabelFont(*_score_font.get());

    _player_2_points_text.reset(new puText(0, 350));
    _player_2_points_text->setLabel("0");
    _player_2_points_text->setColor(PUCOL_LABEL, 1, 1, 1);
    _player_2_points_text->setLabelFont(*_score_font.get());

    _player_1_static_text.reset(new puText(0, 350));
    _player_1_static_text->setLabel("P1");
    _player_1_static_text->setColor(PUCOL_LABEL, 1, 1, 1);
    _player_1_static_text->setLabelFont(*_global_font.get());

    _player_2_static_text.reset(new puText(0, 350));
    _player_2_static_text->setLabel("P2");
    _player_2_static_text->setColor(PUCOL_LABEL, 1, 1, 1);
    _player_2_static_text->setLabelFont(*_global_font.get());

    _undo_button.reset(new puArrowButton(_overlay_borders.margin_left + 40, _overlay_borders.margin_bottom + 80, _overlay_borders.margin_left + 100, _overlay_borders.margin_bottom + 125, PUARROW_FASTLEFT));
    _redo_button.reset(new puArrowButton(_overlay_borders.margin_left + 160, _overlay_borders.margin_bottom + 80, _overlay_borders.margin_left + 220, _overlay_borders.margin_bottom + 125, PUARROW_FASTRIGHT));
    _move_camera_button.reset(new puButton(_overlay_borders.margin_left + 200, _overlay_borders.margin_bottom + 50, _overlay_borders.margin_left + 220, _overlay_borders.margin_bottom + 67, PUBUTTON_VCHECK));
    _move_camera_button->setUserData(&_main_game_state);
    _move_camera_button->setCallback(::move_camera_button_callback);

    _undo_button->setCallback(::undo_button_callback);
    _undo_button->setStyle(PUSTYLE_SHADED);
    _undo_button->setUserData(&_main_game_state);
    _undo_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);

    _redo_button->setCallback(::redo_button_callback);
    _redo_button->setStyle(PUSTYLE_SHADED);
    _redo_button->setUserData(&_main_game_state);
    _redo_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);

    _move_cameras_static_text.reset(new puText(200, 0));
    _move_cameras_static_text->setLabel("Move Cameras:");
    _move_cameras_static_text->setColor(PUCOL_LABEL, 1, 1, 1);
    _move_cameras_static_text->setLabelFont(*_move_cameras_font.get());

    _overlay_group->close();
    _overlay_group->hide();

    _overlay_background_tex = gl::Texture::Load("resources/ui/overlay_background_edgy.png");
    _scoreboard_background_tex = gl::Texture::Load("resources/ui/scoreboard_black.png");

    _overlay_background_mat = gl::make_material(_overlay_background_tex, GL_REPEAT, GL_REPEAT);
    _scoreboard_background_mat = gl::make_material(_scoreboard_background_tex, GL_REPEAT, GL_REPEAT);

    _player_1_background_tex = gl::Texture::Load("resources/ui/scoreboard_white_rectangular.png");
    _player_1_background_mat = gl::make_material(_player_1_background_tex, GL_REPEAT, GL_REPEAT);

    _player_2_background_tex = gl::Texture::Load("resources/ui/scoreboard_black_rectangular.png");
    _player_2_background_mat = gl::make_material(_player_2_background_tex, GL_REPEAT, GL_REPEAT);

    _highlighted_background_tex = gl::Texture::Load("resources/ui/scoreboard_golden_rectangular.png");
    _highlighted_background_mat = gl::make_material(_highlighted_background_tex, GL_REPEAT, GL_REPEAT);

    _two_points_background_tex = gl::Texture::Load("resources/ui/two_points.png");
    _two_points_background_mat = gl::make_material(_player_2_background_tex, GL_REPEAT, GL_REPEAT);

    _scoreboard_big_background_tex =  gl::Texture::Load("resources/ui/scoreboard_wood.png");
    _scoreboard_big_background_mat = gl::make_material(_scoreboard_big_background_tex, GL_REPEAT, GL_REPEAT);
}

void gounki::main_scene::on_resume()
{
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_DEPTH_TEST);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glDisable(GL_CULL_FACE);
    glClearColor(0.0, 0.0   , 0.0, 1.0);
    glShadeModel(GL_SMOOTH);
    glPolygonMode(GL_FRONT, GL_FILL);
    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);

    _overlay_group->reveal();
    _session_elapsed_time = 0;
    _session_start_time = std::numeric_limits<double>::infinity();
    _move_camera_button->setValue(true);
}

void gounki::main_scene::draw(bool select_mode)
{
    // Clear Screen and Transformations
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    if (_camera) _camera->ApplyView();

    _scene.draw();

    const auto& cells = _main_game_state.get_current_board().get_cells();
    const auto& state = _main_game_state.get_current_move_state();
    const auto& board = _main_game_state.get_current_board();
    const auto& cell_size = _board_visualizer.get_cell_size();

    size_t nr = board.get_num_rows();
    size_t nc = board.get_num_cols();
    size_t index = 0;

    glPushName(-1);

    // Player 1
    auto captured_pieces = _main_game_state.get_captured_pieces(player::Player1);
    auto colour = _main_game_state.get_player_colour(player::Player2);

    glm::vec3 location;
    for (unsigned int i = 0; i < captured_pieces.size(); ++i)
    {
        const auto& mesh = get_object(colour, captured_pieces[i]);
        auto size = mesh->get_size();
        location = _board_visualizer.get_captured_cells_position(player::Player1, i);
        glPushMatrix();
        glTranslatef(location.x, location.y, location.z);
        glScalef(_board_visualizer.get_size().x, _board_visualizer.get_size().x, _board_visualizer.get_size().y);
        glScalef(0.6f * _board_visualizer.get_cell_size().x / size.x, 0.3f * _board_visualizer.get_cell_size().x / size.y, 0.6f * _board_visualizer.get_cell_size().y / size.z);
        mesh->draw(nullptr);
        glPopMatrix();
    }

    // player 2
    captured_pieces = _main_game_state.get_captured_pieces(player::Player2);
    colour = _main_game_state.get_player_colour(player::Player1);

    for (unsigned int i = 0; i < captured_pieces.size(); ++i)
    {
        const auto& mesh = get_object(colour, captured_pieces[i]);
        auto size = mesh->get_size();
        location = _board_visualizer.get_captured_cells_position(player::Player2, i);

        glPushMatrix();

        glTranslatef(location.x, location.y, location.z);
        glScalef(_board_visualizer.get_size().x, _board_visualizer.get_size().x, _board_visualizer.get_size().y);
        glScalef(0.6f * _board_visualizer.get_cell_size().x / size.x, 0.3f * _board_visualizer.get_cell_size().x / size.y, 0.6f * _board_visualizer.get_cell_size().y / size.z);
        mesh->draw(nullptr);

        glPopMatrix();
    }

    bool moveMode = state == gounki::move_state::Deployment_Mode || state == gounki::move_state::Move_Mode;
    gl::MaterialPtr highlightMaterial = moveMode ? state == gounki::move_state::Deployment_Mode ? _highlight_deploy_mat : _highlight_move_mat : nullptr;

    glPushMatrix();

    glMultMatrixf(glm::value_ptr(_board_visualizer.get_transforms()));
    if (state == gounki::move_state::Choose_Source)
    {
        for (GLuint r = 0; r < nr; ++r)
        {
            glLoadName(r);

            for (GLuint c = 0; c < nc; ++c, ++index)
            {
                glPushMatrix();

                glm::vec3 pos = _board_visualizer.get_default_cell_position(r, c)[0];
                glTranslatef(pos.x, pos.y, pos.z);
                glPushName(c + 1);

                if (r == 0 || r == (nr - 1))
                    _white_transparent_mat->Apply();
                else if (r % 2 == index % 2)
                    _white_mat->Apply();
                else
                    _black_mat->Apply();

                glScalef(cell_size.x, 1.0f, cell_size.y);

                glBegin(GL_QUADS);
                    glNormal3f(0.0f, 1.0f, 0.0f);
                    glTexCoord2i(1, 0);
                    glVertex3f(-.5f, 0.f, -.5f);
                    glTexCoord2i(0, 0);
                    glVertex3f(-.5f, 0.f, .5f);
                    glTexCoord2i(0, 1);
                    glVertex3f(.5f, 0.f, .5f);
                    glTexCoord2i(1, 1);
                    glVertex3f(.5f, 0.f, -.5f);
                glEnd();


                glPopMatrix();

                draw_cell(cells[r * 8 + c], _board_visualizer.get_cell_position(r, c));

                glPopName();
            }
        }
    }
    else
    {
        for (GLuint r = 0; r < nr; ++r)
        {
            for (GLuint c = 0; c < nc; ++c, ++index)
            {
                glPushMatrix();

                glm::vec3 pos = _board_visualizer.get_default_cell_position(r, c)[0];
                glTranslatef(pos.x, pos.y, pos.z);

                auto ids = _main_game_state.get_index_ids(index);
                std::for_each(ids.begin(), ids.end(), glPushName);

                if (r == 0 || r == (nr - 1))
                    _white_transparent_mat->Apply();
                else if (r % 2 == index % 2)
                    _white_mat->Apply();
                else
                    _black_mat->Apply();

                glScalef(cell_size.x, 1.0f, cell_size.y);

                glBegin(GL_QUADS);
                    glNormal3f(0.0f, 1.0f, 0.0f);
                    glTexCoord2i(1, 0);
                    glVertex3f(-.5f, 0.f, -.5f);
                    glTexCoord2i(0, 0);
                    glVertex3f(-.5f, 0.f, .5f);
                    glTexCoord2i(0, 1);
                    glVertex3f(.5f, 0.f, .5f);
                    glTexCoord2i(1, 1);
                    glVertex3f(.5f, 0.f, -.5f);
                glEnd();

                if (highlightMaterial && !ids.empty())
                {
                    highlightMaterial->Apply();
                    glBegin(GL_QUADS);
                        glNormal3f(0.0f, 1.0f, 0.0f);
                        glVertex3f(-.5f, 0.f, -.5f);
                        glVertex3f(-.5f, 0.f, .5f);
                        glVertex3f(.5f, 0.f, .5f);
                        glVertex3f(.5f, 0.f, -.5f);
                    glEnd();
                }
                glPopMatrix();

                std::for_each(ids.begin(), ids.end(), [](const int&){ glPopName(); });

                if (state == gounki::move_state::Move_Mode && r == _main_game_state.get_selected_position().x && c == _main_game_state.get_selected_position().y - 1)
                {
                    auto id = _main_game_state.get_selected_position_id();
                    glPushName(id);
                    draw_cell(cells[r * 8 + c], _board_visualizer.get_cell_position(r, c));
                    glPopName();
                }
                else
                    draw_cell(cells[r * 8 + c], _board_visualizer.get_cell_position(r, c));

            }
        }
    }

    glPopMatrix();

    if (!select_mode)
    {
        glPushAttrib(GL_ENABLE_BIT | GL_VIEWPORT_BIT | GL_TRANSFORM_BIT | GL_LIGHTING_BIT);

        glDisable(GL_LIGHTING);
        glDisable(GL_FOG);
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);

        unsigned int w = _main_game_state.get_context()->get_window_width();
        unsigned int h = _main_game_state.get_context()->get_window_height();

        glViewport(0, 0, w, h);

        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
        glOrtho(0, w, 0, h, -1, 1);
        glMatrixMode(GL_MODELVIEW);
        glTranslatef(0.375, 0.375, 0);
        glPushMatrix();
        glLoadIdentity();
        
        // interface background
        glColor3f(1.0f, 1.0f, 1.0f);
        _overlay_background_mat->Apply();
        glBegin(GL_QUADS);
        glTexCoord2i(0, 0);
        glVertex2i(_overlay_borders.margin_left, _overlay_borders.margin_bottom);
        glTexCoord2i(1, 0);
        glVertex2i(_overlay_borders.margin_right, _overlay_borders.margin_bottom);
        glTexCoord2i(1, 1);
        glVertex2i(_overlay_borders.margin_right, _overlay_borders.margin_top);
        glTexCoord2i(0, 1);
        glVertex2i(_overlay_borders.margin_left, _overlay_borders.margin_top);
        glEnd();

        // round time background
        glColor3f(1.0f, 1.0f, 1.0f);
        _scoreboard_background_mat->Apply();
        glBegin(GL_QUADS);
        glTexCoord2i(0, 0);
        glVertex2i(_round_time_borders.margin_left, _round_time_borders.margin_bottom);
        glTexCoord2i(1, 0);
        glVertex2i(_round_time_borders.margin_right, _round_time_borders.margin_bottom);
        glTexCoord2i(1, 1);
        glVertex2i(_round_time_borders.margin_right, _round_time_borders.margin_top);
        glTexCoord2i(0, 1);
        glVertex2i(_round_time_borders.margin_left, _round_time_borders.margin_top);
        glEnd();


        // elapsed time background
        glColor3f(1.0f, 1.0f, 1.0f);
        _scoreboard_background_mat->Apply();
        glBegin(GL_QUADS);
        glTexCoord2i(0, 0);
        glVertex2i(_time_elapsed_borders.margin_left, _time_elapsed_borders.margin_bottom);
        glTexCoord2i(1, 0);
        glVertex2i(_time_elapsed_borders.margin_right, _time_elapsed_borders.margin_bottom);
        glTexCoord2i(1, 1);
        glVertex2i(_time_elapsed_borders.margin_right, _time_elapsed_borders.margin_top);
        glTexCoord2i(0, 1);
        glVertex2i(_time_elapsed_borders.margin_left, _time_elapsed_borders.margin_top);
        glEnd();

        // scoreboard background
        glColor3f(1.0f, 1.0f, 1.0f);
        _scoreboard_big_background_mat->Apply();
        glBegin(GL_QUADS);
        glTexCoord2i(0, 0);
        glVertex2i(_scoreboard_borders.margin_left, _scoreboard_borders.margin_bottom);
        glTexCoord2i(1, 0);
        glVertex2i(_scoreboard_borders.margin_right, _scoreboard_borders.margin_bottom);
        glTexCoord2i(1, 1);
        glVertex2i(_scoreboard_borders.margin_right, _scoreboard_borders.margin_top);
        glTexCoord2i(0, 1);
        glVertex2i(_scoreboard_borders.margin_left, _scoreboard_borders.margin_top);
        glEnd();

        // player 1 score background
        glColor3f(1.0f, 1.0f, 1.0f);
        _highlighted_background_mat->Apply();
        glBegin(GL_QUADS);
        glTexCoord2i(0, 0);
        glVertex2i(_golden_highlight_borders.margin_left, _golden_highlight_borders.margin_bottom);
        glTexCoord2i(1, 0);
        glVertex2i(_golden_highlight_borders.margin_right, _golden_highlight_borders.margin_bottom);
        glTexCoord2i(1, 1);
        glVertex2i(_golden_highlight_borders.margin_right, _golden_highlight_borders.margin_top);
        glTexCoord2i(0, 1);
        glVertex2i(_golden_highlight_borders.margin_left, _golden_highlight_borders.margin_top);
        glEnd();

        // player 2 score background
        glColor3f(1.0f, 1.0f, 1.0f);
        _player_2_background_mat->Apply();
        glBegin(GL_QUADS);
        glTexCoord2i(0, 0);
        glVertex2i(_scoreboard_2_borders.margin_left, _scoreboard_2_borders.margin_bottom);
        glTexCoord2i(1, 0);
        glVertex2i(_scoreboard_2_borders.margin_right, _scoreboard_2_borders.margin_bottom);
        glTexCoord2i(1, 1);
        glVertex2i(_scoreboard_2_borders.margin_right, _scoreboard_2_borders.margin_top);
        glTexCoord2i(0, 1);
        glVertex2i(_scoreboard_2_borders.margin_left, _scoreboard_2_borders.margin_top);
        glEnd();

        // player 1 score background
        glColor3f(1.0f, 1.0f, 1.0f);
        _player_1_background_mat->Apply();
        glBegin(GL_QUADS);
        glTexCoord2i(0, 0);
        glVertex2i(_scoreboard_1_borders.margin_left, _scoreboard_1_borders.margin_bottom);
        glTexCoord2i(1, 0);
        glVertex2i(_scoreboard_1_borders.margin_right, _scoreboard_1_borders.margin_bottom);
        glTexCoord2i(1, 1);
        glVertex2i(_scoreboard_1_borders.margin_right, _scoreboard_1_borders.margin_top);
        glTexCoord2i(0, 1);
        glVertex2i(_scoreboard_1_borders.margin_left, _scoreboard_1_borders.margin_top);
        glEnd();

        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
        glPopAttrib();
        puDisplay();
    }
}

void gounki::main_scene::update(double time)
{
    if (std::isinf(_session_start_time)) _session_start_time = time;
    _session_elapsed_time = time - _session_start_time;

    int hours = (int)_session_elapsed_time / 3600000;
    int minutes = (int)(_session_elapsed_time / 60000) % 60;
    int seconds = (int)(_session_elapsed_time / 1000) % 60;

    std::stringstream time_stamp;
    time_stamp << std::setfill('0') << std::setw(2) << hours << ":"
        << std::setfill('0') << std::setw(2) << minutes << ":"
        << std::setfill('0') << std::setw(2) << seconds << std::endl;

    session_elapsed_time_str = time_stamp.str();
    _elapsed_time_text->setLabel(session_elapsed_time_str.c_str());

    if (_animation_manager.hasAnimations())
        disableUndoRedo();
    else
        enableUndoRedo();

    _animation_manager.update(time);
}

void gounki::main_scene::reshape(int width, int height)
{
    glViewport(0, 0, width, height);
    if (_camera) _camera->UpdateProjectionMatrix(width, height);

    // elements border limits configuration
    unsigned int margin = ((width - 700) / 2) > 0 ? (width - 700) / 2 : 0;
    _overlay_borders.margin_left = margin;
    _overlay_borders.margin_right = width - margin;
    _overlay_borders.margin_bottom = 0;
    _overlay_borders.margin_top = 150;

    _round_time_borders.margin_left = _overlay_borders.margin_right - 220;
    _round_time_borders.margin_right = _overlay_borders.margin_right - 30;
    _round_time_borders.margin_bottom = _overlay_borders.margin_bottom + 75;
    _round_time_borders.margin_top = _overlay_borders.margin_bottom + 140;

    _time_elapsed_borders.margin_left = _round_time_borders.margin_left;
    _time_elapsed_borders.margin_right = _round_time_borders.margin_right;
    _time_elapsed_borders.margin_bottom = _overlay_borders.margin_bottom + 10;
    _time_elapsed_borders.margin_top = _overlay_borders.margin_bottom + 75;

    margin = ((width - 300) / 2) > 0 ? (width - 300) / 2 : 0;
    _scoreboard_borders.margin_left = margin;
    _scoreboard_borders.margin_right = width - margin;
    _scoreboard_borders.margin_bottom = height - 130;
    _scoreboard_borders.margin_top = height;

    _scoreboard_1_borders.margin_left = _scoreboard_borders.margin_left + 30;
    _scoreboard_1_borders.margin_right = _scoreboard_borders.margin_left + 105;
    _scoreboard_1_borders.margin_bottom = _scoreboard_borders.margin_top - 85;
    _scoreboard_1_borders.margin_top= _scoreboard_borders.margin_top - 15;

    _scoreboard_2_borders.margin_left = _scoreboard_borders.margin_right - 105;
    _scoreboard_2_borders.margin_right =  _scoreboard_borders.margin_right - 30;
    _scoreboard_2_borders.margin_bottom = _scoreboard_borders.margin_top - 85;
    _scoreboard_2_borders.margin_top = _scoreboard_borders.margin_top - 15;

    _golden_highlight_borders = _scoreboard_1_borders;
    _golden_highlight_borders.margin_left -= 5;
    _golden_highlight_borders.margin_right += 5;
    _golden_highlight_borders.margin_top += 5;
    _golden_highlight_borders.margin_bottom -= 5;

    _player_1_static_text->setPosition(_scoreboard_1_borders.margin_left + 9, _scoreboard_borders.margin_bottom + 5);
    _player_2_static_text->setPosition(_scoreboard_2_borders.margin_left + 9,
        _scoreboard_borders.margin_bottom + 5);

    _undo_button->setPosition(_overlay_borders.margin_left + 40, _overlay_borders.margin_bottom + 85);
    _redo_button->setPosition(_overlay_borders.margin_left + 160, _overlay_borders.margin_bottom + 85);

    _move_cameras_static_text->setPosition(_overlay_borders.margin_left + 30, _overlay_borders.margin_bottom + 10);
    _move_camera_button->setPosition(_overlay_borders.margin_left + 200, _overlay_borders.margin_bottom + 17);

    // update player points text position
    unsigned int text_width = _player_1_points_text->getLabelFont().getStringWidth(_player_1_points_text->getLabel());
    unsigned int score_board_width = ((_scoreboard_1_borders.margin_right - _scoreboard_1_borders.margin_left) - text_width) / 2;
    _player_1_points_text->setPosition(_scoreboard_1_borders.margin_left + score_board_width, _scoreboard_1_borders.margin_bottom + 10);

    text_width = _player_2_points_text->getLabelFont().getStringWidth(_player_2_points_text->getLabel());
    score_board_width = ((_scoreboard_2_borders.margin_right - _scoreboard_2_borders.margin_left) - text_width) / 2;
    _player_2_points_text->setPosition(_scoreboard_2_borders.margin_left + score_board_width, _scoreboard_2_borders.margin_bottom + 10);

    // update round dynamic text position
    text_width = _time_text->getLabelFont().getStringWidth(_time_text->getLabel());
    unsigned int _time_elapsed_width = ((_round_time_borders.margin_right - _round_time_borders.margin_left) - text_width) / 2;
    _time_text->setPosition(_time_elapsed_borders.margin_left + _time_elapsed_width, _round_time_borders.margin_bottom + 2);

    // update time elapsed dynamic text position
    text_width = _elapsed_time_text->getLabelFont().getStringWidth(_elapsed_time_text->getLabel());
    _time_elapsed_width = ((_round_time_borders.margin_right - _round_time_borders.margin_left) - text_width) / 2;

    _elapsed_time_text->setPosition(_time_elapsed_borders.margin_left + _time_elapsed_width, _time_elapsed_borders.margin_bottom + 2);

    // update round static text position
    text_width = _round_time_static_text->getLabelFont().getStringWidth(_round_time_static_text->getLabel());
    _time_elapsed_width = ((_round_time_borders.margin_right - _round_time_borders.margin_left) - text_width) / 2;

    _round_time_static_text->setPosition(_time_elapsed_borders.margin_left + _time_elapsed_width, _round_time_borders.margin_bottom +
        _time_text->getLabelFont().getStringHeight());

    // update time elapsed static text position
    text_width = _elapsed_time_static_text->getLabelFont().getStringWidth(_elapsed_time_static_text->getLabel());
    _time_elapsed_width = ((_round_time_borders.margin_right - _round_time_borders.margin_left) - text_width) / 2;

    _elapsed_time_static_text->setPosition(_time_elapsed_borders.margin_left + _time_elapsed_width, _time_elapsed_borders.margin_bottom + 
        _elapsed_time_text->getLabelFont().getStringHeight());

    if (_pause_dialog != nullptr)
    {
        unsigned int margin_left = (width - 400) > 0 ? width - 400 : 0;
        unsigned int margin_bottom = (height - 200) > 0 ? height - 200 : 0;

        _dialog_frame->setPosition(margin_left / 2, margin_bottom / 2);

        _dialog_text->setPosition(margin_left / 2 + 154, height - (margin_bottom / 2) - 60);

        _dialog_ok_button->setPosition(margin_left / 2 + 80, (margin_bottom / 2) + 60);

        _dialog_interrupt_button->setPosition(margin_left / 2 + 225, (margin_bottom / 2) + 60);
    }
}

gounki::animation_manager& gounki::main_scene::get_animation_manager()
{
    return _animation_manager;
}

gounki::board_visualizer& gounki::main_scene::get_board_visualizer()
{
    return _board_visualizer;
}

void gounki::main_scene::draw_cell(const gounki::cell& cell, const std::array<glm::vec3, 3>& cellPositions)
{
    auto col = cell.get_colour();

    auto elems = cell.get_elements();
    for (int i = 0; i < elems.size(); ++i)
    {
        const auto& mesh = get_object(col, elems[i]);

        auto size = mesh->get_size();
        glPushMatrix();
        glm::vec3 pos = cellPositions[i];
        glTranslated(pos.x, pos.y, pos.z);
        glScalef(0.6f * _board_visualizer.get_cell_size().x / size.x, 0.3f * _board_visualizer.get_cell_size().x / size.y, 0.6f * _board_visualizer.get_cell_size().y / size.z);
        mesh->draw(nullptr);
        glPopMatrix();
    }
}

YAF::ComposedObjectPtr gounki::main_scene::get_object(const gounki::colour& colour, const gounki::piece& piece)
{
    switch (colour)
    {
    case gounki::colour::Black: return piece == gounki::piece::Circle ? _round_black_mesh : _quad_black_mesh;
    case gounki::colour::White: return piece == gounki::piece::Circle ? _round_white_mesh : _quad_white_mesh;
    default: return nullptr;
    }
}

void gounki::main_scene::on_pause()
{
    _overlay_group->hide();
}

void gounki::main_scene::update_round_time(double time)
{
    time_str = std::to_string(static_cast<int>(time / 1000.0));
    _time_text->setLabel(time_str.c_str());

    unsigned int text_width = _time_text->getLabelFont().getStringWidth(_time_text->getLabel());
    unsigned int _time_elapsed_width = ((_round_time_borders.margin_right - _round_time_borders.margin_left) - text_width) / 2;
    _time_text->setPosition(_time_elapsed_borders.margin_left + _time_elapsed_width, _round_time_borders.margin_bottom + 2);
}

void gounki::main_scene::set_player_score(player::player p, unsigned int score)
{
    switch (p)
    {
    case player::Player1:
    {
        _player_1_score_str = std::to_string(score);
        _player_1_points_text->setLabel(_player_1_score_str.c_str());
        break;
    }
    case player::Player2:
    {
        _player_2_score_str = std::to_string(score);
        _player_2_points_text->setLabel(_player_2_score_str.c_str());
        break;
    }
    default:
        break;
    }
}

void gounki::main_scene::update_player_scores()
{
    set_player_score(player::Player1, _main_game_state.get_player_points(player::Player1));
    set_player_score(player::Player2, _main_game_state.get_player_points(player::Player2));
}

const glm::vec3& gounki::main_scene::get_board_normal_vector()
{
    return _board_normal;
}

const glm::vec3& gounki::main_scene::get_board_right_vector()
{
    return _board_right_vector;
}

void gounki::main_scene::disableUndoRedo()
{
    _undo_button->greyOut();
    _undo_button->setColourScheme(101 / 255.0f, 101 / 255.0f, 101 / 255.0f);

    _redo_button->greyOut();
    _redo_button->setColourScheme(101 / 255.0f, 101 / 255.0f, 101 / 255.0f);
}

void gounki::main_scene::enableUndoRedo()
{
    _undo_button->activate();
    _undo_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);

    _redo_button->activate();
    _redo_button->setColourScheme(101 / 255.0f, 67 / 255.0f, 33 / 255.0f);
}

void gounki::main_scene::set_current_player(player::player p)
{
    switch (p)
    {
    case player::Player1:
    {
        _golden_highlight_borders = _scoreboard_1_borders;
        break;
    }
    case player::Player2:
    {
        _golden_highlight_borders = _scoreboard_2_borders;
        break;
    }
    default:
        return;
    }

    _golden_highlight_borders.margin_left -= 5;
    _golden_highlight_borders.margin_right += 5;
    _golden_highlight_borders.margin_top += 5;
    _golden_highlight_borders.margin_bottom -= 5;
}

void gounki::main_scene::activate_pause_dialog(bool value)
{
    if (value)
    {
        int width = _main_game_state.get_context()->get_window_width();
        int height = _main_game_state.get_context()->get_window_height();

        unsigned int margin_left = (width - 400) > 0 ? width - 400 : 0;
        unsigned int margin_bottom = (height - 200) > 0 ? height - 200 : 0;

        _pause_dialog.reset(new puDialogBox(50, 50));
        {
            _dialog_frame.reset(new puFrame(margin_left / 2, margin_bottom / 2, width - (margin_left / 2), height - (margin_bottom / 2)));
            _dialog_text.reset(new puText(margin_left / 2 + 154, height - (margin_bottom / 2) - 60));
            _dialog_text->setColour(PUCOL_LABEL, 1, 1, 1);
            _dialog_text->setLabel("Game Paused");

            _dialog_ok_button.reset(new puOneShot(margin_left / 2 + 80, (margin_bottom / 2) + 60, "Resume Game"));
            _dialog_ok_button->setUserData(this);
            _dialog_ok_button->setCallback(::dialog_resume_button_callback);

            _dialog_interrupt_button.reset(new puOneShot(margin_left / 2 + 225, (margin_bottom / 2) + 60, "Main Menu"));
            _dialog_interrupt_button->setUserData(this);
            _dialog_interrupt_button->setCallback(::dialog_interrupt_buton_callback);
        }
        _pause_dialog->close();
        _pause_dialog->reveal();
    }
    else
    {
        _pause_dialog->hide();
        _pause_dialog.reset(nullptr);
        _dialog_frame.reset(nullptr);
        _dialog_ok_button.reset(nullptr);
        _dialog_text.reset(nullptr);
        _dialog_interrupt_button.reset(nullptr);
    }
}

bool gounki::main_scene::has_active_pause_dialog() const
{
    return _pause_dialog != nullptr;
}

void gounki::main_scene::interrupt_button_cb(puObject* obj)
{
    activate_pause_dialog(false);
    _main_game_state.change_to_state<gounki::menu_state>();
}


void undo_button_callback(puObject* obj)
{
    gounki::main_state* st = reinterpret_cast<gounki::main_state*>(obj->getUserData());
    st->undo();
    obj->setValue(false);
}

void redo_button_callback(puObject* obj)
{
    gounki::main_state* st = reinterpret_cast<gounki::main_state*>(obj->getUserData());
    st->redo();
    obj->setValue(false);
}

void move_camera_button_callback(puObject* obj)
{
    gounki::main_state* st = reinterpret_cast<gounki::main_state*>(obj->getUserData());
    st->set_move_cameras(obj->getValue() == 1);
}

void dialog_resume_button_callback(puObject* obj)
{
    gounki::main_scene* st = reinterpret_cast<gounki::main_scene*>(obj->getUserData());
    st->activate_pause_dialog(false);
}

void dialog_interrupt_buton_callback(puObject* obj)
{
    gounki::main_scene* st = reinterpret_cast<gounki::main_scene*>(obj->getUserData());
    st->interrupt_button_cb(obj);
}