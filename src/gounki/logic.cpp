#include "gounki/logic.hpp"
#include "gounki/board.hpp"
#include "gounki/moves.hpp"

#include <chrono>
#include <sstream>
#include <iomanip>

std::regex gounki::logic::bracket_separator(R"regex(\{([^\}]*)\})regex");
std::regex gounki::logic::spacePlusbracket(R"regex(,\[)regex");
std::regex gounki::logic::yes_reg(R"regex(yes\((\d)\))regex");
std::regex gounki::logic::piece_splitter(R"regex((circle|square))regex");

gounki::logic::logic(const std::string& host, const std::string& port)
{
    using namespace std::chrono;

    std::time_t now_c = system_clock::to_time_t(system_clock::now());

    std::ostringstream oss;
    oss << "logs\\server_" << std::put_time(std::localtime(&now_c), "%d%m%Y-%H%M%S") << ".log";

    // UNTIL BETTER SOLUTION THIS IS WHAT WE HAVE
    // PS: I'M TIRED OF LAUNCHING PROLOG EVERY TIME I WANT TO TEST

    DWORD dwAttrib = GetFileAttributes("logs");

    SECURITY_ATTRIBUTES as;
    ZeroMemory(&as, sizeof(as));

    as.bInheritHandle = true;
    as.nLength = sizeof(as);

    if (!(dwAttrib != INVALID_FILE_ATTRIBUTES && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY)))
    {
        CreateDirectory("logs", &as);
    }

    _hFile = CreateFile(oss.str().c_str(),                // name of the write
        GENERIC_WRITE,       // open for writing
        0,                      // do not share
        &as,                   // default security
        CREATE_ALWAYS,             // create new file only
        FILE_ATTRIBUTE_NORMAL,  // normal file
        NULL);                  // no attr. template

    STARTUPINFO si;

    ZeroMemory(&si, sizeof(si));
    ZeroMemory(&_pi, sizeof(_pi));

    si.hStdOutput = _hFile;
    si.hStdError = _hFile;
    si.dwFlags |= STARTF_USESTDHANDLES;
    si.cb = sizeof(si);

    LPSTR command_lpstr = const_cast<char *>("prolog/server.exe");

    std::string cmdLine = "prolog/server.exe " + port;
    if (!CreateProcess(
            nullptr,   // No module name (use command line)
            const_cast<char *>(cmdLine.c_str()),        // Command line
            nullptr,           // Process handle not inheritable
            nullptr,           // Thread handle not inheritable
            true,          // Set handle inheritance to FALSE
            0,              // No creation flags
            nullptr,           // Use parent's environment block
            nullptr,           // Use parent's starting directory 
            &si,            // Pointer to STARTUPINFO structure
            &_pi)           // Pointer to PROCESS_INFORMATION structure
            ) {
        throw std::runtime_error("CreateProcess failed ("+ std::to_string(GetLastError()) + ").\n");
    }

    _stream.connect(host, port);
    if (!_stream) throw std::runtime_error("Unable to establish connection to prolog module.");
}

gounki::logic::~logic()
{
    if (_stream.good()) disconnect();

    CloseHandle(_hFile);
    CloseHandle(_pi.hProcess);
    CloseHandle(_pi.hThread);
}

gounki::board gounki::logic::initialize() const
{
    if (!_stream) throw std::runtime_error("Invalid Stream used!");

    _stream << "initialize.\n";
    std::string answer = read();
    return gounki::board(8, 8, answer);
}

std::vector<gounki::movement> gounki::logic::get_moves(const board& board, const position& pos) const
{
    if (!_stream) throw std::runtime_error("Invalid Stream used!");

    _stream << "get_moves(" << board.get_prolog_board() << ", " << "position(" << pos.x << ", " << pos.y << ")).\n";
    std::string answer = read();

    auto end = std::sregex_token_iterator();

    int i = 0;
    std::vector<movement> result;
    for (auto itr = std::sregex_token_iterator(answer.begin() + 1, answer.begin() + answer.size() - 2, spacePlusbracket, -1); itr != end; ++itr, ++i)
    {
        auto str = itr->str();
        if (!str.empty())
           result.push_back(movement((i != 0 ? "[" : "") + str));
    }
     

    return result;
}

void gounki::logic::disconnect()
{
    if (!_stream) throw std::runtime_error("Invalid Stream used!");
    
    _stream << "bye.\n";
    read();
    _stream.close();
}

std::string gounki::logic::read() const
{
    std::string result;
    std::getline(_stream, result);
    return result;
}

std::pair<gounki::board, std::vector<gounki::piece>> gounki::logic::exec_move(const board& board, const gounki::movement& move) const
{
    if (!_stream) throw std::runtime_error("Invalid Stream used!");

    _stream << "exec_move(" << move.get_prolog_string() << ", " + board.get_prolog_board() + ").\n";
    std::string answer = read();

    auto itr = std::sregex_token_iterator(answer.begin(), answer.end(), bracket_separator, 1);
    gounki::board new_board(8, 8, itr->str()); ++itr;
    std::vector<piece> pieces;

    auto str = itr->str();
    auto end = std::sregex_token_iterator();
    for (auto p_itr = std::sregex_token_iterator(str.begin(), str.end(), piece_splitter); p_itr != end; ++p_itr)
    {
        pieces.push_back(piece_from_string(p_itr->str()));
    }

    return std::make_pair(new_board, pieces);
}

std::tuple<gounki::movement, gounki::board, std::vector<gounki::piece>> gounki::logic::exec_pc_move(const board& board, const gounki::colour& colour, const gounki::difficulty& difficulty) const
{
    if (!_stream) throw std::runtime_error("Invalid Stream used!");

    _stream << "exec_pc_move(" << colour << ", " << difficulty << ", " << board.get_prolog_board() << ").\n";
    std::string answer = read();

    std::tuple<gounki::movement, gounki::board, std::vector<gounki::piece>> result;

    auto itr = std::sregex_token_iterator(answer.begin(), answer.end(), bracket_separator, 1);
    gounki::movement move(itr->str());  ++itr;
    gounki::board new_board(8, 8, itr->str()); ++itr;
    std::vector<piece> pieces;

    auto str = itr->str();
    auto end = std::sregex_token_iterator();
    for (auto p_itr = std::sregex_token_iterator(str.begin(), str.end(), piece_splitter); p_itr != end; ++p_itr)
    {
        pieces.push_back(piece_from_string(p_itr->str()));
    }

    return std::tie(move, new_board, pieces);
}

boost::optional<unsigned int> gounki::logic::is_game_finished(const gounki::board& board) const
{
    if (!_stream) throw std::runtime_error("Invalid Stream used!");

    _stream << "is_game_finished(" << board.get_prolog_board() << ").\n";
    auto answer = read();
    if (answer == "no")
        return boost::optional<unsigned int>();

    
    auto end = std::sregex_token_iterator();

    for (auto itr = std::sregex_token_iterator(answer.begin(), answer.end(), yes_reg, 1); itr != end; ++itr)
        return boost::make_optional(boost::lexical_cast<unsigned int>(itr->str()));

    return boost::optional<unsigned int>();
}

gounki::difficulty gounki::difficulty_from_string(std::string str)
{
    if (str == "easy") return gounki::difficulty::Easy;
    else if (str == "medium-easy") return gounki::difficulty::Medium_Easy;
    else if (str == "medium-hard") return gounki::difficulty::Medium_Hard;
    else if (str == "hard") return gounki::difficulty::Hard;
    else throw std::invalid_argument("");
}

std::ostream& gounki::operator<<(std::ostream& out, const difficulty& diff)
{
    switch (diff)
    {
    case gounki::difficulty::Easy: out << "easy"; break;
    case gounki::difficulty::Medium_Easy: out << "medium-easy"; break;
    case gounki::difficulty::Medium_Hard: out << "medium-hard"; break;
    case gounki::difficulty::Hard: out << "hard"; break;
    }

    return out;
}

