#include "gounki/main_state.hpp"
#include <gl_4_4.h>
#include <memory>
#include "gounki/game.hpp"
#include "gl/primitives.hpp"
#include "gl/cameras.hpp"
#include "gounki/menu_state.hpp"
#include "gounki/player.hpp"

using namespace gounki;

std::unordered_map<gounki::move_state, gounki::main_state::object_clicked_handler> gounki::main_state::_object_clicked_handlers = { 
    { move_state::Choose_Source, &main_state::handle_choose_source }, 
    { move_state::Move_Mode, &main_state::handle_move_mode },
    { move_state::Deployment_Mode, &main_state::handle_deployment_mode } 
};

std::array<gounki::colour, gounki::player::Num_Players> gounki::main_state::_player_color = { 
    gounki::colour::White, 
    gounki::colour::Black 
};

gounki::main_state::main_state(state_manager& context) : 
    game_state(context), 
    _mainInterface(this), 
    _scene(*this), 
    _current_move_state(move_state::Choose_Source),
    _current_player(player::Player1),
    _player_mode({ { mode::Player, mode::Player } }),
    _player_difficulty({ { difficulty::Easy, difficulty::Easy } }),
    _player_points({ { 0, 0 } }),
    _last_time(std::numeric_limits<double>::infinity()),
    _start_time(std::numeric_limits<double>::infinity()),
    _pause_start_time(std::numeric_limits<double>::infinity()),
    _pause_time(0),
    _game_start(-1),
    _has_cached_info(false)
{
}

void gounki::main_state::init()
{
    _logic = std::make_unique<gounki::logic>("127.0.0.1", "60011");
    _current_board = _logic->initialize();
    _history.commit(_current_board, _current_player);
    _scene.init();
    _move_cameras = true;
    _max_play_time = _context.get_game()->get_round_time() * 1000;
    _animation_time = _context.get_game()->get_animation_time() * 1000;
    set_player(player::Player1);
}

void gounki::main_state::update(double time)
{
    if (_scene.has_active_pause_dialog())
    {
        return;
    }
    else if (!std::isinf(_pause_start_time))
    {
        _pause_time += _context.get_game()->get_time() - _pause_start_time;
        _pause_start_time = std::numeric_limits<double>::infinity();
    }

    if (std::isinf(_last_time)) _last_time = time;

    time -= _pause_time;

    _scene.update(time);
    _mainInterface.update(time);
    if (get_current_move_state() != move_state::Finish && !_scene.get_animation_manager().hasAnimations()) _scene.update_round_time(_max_play_time - (time - _start_time));

    if (get_current_player_mode() == mode::Player && get_current_move_state() != move_state::Finish && (time - _start_time) >= _max_play_time) 
    {
        _history.commit(_current_board, _current_player == player::Player1 ? player::Player2 : player::Player1, _play_captured_pieces);
        next_player();
        _start_time = time;
        _current_move_state = move_state::Choose_Source;
    }
    if (_current_move_state == move_state::Finish)
    {
        if (!_scene.get_animation_manager().hasAnimations())
        {
            if (_has_cached_info) 
            {
                _current_board = _cached_board;
                if (!_cached_captured_pieces.empty())
                {
                    _play_captured_pieces[_current_player].insert(_play_captured_pieces[_current_player].end(), _cached_captured_pieces.begin(), _cached_captured_pieces.end());
                    _cached_captured_pieces.clear();
                }

                _scene.get_animation_manager().addDummyAnimation(1000);

                _has_cached_info = false;
            }
            else
            {
                next_player();
                _start_time = time;
                if (auto winner = _logic->is_game_finished(_current_board))
                {
                    _player_points[winner.get() - 1] += 1;
                    set_player(winner.get() == 1 ? player::Player2 : player::Player1); // other player starts the new game
                    save_game();
                    _history.clear();
                    _game_start = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
                    _current_board = _logic->initialize();
                    for (auto& cp : _play_captured_pieces) cp.clear();
                    _history.commit(_current_board, _current_player);
                    _scene.get_board_visualizer().reset();
                    _scene.update_player_scores();
                }
                _current_move_state = move_state::Choose_Source;
            }
             

            
        }
    }
    else if (get_current_player_mode() == mode::Pc && !_scene.get_animation_manager().hasAnimations()) 
    {
        exec_pc_move();
    }
}

void gounki::main_state::display(bool select_mode)
{
    _scene.draw(select_mode);
}

void gounki::main_state::on_resume(game_state*)
{
    _move_cameras = true;
    _scene.on_resume();
    set_player(_current_player);

    if (_game_start == -1) _game_start = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

    if (isinf(_start_time)) _start_time = _context.get_game()->get_time();
    if (!isinf(_pause_start_time)) _pause_time = _context.get_game()->get_time() - _pause_start_time;
}

void gounki::main_state::on_pause(game_state*)
{
    _pause_time = _context.get_game()->get_time();
    _scene.on_pause();
}

gounki::main_state::~main_state()
{
}

void gounki::main_state::reshape(int width, int height)
{
    _scene.reshape(width, height);
}

void gounki::main_state::mouse_move(double x, double y)
{
    _mainInterface.mouse_move(x, y);

    auto displacement = _mainInterface.get_mouse_displacement();
    
    int modifiers = _mainInterface.get_modifiers();

    auto prev_position = _scene.get_camera()->GetPosition();

    if (_mainInterface.is_right_button_pressed() && modifiers == 0)
    {
        _scene.get_camera()->RotatePosition(-displacement.y * 0.5f, _scene.get_camera()->GetRightVector());
        _scene.get_camera()->RotatePosition(-displacement.x * 0.5f, glm::vec3(0.0f, 1.0f, 0.0f));
    }
    if (_mainInterface.is_middle_button_pressed() || (_mainInterface.is_right_button_pressed() && modifiers & GLFW_MOD_CONTROL))
    {
        _scene.get_camera()->Translate(glm::vec3(0.0f, 0.0f, displacement.y * 0.5f));
    }

    auto angle = (1 - 2 * (_current_player == player::Player1)) * glm::orientedAngle(glm::normalize(_scene.get_camera()->GetPosition() - _scene.get_camera()->GetTarget()), _scene.get_board_normal_vector(), _scene.get_board_right_vector());
    if (glm::dot(glm::normalize(_scene.get_camera()->GetPosition() - _scene.get_camera()->GetTarget()), _scene.get_board_normal_vector()) < 0)
    {
        _scene.get_camera()->SetPosition(prev_position);
    }
    else if (_move_cameras && angle < 0)
    {
        _scene.get_camera()->SetPosition(prev_position);
    }
    else if (glm::distance(_scene.get_camera()->GetTarget(), _scene.get_camera()->GetPosition()) < 1.0f)
    {
        _scene.get_camera()->SetPosition(_scene.get_camera()->GetTarget() - _scene.get_camera()->GetDirection());
    }
    
}

void gounki::main_state::mouse_button(int button, int action, int mods)
{
    _mainInterface.mouse_button(button, action, mods);

    if (_mainInterface.is_left_button_pressed() && (get_current_player_mode() == mode::Player))
    {
        auto handlerItr = _object_clicked_handlers.find(_current_move_state);
        if (handlerItr != _object_clicked_handlers.end())
        {
            auto clicked_objects = _mainInterface.pick_objects();
            clicked_objects.erase(std::remove_if(clicked_objects.begin(), clicked_objects.end(), [](const int& val){return val < 0; }), clicked_objects.end());
            (this->*handlerItr->second)(_current_move_state, std::move(clicked_objects));
        }
    }

    int btn;
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT: btn = PU_LEFT_BUTTON; break;
    case GLFW_MOUSE_BUTTON_MIDDLE: btn = PU_MIDDLE_BUTTON; break;
    case GLFW_MOUSE_BUTTON_RIGHT: btn = PU_RIGHT_BUTTON; break;
    }

    auto mouse_pos = _mainInterface.get_mouse_position();

    puMouse(btn, action == GLFW_PRESS ? PU_DOWN : PU_UP, static_cast<int>(mouse_pos.x), static_cast<int>(mouse_pos.y));
}

void gounki::main_state::next_player()
{
    set_player(_current_player == player::Player1 ? player::Player2 : player::Player1);
}

void gounki::main_state::set_player(gounki::player::player p)
{
    _scene.set_current_player(p);

    if (_current_player != p) 
    {
        _current_player = p;
        if (_move_cameras) _scene.get_animation_manager().addCameraAnimation(_scene.get_camera(), _scene.get_player_position(_current_player), _animation_time);
    }

    if (get_current_player_mode() == mode::Pc)
    {
        _scene.disableUndoRedo();
    }
    else
    {
        _scene.enableUndoRedo();
    }
}

void gounki::main_state::handle_choose_source(move_state, std::vector<int> selected_objects)
{
    if (selected_objects.empty()) return;

    position clicked_position(selected_objects[0], selected_objects[1]);
    const auto& selected_cell = _current_board.cell_at(clicked_position - position(0, 1));
    if (selected_cell.get_colour() != get_current_player_colour()) return;

    _selected_position = clicked_position;
    _moves.clear();
    _moves = _logic->get_moves(_current_board, _selected_position);
    if (_moves.empty()) return;

    _deployments.clear();
    std::remove_copy_if(_moves.begin(), _moves.end(), 
                        std::back_inserter(_deployments), 
                        [](const gounki::movement& move) { return !move.is_deployment(); });

    for (auto& id : _ids) id.clear();

    if (_deployments.size() != 0) _moves.resize(_moves.size() - _deployments.size());

    if (!_moves.empty())
    {
        _current_move_state = move_state::Move_Mode;
        for (unsigned int i = 0; i < _moves.size(); ++i)
        {
            const auto& dest = _moves.at(i).get_positions().back();
            _ids[dest.x * 8 + dest.y - 1].push_back(i);
        }
    }
    else if (!_deployments.empty())
    {
        _current_move_state = move_state::Deployment_Mode;
        for (unsigned int i = 0; i < _deployments.size(); ++i)
        {
            const auto& pieces = _deployments.at(i).get_pieces();
            const auto firsPiece = pieces.front();
            auto itr = std::find_if(pieces.begin(), pieces.end(),
                [&firsPiece](const piece& p) { return p != firsPiece; });
            size_t index = std::distance(pieces.begin(), itr);
            const auto& dest = _deployments.at(i).get_positions().at(index);
            _ids[dest.x * 8 + dest.y - 1].push_back(i);
        }
    }
}

void gounki::main_state::handle_deployment_mode(move_state, std::vector<int> selected_objects)
{
    if (selected_objects.empty()) { _current_move_state = move_state::Choose_Source; return; }

    if (selected_objects.size() == 1) 
        exec_move(_deployments[selected_objects.front()]);
    else
    {
        for (auto& id : _ids) id.clear();
        for (const auto& i : selected_objects)
        {
            const auto& dest = _deployments.at(i).get_positions().back();
            _ids[dest.x * 8 + dest.y - 1].push_back(i);
        }
    }
}

void gounki::main_state::handle_move_mode(move_state, std::vector<int> selected_objects)
{
    if (selected_objects.empty()) { _current_move_state = move_state::Choose_Source; return; }

    if (std::find(selected_objects.begin(), selected_objects.end(), get_selected_position_id()) != selected_objects.end())
    {
        if (_deployments.empty()) { _current_move_state = move_state::Choose_Source; return; }

        for (auto& id : _ids) id.clear();
        _current_move_state = move_state::Deployment_Mode;
        for (unsigned int i = 0; i < _deployments.size(); ++i)
        {
            const auto& pieces = _deployments.at(i).get_pieces();
            const auto firsPiece = pieces.front();
            auto itr = std::find_if(pieces.begin(), pieces.end(),
                [&firsPiece](const piece& p) { return p != firsPiece; });
            size_t index = std::distance(pieces.begin(), itr);
            const auto& dest = _deployments.at(i).get_positions().at(index);
            _ids[dest.x * 8 + dest.y - 1].push_back(i);
        }
    }
    else
        exec_move(_moves[selected_objects.front()]);
}

void gounki::main_state::exec_move(gounki::movement move)
{
    std::tie(_cached_board, _cached_captured_pieces) = _logic->exec_move(_current_board, move);
    _scene.get_animation_manager().addMovementAnimation(move, _current_board, _animation_time);

    auto play_cap_pieces = _play_captured_pieces;
    play_cap_pieces[_current_player].insert(play_cap_pieces[_current_player].end(), _cached_captured_pieces.begin(), _cached_captured_pieces.end());

    _history.commit(_cached_board, _current_player == player::Player1 ? player::Player2 : player::Player1, move, play_cap_pieces);
    _current_move_state = move_state::Finish;

    _has_cached_info = true;
}

void gounki::main_state::exec_pc_move()
{
    movement move;
    std::tie(move, _cached_board, _cached_captured_pieces) = _logic->exec_pc_move(_current_board, get_current_player_colour(), get_current_player_difficulty());
    _scene.get_animation_manager().addMovementAnimation(move, _current_board, _animation_time);    

    auto play_cap_pieces = _play_captured_pieces;
    play_cap_pieces[_current_player].insert(play_cap_pieces[_current_player].end(), _cached_captured_pieces.begin(), _cached_captured_pieces.end());

    _history.commit(_cached_board, _current_player == player::Player1 ? player::Player2 : player::Player1, move, play_cap_pieces);
    _current_move_state = move_state::Finish;

    _has_cached_info = true;
}

void gounki::main_state::undo()
{
    _history.undo();
    gounki::player::player player;
    std::tie(_current_board, player, std::ignore, _play_captured_pieces) = _history.get_last();
    _scene.get_board_visualizer().set_current_captured_index(static_cast<unsigned int>(_play_captured_pieces[player::Player1].size()), static_cast<unsigned int>(_play_captured_pieces[player::Player2].size()));

    if (_player_mode[player] == mode::Pc)
    {
        _history.undo();
        player;
        std::tie(_current_board, player, std::ignore, _play_captured_pieces) = _history.get_last();
            _scene.get_board_visualizer().set_current_captured_index(static_cast<unsigned int>(_play_captured_pieces[player::Player1].size()), static_cast<unsigned int>(_play_captured_pieces[player::Player2].size()));
    }
    
    set_player(player);
}

void gounki::main_state::redo()
{
    _history.redo();
    gounki::player::player player;
    std::tie(_current_board, player, std::ignore, _play_captured_pieces) = _history.get_last();
        _scene.get_board_visualizer().set_current_captured_index(static_cast<unsigned int>(_play_captured_pieces[player::Player1].size()), static_cast<unsigned int>(_play_captured_pieces[player::Player2].size()));

    if (_player_mode[player] == mode::Pc)
    {
        _history.redo();
        player;
        std::tie(_current_board, player, std::ignore, _play_captured_pieces) = _history.get_last();
            _scene.get_board_visualizer().set_current_captured_index(static_cast<unsigned int>(_play_captured_pieces[player::Player1].size()), static_cast<unsigned int>(_play_captured_pieces[player::Player2].size()));
    }
    set_player(player);
}

void gounki::main_state::key_action(int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS && key == GLFW_KEY_ESCAPE)
    {
        if (!_scene.has_active_pause_dialog()) 
        {
            _scene.activate_pause_dialog(true);
            _pause_start_time = _context.get_game()->get_time();
        }
    }

    if (_scene.get_animation_manager().hasAnimations()) return;

    if (get_current_player_mode() == mode::Player && get_current_move_state() == move_state::Choose_Source) {
        if (action == GLFW_PRESS && key == GLFW_KEY_Z && mods & GLFW_MOD_CONTROL)
            undo();
        else if (action == GLFW_PRESS && key == GLFW_KEY_R && mods & GLFW_MOD_CONTROL)
            redo();
    }
}

const std::vector<gounki::piece>& gounki::main_state::get_captured_pieces(gounki::player::player player) const
{
    return _play_captured_pieces[player];
}

void gounki::main_state::set_player_2_mode(gounki::mode mode)
{
    set_player_mode(player::Player2, mode);
}

void gounki::main_state::set_player_1_mode(gounki::mode mode)
{
    set_player_mode(player::Player1, mode);
}

void gounki::main_state::set_player_mode(player::player player, gounki::mode mode)
{
    _player_mode[player] = mode;
}

void gounki::main_state::set_player_2_difficulty(gounki::difficulty val)
{
    set_player_difficulty(gounki::player::Player2, val);
}

void gounki::main_state::set_player_1_difficulty(gounki::difficulty val)
{
    set_player_difficulty(gounki::player::Player1, val);
}

void gounki::main_state::set_player_difficulty(player::player player, gounki::difficulty val)
{
    _player_difficulty[player] = val;
}

gounki::difficulty gounki::main_state::get_current_player_difficulty() const
{
    return _player_difficulty[_current_player];
}

GLuint gounki::main_state::get_selected_position_id()
{
    return static_cast<GLuint>(_moves.size());
}

const position& gounki::main_state::get_selected_position()
{
    return _selected_position;
}

const std::vector<uint32_t>& gounki::main_state::get_index_ids(size_t index) const
{
    return _ids[index];
}

gounki::colour gounki::main_state::get_player_colour(gounki::player::player player) const
{
    return _player_color[player];
}

gounki::colour gounki::main_state::get_current_player_colour() const
{
    return _player_color[_current_player];
}

gounki::mode gounki::main_state::get_current_player_mode() const
{
    return _player_mode[_current_player];
}

gounki::move_state gounki::main_state::get_current_move_state() const
{
    return _current_move_state;
}

const std::vector<gounki::piece>& gounki::main_state::get_current_player_captured_pieces() const
{
    return _play_captured_pieces[_current_player];
}

const gounki::board& gounki::main_state::get_current_board() const
{
    return _current_board;
}

void gounki::main_state::save_game()
{
    DWORD dwAttrib = GetFileAttributes("saves");

    if (!(dwAttrib != INVALID_FILE_ATTRIBUTES && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY)))
    {
        SECURITY_ATTRIBUTES as;
        ZeroMemory(&as, sizeof(as));

        as.bInheritHandle = true;
        as.nLength = sizeof(as);

        CreateDirectory("saves", &as);
    }

    std::ostringstream oss;
    oss << "saves\\game_" << std::put_time(std::localtime(&_game_start), "%d-%m-%Y_%H-%M-%S") << ".txt";

    std::ofstream save_file(oss.str(), std::ios::out | std::ios::trunc);

    if (save_file.is_open())
    {
        auto hist = _history.get_history();

        std::vector<game_history::entry> moves;
        std::copy_if(hist.begin(), hist.end(), std::back_inserter(moves), [](const game_history::entry& entry) { return std::get<2>(entry); });
        std::transform(moves.begin(), moves.end(), std::ostream_iterator<std::string>(save_file, "\n"), [](const game_history::entry& entry) {return std::get<2>(entry).get().get_prolog_string(); });
    }
}

unsigned int gounki::main_state::get_player_points(player::player p) const
{
    return _player_points[p];
}

void gounki::main_state::set_move_cameras(bool value)
{
    _move_cameras = value;
}
