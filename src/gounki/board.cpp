#include "gounki/board.hpp"
#include "gounki/cell.hpp"
#include <regex>
#include <cassert>

const std::string boardPattern = R"str([[cell(blank), cell(white, [circle]), cell(black, [square, circle]), cell(white, [square, circle, circle])], [cell(black, [square, circle, circle]), cell(white, [square, circle, circle]), cell(black, [square, circle, circle]), cell(white, [square, circle, circle])]])str";
const auto cellSplitter = std::regex(R"regex((cell\([^\)]*\)))regex");

gounki::board::board(unsigned int num_rows, unsigned int num_cols, const std::string& initialConfiguration)
{
    _num_cols = num_cols;
    _num_rows = num_rows + 2;

    update(initialConfiguration);
}

void gounki::board::update(const std::string& prologString)
{
    _cells.clear();
    auto end = std::sregex_token_iterator();
    
    for (auto itr = std::sregex_token_iterator(prologString.begin(), prologString.end(), cellSplitter, 1); itr != end; ++itr)
        _cells.push_back(parse_cell(itr->str()));

    _prolog_board = prologString;
}
