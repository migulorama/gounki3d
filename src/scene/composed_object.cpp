#include <stdexcept>

#include "scene/graph.hpp"
#include "scene/composed_object.hpp"
#include "glm/gtc/type_ptr.hpp"

YAF::ComposedObject::ComposedObject(const YAF::Graph& graph) : _graph(graph), _animation(nullptr)
{
}

void YAF::ComposedObject::AddChild(const std::string& childName)
{
    gl::ObjectPtr node = _graph.GetNode(childName);
    if (node == nullptr) throw std::runtime_error("Undefined \"" + childName + "\" node referenced.");

    _children.push_back(node);
}

void YAF::ComposedObject::AddChild(std::shared_ptr<gl::Primitive> child)
{
    _children.push_back(child);
}

void YAF::ComposedObject::init(gl::MaterialPtr mat)
{
    gl::MaterialPtr& material = _material ? _material : mat;

    for(auto childIt = _children.begin(); childIt != _children.end(); ++childIt)
        (*childIt)->init(material);

    _size = glm::vec3(0.0f, 0.0f, 0.0f);

    for (const auto& object : _children)
        _size += object->get_size();
}

void YAF::ComposedObject::draw(gl::MaterialPtr mat) const
{
    glPushMatrix();


    glMultMatrixf(glm::value_ptr(_transformation));
    if (_animation) _animation->Draw();


    const gl::MaterialPtr& material = _material ? _material : mat;

    for (auto childIt = _children.begin(); childIt != _children.end(); ++childIt)
        (*childIt)->draw(material);

    glPopMatrix();
}

void YAF::ComposedObject::SetAppearance(const std::string& name)
{
    auto material = _graph.GetMaterial(name);
    if (material == nullptr) throw std::runtime_error("Unrecognized \"" + name + "\" appearance referenced.");

    _material = material;
}

void YAF::ComposedObject::SetTransformation(const glm::mat4& transform)
{
    _transformation = transform;
}

const glm::mat4& YAF::ComposedObject::GetTransformation() const
{
    return _transformation;
}

void YAF::ComposedObject::SetAnimation( const std::string& animationName )
{
    gl::AnimationPtr anim = _graph.GetAnimation(animationName);
    if (anim == nullptr) throw std::runtime_error("Undefined \"" + animationName + "\" node referenced.");

    _animation = anim;
}

void YAF::ComposedObject::update( double time )
{
    for (auto itr = _children.begin(); itr != _children.end(); ++itr)
        (*itr)->update(time);
}

glm::vec3 YAF::ComposedObject::get_size() const
{
    return _size;
}
