#include <ticpp.h>

#include "scene/loader.hpp"
#include "gl/primitives.hpp"
#include "gl/primitives/model.hpp"
#include "utils.hpp"
#include "gl/lights/spot.hpp"
#include <glm/gtc/type_ptr.hpp>

YAF::Loader::Loader(const std::string& path) : _path(path)
{
}

YAF::Graph YAF::Loader::Load()
{
    ticpp::Document doc( _path );
    doc.LoadFile();

    ticpp::Element* yafElement = doc.FirstChildElement("yaf")->ToElement();

    // reinitialize the Graph structure
    _graph = YAF::Graph();

    LoadSceneGlobals(yafElement);
    LoadAnimations(yafElement);
    LoadTextures(yafElement);
    LoadMaterials(yafElement);
    LoadCameras(yafElement);
    LoadLighting(yafElement);
    LoadNodes(yafElement);
    LoadGounkiSettings(yafElement);

    return _graph;
}

void YAF::Loader::LoadAnimations( ticpp::Element* elem )
{
    ticpp::Element* animationElement = elem->FirstChildElement("animations");

    std::string id;
    float time;
    ticpp::Iterator<ticpp::Element> anim("animation");
    for (anim = anim.begin(animationElement); anim != anim.end(); ++anim)
    {
        anim->GetAttribute("id", &id);
        anim->GetAttribute("span", &time);

        std::vector<glm::vec3> cntrlPoints;
        ticpp::Iterator<ticpp::Element> itr("controlpoint");

        for (itr = itr.begin(anim.Get()); itr != itr.end(); ++itr)
        {
            glm::vec3 to_insert;
            itr->GetAttribute("x", &(to_insert.x));
            itr->GetAttribute("y", &(to_insert.y));
            itr->GetAttribute("z", &(to_insert.z));
            cntrlPoints.push_back(to_insert);
        }

        _graph.AddLinearAnimation(id, time, cntrlPoints);

    }
}

void YAF::Loader::LoadGounkiSettings(ticpp::Element* elem)
{
    ticpp::Element* gounkiElement = elem->FirstChildElement("gounki");
    ticpp::Element* boardElement = gounkiElement->FirstChildElement("board");

    std::string positionStr;
    boardElement->GetAttribute("position", &positionStr);
    parse_string(positionStr, _board_settings.position);

    std::string sizeStr;
    boardElement->GetAttribute("size", &sizeStr);
    parse_string(sizeStr, _board_settings.size);

    std::string axis;
    float angle;

    _board_settings.rotation = glm::mat4();

    ticpp::Iterator< ticpp::Element > rotations("rotation");
    for (rotations = rotations.begin(boardElement); rotations != rotations.end(); ++rotations)
    {
        rotations->GetAttribute("axis", &axis);
        rotations->GetAttribute("angle", &angle, true);

        if (axis == "x")
            _board_settings.rotation = glm::rotate(_board_settings.rotation, angle, glm::vec3(1.0f, 0.0f, 0.0f));
        else if (axis == "y")
            _board_settings.rotation = glm::rotate(_board_settings.rotation, angle, glm::vec3(0.0f, 1.0f, 0.0f));
        else if (axis == "z")
            _board_settings.rotation = glm::rotate(_board_settings.rotation, angle, glm::vec3(0.0f, 0.0f, 1.0f));
        else
            throw std::runtime_error("Invalid rotation axis specified: " + axis + ".");
    }

    std::string id;
    auto black_app_elem = boardElement->FirstChildElement("black-app");
    black_app_elem->GetAttribute("id", &id);
    _board_settings.black_app = _graph.GetMaterial(id);

    auto white_app_elem = boardElement->FirstChildElement("white-app");
    white_app_elem->GetAttribute("id", &id);
    _board_settings.white_app = _graph.GetMaterial(id);

    ticpp::Iterator< ticpp::Element > cpp1("captured-pieces-p1");
    for (cpp1 = cpp1.begin(gounkiElement); cpp1 != cpp1.end(); ++cpp1)
    {
        int no = 1;
        if (cpp1->HasAttribute("no")) cpp1->GetAttribute("no", &no, false);
        no--;

        std::string positionStr;
        cpp1->GetAttribute("position", &positionStr);
        glm::vec3 position;
        parse_string(positionStr, position);
        
        glm::vec3 difference(0, 0, 0);
        if (cpp1->HasAttribute("next"))
        {
            std::string next;
            cpp1->GetAttribute("next", &next);
            parse_string(next, difference);
        }

        _board_settings.player_1_capPieces.emplace_back(no, position, difference);
    }

    if (_board_settings.player_1_capPieces.empty()) throw std::runtime_error("No Captured pieces positions for player 1 were supplied.");

    ticpp::Iterator< ticpp::Element > cpp2("captured-pieces-p2");
    for (cpp2 = cpp2.begin(gounkiElement); cpp2 != cpp2.end(); ++cpp2)
    {
        int no = 1;
        if (cpp2->HasAttribute("no")) cpp2->GetAttribute("no", &no, false);
        no--;

        std::string positionStr;
        cpp2->GetAttribute("position", &positionStr);
        glm::vec3 position;
        parse_string(positionStr, position);

        glm::vec3 difference(0, 0, 0);
        if (cpp2->HasAttribute("next"))
        {
            std::string next;
            cpp2->GetAttribute("next", &next);
            parse_string(next, difference);
        }

        _board_settings.player_2_capPieces.emplace_back(no, position, difference);
    }

    if (_board_settings.player_2_capPieces.empty()) throw std::runtime_error("No Captured pieces positions for player 2 were supplied.");

    ticpp::Element* roundNode = gounkiElement->FirstChildElement("round-piece-node");
    std::string roundWhiteId, roundBlackId;
    roundNode->GetAttribute("white-id", &roundWhiteId);
    _board_settings.round_white_object = _graph.GetNode(roundWhiteId);
    if (!_board_settings.round_white_object) throw std::runtime_error("Invalid round white object supplied.");

    roundNode->GetAttribute("black-id", &roundBlackId);
    _board_settings.round_black_object = _graph.GetNode(roundBlackId);
    if (!_board_settings.round_black_object) throw std::runtime_error("Invalid round black object supplied.");

    ticpp::Element* quadNode = gounkiElement->FirstChildElement("quad-piece-node");
    std::string quadWhiteId, quadBlackId;
    quadNode->GetAttribute("white-id", &quadWhiteId);
    _board_settings.quad_white_object = _graph.GetNode(quadWhiteId);
    if (!_board_settings.quad_white_object) throw std::runtime_error("Invalid quad white object supplied.");

    quadNode->GetAttribute("black-id", &quadBlackId);
    _board_settings.quad_black_object = _graph.GetNode(quadBlackId);
    if (!_board_settings.quad_black_object) throw std::runtime_error("Invalid quad black object supplied.");
}

void YAF::Loader::LoadTextures(ticpp::Element* elem)
{
    ticpp::Element* texturesElement =  elem->FirstChildElement("textures");

    std::string id, fileName;
    ticpp::Iterator< ticpp::Element > child("texture");
    for (child = child.begin(texturesElement); child != child.end(); ++child)
    {
        child->GetAttribute("id", &id);
        child->GetAttribute("file", &fileName);
        _graph.AddTexture(id, fileName);
    }
}

void YAF::Loader::LoadMaterials(ticpp::Element* elem)
{
    ticpp::Element* materialsElement = elem->FirstChildElement("appearances");

    std::string id, textureRef;

    gl::ColorRGBA    emissive, 
                    ambient, 
                    diffuse, 
                    specular;

    float shininess, texLength_s, texLength_t;

    ticpp::Iterator< ticpp::Element > child;
    gl::MaterialPtr material;
    for ( child = child.begin(materialsElement); child != child.end(); child++ )
    {
        child->GetAttribute("id", &id);
        get_attribute(child.Get(), "emissive", emissive);
        get_attribute(child.Get(), "ambient", ambient);
        get_attribute(child.Get(), "diffuse", diffuse);
        get_attribute(child.Get(), "specular", specular);
        child->GetAttribute("shininess", &shininess);

        material = _graph.AddMaterial(id);
        material->SetAmbient(ambient);
        material->SetEmissive(emissive);
        material->SetDiffuse(diffuse);
        material->SetSpecular(specular);
        material->SetShininess(shininess);

        if (child->HasAttribute("textureref"))
        {
            child->GetAttribute("textureref", &textureRef, false);

            gl::details::TexturePtr tex = _graph.GetTexture(textureRef);
            if (tex == nullptr)
                throw std::runtime_error("Parsing error: textureref '" + textureRef + "' does not exist.");

            material->SetTexture(_graph.GetTexture(textureRef));
            child->GetAttribute("texlength_s", &texLength_s);
            child->GetAttribute("texlength_t", &texLength_t);

            material->SetTextureScale(texLength_s, texLength_t);
        }     
    }
}

void YAF::Loader::LoadCameras(ticpp::Element* elem)
{
    ticpp::Element* camerasElement = elem->FirstChildElement("cameras");

    float    near_val, 
            far_val, 
            angle, 
            left, 
            right, 
            top, 
            bottom;

    glm::vec3 pos, target;
    ticpp::Iterator< ticpp::Element > child;
    std::string id, camType, initialCamera = camerasElement->GetAttribute<std::string>("initial");
    for (child = child.begin(camerasElement); child != child.end(); ++child)
    {
        camType = child->Value();

        if (camType != "perspective" && camType != "ortho")
            throw std::exception("Invalid camera type specified.");

        child->GetAttribute("id", &id);
        child->GetAttribute("near", &near_val);
        child->GetAttribute("far", &far_val);

        if (camType == "perspective")
        {
            child->GetAttribute("angle", &angle);
            get_attribute(child.Get(), "pos", pos);
            get_attribute(child.Get(), "target", target);

            gl::PerspectiveCameraPtr cam = _graph.AddPerspectiveCamera(id);
            cam->SetNear(near_val);
            cam->SetFar(far_val);
            cam->SetFOV(angle);
            cam->SetPosition(pos);
            cam->SetTarget(target);
        }

        if (camType == "ortho")
        {
            child->GetAttribute("left", &left);
            child->GetAttribute("right", &right);
            child->GetAttribute("top", &top);
            child->GetAttribute("bottom", &bottom);

            gl::OrthoCameraPtr cam = _graph.AddOrthoCamera(id);
            cam->SetNear(near_val);
            cam->SetFar(far_val);
            cam->SetLeft(left);
            cam->SetRight(right);
            cam->SetTop(top);
            cam->SetBottom(bottom);
        }
    }

    _graph.SetInitialCamera(initialCamera);

}

void YAF::Loader::LoadLighting(ticpp::Element * elem)
{
    ticpp::Element* lightingElement = elem->FirstChildElement("lighting");

    LoadLightGlobals(elem);

    bool enabled;
    glm::vec3 location, direction;
    glm::vec4 ambient, diffuse, specular;
    float angle, exponent;
    std::string id, lightType;

    gl::LightPtr light;
    gl::SpotPtr spot;
    ticpp::Iterator< ticpp::Element > child;
    GLuint NextId = GL_LIGHT0;

    for (child = child.begin(lightingElement); child != child.end(); ++child)
    {
        lightType = child->Value();
        if (NextId > GL_LIGHT7)
            throw std::runtime_error("Too many lights specified. Maximum number supported is 8.");
        if (lightType != "spot" && lightType != "omni")
            throw std::runtime_error("Invalid light type specified.");

        child->GetAttribute("id", &id);

        enabled = parse_bool_from_string(child->GetAttribute("enabled"));

        get_attribute(child.Get(), "location",   location);
        get_attribute(child.Get(), "ambient",    ambient);
        get_attribute(child.Get(), "diffuse",    diffuse);
        get_attribute(child.Get(), "specular",   specular);

        if (lightType == "spot")
        {
            child->GetAttribute("angle", &angle);
            child->GetAttribute("exponent", &exponent);
            get_attribute(child.Get(), "direction", direction);

            spot = _graph.AddSpotLight(id, NextId++);
            spot->Enable(enabled);
            spot->SetPosition(location);
            spot->SetAmbient(ambient);
            spot->SetDiffuse(diffuse);
            spot->SetSpecular(specular);
            spot->SetAngle(angle);
            spot->SetExponent(exponent);
            spot->SetDirection(direction);
        }
        else
        {
            light = _graph.AddLight(id, NextId++);
            light->Enable(enabled);
            light->SetPosition(location);
            light->SetAmbient(ambient);
            light->SetDiffuse(diffuse);
            light->SetSpecular(specular);
        }
        
    }
}

void YAF::Loader::LoadSceneGlobals(ticpp::Element* elem)
{
    ticpp::Element* globalsElement = elem->FirstChildElement("globals");

    gl::ColorRGBA background;
    get_attribute(globalsElement, "background", background);
    _graph.SetBackground(background);
    _graph.SetDrawMode(globalsElement->GetAttribute<std::string>("drawmode"));
    _graph.SetShading(globalsElement->GetAttribute<std::string>("shading"));
    _graph.SetCullface(globalsElement->GetAttribute<std::string>("cullface"));
    _graph.SetCullorder(globalsElement->GetAttribute<std::string>("cullorder"));
}

void YAF::Loader::LoadNodes(ticpp::Element* elem)
{
    ticpp::Element* graphElement = elem->FirstChildElement("graph");

    std::string id;

    ComposedObjectPtr actualNode;
    ticpp::Iterator< ticpp::Element > child("node");
    for (child = child.begin(graphElement); child != child.end(); ++child)
    {
        child->GetAttribute("id", &id);

        if (_graph.GetNode(id) != nullptr) throw std::runtime_error("Multiple references to node '" + id + "'.");

        _graph.AddNode(id);
    }

    graphElement->GetAttribute("rootid", &id);
    _graph.SetRoot(id);

    glm::mat4 transformation;
    ticpp::Element* material;
    ticpp::Element* animation;
    ticpp::Iterator< ticpp::Element > nodes("node"), children;
    YAF::ComposedObjectPtr parentNode;
    bool appearanceFound = true;
    for (nodes = nodes.begin(graphElement); nodes != nodes.end(); ++nodes)
    {
        parentNode = _graph.GetNode(nodes->GetAttribute<std::string>("id"));

        transformation = LoadTransformations(nodes.Get());

        if ((material = nodes->FirstChildElement("appearanceref", false)))
        {
            parentNode->SetAppearance(material->GetAttribute<std::string>("id"));
        }

        if ((animation = nodes->FirstChildElement("animationref", false)))
        {
            parentNode->SetAnimation(animation->GetAttribute<std::string>("id"));
        }

        for (children = children.begin(nodes->FirstChildElement("children")); children != children.end(); ++children)
        {
            if(children->Value() == "noderef")
                parentNode->AddChild(children->GetAttribute<std::string>("id"));
            else
                parentNode->AddChild(LoadPrimitive(children.Get()));
        }

        parentNode->SetTransformation(transformation);
    }
}

gl::PrimitivePtr YAF::Loader::LoadPrimitive(ticpp::Element* elem)
{
    std::string type = elem->Value();

    if (type == "cylinder")
    {
        float base, top, height;
        unsigned int stacks, slices;
        elem->GetAttribute("base", &base);
        elem->GetAttribute("top", &top);
        elem->GetAttribute("height", &height);
        elem->GetAttribute("slices", &slices);
        elem->GetAttribute("stacks", &stacks);

        return std::make_shared<gl::primitives::Cylinder>(base, top, height, slices, stacks);
    }
    else if (type == "sphere")
    {
        float radius;
        unsigned int slices, stacks;
        elem->GetAttribute("radius", &radius);
        elem->GetAttribute("slices", &slices);
        elem->GetAttribute("stacks", &stacks);

        return std::make_shared<gl::primitives::Sphere>(radius, slices, stacks);
    }
    else if (type == "rectangle")
    {
        std::array<float, 2> xy1, xy2;
        get_attribute(elem, "xy1", xy1);
        get_attribute(elem, "xy2", xy2);

        return std::make_shared<gl::primitives::Rectangle>(glm::make_vec2(xy1.data()), glm::make_vec2(xy2.data()));
    }
    else if (type == "triangle")
    {
        std::array<float, 3> xyz1, xyz2, xyz3;
        get_attribute(elem, "xyz1", xyz1);
        get_attribute(elem, "xyz2", xyz2);
        get_attribute(elem, "xyz3", xyz3);

        return std::make_shared<gl::primitives::Triangle>(glm::make_vec3(xyz1.data()), glm::make_vec3(xyz2.data()), glm::make_vec3(xyz3.data()));
    }
    else if (type == "torus")
    {
        float inner, outer;
        unsigned int slices, loops;
        elem->GetAttribute("inner", &inner);
        elem->GetAttribute("outer", &outer);
        elem->GetAttribute("slices", &slices);
        elem->GetAttribute("loops", &loops);

        return std::make_shared<gl::primitives::Torus>(inner, outer, slices, loops);
    }
    else if (type == "plane")
    {
        GLint parts;
        elem->GetAttribute("parts", &parts);

        return std::make_shared<gl::primitives::Plane>(parts, parts);
    }
    else if (type == "patch")
    {
        GLint order, uParts, vParts;
        elem->GetAttribute("order", &order);

        if (order < 1 || order > 3) throw std::runtime_error("Invalid patch order!");

        elem->GetAttribute("partsU", &uParts);

        if (uParts <= 0) throw std::runtime_error("Invalid number of U parts in patch!");

        elem->GetAttribute("partsV", &vParts);

        if (vParts <= 0) throw std::runtime_error("Invalid number of V parts in patch!");

        int numberOfPoints = (order + 1) * (order + 1);

        std::vector<glm::vec3> cntrlPoints;
        cntrlPoints.reserve(numberOfPoints);
        ticpp::Iterator<ticpp::Element> itr("controlpoint");

        for (itr = itr.begin(elem); itr != itr.end(); ++itr)
        {
            glm::vec3 to_insert;
            itr->GetAttribute("x", &(to_insert.x));
            itr->GetAttribute("y", &(to_insert.y));
            itr->GetAttribute("z", &(to_insert.z));
            cntrlPoints.push_back(to_insert);
        }

        if (cntrlPoints.size() != numberOfPoints)
            throw std::runtime_error("Invalid number of control points!");

        return std::make_shared<gl::primitives::Patch>(order, uParts, vParts, cntrlPoints);
    }
    else if (type == "model")
    {
        std::string file;

        elem->GetAttribute("file", &file);

        return std::make_shared<gl::mesh>(file);
    }
    else 
        throw std::runtime_error("Invalid primitive type specified: " + type + ".");
}

void ExecTransformation(ticpp::Element* elem, bool throwIfNotFound = true);

glm::mat4 YAF::Loader::LoadTransformations(ticpp::Element* elem)
{   
    glm::mat4 ret(1.0f);    // identity matrix
    glm::vec3 transform;
    std::string type;
    ticpp::Iterator< ticpp::Element > child;
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    for (child = child.begin(elem->FirstChildElement("transforms")); child != child.end(); ++child)
        /* ret *= GetTransformation(child.Get()); */ExecTransformation(child.Get());

    glGetFloatv(GL_MODELVIEW_MATRIX, &ret[0][0]);

    return ret;
}

void ExecTransformation(ticpp::Element* elem, bool throwIfNotFound)
{
    glm::vec3 transform;
    std::string type = elem->Value();

    if (type == "scale")
    {
        get_attribute(elem, "factor", transform, throwIfNotFound);
        glScalef(transform.x, transform.y, transform.z);
    }
    else if (type == "translate")
    {
        get_attribute(elem, "to", transform, throwIfNotFound);
        glTranslatef(transform.x, transform.y, transform.z);
    }
    else if (type == "rotate")
    {
        std::string axis;
        float angle;
        elem->GetAttribute("axis", &axis);
        elem->GetAttribute("angle", &angle, throwIfNotFound);

        if (axis == "x")
            glRotatef(angle, 1.0f, 0.0f, 0.0f);
        else if (axis == "y")
            glRotatef(angle, 0.0f, 1.0f, 0.0f);
        else if (axis == "z")
            glRotatef(angle, 0.0f, 0.0f, 1.0f);
        else
            throw std::runtime_error("Invalid rotation axis specified: " + axis + ".");
    }
    else
        throw std::runtime_error("Invalid transformation type specified.");
}

glm::mat4 YAF::Loader::GetTransformation(ticpp::Element* elem, bool throwIfNotFound)
{
    glm::mat4 ret;
    glm::vec3 transform;
    std::string type = elem->Value();

    if (type == "scale")
    {
        get_attribute(elem, "factor", transform, throwIfNotFound);
        ret = glm::scale(transform);
    }
    else if (type == "translate")
    {
        get_attribute(elem, "to", transform, throwIfNotFound);
        ret = glm::translate(transform);
    }
    else if (type == "rotate")
    {
        std::string axis;
        float angle;
        elem->GetAttribute("axis", &axis);
        elem->GetAttribute("angle", &angle, throwIfNotFound);

        if (axis == "x")
            ret = glm::rotate(angle, 1.f, 0.f, 0.f);
        else if (axis == "y")
            ret = glm::rotate(angle, 0.f, 1.f, 0.f);
        else if (axis == "z")
            ret = glm::rotate(angle, 0.f, 0.f, 1.f);
        else
            throw std::runtime_error("Invalid rotation axis specified: " + axis + ".");
    }
    else
        throw std::runtime_error("Invalid transformation type specified.");

    return ret;
}

void YAF::Loader::LoadLightGlobals(ticpp::Element* elem)
{
    ticpp::Element* lightingElement = elem->FirstChildElement("lighting");
    gl::ColorRGBA globalAmbient;

    get_attribute(lightingElement, "ambient", globalAmbient);

    _graph.SetGlobalAmbient(globalAmbient);
    _graph.SetDoubleSided(parse_bool_from_string(lightingElement->GetAttribute("doublesided")));
    _graph.SetLocal(parse_bool_from_string(lightingElement->GetAttribute("local")));
    _graph.SetLigthtsEnabled(parse_bool_from_string(lightingElement->GetAttribute("enabled")));
}


