/*
 * graph.cpp
 *
 *  Created on: Sep 18, 2013
 *      Author: miguel
 */

#include <string>
#include <algorithm>

#include <unordered_map>
#include <memory>

#include "gl/animations.hpp"
#include "gl/lights/spot.hpp"
#include "gl/material.hpp"
#include "gl/texture.hpp"

#include "scene/composed_object.hpp"
#include "scene/graph.hpp"

YAF::Graph::Graph()
{
    _defaultMaterial = gl::make_material();
}

YAF::Graph::~Graph()
{
}

YAF::ComposedObjectPtr YAF::Graph::AddNode(const std::string& name)
{
    auto val = _nodes.emplace(name, make_composedObject(*this));
    if (!val.second)  throw std::runtime_error("Failed to insert node.");

    return val.first->second;
}

YAF::ComposedObjectPtr YAF::Graph::AddNode(const char* name)
{
    return AddNode(std::string(name));
}

YAF::ComposedObjectPtr YAF::Graph::GetNode(const std::string& name) const
{
    auto elem = _nodes.find(name);
    return elem == _nodes.end() ? nullptr : elem->second;
}

gl::PerspectiveCameraPtr YAF::Graph::AddPerspectiveCamera(const std::string& name)
{
    gl::PerspectiveCameraPtr ptr = gl::make_perspectiveCamera();
    auto val = _cameras.insert(std::make_pair(name, ptr));
    if (!val.second) throw std::runtime_error("Failed to insert camera: Camera with the same name already exists.");

    return ptr;
}

gl::OrthoCameraPtr YAF::Graph::AddOrthoCamera(const std::string& name)
{
    gl::OrthoCameraPtr ptr = gl::make_orthoCamera();
    auto val = _cameras.insert(std::make_pair(name, ptr));
    if (!val.second) throw std::runtime_error("Failed to insert camera: Camera with the same name already exists.");

    return ptr;
}

gl::CameraPtr YAF::Graph::GetCamera(const std::string& name) const
{
    auto elem = _cameras.find(name);
    return elem == _cameras.end() ? nullptr : elem->second;
}

gl::LightPtr YAF::Graph::AddLight(const std::string& name, GLuint id)
{
    auto val = _lights.insert(std::make_pair(name, gl::make_light(id)));
    if (!val.second) throw std::runtime_error("Failed to insert light: Light with the same name already exists.");

    return val.first->second;
}

gl::LightPtr YAF::Graph::GetLight(const std::string& name) const
{
    auto elem = _lights.find(name);
    return elem == _lights.end() ? nullptr : elem->second;
}

gl::SpotPtr YAF::Graph::AddSpotLight(const std::string& name, GLuint id)
{
    gl::SpotPtr ptr = gl::make_spot(id);
    auto val = _lights.insert(std::make_pair(name, ptr));
    if (!val.second) throw std::runtime_error("Failed to insert light: Light with the same name already exists.");

    return ptr;
}

gl::MaterialPtr YAF::Graph::AddMaterial(const std::string& name)
{
    auto val = _materials.insert(std::make_pair(name, gl::make_material()));
    if (!val.second) throw std::runtime_error("Failed to insert appearance.");

    return val.first->second;
}

gl::MaterialPtr YAF::Graph::GetMaterial(const std::string& name) const
{
    auto app = _materials.find(name);
    return app == _materials.end() ? nullptr : app->second;
}

gl::TexturePtr YAF::Graph::AddTexture(const std::string& name, const std::string& file)
{
    auto val =_textures.insert(std::make_pair(name, gl::Texture::Load(file)));
    if (!val.second) throw std::runtime_error("Failed to insert texture.");

    return val.first->second;
}

gl::TexturePtr YAF::Graph::GetTexture(const std::string& name)
{
    auto tex = _textures.find(name);
    return tex == _textures.end() ? nullptr : tex->second;
}

void YAF::Graph::SetRoot(const std::string& id)
{
    auto itr = _nodes.find(id);

    if (itr == _nodes.end())
        throw std::runtime_error("SetRoot: invalid id specified: '" + id + "'.");
    else
        _root = itr->second;
}

void YAF::Graph::SetBackground(const gl::ColorRGBA& background)
{
    _background = background;
}

const gl::ColorRGBA& YAF::Graph::GetBackground() const
{
    return _background;
}

void YAF::Graph::SetDrawMode(const std::string& drawMode)
{
    if (drawMode == "fill")
        SetDrawMode(GL_FILL);
    else if (drawMode == "line")
        SetDrawMode(GL_LINE);
    else if (drawMode == "point")
        SetDrawMode(GL_POINT);
    else
        throw std::runtime_error("SetDrawMode: invalid mode specified: " + drawMode + ".");
}

void YAF::Graph::SetDrawMode(GLenum mode)
{
    if (mode != GL_LINE && mode != GL_POINT && mode != GL_FILL) return;

    _drawMode = mode;
}

void YAF::Graph::SetShading(const std::string& shading)
{
    if (shading == "flat")
        _shading = GL_FLAT;
    else if (shading == "gouraud")
        _shading = GL_SMOOTH;
    else
        throw std::runtime_error("SetShading: invalid mode specified: " + shading + ".");
}

void YAF::Graph::SetCullface(const std::string& cullface)
{
    if (cullface == "none")
        _cullface = GL_NONE;
    else if (cullface == "back")
        _cullface = GL_BACK;
    else if (cullface == "front")
        _cullface = GL_FRONT;
    else if (cullface == "both")
        _cullface = GL_FRONT_AND_BACK;
    else
        throw std::runtime_error("SetCullface: invalid mode specified: " + cullface + ".");
}

void YAF::Graph::SetCullorder(const std::string& cullorder)
{
    if (cullorder == "CCW")
        _cullorder = GL_CCW;
    else if (cullorder == "CW")
        _cullorder = GL_CW;
    else
        throw std::runtime_error("SetCullorder: invalid mode specified: " + cullorder + ".");
}

void YAF::Graph::SetDoubleSided(bool doubleSided)
{
    _doubleSided = doubleSided;
}

void YAF::Graph::SetLocal(bool local)
{
    _local = local;
}

void YAF::Graph::SetLigthtsEnabled(bool enabled)
{
    _lightsEnabled = enabled;
}

void YAF::Graph::SetGlobalAmbient(const gl::ColorRGBA& ambient)
{
    _ambient = ambient;
}

void YAF::Graph::Init()
{
    _root->init(_defaultMaterial);
}

void YAF::Graph::draw() const
{
    glPushMatrix();

    if (_lightsEnabled)
        for (auto itr = _lights.begin(); itr != _lights.end(); ++itr)
            itr->second->Update();

    _root->draw(_defaultMaterial);
    glPopMatrix();
}

const GLenum YAF::Graph::GetDrawMode() const
{
    return _drawMode;
}

const GLenum YAF::Graph::GetCullface() const
{
    return _cullface;
}

const GLenum YAF::Graph::GetCullorder() const
{
    return _cullorder;
}

const GLenum YAF::Graph::GetShading() const
{
    return _shading;
}

const bool YAF::Graph::GetDoubleSided() const
{
    return _doubleSided;
}

const bool YAF::Graph::GetLightsEnabled() const
{
    return _lightsEnabled;
}

const bool YAF::Graph::GetLocal() const
{
    return _local;
}

const gl::ColorRGBA& YAF::Graph::GetGlobalAmbient() const
{
    return _ambient;
}

void YAF::Graph::SetInitialCamera(const std::string& id)
{
    auto elem = _cameras.find(id);
    if (elem == _cameras.end()) throw std::runtime_error("Unrecognized initial camera.");

    _initialCamera = elem->second;
    _initialCameraId = id;
}
const gl::CameraPtr YAF::Graph::GetInitialCamera() const
{
    return _initialCamera;
}

std::string YAF::Graph::GetInitialCameraId() const
{
    return _initialCameraId;
}

std::vector<std::string> YAF::Graph::GetCameraNames() const
{
    std::vector<std::string> result;
    result.reserve(_cameras.size());

    for (auto itr = _cameras.begin(); itr != _cameras.end(); ++itr)
        result.push_back(itr->first);

    std::sort(result.begin(), result.end());

    return result;
}

std::vector<std::string> YAF::Graph::GetLightNames() const
{
    std::vector<std::string> result;
    result.reserve(_lights.size());

    for (auto itr = _lights.begin(); itr != _lights.end(); ++itr)
        result.push_back(itr->first);

    std::sort(result.begin(), result.end());

    return result;
}

void YAF::Graph::Update(double time)
{
    for (auto itr = _animations.begin(); itr != _animations.end(); ++itr)
        itr->second->Update(time);

    _root->update(time);
}

gl::AnimationPtr YAF::Graph::GetAnimation( const std::string& name ) const
{
    auto anim = _animations.find(name);
    return anim == _animations.end() ? nullptr : anim->second;
}

gl::LinearAnimationPtr YAF::Graph::AddLinearAnimation( const std::string& name, float time, std::vector<glm::vec3> controlPoints )
{
    auto anim = std::make_shared<gl::Animations::Linear>(time, controlPoints);

    auto val = _animations.insert(std::make_pair(name, anim));
    if (!val.second) throw std::runtime_error("Failed to insert appearance.");

    return anim;
}


