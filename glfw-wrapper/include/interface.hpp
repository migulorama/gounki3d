#ifndef INTERFACE_HPP__
#define INTERFACE_HPP__

#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <cstdint>
#include <unordered_map>
#include <functional>

namespace framework
{

class application;

class input_listener
{
public:
    virtual void mouse_move(double x, double y) = 0;
    virtual void mouse_button(int button, int action, int mods) = 0;
    virtual void mouse_scroll(double xOffset, double yOffset) = 0;
    virtual void key_action(int key, int scancode, int action, int mods) = 0;
    virtual void char_input(unsigned int character) = 0;
};

class interface : public input_listener
{
public:
    interface() : _key_mods(0), modifiers(0) { }

    void init(framework::application* app);
    virtual void init_gui() {}
    void display();
    void reshape(int width, int height);

    virtual void mouse_move(double x, double y) override final;
    virtual void mouse_button(int button, int action, int mods) override final;
    virtual void mouse_scroll(double xOffset, double yOffset) override final;
    virtual void key_action(int key, int scancode, int action, int mods) override final;
    virtual void char_input(unsigned int character) override final;

protected:

    virtual void mouse_move_handler(double x, double y) { }
    virtual void mouse_button_handler(int button, int action) { }
    virtual void mouse_scroll_handler(double xOffset, double yOffset) { }
    virtual void key_action_handler(int key, int scancode, int action) { }
    virtual void char_input_handler(unsigned int character) { }

    virtual void reshape_handler(int width, int height) { }

    framework::application* _application;

    int modifiers;

private:
    int _key_mods;
};

}

#endif // INTERFACE_HPP__
