#ifndef WINDOW_HINTS_HPP__
#define WINDOW_HINTS_HPP__

#include <unordered_map>

namespace framework
{

enum class client_api : int
{
    OpenGL,
    OpenGL_ES
};  

enum class context_robustness
{
    None,
    NoResetNotification,
    LoseContextOnReset
};

enum class profile
{
    Any,
    Compat,
    Core
};

class window_hints
{
    friend class window;
public:
    window_hints& set_resizable(bool value);
    window_hints& set_visible(bool value);
    window_hints& set_decorated(bool value);
    window_hints& set_red_bits(unsigned int value);
    window_hints& set_green_bits(unsigned int value);
    window_hints& set_blue_bits(unsigned int value);
    window_hints& set_alpha_bits(unsigned int value);
    window_hints& set_depth_bits(unsigned int value);
    window_hints& set_stencil_bits(unsigned int value);
    window_hints& set_accum_red_bits(unsigned int value);
    window_hints& set_accum_green_bits(unsigned int value);
    window_hints& set_accum_blue_bits(unsigned int value);
    window_hints& set_accum_alpha_bits(unsigned int value);
    window_hints& set_aux_buffers(unsigned int value);
    window_hints& set_samples(unsigned int value);
    window_hints& set_refresh_rate(unsigned int value);
    window_hints& set_stereo(bool value);
    window_hints& set_srgb_capable(bool value);
    window_hints& set_client_api(const client_api& value);
    window_hints& set_context_version(unsigned int major, unsigned int minor = 0);
    window_hints& set_context_robustness(const context_robustness& value);
    window_hints& set_opengl_forward_compat(bool value);
    window_hints& set_opengl_debug_context(bool value);
    window_hints& set_opengl_profile(const profile& value);

    bool get_resizable() const;
    bool get_visible() const;
    bool get_decorated() const;
    unsigned int get_red_bits() const;
    unsigned int get_green_bits() const;
    unsigned int get_blue_bits() const;
    unsigned int get_alpha_bits() const;
    unsigned int get_depth_bits() const;
    unsigned int get_stencil_bits() const;
    unsigned int get_accum_red_bits() const;
    unsigned int get_accum_green_bits() const;
    unsigned int get_accum_blue_bits() const;
    unsigned int get_accum_alpha_bits() const;
    unsigned int get_aux_buffers() const;
    unsigned int get_samples() const;
    unsigned int get_refresh_rate() const;
    bool get_stereo() const;
    bool get_srgb_capable() const;
    client_api get_client_api() const;
    unsigned int get_context_version_major() const;
    unsigned int get_context_version_minor() const;
    context_robustness get_context_robustness() const;
    bool get_opengl_forward_compat() const;
    bool get_opengl_debug_context() const;
    profile get_opengl_profile() const;

private:
    void apply() const;

    std::unordered_map<int, int> _hints;
};
}

#endif // WINDOW_HINTS_HPP__
