#ifndef SCENE_HPP__
#define SCENE_HPP__

namespace framework
{

class application;

class scene
{
public:
    scene() { }
    virtual ~scene() { }

    void init(framework::application* app);
    virtual void init() = 0;
    virtual void update(double time) { }
    virtual void display() = 0;
    virtual void reshape(int width, int height) { }

    void check_update();

protected:
    void set_update_period(double interval);
    
    framework::application* _application;
    double _last_update;
    double _update_period;
};

}

#endif // SCENE_HPP__
