#ifndef VERSION_HPP__
#define VERSION_HPP__

#include <ostream>

namespace framework
{
    struct version
    {
        version(unsigned int maj = 0, unsigned int min = 0, unsigned int rev = 0) : major(maj), minor(min), revision(rev) { }
        unsigned int major;
        unsigned int minor;
        unsigned int revision;
    };
}

inline std::ostream& operator << (std::ostream& out, const framework::version& ver)
{
    out << ver.major << "." << ver.minor << "." << ver.revision;
    return out;
}

#endif // VERSION_HPP__
