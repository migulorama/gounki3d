#ifndef APPLICATION_HPP__
#define APPLICATION_HPP__

#include <memory>

#include "window.hpp"

namespace framework
{

class scene;
class interface;

class application
{
public:
    application();

    void init();
    void run();
    void terminate();

    void set_scene(std::shared_ptr<scene> sc);
    void set_interface(std::shared_ptr<interface> iface);

    std::shared_ptr<scene> get_scene() const;
    std::shared_ptr<interface> get_interface() const;

    void reshape(); 
    void reshape(int width, int height);

    window* get_window() { return &_window; }

    double get_time() const;

    void force_refresh();

private:
    window _window;

    bool _refresh_requested;

    std::shared_ptr<scene> _scene;
    std::shared_ptr<interface> _interface;
};

}

#endif // APPLICATION_HPP__
