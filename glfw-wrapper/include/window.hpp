#ifndef WINDOW_HPP__
#define WINDOW_HPP__

#include <functional>
#include <string>

#include <memory>
#include <glm/glm.hpp>

#include "window_hints.hpp"
#include "version.hpp"

#include <GLFW/glfw3.h>

namespace framework
{

class input_listener;

class window
{
public:
    window();

    ~window();

    void init(int width, int height, const std::string& title);
    void init(int width, int height, const std::string& title, const window_hints& hints);

    void init_fullscreen();
    void init_fullscreen(int width, int height);
    void init_fullscreen(int width, int height, const window_hints& hints);
    void init_fullscreen(window_hints hints);

    void close();

    bool is_open() const;

    void swap_buffers();
    void poll_events();

    void activate();

    bool should_close() const;
    void set_should_close(bool val);

    bool is_focused() const;

    bool is_iconified() const;
    void iconify();
    void restore();

    bool is_visible() const;
    void show();
    void hide();

    bool is_resizable() const;
    bool is_decorated() const;

    client_api get_client_api() const;
    version get_context_version() const;
    bool is_context_forward_compat() const;
    bool is_context_debug() const;
    profile get_opengl_profile() const;
    context_robustness get_context_robustness() const;

    void set_title(const std::string& title);
    std::string get_title() const;

    void set_position(const glm::uvec2& pos);
    void set_position(unsigned int x, unsigned int y);
    glm::uvec2 get_position() const;

    void set_size(const glm::uvec2& pos);
    void set_size(unsigned int x, unsigned int y);
    glm::uvec2 get_size() const;

    glm::uvec2 get_framebuffer_size();
    unsigned int get_framebuffer_width();
    unsigned int get_framebuffer_height();

    int get_unique_identifier() const;

    void set_fb_size_changed_callback(const std::function<void(int, int)> call);

    void set_input_listener(input_listener* listener);

protected:
    static void window_fb_size_fun(GLFWwindow*, int, int);
    static void mouse_button_fun(GLFWwindow*, int, int, int);
    static void mouse_pos_fun(GLFWwindow*, double, double);
    static void mouse_enter_fun(GLFWwindow*, int);
    static void mouse_scroll_fun(GLFWwindow*, double, double);
    static void key_fun(GLFWwindow*, int, int, int, int);
    static void char_fun(GLFWwindow*, unsigned int);

    void fb_size_changed(int width, int height);

private:
    void _init(int width, int height, const std::string& title, bool fullscreen, const window_hints& hints);

    std::string _title;

    input_listener* _input_listener;
    std::function<void(int, int)> _fb_size_changed_callback;

    GLFWwindow* _window;
    int _unique_identifier;
    static int _last_id;
};

}

#endif // WINDOW_HPP__
