#include "window_hints.hpp"

#include <gl_4_4.h>
#include <GLFW/glfw3.h>

using namespace framework;

template <class Enum>
int from_enum(Enum) { static_assert(false, "Invalid Enum type."); }

template <>
int from_enum(context_robustness ca)
{
    switch (ca)
    {
    case context_robustness::None:
        return GLFW_NO_ROBUSTNESS;
    case context_robustness::NoResetNotification:
        return GLFW_NO_RESET_NOTIFICATION;
    case context_robustness::LoseContextOnReset:
        return GLFW_LOSE_CONTEXT_ON_RESET;
    default:
        throw std::invalid_argument("Invalid Context Robustness.");
    }
}

template <>
int from_enum(profile ca)
{
    switch (ca)
    {
    case profile::Any:
        return GLFW_OPENGL_ANY_PROFILE;
    case profile::Compat:
        return GLFW_OPENGL_COMPAT_PROFILE;
    case profile::Core:
        return GLFW_OPENGL_CORE_PROFILE;
    default:
        throw std::invalid_argument("Invalid OpenGL Profile.");
    }
}

template <>
int from_enum(client_api ca)
{
    switch (ca)
    {
    case client_api::OpenGL:
        return GLFW_OPENGL_API;
    case client_api::OpenGL_ES:
        return GLFW_OPENGL_ES_API;
    default:
        throw std::invalid_argument("Invalid Client API.");
    }
}

template <class Enum>
Enum to_enum(int) { static_assert(false, "Invalid Enum type."); }

template <>
context_robustness to_enum(int ca)
{
    switch (ca)
    {
    case GLFW_NO_ROBUSTNESS:
        return context_robustness::None;
    case GLFW_NO_RESET_NOTIFICATION:
        return context_robustness::NoResetNotification;
    case GLFW_LOSE_CONTEXT_ON_RESET:
        return context_robustness::LoseContextOnReset;
    default:
        throw std::invalid_argument("Invalid Context Robustness integer.");
    }
}

template <>
profile to_enum(int ca)
{
    switch (ca)
    {
    case GLFW_OPENGL_ANY_PROFILE:
        return profile::Any;
    case GLFW_OPENGL_COMPAT_PROFILE:
        return profile::Compat;
    case GLFW_OPENGL_CORE_PROFILE:
        return profile::Core;
    default:
        throw std::invalid_argument("Invalid OpenGL Profile integer.");
    }
}

template <>
client_api to_enum(int ca)
{
    switch (ca)
    {
    case GLFW_OPENGL_API:
        return client_api::OpenGL;
    case GLFW_OPENGL_ES_API:
        return client_api::OpenGL_ES;
    default:
        throw std::invalid_argument("Invalid Client API integer.");
    }
}

void window_hints::apply() const
{
    glfwDefaultWindowHints();
    for (const auto& hint : _hints)
        glfwWindowHint(hint.first, hint.second);
}

window_hints& window_hints::set_resizable(bool value)
{
    _hints[GLFW_RESIZABLE] = value ? GL_TRUE : GL_FALSE; return *this;
}

window_hints& window_hints::set_visible(bool value)
{
    _hints[GLFW_VISIBLE] = value ? GL_TRUE : GL_FALSE; return *this;
}

window_hints& window_hints::set_decorated(bool value)
{
    _hints[GLFW_DECORATED] = value ? GL_TRUE : GL_FALSE; return *this;
}

window_hints& window_hints::set_red_bits(unsigned int value)
{
    _hints[GLFW_RED_BITS] = value; return *this;
}

window_hints& window_hints::set_green_bits(unsigned int value)
{
    _hints[GLFW_GREEN_BITS] = value; return *this;
}

window_hints& window_hints::set_blue_bits(unsigned int value)
{
    _hints[GLFW_BLUE_BITS] = value; return *this;
}

window_hints& window_hints::set_alpha_bits(unsigned int value)
{
    _hints[GLFW_ALPHA_BITS] = value; return *this;
}

window_hints& window_hints::set_depth_bits(unsigned int value)
{
    _hints[GLFW_DEPTH_BITS] = value; return *this;
}

window_hints& window_hints::set_stencil_bits(unsigned int value)
{
    _hints[GLFW_STENCIL_BITS] = value; return *this;
}

window_hints& window_hints::set_accum_red_bits(unsigned int value)
{
    _hints[GLFW_ACCUM_RED_BITS] = value; return *this;
}

window_hints& window_hints::set_accum_green_bits(unsigned int value)
{
    _hints[GLFW_ACCUM_GREEN_BITS] = value; return *this;
}

window_hints& window_hints::set_accum_blue_bits(unsigned int value)
{
    _hints[GLFW_ACCUM_BLUE_BITS] = value; return *this;
}

window_hints& window_hints::set_accum_alpha_bits(unsigned int value)
{
    _hints[GLFW_ACCUM_ALPHA_BITS] = value; return *this;
}

window_hints& window_hints::set_aux_buffers(unsigned int value)
{
    _hints[GLFW_AUX_BUFFERS] = value; return *this;
}

window_hints& window_hints::set_samples(unsigned int value)
{
    _hints[GLFW_SAMPLES] = value; return *this;
}

window_hints& window_hints::set_refresh_rate(unsigned int value)
{
    _hints[GLFW_REFRESH_RATE] = value; return *this;
}

window_hints& window_hints::set_stereo(bool value)
{
    _hints[GLFW_STEREO] = value ? GL_TRUE : GL_FALSE; return *this;
}

window_hints& window_hints::set_srgb_capable(bool value)
{
    _hints[GLFW_SRGB_CAPABLE] = value ? GL_TRUE : GL_FALSE; return *this;
}

window_hints& window_hints::set_client_api(const client_api& value)
{
    _hints[GLFW_CLIENT_API] = from_enum(value); return *this;
}

window_hints& window_hints::set_context_version(unsigned int major, unsigned int minor/* = 0*/)
{
    _hints[GLFW_CONTEXT_VERSION_MAJOR] = major;
    _hints[GLFW_CONTEXT_VERSION_MINOR] = minor; return *this;
}

window_hints& window_hints::set_context_robustness(const context_robustness& value)
{
    _hints[GLFW_CONTEXT_ROBUSTNESS] = from_enum(value); return *this;
}

window_hints& window_hints::set_opengl_forward_compat(bool value)
{
    _hints[GLFW_OPENGL_FORWARD_COMPAT] = value ? GL_TRUE : GL_FALSE; return *this;
}

window_hints& window_hints::set_opengl_debug_context(bool value)
{
    _hints[GLFW_OPENGL_DEBUG_CONTEXT] = value ? GL_TRUE : GL_FALSE; return *this;
}

window_hints& window_hints::set_opengl_profile(const profile& value)
{
    _hints[GLFW_OPENGL_PROFILE] = from_enum(value); return *this;
}


bool window_hints::get_resizable() const
{
    auto elem = _hints.find(GLFW_RESIZABLE);
    return elem != _hints.end() ? elem->second == GL_TRUE : true;
}

bool window_hints::get_visible() const
{
    auto elem = _hints.find(GLFW_VISIBLE);
    return elem != _hints.end() ? elem->second == GL_TRUE : true;
}

bool window_hints::get_decorated() const
{
    auto elem = _hints.find(GLFW_DECORATED);
    return elem != _hints.end() ? elem->second == GL_TRUE : true;
}

unsigned int window_hints::get_red_bits() const
{
    auto elem = _hints.find(GLFW_RED_BITS);
    return elem != _hints.end() ? elem->second : 8;
}

unsigned int window_hints::get_green_bits() const
{
    auto elem = _hints.find(GLFW_GREEN_BITS);
    return elem != _hints.end() ? elem->second : 8;
}

unsigned int window_hints::get_blue_bits() const
{
    auto elem = _hints.find(GLFW_BLUE_BITS);
    return elem != _hints.end() ? elem->second : 8;
}

unsigned int window_hints::get_alpha_bits() const
{
    auto elem = _hints.find(GLFW_ALPHA_BITS);
    return elem != _hints.end() ? elem->second : 8;
}

unsigned int window_hints::get_depth_bits() const
{
    auto elem = _hints.find(GLFW_DEPTH_BITS);
    return elem != _hints.end() ? elem->second : 24;
}

unsigned int window_hints::get_stencil_bits() const
{
    auto elem = _hints.find(GLFW_STENCIL_BITS);
    return elem != _hints.end() ? elem->second : 8;
}

unsigned int window_hints::get_accum_red_bits() const
{
    auto elem = _hints.find(GLFW_ACCUM_RED_BITS);
    return elem != _hints.end() ? elem->second : 0;
}

unsigned int window_hints::get_accum_green_bits() const
{
    auto elem = _hints.find(GLFW_ACCUM_GREEN_BITS);
    return elem != _hints.end() ? elem->second : 0;
}

unsigned int window_hints::get_accum_blue_bits() const
{
    auto elem = _hints.find(GLFW_ACCUM_BLUE_BITS);
    return elem != _hints.end() ? elem->second : 0;
}

unsigned int window_hints::get_accum_alpha_bits() const
{
    auto elem = _hints.find(GLFW_ACCUM_ALPHA_BITS);
    return elem != _hints.end() ? elem->second : 0;
}

unsigned int window_hints::get_aux_buffers() const
{
    auto elem = _hints.find(GLFW_AUX_BUFFERS);
    return elem != _hints.end() ? elem->second : 0;
}

unsigned int window_hints::get_samples() const
{
    auto elem = _hints.find(GLFW_SAMPLES);
    return elem != _hints.end() ? elem->second : 0;
}

unsigned int window_hints::get_refresh_rate() const
{
    auto elem = _hints.find(GLFW_REFRESH_RATE);
    return elem != _hints.end() ? elem->second : 0;
}

bool window_hints::get_stereo() const
{
    auto elem = _hints.find(GLFW_STEREO);
    return elem != _hints.end() ? elem->second == GL_TRUE : false;
}

bool window_hints::get_srgb_capable() const
{
    auto elem = _hints.find(GLFW_SRGB_CAPABLE);
    return elem != _hints.end() ? elem->second == GL_TRUE : false;
}

client_api window_hints::get_client_api() const
{
    auto elem = _hints.find(GLFW_CLIENT_API);
    return elem != _hints.end() ? to_enum<client_api>(elem->second) : client_api::OpenGL;
}

unsigned int window_hints::get_context_version_major() const
{
    auto elem = _hints.find(GLFW_CONTEXT_VERSION_MAJOR);
    return elem != _hints.end() ? elem->second : 1;
}

unsigned int window_hints::get_context_version_minor() const
{
    auto elem = _hints.find(GLFW_CONTEXT_VERSION_MINOR);
    return elem != _hints.end() ? elem->second : 0;
}

context_robustness window_hints::get_context_robustness() const
{
    auto elem = _hints.find(GLFW_CONTEXT_ROBUSTNESS);
    return elem != _hints.end() ? to_enum<context_robustness>(elem->second) : context_robustness::None;
}

bool window_hints::get_opengl_forward_compat() const
{
    auto elem = _hints.find(GLFW_OPENGL_FORWARD_COMPAT);
    return elem != _hints.end() ? elem->second == GL_TRUE : false;
}

bool window_hints::get_opengl_debug_context() const
{
    auto elem = _hints.find(GLFW_OPENGL_DEBUG_CONTEXT);
    return elem != _hints.end() ? elem->second == GL_TRUE : false;

}

profile window_hints::get_opengl_profile() const
{
    auto elem = _hints.find(GLFW_OPENGL_PROFILE);
    return elem != _hints.end() ? to_enum<profile>(elem->second) : profile::Any;
}
