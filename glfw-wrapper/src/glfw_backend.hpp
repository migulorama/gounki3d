#ifndef GLFW_BACKEND_HPP__
#define GLFW_BACKEND_HPP__

#include <string>
#include <stdexcept>

struct GLFWwindow;
struct GLFWmonitor;

namespace framework
{
    namespace details
    {
        class glfw
        {
        public:
            glfw();
            ~glfw();

            static GLFWwindow* create_window(int width, int height, const char* title, GLFWmonitor* monitor);

            static const std::string& get_last_error();
        private:
            static void error_callback(int code, const char* str);

            static std::string _lastError;

            static GLFWwindow* _window;
        };
    }

    namespace glfw
    {
        const std::string& get_last_error();

        void check_init();

        inline GLFWwindow* create_window(int width, int height, const char* title, GLFWmonitor* monitor) { return details::glfw::create_window(width, height, title, monitor); }
        inline GLFWwindow* create_window(int width, int height, const std::string& title, GLFWmonitor* monitor) { return details::glfw::create_window(width, height, title.c_str(), monitor); }

        class temp_context
        {
        public:
            temp_context(GLFWwindow* ctx);
            ~temp_context();

        private:
            GLFWwindow* _prev;
        };
    }
}

#endif // GLFW_BACKEND_HPP__
