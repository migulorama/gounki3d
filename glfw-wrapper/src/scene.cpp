#include "scene.hpp"
#include "application.hpp"

void framework::scene::init(framework::application* app)
{
    _application = app;
    _last_update = _application->get_time();
    set_update_period(0.0);
    init();
}

void framework::scene::check_update()
{
    if (_update_period <= 0.0) return;

    double current_time = _application->get_time();
    if (current_time - _last_update < _update_period) return;

    update(current_time);
    _last_update = current_time;
}

void framework::scene::set_update_period(double interval)
{
    _update_period = interval;
}
