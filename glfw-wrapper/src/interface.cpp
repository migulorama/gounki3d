#include "interface.hpp"

#include "application.hpp"

void framework::interface::init(framework::application* app)
{
    _application = app;
    init_gui();
}

void framework::interface::display()
{

}

void framework::interface::reshape(int width, int height)
{
    reshape_handler(width, height);
}


void framework::interface::key_action(int key, int scancode, int action, int mods)
{
    modifiers = mods;
    key_action_handler(key, scancode, action);
}

void framework::interface::mouse_scroll(double xOffset, double yOffset)
{
    mouse_scroll_handler(xOffset, yOffset);
}

void framework::interface::mouse_button(int button, int action, int mods)
{
    mouse_button_handler(button, action);
}

void framework::interface::mouse_move(double x, double y)
{
    mouse_move_handler(x, y);
}

void framework::interface::char_input(unsigned int character)
{
    char_input_handler(character);
}
