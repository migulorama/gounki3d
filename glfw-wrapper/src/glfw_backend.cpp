#include "glfw_backend.hpp"

#include <GLFW/glfw3.h>
#include <gl_4_4.h>


std::string framework::details::glfw::_lastError("No Error");

GLFWwindow* framework::details::glfw::_window = nullptr;

framework::details::glfw::glfw()
{
    if (!glfwInit())
        throw std::runtime_error("Failed to initialize glfw.");

    glfwSetErrorCallback(error_callback);

    glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
    _window = glfwCreateWindow(1, 1, "Hidden Window", nullptr, nullptr);
    if (!_window)
        throw std::runtime_error("Failed to create default window.");
    glfwDefaultWindowHints();

}

framework::details::glfw::~glfw()
{
    glfwDestroyWindow(_window);
    glfwTerminate();
}

const std::string& framework::details::glfw::get_last_error()
{
    return _lastError;
}

void framework::details::glfw::error_callback(int code, const char* str)
{
    _lastError = str;
}

GLFWwindow* framework::details::glfw::create_window(int width, int height, const char* title, GLFWmonitor* monitor)
{
    return glfwCreateWindow(width, height, title, monitor, _window);
}

const std::string& framework::glfw::get_last_error()
{
    return details::glfw::get_last_error();
}

void framework::glfw::check_init()
{
    static details::glfw dummie;
}

framework::glfw::temp_context::temp_context(GLFWwindow* ctx)
{
    _prev = glfwGetCurrentContext();
    glfwMakeContextCurrent(ctx);
}

framework::glfw::temp_context::~temp_context()
{
    glfwMakeContextCurrent(_prev);
}
