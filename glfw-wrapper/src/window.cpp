#include "window.hpp"
#include "glfw_backend.hpp"

#include <memory>
#include <GLFW/glfw3.h>
#include <gl_4_4.h>

#include "window_hints.hpp"
#include "interface.hpp"

template <class Enum>
Enum to_enum_2(int) { static_assert(false, "Invalid Enum type."); }

template <>
framework::context_robustness to_enum_2(int ca)
{
    switch (ca)
    {
    case GLFW_NO_ROBUSTNESS:
        return framework::context_robustness::None;
    case GLFW_NO_RESET_NOTIFICATION:
        return framework::context_robustness::NoResetNotification;
    case GLFW_LOSE_CONTEXT_ON_RESET:
        return framework::context_robustness::LoseContextOnReset;
    default:
        throw std::invalid_argument("Invalid Context Robustness integer.");
    }
}

template <>
framework::profile to_enum_2(int ca)
{
    switch (ca)
    {
    case GLFW_OPENGL_ANY_PROFILE:
        return framework::profile::Any;
    case GLFW_OPENGL_COMPAT_PROFILE:
        return framework::profile::Compat;
    case GLFW_OPENGL_CORE_PROFILE:
        return framework::profile::Core;
    default:
        throw std::invalid_argument("Invalid OpenGL Profile integer.");
    }
}

template <>
framework::client_api to_enum_2(int ca)
{
    switch (ca)
    {
    case GLFW_OPENGL_API:
        return framework::client_api::OpenGL;
    case GLFW_OPENGL_ES_API:
        return framework::client_api::OpenGL_ES;
    default:
        throw std::invalid_argument("Invalid Client API integer.");
    }
}


int framework::window::_last_id = 0;

framework::window::window() : _window(nullptr), _fb_size_changed_callback(nullptr), _unique_identifier(0)
{
    glfw::check_init();
    _input_listener = nullptr;
}

framework::window::~window()
{
    close();
}

void framework::window::init(int width, int height, const std::string& title)
{
    _init(width, height, title, false, window_hints());
}

void framework::window::init(int width, int height, const std::string& title, const window_hints& hints)
{
    _init(width, height, title, false, hints);
}

void framework::window::init_fullscreen()
{
    init_fullscreen(framework::window_hints());
}

void framework::window::init_fullscreen(int width, int height)
{
    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(monitor);

    window_hints hints;
    hints.set_red_bits(mode->redBits)
         .set_green_bits(mode->greenBits)
         .set_blue_bits(mode->blueBits)
         .set_refresh_rate(mode->refreshRate);

    _init(width, height, "", true, hints);
}

void framework::window::init_fullscreen(int width, int height, const window_hints& hints)
{
    _init(width, height, "", true, hints);
}

void framework::window::init_fullscreen(window_hints hints)
{
    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(monitor);

    hints.set_red_bits(mode->redBits)
         .set_green_bits(mode->greenBits)
         .set_blue_bits(mode->blueBits)
         .set_refresh_rate(mode->refreshRate);

    _init(mode->width, mode->height, "", true, hints);
}

void framework::window::close()
{
    if (is_open())
    {
        glfwDestroyWindow(_window);
        _window = nullptr;
    }
}

bool framework::window::is_open() const
{
    return _window != nullptr;
}

void framework::window::swap_buffers()
{
    glfwSwapBuffers(_window);
}

void framework::window::poll_events()
{
    glfwPollEvents();
}

void framework::window::activate()
{
    glfwMakeContextCurrent(_window);
}

bool framework::window::should_close() const
{
    return !is_open() || (glfwWindowShouldClose(_window) == 1);
}

void framework::window::set_should_close(bool val)
{
    glfwSetWindowShouldClose(_window, val ? GL_TRUE : GL_FALSE);
}

bool framework::window::is_focused() const
{
    return glfwGetWindowAttrib(_window, GLFW_FOCUSED) == GL_TRUE;
}

bool framework::window::is_iconified() const
{
    return glfwGetWindowAttrib(_window, GLFW_ICONIFIED) == GL_TRUE;
}

void framework::window::iconify()
{
    glfwIconifyWindow(_window);
}

void framework::window::restore()
{
    glfwRestoreWindow(_window);
}

bool framework::window::is_visible() const
{
    return glfwGetWindowAttrib(_window, GLFW_VISIBLE) == GL_TRUE;
}

void framework::window::show()
{
    glfwShowWindow(_window);
}

void framework::window::hide()
{
    glfwHideWindow(_window);
}

bool framework::window::is_resizable() const
{
    return glfwGetWindowAttrib(_window, GLFW_RESIZABLE) == GL_TRUE;
}

bool framework::window::is_decorated() const
{
    return glfwGetWindowAttrib(_window, GLFW_DECORATED) == GL_TRUE;
}

framework::client_api framework::window::get_client_api() const
{
    return to_enum_2<client_api>(glfwGetWindowAttrib(_window, GLFW_CLIENT_API));
}

framework::version framework::window::get_context_version() const
{
    auto maj = glfwGetWindowAttrib(_window, GLFW_CONTEXT_VERSION_MAJOR);
    auto min = glfwGetWindowAttrib(_window, GLFW_CONTEXT_VERSION_MINOR);
    auto rev = glfwGetWindowAttrib(_window, GLFW_CONTEXT_REVISION);

    return version(maj, min, rev);
}

bool framework::window::is_context_forward_compat() const
{
    return glfwGetWindowAttrib(_window, GLFW_OPENGL_FORWARD_COMPAT) == GL_TRUE;
}

bool framework::window::is_context_debug() const
{
    return glfwGetWindowAttrib(_window, GLFW_OPENGL_DEBUG_CONTEXT) == GL_TRUE;
}

framework::profile framework::window::get_opengl_profile() const
{
    return to_enum_2<profile>(glfwGetWindowAttrib(_window, GLFW_OPENGL_PROFILE));
}

framework::context_robustness framework::window::get_context_robustness() const
{
    return to_enum_2<context_robustness>(glfwGetWindowAttrib(_window, GLFW_CONTEXT_ROBUSTNESS));
}

void framework::window::set_title(const std::string& title)
{
    _title = title;
    glfwSetWindowTitle(_window, title.c_str());
}

std::string framework::window::get_title() const
{
    return _title;
}

void framework::window::set_position(const glm::uvec2& pos)
{
    glfwSetWindowPos(_window, pos.x, pos.y);
}

void framework::window::set_position(unsigned int x, unsigned int y)
{
    glfwSetWindowPos(_window, x, y);
}

glm::uvec2 framework::window::get_position() const
{
    int x;
    int y;

    glfwGetWindowPos(_window, &x, &y);

    return glm::uvec2(x, y);
}

void framework::window::set_size(const glm::uvec2& size)
{
    glfwSetWindowSize(_window, size.x, size.y);
}

void framework::window::set_size(unsigned int x, unsigned int y)
{
    glfwSetWindowSize(_window, x, y);
}

glm::uvec2 framework::window::get_size() const
{
    int width;
    int height;

    glfwGetWindowSize(_window, &width, &height);

    return glm::uvec2(width, height);
}

glm::uvec2 framework::window::get_framebuffer_size()
{
    activate();

    int width;
    int height;

    glfwGetFramebufferSize(_window, &width, &height);

    return glm::uvec2(width, height);
}

unsigned int framework::window::get_framebuffer_width()
{
    activate();

    int width;
    int height;

    glfwGetFramebufferSize(_window, &width, &height);

    return width;
}

unsigned int framework::window::get_framebuffer_height()
{
    activate();

    int width;
    int height;

    glfwGetFramebufferSize(_window, &width, &height);

    return height;
}

void framework::window::window_fb_size_fun(GLFWwindow* win, int width, int height)
{
    framework::window* window = reinterpret_cast<framework::window*>(glfwGetWindowUserPointer(win));
    window->fb_size_changed(width, height);
}

void framework::window::mouse_button_fun(GLFWwindow* win, int button, int action, int mods)
{
    framework::window* window = reinterpret_cast<framework::window*>(glfwGetWindowUserPointer(win));
    window->activate();
    if (window->_input_listener) window->_input_listener->mouse_button(button, action, mods);
}

void framework::window::mouse_pos_fun(GLFWwindow* win, double x, double y)
{
    framework::window* window = reinterpret_cast<framework::window*>(glfwGetWindowUserPointer(win));
    window->activate();
    if (window->_input_listener) window->_input_listener->mouse_move(x, y);
}

void framework::window::mouse_enter_fun(GLFWwindow* win, int)
{
    framework::window* window = reinterpret_cast<framework::window*>(glfwGetWindowUserPointer(win));
}

void framework::window::mouse_scroll_fun(GLFWwindow* win, double x, double y)
{
    framework::window* window = reinterpret_cast<framework::window*>(glfwGetWindowUserPointer(win));
    window->activate();
    if (window->_input_listener) window->_input_listener->mouse_scroll(x, y);
}

void framework::window::key_fun(GLFWwindow* win, int key, int scancode, int action, int mods)
{
    framework::window* window = reinterpret_cast<framework::window*>(glfwGetWindowUserPointer(win));
    window->activate();
    if (window->_input_listener) window->_input_listener->key_action(key, scancode, action, mods);
}

void framework::window::char_fun(GLFWwindow* win, unsigned int character)
{
    framework::window* window = reinterpret_cast<framework::window*>(glfwGetWindowUserPointer(win));
    window->activate();
    if (window->_input_listener) window->_input_listener->char_input(character);
}

void framework::window::_init(int width, int height, const std::string& title, bool fullscreen, const window_hints& hints)
{
    if (is_open())
        return;

    _title = title;

    hints.apply();

    _window = glfw::create_window(width, height, _title, fullscreen ? glfwGetPrimaryMonitor() : nullptr);
    if (!_window)
        throw std::runtime_error("Failed to create window: " + glfw::get_last_error());

    glfw::temp_context guard(_window);

    glfwSetWindowUserPointer(_window, this);

    glfwSetMouseButtonCallback(_window, mouse_button_fun);
    glfwSetCursorPosCallback(_window, mouse_pos_fun);
    glfwSetCursorEnterCallback(_window, mouse_enter_fun);
    glfwSetScrollCallback(_window, mouse_scroll_fun);
    glfwSetKeyCallback(_window, key_fun);
    glfwSetCharCallback(_window, char_fun);
    glfwSetFramebufferSizeCallback(_window, window_fb_size_fun);

    int exts_error = ogl_LoadFunctions();
    std::cout << "OpenGL version: " << ogl_GetMajorVersion() << "." << ogl_GetMinorVersion() << std::endl;
    std::cout << exts_error << " extensions not loaded." << std::endl;
    if (exts_error <= 0)
        throw std::runtime_error("Failed to load OpenGL extensions.");

    _unique_identifier = ++_last_id;
}

void framework::window::fb_size_changed(int width, int height)
{
    if (_fb_size_changed_callback)
        _fb_size_changed_callback(width, height);
}

int framework::window::get_unique_identifier() const
{
    return _unique_identifier;
}

void framework::window::set_fb_size_changed_callback(const std::function<void(int, int)> call)
{
    _fb_size_changed_callback = call;
}

void framework::window::set_input_listener(input_listener* listener)
{
    _input_listener = listener;
}
