#include "time.hpp"

#include <GLFW/glfw3.h>

double framework::get_time()
{
    return glfwGetTime() * 1000.0;
}
