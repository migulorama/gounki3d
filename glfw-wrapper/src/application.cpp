#include "application.hpp"
#include <memory>

#include <iostream>

#include "scene.hpp"
#include "interface.hpp"

framework::application::application() : _scene(nullptr), _interface(nullptr), _refresh_requested(false)
{
}

void framework::application::init()
{
    _window.init(500, 500, "My title", framework::window_hints().set_aux_buffers(1));
    _window.set_fb_size_changed_callback([this](int width, int height) { this->reshape(width, height); });
}

void framework::application::run()
{
    _window.activate();

    if (!_scene) throw std::runtime_error("No Scene defined!");
    _scene->init(this);

    if (_interface) _interface->init(this);

    _refresh_requested = true;

    while (!_window.should_close())
    {
        _window.activate();
        
        if (_scene)
        {
            _scene->check_update();

            if (_refresh_requested)
            {
                reshape();
                _refresh_requested = false;
            }

            _scene->display();
        }

        if (_interface)
        {
            _interface->display();
        }

        _window.swap_buffers();
        _window.poll_events();
    }
}

void framework::application::set_scene(std::shared_ptr<framework::scene> sc)
{
    _scene = sc;
}

void framework::application::set_interface(std::shared_ptr<framework::interface> iface)
{
    _interface = iface;
    _window.set_input_listener(_interface.get());
}

std::shared_ptr<framework::scene> framework::application::get_scene() const
{
    return _scene;
}

std::shared_ptr<framework::interface> framework::application::get_interface() const
{
    return _interface;
}

void framework::application::reshape()
{
    glm::uvec2 fbSize = _window.get_framebuffer_size();
    reshape(fbSize.x, fbSize.y);
}

void framework::application::reshape(int width, int height)
{
    std::cout << "Reshape called\t Size: " << width << " x " << height << std::endl;
    if (_scene) _scene->reshape(width, height);
    if (_interface) _interface->reshape(width, height);
}

double framework::application::get_time() const
{
    return glfwGetTime() * 1000.0f;
}

void framework::application::terminate()
{
    _window.set_should_close(true);
}

void framework::application::force_refresh()
{
    _refresh_requested = true;
}
