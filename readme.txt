Turma: 3MIEIC01 
Grupo: 01
Miguel Rui Pereira Marques - 201109178
Ruben Fernando Pinto Cordeiro - 201109177

O manual é o ‘users_manual.pdf’ que se encontra na raíz do arquivo.
Os ficheiros de prolog encontram-se na pasta ‘prolog’.
Os ficheiros .yaf utilizados situam-se na pasta ‘themes’.
Os assets utilizados situam-se na pasta ‘resources’.
As sessões de jogo são guardadas na pasta ‘saves’.
Os logs do servidor são guardados na pasta ‘logs’.

Para executar o jogo, basta executar o ficheiro ‘gounki3d.exe’ que se encontra na raíz do arquivo.

O projeto foi desenvolvido em Visual Studio 2013, pelo que a sua correta compilação é apenas assegurada com esta versão do software, 
visto que que foram usadas funcionalidades ainda não implementadas nas versões anteriores do compilador. 
O programa apenas funciona em Windows, uma vez que o servidor é executado com recurso à API do Windows para criar o processo.