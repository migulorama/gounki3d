#ifndef GL_PRIMITIVES_TEXTURE_HPP_
#define GL_PRIMITIVES_TEXTURE_HPP_

#include <gl_4_4.h>

#include <cstdint>
#include <string>

#include "utils.hpp"

namespace gl
{

namespace details
{
class Texture;

typedef std::shared_ptr<Texture> TexturePtr;

//! Texture class
class Texture
{
private:
    Texture(Texture&& other);
    Texture(const Texture& other);

    Texture& operator=(Texture&& other);
    Texture& operator=(const Texture& other);

public:
    Texture(const std::string& fileName);
    ~Texture();

    void Apply(GLenum textureUnit = GL_TEXTURE0);

    void SetWrap(GLenum sWrap, GLenum tWrap);

    //! Loads the texture from a file.
    /*!
        \param fileName path of the file to be loaded.
        \return Reference to the loaded Texture.
    */
    static TexturePtr Load(const std::string& fileName);

protected:
    void LoadFromFile(const std::string& fileName);

private:
    GLuint _id;

    static bool is_constructing;
};
}

using details::TexturePtr;

namespace Texture
{
inline TexturePtr Load(const std::string& fileName) { return details::Texture::Load(fileName); }
}

}

#endif // GL_PRIMITIVES_TEXTURE_HPP_
