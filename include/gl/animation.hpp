#ifndef GL_ANIMATION_HPP__
#define GL_ANIMATION_HPP__

#include <memory>

namespace gl
{

//! Animation class
/*!
    Abstract class that ensures minimum functionality for all animations.
*/
class Animation
{
public:
    Animation() { _doReset = false; _isFinished = false; }
    Animation(bool doReset) { _doReset = doReset; _isFinished = false; }

    virtual ~Animation() { }

    //! Updates the animation given a time offset.
    /*!
        \param time time offset.
    */
    virtual void Update(double time) = 0;

    virtual void Draw() const {}

    virtual bool isFinished() const { return _isFinished; }

protected:
    bool _doReset;
    bool _isFinished;
};

typedef std::shared_ptr<Animation> AnimationPtr;

}


#endif // GL_ANIMATION_HPP_
