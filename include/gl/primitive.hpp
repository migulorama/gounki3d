#ifndef GL_PRIMITIVE_HPP_
#define GL_PRIMITIVE_HPP_

#include "gl/object.hpp"

namespace gl
{

//! Base Primitive class
/*!
    Describes a geometrical object directly addressable in OpenGl.
*/
class Primitive : public gl::Object
{
public:
    virtual ~Primitive() { };
    virtual void init(gl::MaterialPtr mat) override { };
    virtual void draw(gl::MaterialPtr mat) const override = 0; ///< Draws the object.
    virtual glm::vec3 get_size() const override { return glm::vec3(0.0f, 0.0f, 0.0f); }
};

typedef std::shared_ptr<Primitive> PrimitivePtr;
}


#endif // GL_PRIMITIVE_HPP_
