#ifndef GL_TYPES_HPP_
#define GL_TYPES_HPP_

#include <array>

namespace gl
{

typedef std::array<float, 4> ColorRGBA;
typedef std::array<float, 3> Position;
typedef std::array<float, 3> Direction;

}


#endif /* GL_TYPES_HPP_ */
