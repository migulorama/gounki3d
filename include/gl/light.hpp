#ifndef GL_LIGHT_HPP__
#define GL_LIGHT_HPP__

#include <gl_4_4.h>

#include <glm/glm.hpp>

#include <iomanip>

#include "gl/types.hpp"
#include "gl/material.hpp"

namespace gl
{

//! Light class
/*!
 It represents a omni light.
*/
class Light
{
public:
    explicit Light(GLuint lightid);
    Light(GLuint lightId, const glm::vec3& position);

    virtual ~Light();

    virtual void Update();  

    void Enable(bool val);
    bool IsEnabled() const;

    void SetPosition(const glm::vec3& position);
    void SetAmbient(const glm::vec4& ambient);
    void SetDiffuse(const glm::vec4& diffuse);
    void SetSpecular(const glm::vec4& specular);
    void SetKc(float kc);
    void SetKl(float kl);
    void SetKq(float kq);

protected:
    GLuint _id;

    glm::vec4 _position;
    glm::vec3 _direction;

    glm::vec4 _ambient;
    glm::vec4 _diffuse;
    glm::vec4 _specular;

    float _kc;
    float _kl;
    float _kq;
    float _angle;
    float _exponent;

    bool _enabled;
};

typedef std::shared_ptr<Light> LightPtr;
inline LightPtr make_light(GLuint lightId) { return std::make_shared<Light>(lightId); }
inline LightPtr make_light(GLuint lightId, const glm::vec3& position) { return std::make_shared<Light>(lightId, position); }

}


#endif // GL_LIGHT_HPP__
