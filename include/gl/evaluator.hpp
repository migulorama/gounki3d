#ifndef GL_EVALUATOR_HPP__
#define GL_EVALUATOR_HPP__

#include <gl_4_4.h>

#include <vector>

namespace gl
{

/** @defgroup Evaluators
*  Set of classes that describe any polynomial or rational polynomial splines or surfaces given a set of control points.
*  @{
*/

class Evaluator
{
public:
    enum Target { VERTEX_3, VERTEX_4, INDEX, COLOR_4, NORMAL, TEXTURE_COORD_1, TEXTURE_COORD_2, TEXTURE_COORD_3, TEXTURE_COORD_4 }; 

    Evaluator(Target tar);
    virtual ~Evaluator();
    
    virtual void Map() const = 0;
    virtual void UnMap() const = 0;
    virtual void Draw() const = 0;

protected:
    GLint CalculateUStride();

    Target _target;
};

class Evaluator1D : public Evaluator
{
public:
    Evaluator1D(Target tar, GLint order, GLint num_divisions, std::vector<float> controlPoints);
    virtual ~Evaluator1D();

    virtual void Map() const override;
    virtual void UnMap() const override;
    virtual void Draw() const override;

private:
    GLenum _targetEnum;
    GLfloat _u1, _u2;
    GLint _i1, _i2, _un;
    GLint _stride;
    GLint _order;

    std::vector<float> _controlPoints;

    GLenum GetTargetEnum();
};

class Evaluator2D : public Evaluator
{
public:
    Evaluator2D(Target tar, GLint uorder, GLint vorder, GLint num_u_divisions, GLint num_v_division, std::vector<float> controlPoints);
    virtual ~Evaluator2D();

    virtual void Map() const override;
    virtual void UnMap() const override;
    virtual void Draw() const override;

    std::vector<float>& GetControlPoints() { return _controlPoints; }
private:
    GLenum _targetEnum;

    GLfloat _u1, _u2;
    GLint _i1, _i2, _un;
    GLint _ustride;
    GLint _uorder;

    GLfloat _v1, _v2;
    GLint _j1, _j2, _vn;
    GLint _vstride;
    GLint _vorder;

    std::vector<float> _controlPoints;

    GLenum GetTargetEnum();
};

/** @} */ // end of Evaluators

}


#endif // GL_EVALUATOR_HPP__
