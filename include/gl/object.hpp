#ifndef GL_OBJECT_HPP_
#define GL_OBJECT_HPP_

#include "gl/material.hpp"

namespace gl
{

//! Object interface class
/*!
    Ensures minimum functionality for any object in the scene.
*/
class Object
{
public:
    virtual ~Object() { };
    virtual void init(gl::MaterialPtr mat) = 0; ///< Initializes the object.
    virtual void update(double time) { };
    virtual void draw(gl::MaterialPtr mat) const = 0; ///< Draws the object.
    virtual glm::vec3 get_size() const = 0;

};

typedef std::shared_ptr<Object> ObjectPtr;

}

#endif // GL_OBJECT_HPP_