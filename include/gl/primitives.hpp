#ifndef GL_PRIMITIVES_HPP_
#define GL_PRIMITIVES_HPP_

#include "gl/primitives/cylinder.hpp"
#include "gl/primitives/rectangle.hpp"
#include "gl/primitives/sphere.hpp"
#include "gl/primitives/torus.hpp"
#include "gl/primitives/triangle.hpp"
#include "gl/primitives/plane.hpp"
#include "gl/primitives/patch.hpp"
#include "gl/primitives/waterline.hpp"
#include "gl/primitives/vehicle.hpp"


#endif // GL_PRIMITIVES_HPP_
