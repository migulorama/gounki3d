#ifndef GL_CAMERAS_PERSPECTIVE_HPP_
#define GL_CAMERAS_PERSPECTIVE_HPP_

#include <memory>

#include "gl/camera.hpp"

#include <glm/glm.hpp>
#include <ostream>

namespace gl
{
namespace Cameras
{

//! Perspective class
/*!
    Represents a perspective camera with a frustum as a viewing volume.
*/
class Perspective : public Camera
{
public:
    Perspective() {}

    //! Constructs a perspective camera with the properties given as parameters
    /*! To be completed
    */
    Perspective(float fov, float near_val, float far_val, const glm::vec3& pos, const glm::vec3& tar);

    virtual ~Perspective() { }

    virtual glm::mat4 GetModelViewMatrix() const override;
    virtual glm::mat4 GetProjectionMatrix(int width, int height) const override;

    virtual void Translate(const glm::vec3& val) override;
    virtual void Rotate(float angle, const glm::vec3& axis) override;
    void RotatePosition(float angle, const glm::vec3& axis);

    void SetFOV(float val);
    void SetNear(float val);
    void SetFar(float val);

    void SetPosition(const glm::vec3& val);
    virtual glm::vec3 GetPosition() const;

    void SetTarget(const glm::vec3& val);
    glm::vec3 GetTarget() const;

    glm::vec3 GetDirection() const;

    virtual glm::vec3 GetRightVector() const override { return _right; }

    void UpdateUp();

    void Reset();

private:
    glm::vec4 _position;
    glm::vec4 _initPosition;
    glm::vec4 _target;
    glm::vec4 _initTarget;
    glm::vec4 _direction;
    glm::vec3 _up;
    glm::vec3 _right;

    float _fov;
    float _near;
    float _far;
};

}

typedef std::shared_ptr<Cameras::Perspective> PerspectiveCameraPtr;
inline PerspectiveCameraPtr make_perspectiveCamera() {return std::make_shared<Cameras::Perspective>();};
}


#endif // GL_CAMERAS_PERSPECTIVE_HPP_
