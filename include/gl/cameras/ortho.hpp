#ifndef GL_CAMERAS_ORTHO_HPP_
#define GL_CAMERAS_ORTHO_HPP_

#include <memory>

#include "gl/camera.hpp"
#include <glm/glm.hpp>

namespace gl
{
namespace Cameras
{

//! Ortho class
/*!
    Represents a camera with orthogonal projection lines relative to the projection plane.
*/
class Ortho : public Camera
{
public:
    Ortho() {}

    //! Constructs an orthogonal camera with properties given as parameters.
    /*!
        \param near nearest clipping plane.
        \param far farthest clipping plane.
    */
    Ortho(float near_val, float far_val, float left, float right, float top, float bottom);

    virtual ~Ortho() { }

    virtual glm::mat4 GetProjectionMatrix(int width, int height) const override;
    virtual glm::mat4 GetModelViewMatrix() const override;

    virtual void Translate(const glm::vec3& val) override;
    virtual void Rotate(float angle, const glm::vec3& axis) override;

    void UpdateUp();

    void SetNear(float val);
    void SetFar(float val);
    void SetLeft(float left);
    void SetRight(float right);
    void SetTop(float top);
    void SetBottom(float bottom);

private:
    glm::vec4 _position;
    glm::vec4 _target;
    glm::vec4 _direction;
    glm::vec3 _up;
    glm::vec3 _rightVec;

    float _near;
    float _far;
    float _left;
    float _right;
    float _top;
    float _bottom;
};

}

typedef std::shared_ptr<Cameras::Ortho> OrthoCameraPtr;
 inline OrthoCameraPtr make_orthoCamera() {return std::make_shared<Cameras::Ortho>();};
}

#endif // GL_CAMERAS_ORTHO_HPP_
