/*
 * lights.hpp
 *
 *  Created on: Sep 18, 2013
 *      Author: miguel
 */

#ifndef GL_LIGHTS_HPP_
#define GL_LIGHTS_HPP_

#include "gl/light.hpp"
#include "gl/lights/spot.hpp"

#endif // GL_LIGHTS_HPP_
