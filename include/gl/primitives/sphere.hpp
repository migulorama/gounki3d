#ifndef GL_PRIMITIVES_SPHERE_HPP_
#define GL_PRIMITIVES_SPHERE_HPP_

#include <gl_4_4.h>

#include "gl/primitive.hpp"

namespace gl
{

namespace primitives
{

//! Sphere class
class Sphere : public gl::Primitive
{
public:
    //! Constructs a sphere with the properties given as parameters
    /*!
        \param radius radius of the sphere.
        \param slices number of subdivisions around the z axis (similar to lines of longitude).
        \param stacks number of subdivisions along the z axis  (similar to lines of latitude).
    */
    Sphere(float radius, unsigned int slices, unsigned int stacks);
    virtual ~Sphere();

    virtual void draw(gl::MaterialPtr mat) const override;
    
private:
    float _radius;
    unsigned int _slices;
    unsigned int _stacks;

//    GLUquadric* _quadric;
};

}

typedef std::shared_ptr<primitives::Sphere> SpherePtr;
inline SpherePtr make_sphere() {return std::shared_ptr<primitives::Sphere>();};

}

#endif // GL_PRIMITIVES_SPHERE_HPP_
