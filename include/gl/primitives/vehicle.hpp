#ifndef _VEHICLE_HPP_
#define _VEHICLE_HPP_

#include "gl/primitive.hpp"
#include "gl/evaluator.hpp"
#include "gl/primitives/vehicle.hpp"
#include "gl/primitives/patch.hpp"
#include "gl/primitives/sphere.hpp"
#include "gl/primitives/cylinder.hpp"

namespace gl
{

namespace primitives
{

//! Vehicle class
/*!
    Represents a ship like vehicle.
*/
class Vehicle : public Primitive
{
public:
    Vehicle();
    virtual ~Vehicle();
    void draw(gl::MaterialPtr mat) const override;

private:
   static gl::primitives::Patch ovniPatch;
   static gl::primitives::Cylinder ovniPatchSupport;
   static gl::primitives::Patch legSupportPatch;
   static gl::primitives::Sphere s;
};

}
}
#endif /* _VEHICLE_HPP_*/