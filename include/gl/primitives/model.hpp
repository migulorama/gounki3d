#ifndef GL_PRIMITIVES_MODEL_HPP_
#define GL_PRIMITIVES_MODEL_HPP_

#include "gl/primitive.hpp"
#include "gl/material.hpp"

#include <unordered_map>
#include <string>

namespace gl
{

class mesh : public gl::Primitive
{
public:
    explicit mesh(const std::string& fileName);

    virtual void init(gl::MaterialPtr mat) override;

    virtual void draw(gl::MaterialPtr mat) const override;

    float get_height() const { return _size.y; }
    virtual glm::vec3 get_size() const override { return _size; }

private:
    void load_obj_file();
    void load_mat_file(const std::string& filePath, const std::string& fileName);

private:
    struct face;

    struct group
    {
        std::string name;
        std::vector<face> faces;
        std::weak_ptr<gl::Material> material_ptr;
    };

    struct face
    {
        face() : vertices{ { 0, 0, 0 } }, texCoords{ { 0, 0, 0 } }, normals{ { 0, 0, 0 } }
        {
        }

        std::array<int, 3> vertices;
        std::array<int, 3> texCoords;
        std::array<int, 3> normals;
    };

    bool _initialized;

    std::string _fileName;

    glm::vec3 _size;
    std::vector<glm::vec3> _vertices;
    std::vector<glm::vec2> _texCoords;
    std::vector<glm::vec3> _normals;

    std::vector<group> _groups;

    std::unordered_map<std::string, gl::MaterialPtr> _materials;
};

}

#endif // GL_PRIMITIVES_MODEL_HPP_
