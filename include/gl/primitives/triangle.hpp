#ifndef GL_PRIMITIVES_TRIANGLE_HPP_
#define GL_PRIMITIVES_TRIANGLE_HPP_

#include "gl/primitive.hpp"
#include <glm/glm.hpp>
#include <array>

namespace gl
{

namespace primitives
{

//! Triangle class
class Triangle : public gl::Primitive
{
public:

    //! Constructs a triangle with the properties given as parameters.
    /*!
        The vertices are defined in a counter clockwise order, the edge between xyz1 and xyz2 represents the base of the triangle for texture mapping purposes.

        \param xyz1 Specifies the first vertex of the triangle.
        \param xyz2 Specifies the second vertex of the triangle.
        \param xyz3 Specifies the third vertex of the triangle.
    */
    Triangle(const glm::vec3& xyz1, const glm::vec3& xyz2, const glm::vec3& xyz3);
    virtual ~Triangle() { }

    virtual void draw(gl::MaterialPtr mat) const override;

    void SetPosition(const glm::vec3& xyz1, const glm::vec3& xyz2, const glm::vec3& xyz3);

private:
    std::array<glm::vec3, 3> _vertices;
    glm::vec3 _normal;
};

}

typedef std::shared_ptr<primitives::Triangle> TrianglePtr;
inline TrianglePtr make_triangle() { return std::shared_ptr<primitives::Triangle>(); };
}


#endif // GL_PRIMITIVES_TRIANGLE_HPP_
