#ifndef GL_PRIMITIVES_TORUS_HPP_
#define GL_PRIMITIVES_TORUS_HPP_

#include "gl/primitive.hpp"

#include <vector>

#include <glm/glm.hpp>

namespace gl
{

namespace primitives
{

//! Torus class
class Torus : public gl::Primitive
{
public:
    //! Constructs a torus with the properties given as parameters
    /*!
        \param inner inner radius of the torus.
        \param outer outer radius of the torus.
        \param slices number of sides for each radial section.
        \param number of radial divisions for the torus.
    */
    Torus(float inner, float outer, unsigned int slices, unsigned int loops);

    virtual void draw(gl::MaterialPtr mat) const override;
    virtual ~Torus() { }

private:
    float _inner;
    float _outer;
    unsigned int _slices;
    unsigned int _loops;

    std::vector<std::vector<glm::vec3>> _vertices;
    std::vector<std::vector<glm::vec3>> _normals;
    std::vector<std::vector<glm::vec2>> _texCoords;
};

}

typedef std::shared_ptr<primitives::Torus> TorusPtr;
inline TorusPtr make_torus() {return std::shared_ptr<primitives::Torus>();};
}



#endif // GL_PRIMITIVES_TORUS_HPP_
