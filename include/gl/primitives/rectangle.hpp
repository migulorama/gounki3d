#ifndef GL_PRIMITIVES_RECTANGLE_HPP_
#define GL_PRIMITIVES_RECTANGLE_HPP_

#include "gl/primitive.hpp"
/*#include <pair>*/
#include "glm/glm.hpp"

namespace gl
{

namespace primitives
{

//! Rectangle class
class Rectangle : public gl::Primitive
{
public:
    //! Constructs a rectangle with the properties given as parameters.
    /*!
        \param xy1 Specifies a vertex of the rectangle.
        \param xy2 Specifies the opposite vertex of the rectangle.
    */
    Rectangle(glm::vec2 xy1, glm::vec2 xy2);
    virtual ~Rectangle() { }

    virtual void draw(gl::MaterialPtr mat) const override;

    //! Changes the dimensions of the rectangle
    /*!
        \param xy1 Specifies a vertex of the rectangle.
        \param xy2 Specifies the opposite vertex of the rectangle.
    */
    void SetDimensions(glm::vec2 xy1, glm::vec2 xy2);

private:
    glm::vec2 _xy1;
    glm::vec2 _xy2;
    glm::vec3 _normal;
};

}

typedef std::shared_ptr<primitives::Rectangle> RectanglePtr;
inline RectanglePtr make_rectangle() {return std::shared_ptr<primitives::Rectangle>();};
}




#endif // GL_PRIMITIVES_RECTANGLE_HPP_
