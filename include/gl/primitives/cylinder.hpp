#ifndef GL_PRIMITIVES_CYLINDER_HPP_
#define GL_PRIMITIVES_CYLINDER_HPP_

#include "gl/primitive.hpp"
#include <memory>
#include <gl_4_4.h>

#include <vector>
#include <glm/glm.hpp>

namespace gl
{

namespace primitives
{

//! Cylinder class
class Cylinder : public gl::Primitive
{
public:
    //! Constructs a Cylinder with the properties given as parameters.
    /*!
        \param base radius of the cylinder base at z = 0.
        \param top radius of the cylinder top at z = height.
        \param height height of the cylinder.
        \param slices number of subdivisions around the z axis.
        \param stacks number of subdivisions along the z axis.
    */
    Cylinder(float base, float top, float height, unsigned int slices, unsigned int stacks);
    virtual ~Cylinder() { }

    virtual void draw(gl::MaterialPtr mat) const override;

private:
    float _base;
    float _top;
    float _height;

    unsigned int _slices;
    unsigned int _stacks;

//     GLUquadric* _gluQuadric;
};

}

typedef std::shared_ptr<primitives::Cylinder> CylinderPtr;
inline CylinderPtr make_cylinder() {return std::shared_ptr<primitives::Cylinder>();};
}



#endif // GL_PRIMITIVES_CYLINDER_HPP_
