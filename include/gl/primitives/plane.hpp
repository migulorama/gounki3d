#ifndef GL_PRIMITIVES_PLANE_HPP__
#define GL_PRIMITIVES_PLANE_HPP__

#include "gl/primitive.hpp"
#include "gl/evaluator.hpp"

namespace gl
{

namespace primitives
{

class Plane : public gl::Primitive
{
public:
    Plane(GLint num_div_x, GLint num_div_z);

    virtual void draw(gl::MaterialPtr mat) const override;

private:
    std::unique_ptr<gl::Evaluator2D> _verticesEval;
    std::unique_ptr<gl::Evaluator2D> _normalsEval;
    std::unique_ptr<gl::Evaluator2D> _texCoordsEval;
};

}

}

#endif // GL_PRIMITIVES_PLANE_HPP__
