#ifndef GL_PRIMITIVES_WATERLINE_HPP_
#define GL_PRIMITIVES_WATERLINE_HPP_

#include "gl/primitive.hpp"
#include "gl/primitives/plane.hpp"
#include "gl/shader.hpp"

#include <string>

#include <gl_4_4.h>

namespace gl
{

namespace primitives
{

//! Waterline class
/*!
    Represents a river bed running in the positive direction of the \a Z axis.
*/
class WaterLine : public Primitive
{
public:
    WaterLine(const std::string& vertexShaderFile, const std::string& fragmentShaderFile, const std::string& textureFileName, const std::string& heightTextureFileName):
        _vertexShaderFileName(vertexShaderFile),
        _fragmentShaderFileName(fragmentShaderFile),
        _texture(gl::Texture::Load(textureFileName)),
        _heightTexture(gl::Texture::Load(heightTextureFileName)),
        _plane(128, 128),
        _step(0),
        _doReset(true)
    {
        _texture->SetWrap(GL_REPEAT, GL_REPEAT);
        _heightTexture->SetWrap(GL_REPEAT, GL_REPEAT);
    }

    virtual void init(gl::MaterialPtr) override
    {
        std::vector<gl::ShaderPtr> shaders;

        shaders.push_back(gl::make_shader(GL_VERTEX_SHADER, _vertexShaderFileName));
        shaders.push_back(gl::make_shader(GL_FRAGMENT_SHADER, _fragmentShaderFileName));

        _program = gl::make_program(shaders);

        _program->Compile();

        _program->Bind();

        _program->AddUniform("time");
        _program->AddUniform("tex");
        _program->AddUniform("heightTex");

        glUniform1i((*_program)("tex"), 0);
        glUniform1i((*_program)("heightTex"), 2);

        _program->Unbind();
    }

    virtual void update( double time ) override
    {
        if (_doReset)
        {
            _animStartTime = static_cast<long long>(time);
            _doReset = false;
        }

        long long animTime = static_cast<long long>(time) - _animStartTime;

        if (animTime > 10000)
        {
            _animStartTime = static_cast<long long>(time);
            animTime -= 10000;
        }

        _program->Bind();

        glUniform1f((*_program)("time"), (animTime / 10000.0f) );

        _program->Unbind();
    }

    virtual void draw( gl::MaterialPtr mat ) const override
    {
        _program->Bind();

        _heightTexture->Apply(GL_TEXTURE2);
        _texture->Apply(GL_TEXTURE0);
        _plane.draw(mat);

        _program->Unbind();
    }

private:
    gl::ProgramPtr _program;
    gl::TexturePtr _texture;
    gl::TexturePtr _heightTexture;
    gl::primitives::Plane _plane;

    std::string _fragmentShaderFileName;
    std::string _vertexShaderFileName;

    bool _doReset;
    long long _animStartTime;
    int _step;
};

}

}

#endif // GL_PRIMITIVES_WATERLINE_HPP_
