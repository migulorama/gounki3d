#ifndef GL_PRIMITIVES_PATCH_HPP__
#define GL_PRIMITIVES_PATCH_HPP__

#include <gl_4_4.h>
#include "gl/evaluator.hpp"
#include "gl/primitive.hpp"


#include <memory>
#include <glm/glm.hpp>
#include <vector>

namespace gl
{

namespace primitives
{

class Patch : public Primitive
{
public:
    Patch(GLint order, GLint uParts, GLint vParts, std::vector<glm::vec3> controlPoints);

    virtual ~Patch();

    virtual void draw(gl::MaterialPtr mat) const override;

private:
    GLint _order;
    GLint _uParts, _vParts;

    std::unique_ptr<gl::Evaluator2D> _verticesEval;
    std::unique_ptr<gl::Evaluator2D> _texCoordEval;
};

}

typedef std::shared_ptr<primitives::Patch> PatchPtr;

}

#endif // GL_PRIMITIVES_PATCH_HPP__
