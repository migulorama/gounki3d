#ifndef GL_CAMERA_HPP_
#define GL_CAMERA_HPP_

#include <gl_4_4.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <memory>

namespace gl
{

//! Base Camera interface
/*!
    Ensures minimum functionality for all the cameras in the scene
*/
class Camera
{
public:
    virtual ~Camera() { };
    
    virtual glm::mat4 GetModelViewMatrix() const { return glm::mat4(); }
    virtual glm::mat4 GetProjectionMatrix(int width, int height) const { return glm::mat4(); }
    
    void ApplyView() const;
    void UpdateProjectionMatrix(int width, int height) const;

    virtual glm::vec3 GetRightVector() const { return glm::vec3(1.0f, 0.0f, 0.0f); }

    virtual void Translate(const glm::vec3& val) = 0;
    virtual void Rotate(float angle, const glm::vec3& axis) = 0;
    virtual void Reset() { }
};

typedef std::shared_ptr<Camera> CameraPtr;

}


#endif // GL_CAMERA_HPP_
