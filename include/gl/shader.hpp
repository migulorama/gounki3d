#ifndef GL_SHADER_HPP__
#define GL_SHADER_HPP__

#include <gl_4_4.h>

#include <memory>
#include <unordered_map>

#include <iostream>
#include <fstream>
#include <string>
#include <exception>
#include <stdexcept>

namespace gl
{

class ShaderCompileError : public std::runtime_error
{
public:
    explicit ShaderCompileError(GLuint shaderID);
};

class ProgramLinkError : public std::runtime_error
{
public:
    explicit ProgramLinkError(GLuint programID);
};

namespace details
{

class Program;

//! Shader class
class Shader
{
public:
    friend class Program;

    Shader(GLenum type, const std::string& fileName);
    virtual ~Shader();

protected:
    void Compile();

private:
    std::string _fileName;
    GLenum _type;
    GLuint _shaderID;
};

typedef std::shared_ptr<Shader> ShaderPtr;

//! Program class
/*!
    A Program is responsible for compiling and linking shader objects.
*/
class Program
{
public:
    explicit Program(std::vector<ShaderPtr> shaders);
    virtual ~Program(); 

    void Compile();

    void Bind();
    void Unbind();

    void AddAttribute(const std::string& attribute);
    void AddUniform(const std::string& uniform);

    GLuint operator [](const std::string& attribute) const;
    GLuint operator ()(const std::string& uniform) const;

private:
    GLuint _programID;
    std::vector<ShaderPtr> _shaders;

    std::unordered_map<std::string, GLuint> _attributeList;
    std::unordered_map<std::string, GLuint> _uniformLocationList;
};

typedef std::shared_ptr<Program> ProgramPtr;

}

using details::ShaderPtr;

inline ShaderPtr make_shader(GLenum type, const std::string& fileName) 
{
    return std::make_shared<details::Shader>(type, fileName); 
}

using details::ProgramPtr;

inline ProgramPtr make_program(const std::vector<ShaderPtr>& shaders)
{
    return std::make_shared<details::Program>(shaders);
}

}

#endif // GL_SHADER_HPP__
