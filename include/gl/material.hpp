#ifndef GL_MATERIAL_HPP_
#define GL_MATERIAL_HPP_

#include <gl_4_4.h>

#include <memory>

#include "gl/texture.hpp"
#include "gl/types.hpp"
#include "utils.hpp"

namespace gl
{

//! Material class
/*!
    It stores information about an object's color reflection properties and texture.
*/
class Material
{
public:
    Material();
    Material(ColorRGBA ambient, ColorRGBA diffuse, ColorRGBA specular, ColorRGBA emissive, float shininess);
    Material(TexturePtr tex, GLenum sWrap, GLenum tWrap);

    virtual ~Material() { }

    void Apply();

    void SetAmbient(ColorRGBA ambient);
    void SetDiffuse(ColorRGBA diffuse);
    void SetSpecular(ColorRGBA specular);
    void SetEmissive(ColorRGBA emissive);
    void SetShininess(float shininess);

    const ColorRGBA& GetDiffuse() const { return _diffuse; }

    void SetTexture(TexturePtr tex);
    TexturePtr GetTexture() const;

    void SetTextureWrap(GLenum sWrap, GLenum tWrap);
    void SetTextureSWrap(GLenum sWrap);
    void SetTextureTWrap(GLenum tWrap);

    void SetTextureScale(float s, float t);
    const glm::vec2& GetTextureScale() const;

    Material operator*(const ColorRGBA& color) const
    {
        Material result(*this);

        for (size_t i = 0; i < color.size(); ++i) 
        {
            result._ambient[i] = std::min(result._ambient[i] * color[i], 1.0f);
            result._diffuse[i] = std::min(result._diffuse[i] * color[i], 1.0f);
            result._specular[i] = std::min(result._specular[i] * color[i], 1.0f);
            result._emissive[i] = std::min(result._emissive[i] * color[i], 1.0f);
        }
        
        return result;
    }
private:
    ColorRGBA _ambient;
    ColorRGBA _diffuse;
    ColorRGBA _specular;
    ColorRGBA _emissive;
    float _shininess;
    glm::vec2 _textureScale;

    TexturePtr _texture;
    GLenum _sWrap;
    GLenum _tWrap;
};

typedef std::shared_ptr<Material> MaterialPtr;

inline MaterialPtr make_material() { return std::make_shared<Material>(); }
inline MaterialPtr make_material(ColorRGBA ambient, ColorRGBA diffuse, ColorRGBA specular, ColorRGBA emissive, float shininess) { return std::make_shared<Material>(ambient, diffuse, specular, emissive, shininess); }
inline MaterialPtr make_material(TexturePtr tex, GLenum sWrap, GLenum tWrap) { return std::make_shared<Material>(tex, sWrap, tWrap); }
}


#endif // GL_MATERIAL_HPP_
