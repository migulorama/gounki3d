#ifndef GL_DISPLAY_LIST_HPP__
#define GL_DISPLAY_LIST_HPP__

#include <gl_4_4.h>
#include <memory>
#include <functional>

namespace gl
{

namespace details
{

//! Display list class
    /*!
        Stores commands for later execution.
    */
class DisplayList
{
public:
    DisplayList();

    virtual ~DisplayList();

    //! Stores the function given as a parameter for later execution.
    /*!
        \param func function to be stored.
    */
    void Init(std::function<void()> func);
    
    //! Checks if the DisplayList is already initialized.
    /*!
        \return true if the DisplayList is initialized, false otherwise.
    */
    bool IsInitialized() const;

    //! Executes the stored commands in the DisplayList.
    void Call() const;

private:
    GLuint _id;
    bool _initialized;
};

typedef std::shared_ptr<DisplayList> DisplayListPtr;

}

using details::DisplayListPtr;
DisplayListPtr make_display_list();

}

#endif // GL_DISPLAY_LIST_HPP__
