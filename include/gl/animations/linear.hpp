#ifndef GL_ANIMATIONS_LINEAR_HPP__
#define GL_ANIMATIONS_LINEAR_HPP__

#include <gl_4_4.h>
#include "gl/animation.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <memory>

#include <vector>

namespace gl
{

namespace Animations
{

class Linear : public Animation
{
public:
    Linear(float timeInSeconds, std::vector<glm::vec3> controlPoints);

    virtual ~Linear();

    virtual void Update(double time) override;

    virtual void Draw() const override;

private:
    struct Path
    {
        float timeStart;
        glm::vec3 positionStart;
        float distance;
        glm::vec3 direction;
        float rotY;
    };

    size_t GetPath(float animTime) const;

    std::vector<Path> _paths;

    std::vector<glm::vec3> _controlPoints;
    float _timeInMs;
    float _animTimeStart;
    float _speedPerMs;

    bool _doReset;

    glm::vec3 _position;
    float _rotY;
};

}

typedef std::shared_ptr<Animations::Linear> LinearAnimationPtr;

}



#endif // GL_ANIMATIONS_LINEAR_HPP__

