#ifndef GL_LIGHTS_SPOT_HPP_
#define GL_LIGHTS_SPOT_HPP_

#include "gl/light.hpp"

#include <iomanip>

namespace gl
{
namespace Lights
{

//! Spot Class
/*!
    Represents a spotlight with a radius of effect and a specific direction.
*/
class Spot : public Light
{
public:
    explicit Spot(GLuint lightid);

    //! Constructs a spotlight with the properties given as parameters.
    Spot(unsigned int lightid, const glm::vec3& pos, float angle, float exponent, const glm::vec3& dir);

    void SetExponent(float exponent);
    void SetDirection(const glm::vec3& dir);
    void SetAngle(float angle);

};

}

typedef std::shared_ptr<Lights::Spot> SpotPtr;
inline SpotPtr make_spot(GLuint lightId) { return std::make_shared<gl::Lights::Spot>(lightId);};
}

#endif // GL_LIGHTS_SPOT_HPP_
