#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <string>
#include <vector>
#include <boost/array.hpp>
#include <limits>
#include <ticpp.h>
#include <glm/glm.hpp>

template <typename Collection>
void parse_string(const std::string& str, Collection& output)
{
    std::istringstream ss(str);
    for(auto itr = output.begin();itr != output.end(); ++itr)
        ss >> (*itr);
    if (!ss || ss.rdbuf()->in_avail() != 0)
        throw std::runtime_error("Error parsing string \"" + str + "\"");
}

template <>
inline void parse_string(const std::string& str, glm::vec2& output)
{
    std::istringstream ss(str);
    ss >> output.x;
    ss >> output.y;
    if (!ss || ss.rdbuf()->in_avail() != 0)
        throw std::runtime_error("Error parsing string \"" + str + "\"");
}

template <>
inline void parse_string(const std::string& str, glm::vec3& output)
{
    std::istringstream ss(str);
    ss >> output.x;
    ss >> output.y;
    ss >> output.z;
    if (!ss || ss.rdbuf()->in_avail() != 0)
        throw std::runtime_error("Error parsing string \"" + str + "\"");
}

template <>
inline void parse_string(const std::string& str, glm::vec4& output)
{
    std::istringstream ss(str);
    ss >> output.x;
    ss >> output.y;
    ss >> output.z;
    ss >> output.w;
    if (!ss || ss.rdbuf()->in_avail() != 0)
        throw std::runtime_error("Error parsing string \"" + str + "\"");
}

template <typename Collection>
void get_attribute(ticpp::Element* elem, const std::string& attr, Collection& output, bool throwIfNotFound = true)
{
    std::string attributeStr;
    try
    {
        elem->GetAttribute(attr, &attributeStr, throwIfNotFound);
        parse_string(attributeStr, output);
    }
    catch (std::runtime_error& re)
    {
        throw std::runtime_error(re.what() + std::string(" on line ") + std::to_string(static_cast<long long>(elem->Row())));
    }
}

template <typename Collection>
glm::vec3 calc_newell(const Collection& polygon)
{
    glm::vec3 normal(0, 0, 0);
    for (auto itr = polygon.begin(); itr != polygon.end(); ++itr)
    {
        const glm::vec3& current = *itr;
        const glm::vec3& next = (itr + 1) != polygon.end() ? (*(itr + 1)) : (*(polygon.begin()));

        normal.x += (current.y - next.y) * (current.z + next.z);
        normal.y += (current.z - next.z) * (current.x + next.x);
        normal.z += (current.x - next.x) * (current.y + next.y);
    }

    return normal;
}

bool parse_bool_from_string(const std::string& str);

#endif /* UTILS_HPP_ */
