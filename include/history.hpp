#ifndef HISTORY_HPP__
#define HISTORY_HPP__

#include "gounki/board.hpp"
#include "gounki/moves.hpp"
#include "gounki/player.hpp"

#include <boost/optional.hpp>

#include <tuple>
#include <vector>

class game_history
{
public:
    typedef std::tuple<gounki::board, gounki::player::player, boost::optional<gounki::movement>, gounki::captured_pieces> entry;

    void commit(gounki::board board, gounki::player::player player, gounki::captured_pieces pieces = gounki::captured_pieces());
    void commit(gounki::board board, gounki::player::player player, gounki::movement movement, gounki::captured_pieces pieces = gounki::captured_pieces());

    void clear();

    void undo_all();

    bool undo();
    bool redo();

    entry get_last();

    const std::vector<entry>& get_history() const;

private:
    std::vector<entry> _entries;
    std::vector<entry> _future_entries;
};

#endif // HISTORY_HPP__
