#ifndef YAF_COMPOSED_OBJECT_HPP_
#define YAF_COMPOSED_OBJECT_HPP_

#include <memory>

#include <vector>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include "gl/material.hpp"

#include "gl/object.hpp"
#include "gl/primitive.hpp"
#include "utils.hpp"

#include "gl/animation.hpp"

namespace YAF
{

//! ComposedObject Class
/*!
    A ComposedObject represents a node in the graph logic. A ComposedObject has one or more references to other objects.
*/
class Graph;

class ComposedObject : public gl::Object
{
public:
    explicit ComposedObject(const YAF::Graph& graph);
    virtual ~ComposedObject() { }

    void AddChild(const std::string& childName);
    void AddChild(std::shared_ptr<gl::Primitive> child);

    void SetAnimation(const std::string& animationName);

    void SetAppearance(const std::string& name);

    void SetTransformation(const glm::mat4& transform);
    const glm::mat4& GetTransformation() const;

    virtual void init(gl::MaterialPtr mat) override;
    virtual void draw(gl::MaterialPtr mat) const override;

    virtual void update( double time ) override;

    virtual glm::vec3 get_size() const override;

private:
    gl::MaterialPtr _material;
    std::vector<gl::ObjectPtr> _children;
    gl::AnimationPtr _animation;
    const YAF::Graph& _graph;

    glm::vec3 _size;
    glm::mat4 _transformation;
};

typedef std::shared_ptr<ComposedObject> ComposedObjectPtr;
inline ComposedObjectPtr make_composedObject(const YAF::Graph& graph) { return std::make_shared<ComposedObject>(graph); }

}


#endif /* YAF_COMPOSED_OBJECT_HPP_ */
