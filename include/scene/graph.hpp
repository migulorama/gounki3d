#ifndef YAF_GRAPH_HPP_
#define YAF_GRAPH_HPP_

#include <string>
#include <unordered_map>

#include "utils.hpp"

#include "gl/animations.hpp"
#include "gl/cameras.hpp"
#include "gl/lights.hpp"
#include "gl/light.hpp"
#include "gl/material.hpp"
#include "gl/texture.hpp"

#include "scene/composed_object.hpp"

#include <gl_4_4.h>


#include <vector>

namespace YAF
{

//! Graph class
/*!
    Encapsulates all the scene information to be rendered.
*/
class Graph
{
    friend class Interface;
public:
    Graph();

    ~Graph();

    gl::LinearAnimationPtr AddLinearAnimation(const std::string& name, float time, std::vector<glm::vec3> controlPoints);

    gl::AnimationPtr GetAnimation(const std::string& name) const;

    ComposedObjectPtr AddNode(const std::string& name);
    ComposedObjectPtr AddNode(const char* name);
    ComposedObjectPtr GetNode(const std::string& name) const;

    gl::PerspectiveCameraPtr AddPerspectiveCamera(const std::string& name);
    gl::OrthoCameraPtr AddOrthoCamera(const std::string& name);
    gl::CameraPtr GetCamera(const std::string& name) const;

    gl::LightPtr AddLight(const std::string& name, GLuint id);
    gl::LightPtr GetLight(const std::string& name) const;

    gl::SpotPtr AddSpotLight(const std::string& name, GLuint id);

    gl::MaterialPtr AddMaterial(const std::string& name);
    gl::MaterialPtr GetMaterial(const std::string& name) const;

    gl::TexturePtr GetTexture(const std::string& name);
    gl::TexturePtr AddTexture(const std::string& name, const std::string& file);

    void SetRoot(const std::string& rootId);

    void SetBackground(const gl::ColorRGBA& background);
    const gl::ColorRGBA& GetBackground() const;

    void SetDrawMode(const std::string& drawMode);
    void SetDrawMode(GLenum mode);
    const GLenum GetDrawMode() const;

    void SetShading(const std::string& shading);
    const GLenum GetShading() const;

    void SetCullface(const std::string& cullface);
    const GLenum GetCullface() const;

    void SetCullorder(const std::string& cullorder);
    const GLenum GetCullorder() const;

    void SetDoubleSided(bool doubleSided);
    const bool GetDoubleSided() const;

    void SetLocal(bool local);
    const bool GetLocal() const;

    void SetLigthtsEnabled(bool enabled); 
    const bool GetLightsEnabled() const;

    void SetGlobalAmbient(const gl::ColorRGBA& ambient);
    const gl::ColorRGBA& GetGlobalAmbient() const;

    void SetInitialCamera(const std::string& id);
    const gl::CameraPtr GetInitialCamera() const;
    std::string GetInitialCameraId() const;

    std::vector<std::string> GetCameraNames() const;
    std::vector<std::string> GetLightNames() const;

    void Init();

    void Update(double time);

    //! Draws all the objects
    void draw() const;

private:
    ComposedObjectPtr _root;    ///< Root node

    std::unordered_map<std::string, ComposedObjectPtr >    _nodes;
    std::unordered_map<std::string, gl::CameraPtr     >    _cameras;
    std::unordered_map<std::string, gl::LightPtr      >    _lights;
    std::unordered_map<std::string, gl::MaterialPtr   >    _materials;
    std::unordered_map<std::string, gl::TexturePtr    >    _textures;
    std::unordered_map<std::string, gl::AnimationPtr  >    _animations;

    std::string _initialCameraId;
    gl::CameraPtr _initialCamera;

    gl::ColorRGBA _background;

    gl::MaterialPtr _defaultMaterial;

/**
 * \defgroup RenderingGlobals Global rendering variables
 */
/**@{*/

    GLenum _drawMode;
    GLenum _shading;
    GLenum _cullface;
    GLenum _cullorder;
    gl::ColorRGBA _ambient;

/**@}*/

/**
 * \defgroup LightGlobals Global lighting variables
 */
/**@{*/
    
    bool _doubleSided;
    bool _local;            ///< Local illumination
    bool _lightsEnabled;

/**@}*/

public:
    // auto GetNodes() const -> decltype(_nodes) { return _nodes; }
};

}

#endif /* YAF_GRAPH_HPP_ */
