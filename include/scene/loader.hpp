#ifndef YAF_LOADER_HPP_
#define YAF_LOADER_HPP_

#include <string>
#include <ticpp.h>

#include "graph.hpp"
#include "scene/composed_object.hpp"

namespace YAF
{

//! Loader class
/*!
    Loads a \a xml file information to a Graph structure.
*/
class Loader
{
public:
    struct CapturedPieces
    {
        CapturedPieces() : number(0), position(), difference() { }
        CapturedPieces(unsigned int n, glm::vec3 pos, glm::vec3 diff) : number(n), position(pos), difference(diff) { }

        bool operator< (const CapturedPieces& other) { return number < other.number; }
        bool operator== (const CapturedPieces& other) { return number == other.number; }

        unsigned int number;
        glm::vec3 position;
        glm::vec3 difference;
    };

    struct BoardSettings
    {
        glm::vec3 position;
        glm::vec2 size;
        glm::mat4 rotation;
        gl::MaterialPtr black_app;
        gl::MaterialPtr white_app;

        YAF::ComposedObjectPtr round_white_object;
        YAF::ComposedObjectPtr round_black_object;
        YAF::ComposedObjectPtr quad_white_object;
        YAF::ComposedObjectPtr quad_black_object;

        std::vector<CapturedPieces> player_1_capPieces;
        std::vector<CapturedPieces> player_2_capPieces;
    };

    //! Class constructor
    /*!
        \param path location of the file to be parsed.
    */
    Loader(const std::string& path);

    //! Loads the information into a Graph.
    /*!
        \return Graph structure with the file information.
    */
    Graph Load();

    BoardSettings GetBoardSettings() const { return _board_settings; }
private:

    YAF::Graph _graph;
    BoardSettings _board_settings;
    std::string _path;

    void LoadAnimations(ticpp::Element* elem);
    void LoadTextures(ticpp::Element* elem);
    void LoadMaterials(ticpp::Element* elem);
    void LoadCameras(ticpp::Element* elem);
    void LoadLighting(ticpp::Element * elem);
    void LoadSceneGlobals(ticpp::Element* elem);
    void LoadLightGlobals(ticpp::Element* elem);
    void LoadNodes(ticpp::Element* elem);

    gl::PrimitivePtr LoadPrimitive(ticpp::Element* elem);
    glm::mat4 LoadTransformations(ticpp::Element* elem);

    glm::mat4 GetTransformation(ticpp::Element* elem, bool throwIfNotFound = true);
    void LoadGounkiSettings(ticpp::Element* yafElement);
};

}

#endif /* YAF_LOADER_HPP_ */

