#ifndef ANIMATION_MANAGER_HPP__
#define ANIMATION_MANAGER_HPP__

#include <vector>
#include <utility>
#include <glm/glm.hpp>
#include <memory>

#include "gl/animation.hpp"
#include "gounki/board_visualizer.hpp"
#include "gounki/camera_animation.hpp"
#include "gounki/board.hpp"

namespace gounki
{
//! animation_manager class
/*!
    Responsible for updating all active animations and removing the finished ones
*/
class animation_manager
{
public:
    animation_manager();

    void init(board_visualizer* boardPositions, const board* logicBoard);

    void addAnimation(gl::AnimationPtr animation);
    void addMovementAnimation(gounki::movement movement, const gounki::board& board, double timeInSeconds);
    void addCameraAnimation(gl::PerspectiveCameraPtr cam, const glm::vec3& targetPosition, double timeInMs);
    void addDummyAnimation(double time);

    void update(double timeElapsed);
    bool hasAnimations() const;

private:

    void addCaptureAnimation(unsigned int row, unsigned int column, double timeInMilliseconds, colour properColour);

    std::array<glm::vec3, 3>& toRealPosition(unsigned int row, unsigned int column);
    std::array<glm::vec3, 3>& toRealPosition(const glm::uvec2& logicPosition);
    unsigned int getAvailablePos(unsigned int row, unsigned int column, colour properColour) const;
    colour getCellColor(unsigned int row, unsigned int column) const;
        
    std::vector<gl::AnimationPtr> _animations;
    board_visualizer* _boardPositions;
    const board* _logicBoard;
};

}

#endif /* ANIMATION_MANAGER_HPP__*/