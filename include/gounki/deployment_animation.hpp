#ifndef DEPLOYMENTANIMATION_HPP__
#define DEPLOYMENTANIMATION_HPP__

#include "gl/animation.hpp"
#include "gounki/linear_animation.hpp"

#include <array>

namespace gounki
{

//! deployment_animation class
class deployment_animation : public gl::Animation
{
public:
    deployment_animation(std::array<glm::vec3, 3>& _source, std::vector<glm::vec3> controlPoints, const glm::vec2& cell_size, double timeInMilliseconds);
    virtual void Update(double time);

private:
    std::vector<linear_animation> _linearAnimations;
    std::vector<linear_animation> _linearAnimations_2;
    bool _finished_all_animations;
};

}

#endif /* DEPLOYMENTANIMATION_HPP__ */