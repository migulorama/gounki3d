#ifndef DUMMY_ANIMATION_HPP__
#define DUMMY_ANIMATION_HPP__

#include "gl/animation.hpp"

namespace gounki
{

//! dummy_animation class
/*!
    An empty animation used to wait for an amount of time.
*/
class dummy_animation : public gl::Animation
{
public:
    dummy_animation(double time);
    virtual void Update(double time);

private:
    double _timeInMs;
    double _animTimeStart;
};

}

#endif // DUMMY_ANIMATION_HPP__
