#ifndef _GOUNKI_BOARD_HPP_
#define _GOUNKI_BOARD_HPP_

#include <vector>
#include <string>

#include "gounki/cell.hpp"
#include "gounki/moves.hpp"
#include "gounki/player.hpp"

namespace gounki
{

//! Represents a the players captured pieces
typedef std::array<gounki::cell::elements, gounki::player::Num_Players> captured_pieces;

//! board class
/*!
    Represents a logic board
*/
class board
{
public:
    board() { }
    board(unsigned int num_rows, unsigned int num_cols, const std::string& initialConfiguration);
    void update(const std::string& prologString);

    const std::string& get_prolog_board() const { return _prolog_board; }
    const std::vector<cell>& get_cells() const { return _cells; }

    const cell& cell_at(size_t row, size_t column) const { return _cells[row * _num_cols + column]; }
    const cell& cell_at(const position& position) const { return cell_at(position.x, position.y); }

    glm::uvec2 get_dimensions() const { return glm::uvec2(_num_rows, _num_cols); }

    unsigned int get_num_cols() const { return _num_cols; }
    unsigned int get_num_rows() const { return _num_rows; }

private:
    std::string _prolog_board;

    unsigned int _num_rows;
    unsigned int _num_cols;

    std::vector<cell> _cells;
};

}

#endif /* _GOUNKI_BOARD_HPP_ */