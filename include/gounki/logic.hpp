#ifndef LOGIC_HPP__
#define LOGIC_HPP__

#include "gounki/board.hpp"
#include "gounki/moves.hpp"

#include <asio.hpp>
#include <string>
#include <cstdint>
#include <ostream>
#include <vector>

#include <glm/glm.hpp>

#include <boost/optional.hpp>

namespace gounki
{

//! difficulty enum
enum class difficulty : uint8_t { Easy, Medium_Easy, Medium_Hard, Hard };

difficulty difficulty_from_string(std::string str);


//! logic class
/*!
    Responsible for communicating with the prolog module.
*/
class logic
{
public:
    logic() { };

    logic(const std::string& host, const std::string& port); ///< constructor that launches the process of the prolog module and establishes a connection with it.

    ~logic();

    gounki::board initialize() const;
    std::vector<movement> get_moves(const board& board, const position& pos) const;
    std::pair<gounki::board, std::vector<gounki::piece>> exec_move(const board& board, const gounki::movement& move) const;
    std::tuple<movement, board, std::vector<piece>> exec_pc_move(const board& board, const gounki::colour& colour, const gounki::difficulty& difficulty) const;
    boost::optional<unsigned int> is_game_finished(const gounki::board& b) const;
    void disconnect();
    
protected:
    std::string read() const;

private:
    static std::regex bracket_separator; 
    static std::regex spacePlusbracket;
    static std::regex yes_reg;
    static std::regex piece_splitter;

    mutable asio::ip::tcp::iostream _stream;

    HANDLE _hFile;
    PROCESS_INFORMATION _pi;
};

std::ostream& operator<<(std::ostream& out, const gounki::difficulty& diff);

}



#endif // LOGIC_HPP__
