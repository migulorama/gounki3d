#ifndef MYINTERFACE_HPP__
#define MYINTERFACE_HPP__

#include <array>
#include <cstdint>
#include <glm/glm.hpp>
#include <vector>

namespace gounki
{

class game_state;

//! pick_interface class
/*!
    Responsible for maintaining mouse state and picking objects.
*/
class pick_interface
{
public:
    explicit pick_interface(gounki::game_state* game_state);
    virtual ~pick_interface() { }

    void mouse_move(double x, double y);

    void mouse_button(int button, int action, int mods);

    void update(double timeElapsed);

    std::vector<int> pick_objects();
        
    bool is_left_button_pressed();
    bool is_right_button_pressed() { return _mouse.pressing_right; }
    bool is_middle_button_pressed() { return _mouse.pressing_middle; }

    int get_modifiers() { return modifiers; }

    glm::vec2 get_mouse_displacement() { return _mouse.prev_position - _mouse.position; }

    glm::vec2 get_mouse_position() { return _mouse.position; }

protected:
    struct
    {
        bool pressing_left;
        bool pressing_right;
        bool pressing_middle;

        glm::vec2 prev_position;
        glm::vec2 position;
    } _mouse;

    int modifiers;

private:
    gounki::game_state* _game_state;
};

}

#endif