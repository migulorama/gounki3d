#ifndef LINEARANIMATION_HPP__
#define LINERAANIMATION_HPP__

#include "gl/animation.hpp"

#include <vector>
#include <glm/glm.hpp>

namespace gounki
{

//! linear_animation class
class linear_animation : public gl::Animation
{
public:
    linear_animation(glm::vec3& source, std::vector<glm::vec3> control_points, double time_in_ms);
    virtual void Update(double time) override;

protected:

    struct Path
    {
        float timeStart;
        glm::vec3 positionStart;
        float distance;
        glm::vec3 direction;
    };

    size_t get_path(double anim_time) const;

    std::vector<Path> _paths;

    std::vector<glm::vec3> _controlPoints;
    double _time_in_ms;
    double _anim_time_start;
    double _speed_per_ms;

    glm::vec3 _position;

    glm::vec3& _source;
    glm::vec3 _sourceCache;
};

}
#endif /* LINEARANIMATION_HPP__*/