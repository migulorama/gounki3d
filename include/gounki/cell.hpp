#ifndef CELL_HPP__
#define CELL_HPP__

#include <cstdint>
#include <iostream>
#include <vector>

namespace gounki
{

//! colour enum
enum class colour : uint8_t
{
    Black,
    White,
    Blank
};

//! piece enum
enum class piece : uint8_t
{
    Square,
    Circle
};

colour colour_from_string(std::string str);
std::ostream& operator << (std::ostream& out, const colour& col);

piece piece_from_string(std::string str);
std::ostream& operator<< (std::ostream& out, const piece& col);

//! cell class
/*!
    Represents a logic cell
*/
class cell
{
    friend std::ostream& operator << (std::ostream& out, const cell& cell);

public:
    typedef std::vector<piece> elements;

    cell(const colour& colour, const elements& elems);
    cell(const colour& colour);

    const elements& get_elements() const { return _elements; }
    const colour& get_colour() const { return _colour; }
private:
    colour _colour;
    elements _elements;
};

cell parse_cell(const std::string& cell);
std::ostream& operator << (std::ostream& out, const cell& cell);

}


#endif /* CELL_HPP__ */
