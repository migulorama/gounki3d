#ifndef CAPTUREANIMATION_HPP__
#define CAPTUREANIMATION_HPP__

#include <array>
#include <vector>
#include <glm/glm.hpp>

#include "gl/animation.hpp"

namespace gounki
{

class linear_animation;
    
//! capture_animation class
class capture_animation : public gl::Animation
{
public:
    capture_animation(std::array<glm::vec3, 3>& _source, std::vector<glm::vec3> controlPoints, const glm::vec2& cell_size, double timeInMilliseconds);
    virtual void Update(double time);
private:
    std::vector<linear_animation> _linearAnimations;
};

}

#endif /* CAPTUREANIMATION_HPP__ */