#ifndef _GOUNKI_MAINGAMESTATE_HPP_
#define _GOUNKI_MAINGAMESTATE_HPP_

#include "gounki/game_state.hpp"
#include "gounki/logic.hpp"
#include "gounki/main_scene.hpp"
#include "gounki/pick_interface.hpp"

#include "gl/object.hpp"
#include "gl/camera.hpp"
#include "gl/lights.hpp"
#include "gl/primitives/model.hpp"
#include "gounki/animation_manager.hpp"
#include "gounki/player.hpp"

#include "history.hpp"

#include <memory>

namespace gounki
{
//! move_state enum
enum class move_state : uint8_t { Choose_Source, Deployment_Mode, Move_Mode, Finish };

//! mode enum
enum class mode : uint8_t { Player, Pc };

//! main_state class
/*!
    Responsible for the game functionality.
*/
class main_state : public game_state
{
public:
    main_state(state_manager& context);

    virtual void init() override;
    virtual void on_resume(game_state*) override;
    virtual void on_pause(game_state*) override;

    virtual void reshape(int width, int height) override;
    virtual void update(double time_elapsed) override;
    virtual void display(bool select_mode = false) override;

    virtual void mouse_move(double x, double y) override;
    virtual void mouse_button(int button, int action, int mods) override;

    void handle_choose_source(move_state, std::vector<int>);
    void handle_deployment_mode(move_state, std::vector<int>);
    void handle_move_mode(move_state, std::vector<int>);

    const gounki::board& get_current_board() const;
    const std::vector<piece>& get_current_player_captured_pieces() const;
    move_state get_current_move_state() const;
    mode get_current_player_mode() const;
    gounki::colour get_current_player_colour() const;
    gounki::colour get_player_colour(gounki::player::player player) const;

    const std::vector<uint32_t>& get_index_ids(size_t index) const;
    const position& get_selected_position();
    GLuint get_selected_position_id();
    unsigned int get_player_points(player::player p) const;

    gounki::difficulty get_current_player_difficulty() const;

    void set_player_difficulty(player::player player, gounki::difficulty val);
    void set_player_1_difficulty(gounki::difficulty val);
    void set_player_2_difficulty(gounki::difficulty val);

    void set_player_mode(player::player player, gounki::mode mode);

    void set_player_1_mode(gounki::mode mode);

    void set_player_2_mode(gounki::mode mode);

    void set_move_cameras(bool value);

    const std::vector<gounki::piece>& get_captured_pieces(gounki::player::player player) const;
    void undo();
    void redo();

    virtual void key_action(int key, int scancode, int action, int mods) override;

    ~main_state();

    static std::array<gounki::colour, gounki::player::Num_Players> _player_color;

protected:
    void save_game();

    void next_player();
    void set_player(gounki::player::player p);

    void exec_move(gounki::movement move);
    void exec_pc_move();

    move_state _current_move_state;
    player::player _current_player;

    position _selected_position;
    std::vector<gounki::movement> _moves;
    std::vector<gounki::movement> _deployments;
    std::array<std::vector<uint32_t>, 80> _ids;

    static std::array<glm::vec3, gounki::player::Num_Players> _player_position;
    std::array<mode, player::Num_Players> _player_mode;
    gounki::captured_pieces _play_captured_pieces;
    std::array<unsigned int, gounki::player::Num_Players> _player_points;
    std::array<gounki::difficulty, gounki::player::Num_Players> _player_difficulty;

    std::vector<gounki::movement> _executed_moves;

private:
    double _last_time;
    double _start_time;
    double _pause_time;
    double _pause_start_time;
    std::time_t _game_start;

    double _max_play_time;
    double _animation_time;

    std::unique_ptr<gounki::logic> _logic;

    gounki::board _current_board;

    game_history _history;

    bool _has_cached_info;
    gounki::board _cached_board;
    std::vector<piece> _cached_captured_pieces;

    pick_interface _mainInterface;
    gounki::main_scene _scene;

    bool _move_cameras;

    typedef void(main_state::*object_clicked_handler)(move_state, std::vector<int>);

    static std::unordered_map<move_state, object_clicked_handler> _object_clicked_handlers;
};
}
#endif /* _GOUNKI_MAINGAMESTATE_HPP_ */