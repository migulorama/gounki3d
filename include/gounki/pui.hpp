#ifndef PUI_HPP__
#define PUI_HPP__

#define GL_VERSION_1_1 1
#include <pu.h>
#include <puAux.h>
#include <fnt.h>
#include <memory>

namespace gounki
{
    template <typename T> using puPtr = std::unique_ptr<T, void(*)(puObject*)>;
}

int pu_glfw_get_window();
void pu_glfw_get_window_size(int *width, int *height);

struct overlay_borders
{
    unsigned int margin_left;
    unsigned int margin_right;
    unsigned int margin_bottom;
    unsigned int margin_top;
};

#endif /* PUI_HPP__ */