#ifndef PLAYER_HPP__
#define PLAYER_HPP__

#include <cstdint>

namespace gounki
{
    //! player enum
    namespace player { enum player : uint8_t { Player1 = 0, Player2 = 1, Num_Players }; }
}

#endif // PLAYER_HPP__
