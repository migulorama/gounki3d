#ifndef _GOUNKI_MAIN_SCENE_HPP__
#define _GOUNKI_MAIN_SCENE_HPP__

#include <array>
#include "gl/cameras.hpp"
#include "gl/lights.hpp"
#include "gl/material.hpp"
#include "gl/primitives/model.hpp"
#include <glm/glm.hpp>
#include "gounki/animation_manager.hpp"
#include "gounki/board_visualizer.hpp"
#include "scene/graph.hpp"
#include "scene/composed_object.hpp"
#include "gounki/pui.hpp"

namespace gounki
{

class cell;
class main_state;

//! main_scene class
/*!
    Draws the game scene.
*/
class main_scene
{
public:
    main_scene(main_state& gs);

    void init();
    void on_resume();
    void on_pause();
    void draw(bool select_mode = false);
    void update(double time);
    void update_round_time(double time);
    void reshape(int width, int height);

    void set_player_score(player::player p, unsigned int score);
    void set_current_player(player::player p);
    void update_player_scores();

    glm::vec3 get_player_position(const player::player& player) const { return _player_position[player]; }

    animation_manager& get_animation_manager();
    board_visualizer&  get_board_visualizer();

    std::shared_ptr<gl::Cameras::Perspective>& get_camera() { return _camera; }

    const glm::vec3& get_board_normal_vector(); 
    const glm::vec3& get_board_right_vector();

    void disableUndoRedo();
    void enableUndoRedo();

        void activate_pause_dialog(bool value);
        bool has_active_pause_dialog() const;

        void interrupt_button_cb(puObject* obj);
protected:
    YAF::ComposedObjectPtr get_object(const gounki::colour& colour, const gounki::piece& piece);

    void draw_cell(const gounki::cell& cell, const std::array<glm::vec3, 3>& cellPositions);

    // Cameras
    std::shared_ptr<gl::Cameras::Perspective> _camera;

    // Meshes
    YAF::ComposedObjectPtr _quad_white_mesh;
    YAF::ComposedObjectPtr _round_white_mesh;
    YAF::ComposedObjectPtr _quad_black_mesh;
    YAF::ComposedObjectPtr _round_black_mesh;

    // Materials
    gl::MaterialPtr _white_mat;
    gl::MaterialPtr _black_mat;
    gl::MaterialPtr _white_transparent_mat;
    gl::MaterialPtr _highlight_move_mat;
    gl::MaterialPtr _highlight_deploy_mat;

    animation_manager _animation_manager;
    board_visualizer _board_visualizer;

    YAF::Graph _scene;

    std::array<glm::vec3, gounki::player::Num_Players> _player_position;
    glm::mat4 _board_rotation;
    glm::vec3 _board_normal;
    glm::vec3 _board_right_vector;
       
    // Interface controls
    puPtr<puGroup>                    _overlay_group;
    puPtr<puText>                     _time_text;
    puPtr<puText>                     _player_1_points_text;
    puPtr<puText>                     _player_2_points_text;
    puPtr<puText>                     _round_time_static_text;
    puPtr<puText>                     _player_1_static_text;
    puPtr<puText>                     _player_2_static_text;
    puPtr<puText>                     _move_cameras_static_text;
    puPtr<puText>                     _elapsed_time_text;
    puPtr<puText>                     _elapsed_time_static_text;

    puPtr<puArrowButton>              _undo_button;
    puPtr<puArrowButton>              _redo_button;
    puPtr<puButton>                   _move_camera_button;
    std::unique_ptr<puFont>           _global_font;
    std::unique_ptr<puFont>           _score_font;
    std::unique_ptr<puFont>           _move_cameras_font;

        puPtr<puDialogBox> _pause_dialog;
        puPtr<puText>      _dialog_text;
        puPtr<puFrame>     _dialog_frame;
        puPtr<puOneShot>   _dialog_ok_button;
        puPtr<puOneShot>   _dialog_interrupt_button;

    std::string elapsed_time_str;
    std::string time_str;
    std::string _player_1_score_str;
    std::string _player_2_score_str;
    std::string session_elapsed_time_str;

private:
    double _session_start_time;
    double _session_elapsed_time;
    main_state& _main_game_state;
};

}

#endif // _GOUNKI_MAIN_SCENE_HPP__
