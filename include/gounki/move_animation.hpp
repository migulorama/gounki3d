#ifndef MOVEANIMATION_HPP__
#define MOVEANIMATION_HPP__

#include <glm/glm.hpp>
#include <array>

#include "gl/animation.hpp"
#include "gounki/moves.hpp"

namespace gounki
{

//! move_animation class
class move_animation : public gl::Animation
{
public:
    move_animation(std::array<glm::vec3, 3>& source, std::vector<glm::vec3> control_points, double time_in_ms);
    virtual void Update(double time) override;

private:

    struct path
    {
        float timeStart;
        glm::vec3 positionStart;
        float distance;
        glm::vec3 direction;
    };

    size_t get_path(double animTime) const;

    std::vector<path> _paths;

    std::vector<glm::vec3> _controlPoints;
    double _time_in_ms;
    double _anim_time_start;
    double _speed_per_ms;

    glm::vec3 _position;

    std::array<glm::vec3, 3>& _source;
    std::array<glm::vec3, 3> _sourceCache;
};

}

#endif /* MOVEANIMATION_HPP__ */