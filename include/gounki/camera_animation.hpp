#ifndef CAMERAANIMATION_HPP__
#define CAMERAANIMATION_HPP__

#include "gl/animation.hpp"
#include "gl/cameras/perspective.hpp"

namespace gounki
{

//! Translates a Camera's position
class camera_animation : public gl::Animation
{
public:
    camera_animation(gl::PerspectiveCameraPtr cam, const glm::vec3& targetPosition, double timeInMs);
    virtual void Update(double time) override;

private:

    double _angle;
    glm::vec2 _angles_per_ms;
    double _speedPerMs;
    double _timeInMs;
    double _animTimeStart;
    double _last_update;

    glm::vec3 _target_position;
    glm::vec3 _rotation_axis;

    gl::PerspectiveCameraPtr _cam;

    bool _doReset;
};

}

#endif /* CAMERAANIMATION_HPP__ */