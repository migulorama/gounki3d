#ifndef BOARDVISUALIZER_HPP__
#define BOARDVISUALIZER_HPP__

#include <vector>
#include <array>
#include <glm/glm.hpp>

#include "gounki/logic.hpp"
#include "gounki/player.hpp"

#include "scene/loader.hpp"

namespace gounki
{

//! board_visualizer class
/*!
    Contains information about the position and transformations of the board and it's pieces.
*/
class board_visualizer
{
public:
    board_visualizer();
    board_visualizer(const gounki::board& logicBoard, const std::vector<YAF::Loader::CapturedPieces>& cpp1, const std::vector<YAF::Loader::CapturedPieces>& cpp2, const glm::mat4& board_rotation);
    
    void init(const gounki::board& logicBoard, std::vector<YAF::Loader::CapturedPieces> cpp1, std::vector<YAF::Loader::CapturedPieces> cpp2, const glm::mat4& board_rotation);
    void reset();

    std::array<glm::vec3, 3>& get_cell_position(unsigned int row, unsigned int column);
    const std::array<glm::vec3, 3>& get_default_cell_position(unsigned int row, unsigned int column) const;

    glm::vec3 get_captured_cells_position(gounki::player::player player) const;
    glm::vec3 get_captured_cells_position(gounki::player::player player, unsigned int idx);
    glm::vec3 get_next_captured_cells_position(player::player player);

    glm::vec2 get_cell_size() const { return _cell_size; }

    const glm::mat4& get_transforms() const { return _board_transforms; }
    const glm::mat4& get_inv_transforms() const { return _inv_board_transforms; }

    void set_size(const glm::vec2& size) { _size = size; }
    glm::vec2 get_size() const { return _size; }
    void set_position(const glm::vec3& position) { _position = position; }
    glm::vec3 get_position() const { return _position; }

    void set_current_captured_index(unsigned int player_1, unsigned int player_2);

private:
    unsigned int _numRows;
    unsigned int _numCols;

    glm::vec3 _position;
    glm::vec2 _size;
    glm::vec2 _cell_size;

    glm::mat4 _board_transforms;
    glm::mat4 _inv_board_transforms;

    std::array<std::vector<glm::vec3>, player::Num_Players> _capturedPieces;
    std::array<unsigned int, player::Num_Players> _currentIndex;

    std::vector<std::array<glm::vec3, 3>> _initialConfiguration;
    std::vector<std::array<glm::vec3, 3>> _currentConfiguration;
};

}

#endif /* BOARDVISUALIZER_HPP__ */