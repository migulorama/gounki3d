#ifndef SETTINGSSTATE_HPP__
#define SETTINGSSTATE_HPP__

#include "gounki/game_state.hpp"
#include "gounki/pick_interface.hpp"

#include <gl_4_4.h>
#include "gounki/pui.hpp"

#include <memory>
#include <filesystem>

namespace gounki
{

//! settings_state class
/*! 
    Setting state menu
*/
class settings_state : public game_state
{
public:
    settings_state(state_manager& manager);
    ~settings_state();

    virtual void init();

    virtual void update(double time_elapsed);

    virtual void display(bool select_mode = false);

    virtual void reshape(int width, int height);

    virtual void mouse_button(int button, int action, int mods) override;

    virtual void mouse_move(double x, double y) override;

    virtual void key_action(int key, int scancode, int action, int mods) override;

    virtual void on_pause(game_state*);

    void cb_callback(puObject*);
    void back_button_cb(puObject*);
    void reset_button_cb(puObject*);
    void button_cb(puObject*);
    void apply_button_cb(puObject*);

    virtual void on_resume(game_state*);

protected:
    std::vector<std::tr2::sys::path> get_all_themes();

    std::vector<std::string> _themes;

private:
    virtual void resize(float width_factor, float height_factor);

    struct background_img
    {
        unsigned int margin_left;
        unsigned int margin_right;
        unsigned int margin_bottom;
        unsigned int margin_top;
    };

    background_img menu_background;

    puPtr<puOneShot>   b;
    puPtr<puOneShot>   settings_mode_button;
    puPtr<puOneShot>   start_button;
    puPtr<puOneShot>   settings_back_button;
    puPtr<puOneShot>   settings_apply_button;
    puPtr<puOneShot>   settings_reset_button;

    puPtr<puInput>     _round_time_input;
    puPtr<puInput>     _animation_time_input;

    puPtr<puaComboBox> _game_enviroment_cb;
    puPtr<puGroup>     _settings_group;

    puPtr<puText>      _environment_text;
    puPtr<puText>      _round_time_text;
    puPtr<puText>      _animation_time_text;
    std::unique_ptr<fntTexFont> _times_roman_fnt;

    fntRenderer _text_out;

    pick_interface _interface;
};

}

#endif /* SETTINGSSTATE_HPP__ */