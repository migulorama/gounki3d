#ifndef THEATER_STATE_HPP__
#define THEATER_STATE_HPP__

#include "gounki/animation_manager.hpp"
#include "gounki/board.hpp"
#include "gounki/game_state.hpp"
#include "gounki/logic.hpp"
#include "gounki/moves.hpp"
#include "gounki/player.hpp"
#include "gounki/theater_scene.hpp"
#include "gounki/pick_interface.hpp"

#include "history.hpp"

#include <fstream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

namespace gounki
{
//! theater_state class
/*!
    State to visualize a game that has been saved.
*/
class theater_state : public game_state
{
public:
    theater_state(state_manager& context);

    virtual void init();

    virtual void update(double time_elapsed);

    virtual void display(bool select_mode);

    virtual void reshape(int width, int height);

    virtual void mouse_move(double x, double y) override;
    virtual void mouse_button(int button, int action, int mods) override;

    virtual void key_action(int key, int scancode, int action, int mods) override;

    virtual void on_resume(game_state*);

    virtual void on_pause(game_state*);

    void undo();

    void redo();

    void set_file_name(const std::string& file);

    const gounki::board& get_current_board() const;

    gounki::colour get_player_colour(gounki::player::player player) const;
    const std::vector<gounki::piece>& get_captured_pieces(gounki::player::player player) const;

    void set_auto_play(bool value);
protected:
    void set_player(gounki::player::player p);

    

private:
    gounki::board _current_board;
    gounki::player::player _current_player;
    std::unique_ptr<gounki::logic> _logic;
    game_history _history;
    gounki::captured_pieces _captured_pieces;
    pick_interface _interface;

    gounki::theater_scene _scene;
    std::string _file_name;

    bool _auto_play_enabled;
    bool _player_transition_pending;
    bool _has_cached_info;
};


}


#endif // THEATER_STATE_HPP__
