#ifndef _GOUNKI_GAME_CPP__
#define _GOUNKI_GAME_CPP__

#include <glm/glm.hpp>
#include "gounki/state_manager.hpp"
#include <string>
#include <window.hpp>

namespace gounki
{

//! game class
/*!
     Main class which is responsible for the main loop and contains configurations and state_manager.
*/
class game
{
public:
    game(const std::string& window_title = "", const glm::uvec2& window_size = glm::uvec2(500, 500));
    ~game();

    void init();
    void run();
    void terminate();

    template <typename St> void change_to_state() { _state_manager.change_to_state<St>(); }

    double get_time() const;
    void set_input_listener(framework::input_listener* in_listener);

    void request_reshape() { _reshape_requested = true; }

    void set_yaf_filename(const std::string& name);
    std::string get_yaf_filename() const { return _yaf_filename; }

    void set_save_filename(const std::string& name) { _save_filename = name; }
    std::string get_save_filename() const { return _save_filename; }

    void set_animation_time(unsigned int time) { _animation_time = time; }
    unsigned int get_animation_time() const { return _animation_time; }

    void set_round_time(unsigned int time) { _round_time = time; }
    unsigned int get_round_time() const { return _round_time; }

    framework::window* get_window() { return &_window; }

protected:
    void reshape();

    glm::uvec2 _window_size;
    std::string _window_title;
    std::string _yaf_filename;
    std::string _save_filename;

    unsigned int _animation_time;
    unsigned int _round_time;

    bool _reshape_requested;

private:
    framework::window _window;

    gounki::state_manager _state_manager;
};

}


#endif // _GOUNKI_GAME_CPP__
