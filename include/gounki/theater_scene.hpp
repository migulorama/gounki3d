#ifndef THEATER_SCENE_HPP__
#define THEATER_SCENE_HPP__

#include <array>
#include "gl/cameras.hpp"
#include "gl/lights.hpp"
#include "gl/material.hpp"
#include <glm/glm.hpp>
#include "gounki/animation_manager.hpp"
#include "gounki/board_visualizer.hpp"
#include "scene/graph.hpp"
#include "scene/composed_object.hpp"
#include "gounki/pui.hpp"

namespace gounki
{

class theater_state;

//! theater_scene state
/*!
    Draws the theater scene.
*/
class theater_scene
{
public:
    theater_scene(theater_state& ts);

    void init();
    void on_resume();
    void on_pause();
    void draw();
    void update(double time);
    void reshape(int width, int height);

    animation_manager& get_animation_manager();
    board_visualizer&  get_board_visualizer();

    std::shared_ptr<gl::Cameras::Perspective>& get_camera() { return _camera; }

    glm::vec3 get_player_position(const player::player& player) const { return _player_position[player]; }

    const glm::vec3& get_board_normal_vector();
    const glm::vec3& get_board_right_vector();

    void disableUndoRedo();
    void enableUndoRedo();

protected:
    YAF::ComposedObjectPtr get_object(const gounki::colour& colour, const gounki::piece& piece);

    void draw_cell(const gounki::cell& cell, const std::array<glm::vec3, 3>& cellPositions);

    // Cameras
    std::shared_ptr<gl::Cameras::Perspective> _camera;

    // Lights
    std::unique_ptr<gl::Light> _light;

    // Meshes
    YAF::ComposedObjectPtr _quad_white_mesh;
    YAF::ComposedObjectPtr _round_white_mesh;
    YAF::ComposedObjectPtr _quad_black_mesh;
    YAF::ComposedObjectPtr _round_black_mesh;

    // Materials
    gl::MaterialPtr _white_mat;
    gl::MaterialPtr _black_mat;
    gl::MaterialPtr _white_transparent_mat;

    animation_manager _animation_manager;
    board_visualizer _board_visualizer;

    YAF::Graph _scene;

    std::array<glm::vec3, gounki::player::Num_Players> _player_position;
    glm::mat4 _board_rotation;
    glm::vec3 _board_normal;
    glm::vec3 _board_right_vector;

    // interface controls
    puPtr<puGroup>                    _controls_group;

    puPtr<puArrowButton>              _undo_button;
    puPtr<puArrowButton>              _redo_button;
    puPtr<puArrowButton>              _auto_play_button;

    std::unique_ptr<puFont>           _global_font;

    overlay_borders _overlay_borders;

private:
    theater_state& _state;
};

}

#endif // THEATER_SCENE_HPP__
