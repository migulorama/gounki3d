#ifndef _GOUNKI_STATEMANAGER_HPP_
#define _GOUNKI_STATEMANAGER_HPP_

#include <unordered_map>
#include <typeindex>

namespace gounki
{
class game;
class game_state;

///! state_manager class

/*!
    Entity responsible for handling and changing the current game_state
*/
class state_manager
{
public:
    state_manager(game& game);
    ~state_manager();

    void init(); ///< Initializes the state_manager and the already added game_states

    void add_state(game_state* state); ///< Adds state to the state_manager
    template <typename DestState> void add_state(); ///< Adds or replaces an instance of DestState to the state_manager
    template <typename DestState> void change_to_state(); ///< Changes the current state to an instance of DestState
    template <typename DestState> bool has_state(); ///< Checks if there is an instance of DestState in the state_manager

    game_state* get_current_state(); ///< Returns the current game_state

    void update(); ///< Updates the current game_state
    void display(); ///< Displays the current game_state

    void terminate(); ///< Shortcut to the method of the same name of the game class 

    unsigned int get_window_width(); ///< Returns the width of the game window
    unsigned int get_window_height(); ///< Returns the height of the game window

    virtual void reshape(int width, int height); ///< Calls the method of the same name of the current game_state

    gounki::game* get_game() { return &_game; } ///< Returns the game

protected:
    gounki::game& _game;

private:
    bool _initialized;

    std::unordered_map<std::type_index, game_state*> _states;
    game_state* _currentState;
};
}

#include "gounki/game.hpp"


template <typename DestState>
bool gounki::state_manager::has_state()
{
static_assert(std::is_base_of<game_state, DestState>::value, "State must derive from GameState.");
std::type_index states_id(typeid(DestState));
auto state_itr = _states.find(states_id);
return state_itr != _states.end();
}



template <typename DestState>
void gounki::state_manager::add_state()
{
static_assert(std::is_base_of<game_state, DestState>::value, "State must derive from GameState.");

std::type_index states_id(typeid(DestState));

auto state_itr = _states.find(states_id);
if (state_itr == _states.end())
{
    state_itr = _states.emplace(states_id, new DestState(*this)).first;
    if (_initialized) state_itr->second->init();
}
else
{
    delete state_itr->second;
    state_itr->second = new DestState(*this);
    if (_initialized) state_itr->second->init();
}
}

template <typename DestState>
void gounki::state_manager::change_to_state()
{
static_assert(std::is_base_of<game_state, DestState>::value, "State must derive from GameState.");

std::type_index states_id(typeid(DestState));

auto state_itr = _states.find(states_id);
if (state_itr == _states.end()) 
{
    state_itr = _states.emplace(states_id, new DestState(*this)).first;
    if (_initialized) state_itr->second->init();
}

auto newState = state_itr->second;

if (_initialized && _currentState) _currentState->on_pause(newState);

auto old_state = _currentState;
_currentState = newState;
    
if (_initialized)
    _currentState->on_resume(old_state);

_game.set_input_listener(_currentState);

_game.request_reshape(); 
}

#endif /* _GOUNKI_STATEMANAGER_HPP_ */