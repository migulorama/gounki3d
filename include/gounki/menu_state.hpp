#ifndef MENU_STATE_HPP__
#define MENU_STATE_HPP__

#include "gounki/game_state.hpp"
#include "gounki/pick_interface.hpp"

#include <gl_4_4.h>
#include "gounki/pui.hpp"

#include <memory>


namespace gounki
{

//! menu_state class
/*!
    Main game menu.
*/
class menu_state : public game_state
{
public:
    menu_state(state_manager& manager);
    ~menu_state();

    virtual void init();

    virtual void update(double time_elapsed);

    virtual void display(bool select_mode = false);

    virtual void reshape(int width, int height);

    virtual void mouse_button(int button, int action, int mods) override;

    virtual void mouse_move(double x, double y) override;

    virtual void key_action(int key, int scancode, int action, int mods) override;

    virtual void on_pause(game_state*);

    void cb_callback(puObject*);
    void back_button_cb(puObject*);
    void button_cb(puObject*);
    void file_selector_cb(puObject*);
    void theater_mode_cb(puObject*);

    virtual void on_resume(game_state*);

private:

    virtual void resize(float width_factor, float height_factor);

    struct background_img
    {
        unsigned int margin_left;
        unsigned int margin_right;
        unsigned int margin_bottom;
        unsigned int margin_top;
    };

    background_img menu_background;

    puPtr<puOneShot>   b;
    puPtr<puOneShot>   theater_mode_button;
    puPtr<puOneShot>   settings_mode_button;
    puPtr<puOneShot>   start_button;
    puPtr<puOneShot>   back_button;
    puPtr<puaComboBox> player_1_mode_cb;
    puPtr<puaComboBox> player_2_mode_cb;
    puPtr<puaComboBox> player_1_diff_cb;
    puPtr<puaComboBox> player_2_diff_cb;
    puPtr<puaFileSelector> theater_mode_file_selector;

    puPtr<puGroup>     _pu_group;

    puPtr<puText>      _player_1_text;
    puPtr<puText>      _player_2_text;
    puPtr<puText>      _vs_text;
    std::unique_ptr<fntTexFont> _times_roman_fnt;


    fntRenderer _text_out;

    pick_interface _interface;
};

}

#endif // MENU_STATE_HPP__
