#ifndef _GOUNKI_GAMESTATE_HPP_
#define _GOUNKI_GAMESTATE_HPP_

#include "gounki/state_manager.hpp"
#include <interface.hpp>
#include <window.hpp>

namespace gounki
{
    //! game_state interface class
	class game_state : public framework::input_listener
	{
	public:
		game_state(state_manager& context);
        virtual ~game_state() { }
        
        std::type_index get_type() const;

        virtual void init() = 0; ///< Initializes the state
        virtual void on_resume(game_state*) {} ///< Called when the current state changes to this
		virtual void on_pause(game_state*) {} ///< Called when the current state changes from this

        virtual void update(double time_elapsed) = 0; ///< Updates the state
        virtual void display(bool select_mode) = 0; ///< Displays the state
        virtual void reshape(int width, int height) = 0; ///< Called when the size of the window's frame buffer changes

        template <typename DestState> void change_to_state(); ///< Shortcut the the method of the same name of the state_manager

        virtual void mouse_move(double x, double y) override { } ///< Called when the mouse moves
        virtual void mouse_button(int button, int action, int mods) override { } ///< Called when a mouse button is pressed or released
        virtual void mouse_scroll(double xOffset, double yOffset) override { } ///< Called when the mouse wheel is scrolled
        virtual void key_action(int key, int scancode, int action, int mods) override { } ///< Called when a key is pressed, released or continuous pressed
        virtual void char_input(unsigned int character) override { } ///< Called when a printable character is written in the keyboard

        framework::window* get_window(); ///< Shortcut to the method of the same name of the state_manager

        state_manager* get_context() { return &_context; } ///< Returns the state_manager

    protected:
        state_manager& _context;
    };
}

template <typename DestState> 
void gounki::game_state::change_to_state()
{
    _context.change_to_state<DestState>();
}

#endif /* _GOUNKI_GAMESTATE_HPP_ */