#ifndef MOVES_HPP__
#define MOVES_HPP__

#include <glm/glm.hpp>

#include <sstream>
#include <vector>
#include <regex>

#include <boost/lexical_cast.hpp>

#include "gounki/cell.hpp"

namespace gounki
{
    //! position typedef
    typedef glm::uvec2 position;

    //! movement class
    /*!
        Represents a gounki movement
    */
    class movement
    {
    public:
        movement() { }
        explicit movement(const std::string& str);

        std::string get_prolog_string() const; ///< Returns the prolog string

        bool is_deployment() const { return !_positions.empty() && !_pieces.empty(); } 
        bool is_simple_move() const { return !_positions.empty() && _pieces.empty(); }

        const std::vector<position>& get_positions() const { return _positions; }
        const std::vector<piece>& get_pieces() const { return _pieces; }

    private:
        static std::regex move_regex;
        static std::regex position_splitter;
        static std::regex deployment_regex;
        static std::regex check_regex;
        static std::regex piece_splitter;

        std::string _prolog_string;

        std::vector<piece> _pieces;
        std::vector<position> _positions;
    };
}

#endif // MOVES_HPP__
