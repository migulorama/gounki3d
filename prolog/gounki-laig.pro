:- use_module(library(lists)).
:- use_module(library(sets)).

%%%%%%%%%%%%%%%%%%%%%%
%% Lists Predicates %%
%%%%%%%%%%%%%%%%%%%%%%

length_(Num_Elems, List) :- length(List, Num_Elems).

number_of(Element, List, Number) :-
    delete(List, Element, Remain),
    length(List, N),
    length(Remain, NR),
    Number is N - NR.
    
%%%%%%%%%%%%%%%%%%%%%%%%%%    
%%%% BOARD PREDICATES %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
    
%%%%%%%%%%%%%%%%%%%%
%% Board Creation %%
%%%%%%%%%%%%%%%%%%%%

create_empty_board(Num_Rows, Num_Columns, Board) :-
    board_dimensions(Board, Num_Rows, Num_Columns).
    
create_new_board(Num_Rows, Num_Columns, Board) :-
    create_empty_board(Num_Rows, Num_Columns, Board),
    initialize_board(Board).
    
board_dimensions(Board, position(Num_Rows, Num_Columns)) :- board_dimensions(Board, Num_Rows, Num_Columns).
    
board_dimensions(Board, Num_Rows, Num_Columns) :-
    board_num_rows(Board, Num_Rows),
    board_num_columns(Board, Num_Columns).
    
board_num_rows(Board, Num_Rows) :-
    nonvar(Num_Rows), !,
    Effective_Num_Rows is Num_Rows + 2,
    length(Board, Effective_Num_Rows).
    
board_num_rows(Board, Num_Rows) :-
    length(Board, Effective_Num_Rows),
    Num_Rows is Effective_Num_Rows - 2.
    
board_num_columns(Board, Num_Columns) :-
    maplist(length_(Num_Columns), Board).
    
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Board Initialization %%
%%%%%%%%%%%%%%%%%%%%%%%%%%
    
initialize_board(Board) :-
    append([OUT_Row, First_Row, Second_Row | Blank_Rows], [Second_Last_Row, Last_Row, OUT_Row], Board),
    maplist(is_cell_blank, OUT_Row),
    initialize_row_alternate(black, circle, First_Row),
    initialize_row_alternate(black, square, Second_Row),
    initialize_row_alternate(white, circle, Second_Last_Row),
    initialize_row_alternate(white, square, Last_Row),
    maplist(maplist(is_cell_blank), Blank_Rows).
    
initialize_row_alternate(_, _, []).

initialize_row_alternate(Colour, Piece, [cell(Colour, [Piece]) | Row_Tail]) :-
    different_pieces(Piece, Other_Piece),
    initialize_row_alternate(Colour, Other_Piece, Row_Tail).
    
%%%%%%%%%%%%%%%%%%%%
%% Board Querying %%
%%%%%%%%%%%%%%%%%%%%

position(Row, Column) :-
    integer(Row),
    integer(Column).
    
is_position(position(Row, Column)) :- position(Row, Column).

position_diff(position(Row_1, Column_1), position(Row_2, Column_2), position(Row_Diff, Column_Diff)) :-
    Row_Diff is Row_1 - Row_2,
    Column_Diff is Column_1 - Column_2.
    
position_add(position(Row_1, Column_1), position(Row_2, Column_2), position(Row_Total, Column_Total)) :-
    Row_Total is Row_1 + Row_2,
    Column_Total is Column_1 + Column_2.
    
position_less(position(Row_1, Column_1), position(Row_2, Column_2)) :-
    Row_1 < Row_2,
    Column_1 < Column_2.
    
position_less_equal(position(Row_1, Column_1), position(Row_2, Column_2)) :-
    Row_1 =< Row_2,
    Column_1 =< Column_2.
    
position_greater(position(Row_1, Column_1), position(Row_2, Column_2)) :-
    Row_1 > Row_2,
    Column_1 > Column_2.
    
position_greater_equal(position(Row_1, Column_1), position(Row_2, Column_2)) :-
    Row_1 >= Row_2,
    Column_1 >= Column_2.

cell_at(position(Num_Row, Num_Column), Board, Cell) :-
    nth0(Num_Row, Board, Row),
    nth1(Num_Column, Row, Cell).
    
cell_of_colour(Position, Board, Colour) :-
    cell_at(Position, Board, cell(Colour, _)).
    
board_has_cell_of_colour(Board, Colour) :-
    cell_of_colour(_, Board, Colour).
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Cells (State Representation) %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

is_piece(square).
is_piece(circle).

is_square(square).
is_circle(circle).

different_pieces(square, circle).
different_pieces(circle, square).

is_valid_colour(black).
is_valid_colour(white).

different_valid_colour(black, white).
different_valid_colour(white, black).

is_valid_elem_list([First_Elem]) :- 
    is_piece(First_Elem).

is_valid_elem_list([First_Elem, Second_Elem]) :- 
    is_piece(First_Elem), 
    is_valid_elem_list([Second_Elem]).

is_valid_elem_list([First_Elem, Second_Elem, Third_Elem]) :-
    is_piece(First_Elem), 
    is_valid_elem_list([Second_Elem, Third_Elem]).
    
cell(blank).
is_cell_blank(cell(blank)).

cell(Colour, Elem_List) :-
    is_valid_colour(Colour),
    is_valid_elem_list(Elem_List).
    
is_cell(cell(blank)).
is_cell(cell(_, _)).

%%%%%%%%%%%
%% Moves %%
%%%%%%%%%%%

next_row(black, Row, Next_Row, Number) :- Next_Row is Row + Number.
next_row(white, Row, Next_Row, Number) :- Next_Row is Row - Number.

value_unit_diff(Val_1, Val_2,  0) :- Val_1 =:= Val_2.
value_unit_diff(Val_1, Val_2, -1) :- Val_1  <  Val_2.
value_unit_diff(Val_1, Val_2,  1) :- Val_1  >  Val_2.

position_unit_diff(position(Row_1, Column_1), position(Row_2, Column_2), position(Row_Unit_Diff, Column_Unit_Diff)) :-
    value_unit_diff(Row_1, Row_2, Row_Unit_Diff),
    value_unit_diff(Column_1, Column_2, Column_Unit_Diff).
    
path_from_to(Position, Position, [Position]) :- is_position(Position), !.

path_from_to(First_Position, Last_Position, [First_Position | Tail]) :-
    position_unit_diff(Last_Position, First_Position, Unit_Diff_Position),
    position_add(First_Position, Unit_Diff_Position, Next_Position),
    path_from_to(Next_Position, Last_Position, Tail).
    
move_in_board(Board_Dimensions, Move) :-
    last(Move, Destination),
    position_greater_equal(Destination, position(0,1)),
    position_add(Board_Dimensions, position(1, 0), Valid_Board_Dimensions),
    position_less_equal(Destination, Valid_Board_Dimensions).
    
possible_moves(position(_, _), position(_, _), _, _, 0, []) :- !.
    
possible_moves(position(Row, Column), position(Num_Board_Rows, Num_Board_Columns), Piece, Colour, Number_Pieces, Moves) :-
    next_row(Colour, Row, Final_Row, Number_Pieces),
    Final_Column_1 is Column + Number_Pieces,
    Final_Column_2 is Column - Number_Pieces,
    Next_Number is Number_Pieces - 1,
    possible_moves_aux(Piece, Row, Column, Final_Row, Final_Column_1, Final_Column_2, Temp_Moves),
    bounce_moves(Temp_Moves, Num_Board_Columns, Temp_Bounced_Moves),
    include(move_in_board(position(Num_Board_Rows, Num_Board_Columns)), Temp_Bounced_Moves, Final_Moves),
    append(Final_Moves, Rest_Of_Moves, Moves),
    possible_moves(position(Row, Column), position(Num_Board_Rows, Num_Board_Columns), Piece, Colour, Next_Number, Rest_Of_Moves).
    
possible_moves_aux(_, Row, Column, Row, Column, Column, []) :- !.
    
possible_moves_aux(circle, Row, Column, Final_Row, Final_Column_1, Final_Column_2, [First_Path, Second_Path]) :- !,
    path_from_to(position(Row, Column), position(Final_Row, Final_Column_1), First_Path ),
    path_from_to(position(Row, Column), position(Final_Row, Final_Column_2), Second_Path).
    
possible_moves_aux(square, Row, Column, Final_Row, Final_Column_1, Final_Column_2, [First_Path, Second_Path, Third_Path]) :-
    path_from_to(position(Row, Column), position(Final_Row, Column),         First_Path ),
    path_from_to(position(Row, Column), position(Row,       Final_Column_1), Second_Path),
    path_from_to(position(Row, Column), position(Row,       Final_Column_2), Third_Path ).

bounce_moves(Move, Num_Board_Columns, Bounced_Move) :-
    maplist(bounce_path(Num_Board_Columns), Move, Bounced_Move).
    
bounce_path(_, [], []) :- !.

bounce_path(_, [position(Row, 1) | Tail], [position(Row, 1) | Tail]) :- !.

bounce_path(Number_Board_Columns, [position(Row, Col) | Tail], [position(Row, Col) | Tail]) :-
    Col =:= Number_Board_Columns, !.
    
bounce_path(Number_Board_Columns, [position(Row, Col) | Tail], [position(Row, Col) | TailB]) :-
    Col >= 2,
    Col < Number_Board_Columns,
    maplist(bounce_position(Number_Board_Columns), Tail, TailB).
    
bounce_position(Number_Board_Columns, position(Row, Col), position(Row, ColB)) :-
    Col > Number_Board_Columns,
	ColB is 2 * Number_Board_Columns - Col.
    
bounce_position(_, position(Row, Col), position(Row, ColB)) :-
    Col < 1,
	ColB is 2 - Col.
    
bounce_position(_, Position, Position) :- is_position(Position).
   

possible_deployments(_, _, _, Num_Squares, Num_Circles, []) :-
    Total_Pieces is Num_Squares + Num_Circles,
    Total_Pieces =< 1, !.
    
possible_deployments(Position, Board_Dimensions, Colour, Num_Squares, Num_Circles, Deployments) :-
    possible_deployments_aux(Position, Board_Dimensions, Colour, Num_Squares, Num_Circles, Deployments).
    

possible_deployments_aux(_, _, _, 0, 0, []) :- !.

possible_deployments_aux(position(Row, Column), position(Num_Board_Rows, Num_Board_Columns), Colour, Num_Squares, 0, Deployments) :- !,
    next_row(Colour, Row, Final_Row, Num_Squares),
    Final_Column_1 is Column + Num_Squares,
    Final_Column_2 is Column - Num_Squares,
    possible_moves_aux(square, Row, Column, Final_Row, Final_Column_1, Final_Column_2, Temp_Moves),
    bounce_moves(Temp_Moves, Num_Board_Columns, Temp_Bounced_Moves),
    include(move_in_board(position(Num_Board_Rows, Num_Board_Columns)), Temp_Bounced_Moves, Moves),
    length(L, Num_Squares),
    maplist(is_square, L),
    maplist(append([L]), Moves, Deployments).
    
possible_deployments_aux(position(Row, Column), position(Num_Board_Rows, Num_Board_Columns), Colour, 0, Num_Circles, Deployments) :- !,
    next_row(Colour, Row, Final_Row, Num_Circles),
    Final_Column_1 is Column + Num_Circles,
    Final_Column_2 is Column - Num_Circles,
    possible_moves_aux(circle, Row, Column, Final_Row, Final_Column_1, Final_Column_2, Temp_Moves),
    bounce_moves(Temp_Moves, Num_Board_Columns, Temp_Bounced_Moves),
    include(move_in_board(position(Num_Board_Rows, Num_Board_Columns)), Temp_Bounced_Moves, Moves),
    length(L, Num_Circles),
    maplist(is_circle, L),
    maplist(append([L]), Moves, Deployments).
    
possible_deployments_aux(position(Row, Column), position(Num_Board_Rows, Num_Board_Columns), Colour, Num_Squares, Num_Circles, Deployments) :- !,
    next_row(Colour, Row, Sq_Final_Row, Num_Squares),
    Sq_Final_Column_1 is Column + Num_Squares,
    Sq_Final_Column_2 is Column - Num_Squares,
    possible_moves_aux(square, Row, Column, Sq_Final_Row, Sq_Final_Column_1, Sq_Final_Column_2, Sq_Temp_Moves),
    bounce_moves(Sq_Temp_Moves, Num_Board_Columns, Sq_Temp_Bounced_Moves),
    include(move_in_board(position(Num_Board_Rows, Num_Board_Columns)), Sq_Temp_Bounced_Moves, Sq_Moves),
    maplist(last, Sq_Moves, Sq_Dests),
    maplist(possible_deployments_aux_(position(Num_Board_Rows, Num_Board_Columns), Colour, 0, Num_Circles), Sq_Dests, Sq_Deployed_Moves),
    maplist(append_deployments, Sq_Moves, Sq_Deployed_Moves, Sq_Deployment_Moves),
    append(Sq_Deployment_Moves, Sq_Final_Deployment_Moves),
    length(Sq_List, Num_Squares), maplist(is_square, Sq_List),
    length(CC_List, Num_Circles), maplist(is_circle, CC_List),
    append(Sq_List, CC_List, Sq_Final_List),
    maplist(append([Sq_Final_List]), Sq_Final_Deployment_Moves, Sq_Deployments),

    next_row(Colour, Row, CC_Final_Row, Num_Circles),
    CC_Final_Column_1 is Column + Num_Circles,
    CC_Final_Column_2 is Column - Num_Circles,
    possible_moves_aux(circle, Row, Column, CC_Final_Row, CC_Final_Column_1, CC_Final_Column_2, CC_Temp_Moves),
    bounce_moves(CC_Temp_Moves, Num_Board_Columns, CC_Temp_Bounced_Moves),
    include(move_in_board(position(Num_Board_Rows, Num_Board_Columns)), CC_Temp_Bounced_Moves, CC_Moves),
    maplist(last, CC_Moves, CC_Dests),
    maplist(possible_deployments_aux_(position(Num_Board_Rows, Num_Board_Columns), Colour, Num_Squares, 0), CC_Dests, CC_Deployed_Moves),
    maplist(append_deployments, CC_Moves, CC_Deployed_Moves, CC_Deployment_Moves),
    append(CC_Deployment_Moves, CC_Final_Deployment_Moves),
    append(CC_List, Sq_List, CC_Final_List),
    maplist(append([CC_Final_List]), CC_Final_Deployment_Moves, CC_Deployments),
    
    append(Sq_Deployments, CC_Deployments, Deployments).
    
possible_deployments_aux_(Board_Dimensions, Colour, Num_Squares, Num_Circles, Position, Deployments) :-
    possible_deployments_aux(Position, Board_Dimensions, Colour, Num_Squares, Num_Circles, Deployments).
    
take_2([_,_ | Tail], Tail).
    
append_deployments(Move, Deployment_List, Final_List) :-
    length(Deployment_List, Num_Deployments),
    length(Final_List, Num_Deployments),
    maplist(take_2, Deployment_List, To_Append),
    maplist(append(Move), To_Append, Final_List).
    
%% available_moves(+Position(+Row_i:int, +Column_i:int), +Board, -Moves)
%
% Gets all the available moves from the Position of the Board into Moves.
% True if Position is inside the bounds of the given Board and Moves unifies
% with all the available moves from that Position.   
available_moves(Position, Board, Moves) :-
    board_dimensions(Board, Board_Dimensions),
    cell_at(Position, Board, cell(Colour, Elem_List)),
    number_of(square, Elem_List, Num_Squares),
    number_of(circle, Elem_List, Num_Circles),
    possible_moves(Position, Board_Dimensions, square, Colour, Num_Squares, Moves_Squares), !,
    possible_moves(Position, Board_Dimensions, circle, Colour, Num_Circles, Moves_Circles), !,
    append(Moves_Squares, Moves_Circles, Moves_All),
    include(is_path_clear(Board), Moves_All, No_Leapfrog_Moves),
    include(is_valid_move(Board), No_Leapfrog_Moves, Simple_Moves),
    possible_deployments(Position, Board_Dimensions, Colour, Num_Squares, Num_Circles, Deployments),
    include(is_deployment_path_clear(Board, Colour), Deployments, No_Leapfrog_Deployments),
    include(is_valid_deployment(Board, Colour), No_Leapfrog_Deployments, Valid_Deployments),
    append(Simple_Moves, Valid_Deployments, Moves).
    
is_board_cell_of_colour_or_blank(Board, Colour, Position) :-
    cell_at(Position, Board, cell(Colour, _)), !.

is_board_cell_of_colour_or_blank(Board, _, Position) :-
    cell_at(Position, Board, cell(blank)).
    
is_deployment_path_clear(Board, Colour, Deployment) :-
    append([_, _ | Path], [_], Deployment),
    maplist(is_board_cell_of_colour_or_blank(Board, Colour), Path).
    
%% Is Valid Deployment
    
valid_number_elements(_, white, _, position(0, _), Number_To_Add) :- !,
    Number_To_Add =< 3.
    
valid_number_elements(Board, black, _, position(OUT_Row, _), Number_To_Add) :-
    board_num_rows(Board, Num_Rows),
    OUT_Row is Num_Rows + 1, !,
    Number_To_Add =< 3.
    
valid_number_elements(_, _, Source_Position, Source_Position, Number_To_Add) :-
    Number_To_Add =< 3, !.
    
valid_number_elements(Board, _, _, Position, Number_To_Add) :-
    cell_at(Position, Board, cell(blank)),
    Number_To_Add =< 3, !.
    
valid_number_elements(Board, Colour, _, Position, Number_To_Add) :-
    different_valid_colour(Colour, Other_Colour),
    cell_at(Position, Board, cell(Other_Colour, _)),
    Number_To_Add =< 3, !.
    
valid_number_elements(Board, Colour, _, Position, Number_To_Add) :-
    cell_at(Position, Board, cell(Colour, Elem_List)),
    length(Elem_List, Num_Elems),
    Total is Num_Elems + Number_To_Add,
    Total =< 3, !.
    
number_of_(List, Elem, Num) :- number_of(Elem, List, Num).
    
is_valid_deployment(Board, Colour, [Elem_List | Deployment_Path]) :-
    is_valid_elem_list(Elem_List),
    Deployment_Path = [Source_Position | Destinations],
    list_to_set(Destinations, Unique_Destinations),
    maplist(number_of_(Destinations), Unique_Destinations, Number_Of_Cells_Per_Unique_Destination),
    maplist(valid_number_elements(Board, Colour, Source_Position), Unique_Destinations, Number_Of_Cells_Per_Unique_Destination).
    
    
%% Is Valid Move
    
is_valid_move(_, Move) :-
    last(Move, position(Row, _)),
    Row =:= 0, !.
    
is_valid_move(Board, Move) :-
    last(Move, position(Row, _)),
    board_num_rows(Board, Num_Rows),
    Valid_Row is Num_Rows + 1,
    Row =:= Valid_Row, !.
    
is_valid_move(Board, Move) :-
    last(Move, Destination),
    cell_at(Destination, Board, cell(blank)), !.

is_valid_move(_, [First_Position | Rest_Of_Move]) :-
    last(Rest_Of_Move, First_Position), !.
    
is_valid_move(Board, [First_Position | Rest_Of_Move]) :-
    last(Rest_Of_Move, Destination),
    cell_at(First_Position, Board, cell(Colour, First_Elem_List)),
    cell_at(Destination, Board, cell(Colour, Destination_Elem_List)),
    !,
    length(First_Elem_List, Num_Elems_First),
    length(Destination_Elem_List, Num_Elems_Dest),
    Total is Num_Elems_First + Num_Elems_Dest,
    Total =< 3.
    
is_valid_move(Board, [First_Position | Rest_Of_Move]) :-
    last(Rest_Of_Move, Destination),
    cell_at(First_Position, Board, cell(Colour, _)),
    different_valid_colour(Colour, Other_Colour),
    cell_at(Destination, Board, cell(Other_Colour, _)).
    
is_board_cell_blank(Board, Position) :- cell_at(Position, Board, cell(blank)).

is_path_clear(Board, Move) :-
    append([_ | Path], [_], Move),
    maplist(is_board_cell_blank(Board), Path).
    
%%%%%%%%%%%%%%%%%%
%% Update Board %%
%%%%%%%%%%%%%%%%%%

update_board(Old_Board, Diff_Board) :-
    maplist(maplist(update_board_elem), Old_Board, Diff_Board).
    
update_board_elem(Old_Elem, Old_Elem) :- !.
update_board_elem(_, New_Elem) :- nonvar(New_Elem).

%%%%%%%%%%
%% Move %%
%%%%%%%%%%

%% exec_move(+Deployment, +Old_Board, -New_Board)
% True if Depoyment is a valid movement in Old_Board and
% New_Board is the updated Old_Board after the Deployment.
exec_move(Deployment, Old_Board, New_Board, Captured_Pieces) :-
    Deployment = [Elem_List, Src_Position | _],
    is_valid_elem_list(Elem_List), !, 
    available_moves(Src_Position, Old_Board, Available_Moves),
    member(Deployment, Available_Moves),
    exec_move_aux(Deployment, Old_Board, New_Board, Captured_Pieces).

%% exec_move(+Move, +Old_Board, -New_Board)
% True if Move is a valid movement in Old_Board and New_Board
% is the updated Old_Board after the Move execution.
exec_move(Move, Old_Board, New_Board, Captured_Pieces) :-
    Move = [Src_Position | _], !,
    available_moves(Src_Position, Old_Board, Available_Moves),
    member(Move, Available_Moves),
    exec_move_aux(Move, Old_Board, New_Board, Captured_Pieces).
    
append_when_equal(_, [], [], []) :- !.
    
append_when_equal(Destination, [Destination | Tail_Destinations], [Element | Tail_Element], [Element | Tail_Elements_To_Destination]) :-
    append_when_equal(Destination, Tail_Destinations, Tail_Element, Tail_Elements_To_Destination).
    
append_when_equal(Destination, [_ | Tail_Destinations], [_ | Tail_Element], Elements_To_Destination) :-
    append_when_equal(Destination, Tail_Destinations, Tail_Element, Elements_To_Destination).
    
append_when_equal_(Destination_List, Elements_List, Destination, Elements_To_Destination) :-
    append_when_equal(Destination, Destination_List, Elements_List, Elements_To_Destination), !.
    
move_to(_, New_Board, Destination, Colour, Destination, Elements) :- !,
    cell_at(Destination, New_Board, cell(Colour, Elements)).
    
move_to(Old_Board, New_Board, _, Colour, Destination, Elements) :-
    cell_at(Destination, Old_Board, cell(blank)), !,
    cell_at(Destination, New_Board, cell(Colour, Elements)).
    
move_to(Old_Board, New_Board, _, Colour, Destination, Elements) :-
    cell_at(Destination, Old_Board, cell(Colour, Old_Elements)), !,
    append(Old_Elements, Elements, New_Elements),
    cell_at(Destination, New_Board, cell(Colour, New_Elements)).
    
move_to(Old_Board, New_Board, _, Colour, Destination, Elements) :-
    cell_at(Destination, Old_Board, cell(_, _)), !,
    cell_at(Destination, New_Board, cell(Colour, Elements)).
    
set_if_non_member(Position, List, _, _) :-
    member(Position, List), !.
    
set_if_non_member(Position, List, Board, Cell) :-
    \+ member(Position, List),
    cell_at(Position, Board, Cell).
    
get_if_of_colour(Colour, Position, Board, Elems) :-
    cell_at(Position, Board, cell(Colour, Elems)), !.
    
get_if_of_colour(Colour, Position, Board, []).
    
exec_move_aux([Elem_List | Deployment_Path], Old_Board, New_Board, Captured_Pieces) :-
    is_valid_elem_list(Elem_List), !,
    Deployment_Path = [Src_Position | Destinations],
    last(Destinations, Last_Dest),
    cell_at(Src_Position, Old_Board, cell(Colour, _)),
    different_valid_colour(Colour, Other_Colour),
    get_if_of_colour(Other_Colour, Last_Dest, Old_Board, Captured_Pieces),
    list_to_set(Destinations, Unique_Destinations),
    maplist(append_when_equal_(Destinations, Elem_List), Unique_Destinations, Elements_Per_Unique_Destination),
    board_dimensions(Old_Board, Num_Rows, Num_Columns),
    create_empty_board(Num_Rows, Num_Columns, New_Board),
    maplist(move_to(Old_Board, New_Board, Src_Position, Colour), Unique_Destinations, Elements_Per_Unique_Destination),
    set_if_non_member(Src_Position, Destinations, New_Board, cell(blank)),
    update_board(Old_Board, New_Board).
    
    
exec_move_aux(Move, Old_Board, Old_Board, []) :-
    Move = [Src_Position | _],
    last(Move, Src_Position), !.
    
exec_move_aux(Move, Old_Board, New_Board, []) :-
    Move = [Src_Position | _],
    last(Move, Dest_Position),
    cell_at(Src_Position, Old_Board, cell(Colour, Src_Elems)),
    cell_at(Dest_Position, Old_Board, cell(Colour, Dest_Elems)), !,
    append(Dest_Elems, Src_Elems, New_Elems),
    board_dimensions(Old_Board, Num_Rows, Num_Columns),
    create_empty_board(Num_Rows, Num_Columns, New_Board),
    cell_at(Dest_Position, New_Board, cell(Colour, New_Elems)),
    cell_at(Src_Position, New_Board, cell(blank)),
    update_board(Old_Board, New_Board).
    
exec_move_aux(Move, Old_Board, New_Board, []) :-
    Move = [Src_Position | _],
    last(Move, Dest_Position),
    cell_at(Src_Position, Old_Board, Cell),
    cell_at(Dest_Position, Old_Board, cell(blank)), !,
    board_dimensions(Old_Board, Num_Rows, Num_Columns),
    create_empty_board(Num_Rows, Num_Columns, New_Board),
    cell_at(Dest_Position, New_Board, Cell),
    cell_at(Src_Position, New_Board, cell(blank)),
    update_board(Old_Board, New_Board).
    
exec_move_aux(Move, Old_Board, New_Board, Captured_Pieces) :-
    Move = [Src_Position | _],
    last(Move, Dest_Position),
    cell_at(Src_Position, Old_Board, cell(Colour, Src_Elems)),
    different_valid_colour(Colour, Other_Colour),
    cell_at(Dest_Position, Old_Board, cell(Other_Colour, Captured_Pieces)), !,
    board_dimensions(Old_Board, Num_Rows, Num_Columns),
    create_empty_board(Num_Rows, Num_Columns, New_Board),
    cell_at(Dest_Position, New_Board, cell(Colour, Src_Elems)),
    cell_at(Src_Position, New_Board, cell(blank)),
    update_board(Old_Board, New_Board).
    
    
%%%%%%%%%%%%%%%%%%%%%%
%% User Interaction %%
%%%%%%%%%%%%%%%%%%%%%%
    
is_valid_mode(player).
is_valid_mode(pc).
    
%  play(N_Rows, N_Columns, Player_1, Player_2, Difficulty) :-
%      is_valid_mode(Player_1),
%      is_valid_mode(Player_2),
%      create_new_board(N_Rows, N_Columns, Board),
%      nl,
%      write('==> Game Objective <=='), nl,
%      write('In order to win a game of Gounki one player needs to reach the the end of the opposite side of the board or capture all of the opponents pieces.'), nl,
%      nl,
%      write('==> Game Notation <=='), nl,
%      write('Capturing  : moving one piece to another of different colour.'), nl,
%      write('Clustering : combining pieces of the same colour by moving one to the other.'), nl,
%      write('Deployment : split combined pieces into its individual components.'), nl,
%      write('A - B  : move from A to B'), nl,
%      write('A x B  : move from A to B with capture'), nl,
%      write('A - B+ : move from A to B with clustering'), nl,
%      write('A * B, C, D : deployment of a combined piece from A to B, C and D'), nl,
%      write('A * B+, C : deployment of a combined piece from A to B, and C with clustering in B'), nl,
%      write('A - OUT : move from A to out of the board resulting in the end of the game'), nl,
%      play_round(Board, Player_1, Player_2, Difficulty, 1).
    
game_finished(Board, 1) :-
    \+ findall(position(0, C), cell_of_colour(position(0, C), Board, white), []), !.
    
game_finished(Board, 1) :-
    \+ board_has_cell_of_colour(Board, black), !.
    
game_finished(Board, 2) :-
    board_num_rows(Board, Num_Rows),
    OUT_Row is Num_Rows + 1,
    \+ findall(position(OUT_Row, C), cell_of_colour(position(OUT_Row, C), Board, black), []), !.
    
game_finished(Board, 2) :-
    \+ board_has_cell_of_colour(Board, white), !.    

% %% Read and Execute Move

make_move(Board, (pc, Colour, Difficulty), Board_Final, Best_Move, Captured_Pieces) :-
    is_hard_mode(Difficulty), !,
    findall(Position, cell_of_colour(Position, Board, Colour), Playable_Cells),
    maplist(available_moves_(Board), Playable_Cells, Available_Moves_Per_Cell),
    append(Available_Moves_Per_Cell, Available_Moves),
    different_valid_colour(Colour, Other_Colour),
    findall(Position, cell_of_colour(Position, Board, Other_Colour), Opponent_Cells),
    maplist(available_moves_(Board), Opponent_Cells, Opponent_Moves_Per_Cell),
    append(Opponent_Moves_Per_Cell, Opponent_Moves),
    maplist(calculate_move_value(Difficulty, Colour, Board, Opponent_Moves), Available_Moves, Available_Moves_Value),
    best_value(Colour, Available_Moves_Value, Best_Value),
    nth0(Index, Available_Moves_Value, Best_Value), !,
    nth0(Index, Available_Moves, Best_Move),
    exec_move(Best_Move, Board, Board_Final, Captured_Pieces).
    
make_move(Board, (pc, Colour, Difficulty), Board_Final, Best_Move, Captured_Pieces) :-
    is_valid_difficulty(Difficulty),
    findall(Position, cell_of_colour(Position, Board, Colour), Playable_Cells),
    maplist(available_moves_(Board), Playable_Cells, Available_Moves_Per_Cell),
    append(Available_Moves_Per_Cell, Available_Moves),
    maplist(calculate_move_value(Difficulty, Colour, Board), Available_Moves, Available_Moves_Value),
    best_value(Colour, Available_Moves_Value, Best_Value),
    nth0(Index, Available_Moves_Value, Best_Value), !,
    nth0(Index, Available_Moves, Best_Move),
    exec_move(Best_Move, Board, Board_Final, Captured_Pieces).
    
%% Artificial Intelligence
    
is_valid_difficulty(easy).
is_valid_difficulty(medium-easy).
is_valid_difficulty(medium-hard).
is_valid_difficulty(hard).
    
fix_value(black, Old_Value, Old_Value).
fix_value(white, Old_Value, New_Value) :- New_Value is - Old_Value.
    
    
num_elems(cell(blank), 0).
    
num_elems(cell(_, Elem_List), Number_Elements) :-
    length(Elem_List, Number_Elements).
    
calculate_value_factor(black, Num_Board_Rows, Dest_Row, Factor) :-
    Quarter is Num_Board_Rows // 3 + 1,
    Dest_Row > Quarter, !,
    Factor is Dest_Row - Quarter.
    
calculate_value_factor(black, Num_Board_Rows, Dest_Row, 1) :-
    Quarter is Num_Board_Rows // 3 + 1,
    Dest_Row =< Quarter, !.

calculate_value_factor(white, Num_Board_Rows, Dest_Row, Factor) :-
    Quarter is Num_Board_Rows // 3 + 1,
    Dest_Row =< Num_Board_Rows - Quarter, !,
    Factor is Num_Board_Rows + 1 - Dest_Row - Quarter.
    
calculate_value_factor(white, Num_Board_Rows, Dest_Row, 1) :-
    Quarter is Num_Board_Rows // 3 + 1,
    Dest_Row > Quarter, !.
    
%% calculate_move_value(+Difficulty, +Colour, +Board, +Move, -Value), !.
% True if Difficulty, Colour and Move are valid for the Board and Value
% is the value of the Move.
    
calculate_move_value(_, white, _, Move, 1000) :-
    last(Move, position(0, _)), !.
    
calculate_move_value(_, black, Board, Move, 1000) :-
    board_num_rows(Board, Num_Board_Rows),
    OUT_Row is Num_Board_Rows + 1,
    last(Move, position(OUT_Row, _)), !.
    
calculate_move_value(_, _, _, Move, 0) :-
    Move = [Source_Position | _],
    last(Move, Source_Position), !.
    
calculate_move_value(easy, Colour, _, Move, Value) :- %% move
    Move = [position(Source_Row, _) | _], !,
    last(Move, position(Dest_Row, _)),
    Pre_Value is Dest_Row - Source_Row,
    fix_value(Colour, Pre_Value, Value).
    
calculate_move_value(easy, _, _, Move, -1000) :- %% Deployment
    Move = [_, position(_, _) | _],
    last(Move, position(_, _)).
    
calculate_move_value(medium-easy, Colour, Board, Move, Value) :- %% move
    Move = [position(Source_Row, _) | _],
    last(Move, position(Dest_Row, Dest_Column)),
    cell_at(position(Dest_Row, Dest_Column), Board, cell(blank)), !,
    Pre_Value is Dest_Row - Source_Row,
    fix_value(Colour, Pre_Value, Easy_Value),
    board_num_rows(Board, Num_Board_Rows),
    calculate_value_factor(Colour, Num_Board_Rows, Dest_Row, Factor),
    Value is Easy_Value * Factor.
    
calculate_move_value(medium-easy, Colour, Board, Move, Value) :- %% move
    Move = [position(Source_Row, Source_Column) | _],
    last(Move, position(Dest_Row, Dest_Column)),
    cell_at(position(Dest_Row, Dest_Column), Board, cell(Colour, Elem_List_2)), !, 
    cell_at(position(Source_Row, Source_Column), Board, cell(Colour, Elem_List_1)),
    append(Elem_List_2, Elem_List_1, Final_Elem_List),
    Pre_Value is Dest_Row - Source_Row,
    fix_value(Colour, Pre_Value, Easy_Value),
    board_num_rows(Board, Num_Board_Rows),
    calculate_value_factor(Colour, Num_Board_Rows, Dest_Row, Factor),
    calculate_cell_value(cell(Colour, Final_Elem_List), Cell_Value),
    Value is Easy_Value * Factor + Cell_Value.
    
calculate_move_value(medium-easy, Colour, Board, Move, Value) :- %% move
    Move = [position(Source_Row, _) | _],
    last(Move, position(Dest_Row, Dest_Column)),
    different_valid_colour(Colour, Other_Colour),
    cell_at(position(Dest_Row, Dest_Column), Board, cell(Other_Colour, Elem_List)), !,
    Pre_Value is Dest_Row - Source_Row,
    fix_value(Colour, Pre_Value, Easy_Value),
    board_num_rows(Board, Num_Board_Rows),
    calculate_value_factor(Colour, Num_Board_Rows, Dest_Row, Factor),
    calculate_cell_value(cell(Other_Colour, Elem_List), Cell_Value),
    Value is Easy_Value * Factor + Cell_Value.
    
calculate_move_value(medium-easy, Colour, Board, Move, Value) :- %% Deployment
    Move = [_, position(Source_Row, _) | _],
    last(Move, position(Dest_Row, _)),
    Pre_Value is Dest_Row - Source_Row,
    fix_value(Colour, Pre_Value, Easy_Value),
    board_num_rows(Board, Num_Board_Rows),
    calculate_value_factor(Colour, Num_Board_Rows, Dest_Row, Factor),
    Value is Easy_Value * Factor.
    
%% calculate_move_value(+Difficulty, +Colour, +Board, +Opponent_Moves, +Move, -Value), !.
% True if Difficulty, Colour and Move are valid for the Board and Value
% is the value of the Move based on Opponent_Moves.
    
calculate_move_value(_, white, _,  _, Move, 1000) :-
    last(Move, position(0, _)), !.
    
calculate_move_value(_, black, Board, _, Move, 1000) :-
    board_num_rows(Board, Num_Board_Rows),
    OUT_Row is Num_Board_Rows + 1,
    last(Move, position(OUT_Row, _)), !.
    
calculate_move_value(Difficulty, Colour, Board, _, Move, 0) :- 
    calculate_move_value(Difficulty, Colour, Board, Move, 0), !.
    
calculate_move_value(medium-hard, Colour, Board, Opponent_Moves, Move, Value) :-%% move
    Move = [Source_Position | _], is_position(Source_Position),
    calculate_move_value(medium-easy, Colour, Board, Move, ME_Value),
    maplist(last, Opponent_Moves, In_Dangered_Positions),    
    list_to_set(In_Dangered_Positions, Unique_In_Dangered_Positions),
    member(Source_Position, Unique_In_Dangered_Positions), !,
    cell_at(Source_Position, Board, Source_Cell),
    calculate_cell_value(Source_Cell, Cell_Value),
    Value is ME_Value + (100 * Cell_Value).
    
calculate_move_value(medium-hard, Colour, Board, Opponent_Moves, Move, Value) :- %% Deployment
    Move = [Elem_List | _], is_valid_elem_list(Elem_List),
    calculate_move_value(medium-easy, Colour, Board, Move, ME_Value),
    maplist(last, Opponent_Moves, In_Dangered_Positions),
    list_to_set(In_Dangered_Positions, Unique_In_Dangered_Positions),
    Move = [_, Source_Position | _],
    member(Source_Position, Unique_In_Dangered_Positions), !,
    cell_at(Source_Position, Board, Source_Cell),
    calculate_cell_value(Source_Cell, Cell_Value),
    Value is ME_Value + (100 * Cell_Value).
    
calculate_move_value(medium-hard, Colour, Board, _, Move, Value) :- %% move
    calculate_move_value(medium-easy, Colour, Board, Move, Value), !.
    
calculate_move_value(hard, Colour, Board, Opponent_Moves, Move, Value) :-%% move
    Move = [Source_Position | _], is_position(Source_Position),
    last(Move, Destination_Position),
    calculate_move_value(medium-easy, Colour, Board, Move, ME_Value),
    maplist(last, Opponent_Moves, In_Dangered_Positions),    
    list_to_set(In_Dangered_Positions, Unique_In_Dangered_Positions),
    is_member_value(Source_Position, Unique_In_Dangered_Positions, Source_Danger_Value),
    is_member_value(Destination_Position, Unique_In_Dangered_Positions, Destination_Danger_Value),
    member(Source_Position, Unique_In_Dangered_Positions), !,
    cell_at(Source_Position, Board, Source_Cell),
    calculate_cell_value(Source_Cell, Cell_Value),
    Value is ME_Value + (Source_Danger_Value * 100 * Cell_Value) - (Destination_Danger_Value * 100 * Cell_Value).
    
calculate_move_value(hard, Colour, Board, Opponent_Moves, Move, Value) :- %% Deployment
    Move = [Elem_List | _], is_valid_elem_list(Elem_List),
    calculate_move_value(medium-easy, Colour, Board, Move, ME_Value),
    maplist(last, Opponent_Moves, In_Dangered_Positions),
    list_to_set(In_Dangered_Positions, Unique_In_Dangered_Positions),
    Move = [_, Source_Position | _],
    last(Move, Destination_Position),
    is_member_value(Source_Position, Unique_In_Dangered_Positions, Source_Danger_Value),
    is_member_value(Destination_Position, Unique_In_Dangered_Positions, Destination_Danger_Value),
    cell_at(Source_Position, Board, Source_Cell),
    calculate_cell_value(Source_Cell, Cell_Value),
    Value is ME_Value + (Source_Danger_Value * 100 * Cell_Value) - (Destination_Danger_Value * 100 * Cell_Value).
    
calculate_move_value(hard, Colour, Board, _, Move, Value) :- %% move
    calculate_move_value(medium-easy, Colour, Board, Move, Value), !.
    
is_member_value(Element, List, 1) :- member(Element, List), !.
is_member_value(Element, List, 0) :- \+ member(Element, List), !.
    
calculate_cell_value(cell(blank), 0).
calculate_cell_value(cell(_, Elem_List), Value) :-
    number_of(square, Elem_List, Number_Sq),
    number_of(circle, Elem_List, Number_Cc),
    Value is Number_Sq * 3 + Number_Cc * 2.
    
best_value(white, Value_List, Best) :- !,
    max_member(Best, Value_List).
    
best_value(black, Value_List, Best) :-
    max_member(Best, Value_List).
    
available_moves_(Board, Position, Result) :- available_moves(Position, Board, Result).
    
is_hard_mode(medium-hard).
is_hard_mode(hard).    
    