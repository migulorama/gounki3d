:- consult('gounki-laig.pro').

:-use_module(library(sockets)).

user:runtime_entry(start) :- main.

main :-
    current_prolog_flag(argv, Args),
    length(Args, N),
    N >= 1,
    Args = [Port |_],
    !, server(Port).

main :-
    server(60011).

server(Port) :-
    format('Starting server in ~q~n', [Port]),
    socket_server_open(Port, Socket, [reuseaddr(true)]),
    socket_server_accept(Socket, _Client, Stream, [type(text)]),
    server_loop(Stream),
    socket_server_close(Socket),
    format("Server Exit~n", []).
    
server_loop(Stream) :-
    repeat,
        read(Stream, ClientRequest),
        format("Received: ~w~n", [ClientRequest]),
        server_input(ClientRequest, ServerReply),
        format(Stream, '~q~n', [ServerReply]),
        format("Send: ~w~n", [ServerReply]),
        flush_output(Stream),
    (ClientRequest == bye; ClientRequest == end_of_file), !.
    
server_input(initialize, Board) :-
    create_new_board(8, 8, Board), !.
    
server_input(get_moves(Board, position(Row, Column)), AvailableMoves) :-
    available_moves(position(Row, Column), Board, AvailableMoves), !.
    
server_input(exec_move(Move, Board), ({Board_Final}, {Captured_Pieces})) :-
    exec_move(Move, Board, Board_Final, Captured_Pieces), !.
    
server_input(exec_pc_move(Colour, Difficulty, Board), ({Best_Move}, {Board_Final}, {Captured_Pieces})) :-
    make_move(Board, (pc, Colour, Difficulty), Board_Final, Best_Move, Captured_Pieces), !.
    
server_input(is_game_finished(Board), Result) :-
    (game_finished(Board, Winner) -> Result = yes(Winner); Result = no), !.

server_input(bye, ok) :- !.
server_input(end_of_file, ok) :- !.
server_input(_, invalid) :- !.